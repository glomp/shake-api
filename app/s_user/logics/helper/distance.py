from math import cos
from typing import Tuple


class Distance:
    @classmethod
    def get_side_length(cls, distance, latitude) -> Tuple[float, float]:
        lat_length = distance / (1000 * max(abs(40075 * cos(latitude) / 360), 1))
        lon_length = distance / (111132.954)
        return lat_length, lon_length

    @classmethod
    def get_range_from_center(
        cls, distance, latitude, longitude
    ) -> Tuple[float, float]:
        lat_length, lon_length = cls.get_side_length(distance, latitude)

        lon_range = [
            max(-180, longitude - lon_length),
            min(180, longitude + lon_length),
        ]
        lat_range = (
            [
                latitude - lat_length,
                min(90, latitude + lat_length),
            ]
            if latitude > 0
            else [
                max(-90, latitude - lat_length),
                latitude + lat_length,
            ]
        )

        return lat_range, lon_range

    @classmethod
    def check(cls, distance, center_lat, center_long, target_lat, target_lon) -> bool:
        center_lat_range, center_lon_range = cls.get_range_from_center(
            distance, center_lat, center_long
        )
        return all(
            [
                target_lat >= center_lat_range[0],
                target_lat <= center_lat_range[1],
                target_lon >= center_lon_range[0],
                target_lon <= center_lon_range[1],
            ]
        )
