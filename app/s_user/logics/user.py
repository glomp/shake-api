from django.db import transaction

from s_notifications.notification.auth import AuthNotification
from v_user.view import VUser
from ..models import SUsers
from ..serializers import SUserBasicsInfoSerializer
from .function import delete_user


class LUser:
    def __init__(self, user: SUsers):
        self.user = user
        self.view = VUser(user)

    @transaction.atomic
    def delete(self, hard=False):
        all_friends = list(self.view.friend.user.know_me.all())
        user_data = SUserBasicsInfoSerializer(self.user).data

        delete_user(self.user, hard=hard)

        AuthNotification(user_data, target_id=user_data["id"]).deleted_account.notify(
            *all_friends, by_user=user_data["name"]
        )
