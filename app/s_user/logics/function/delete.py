from typing import Tuple

from django.db import transaction
from fcm_django.models import FCMDevice
from rest_framework.authtoken.models import Token

from s_auth.models import MAppleAccount
from s_balances.models import MAndroidPurchase
from s_cards.logics.function import delete_card
from s_cards.models import SCards
from s_companies.models import SCompanyUsers
from s_contacts.logics.contact_request import ContactRequest
from s_contacts.models import SContactRequests, SContacts, SShareContactRequests
from s_notifications.models import SNotifications
from ...models import MUserDevices, SUserRequests, SUsers, User


@transaction.atomic
def delete_user(user: SUsers, hard=False) -> Tuple[int, dict]:
    def _delete_cards(user: SUsers, hard=False):
        def _unlink_card(card: SCards):
            card.user_id = None
            card.save(update_fields=["user_id"])

        cards = SCards.objects.filter(user_id=user.id).all()
        for card in cards:
            delete_card(card, hard=hard)
            if not hard:
                _unlink_card(card)

    def _delete_account(user: SUsers):
        def _delete_other(user: SUsers):
            SUserRequests.objects.filter(user_id=user.id).delete()
            MUserDevices.objects.filter(user_id=user.id).delete()
            MAndroidPurchase.objects.filter(user_id=user.id).delete()

            FCMDevice.objects.filter(user_id=user.user_auth.id).delete()
            Token.objects.filter(user_id=user.user_auth.id).delete()
            MAppleAccount.objects.filter(user_auth_id=user.user_auth.id).delete()
            # self.view.company_user.all().delete()

        _delete_other(user)
        user_auth: User = user.user_auth
        user.delete()
        user_auth.delete()

    def _hard_delete(user: SUsers):
        def _delete_contacts(user: SUsers):
            def _delete_contact_detail(contact: SContacts):
                contact.scontactnotes_set.all().delete()
                contact.scontacttasks_set.all().delete()
                contact.delete()

            [
                _delete_contact_detail(contact)
                for contact in SContacts.objects.filter(user_id=user.id).distinct()
            ]
            [
                _delete_contact_detail(contact)
                for contact in SContacts.objects.filter(
                    user_card__user_id=user.id
                ).distinct()
            ]
            [
                _delete_contact_detail(contact)
                for contact in SContacts.objects.filter(
                    card__user_id=user.id
                ).distinct()
            ]

        def _delete_contact_requests(user: SUsers):
            contact_request: SContactRequests
            for contact_request in SContactRequests.objects.filter(
                user_id=user.id
            ).all():
                contact_request.cancel() and contact_request.refund()
                contact_request.delete()

            for contact_request in SContactRequests.objects.filter(
                from_card__user_id=user.id
            ).all():
                contact_request.cancel() and contact_request.refund()
                contact_request.delete()

        def _delete_share_contact_requests(user: SUsers):
            SShareContactRequests.objects.filter(from_card__user_id=user.id).delete()
            SShareContactRequests.objects.filter(by_card__user_id=user.id).delete()
            SShareContactRequests.objects.filter(to_card__user_id=user.id).delete()

        def _delete_notifications(user: SUsers):
            SNotifications.objects.filter(user_id=user.id).delete()
            SNotifications.objects.filter(by_card__user_id=user.id).delete()

        _delete_cards(user, hard=True)

        _delete_contacts(user)
        _delete_contact_requests(user)
        _delete_share_contact_requests(user)
        _delete_notifications(user)

        _delete_account(user)

    def _soft_delete(user: SUsers):
        def _disable_and_unlink_contacts(user: SUsers):
            SContacts.objects.filter(user_id=user.id).update(
                user_id=None, status=SContacts.DESTROYED
            )
            SContacts.objects.filter(related_user_id=user.id).update(
                related_user_id=None, status=SContacts.DESTROYED
            )

        def _disable_and_unlink_contact_requests(user: SUsers):
            for contact_request in SContactRequests.objects.filter(
                user_id=user.id
            ).all():
                ContactRequest.destroy(contact_request, raise_exception=False)
                contact_request.user_id = None
                contact_request.save(update_fields=["user_id"])

            for contact_request in SContactRequests.objects.filter(
                from_card__user_id=user.id
            ).all():
                ContactRequest.destroy(contact_request, raise_exception=False)

        def _disable_and_unlink_share_contact_requests(user: SUsers):
            SShareContactRequests.objects.filter(user_id=user.id).update(
                user_id=None, status=SShareContactRequests.DESTROYED
            )

            SShareContactRequests.objects.filter(from_card__user_id=user.id).update(
                status=SShareContactRequests.DESTROYED
            )
            SShareContactRequests.objects.filter(to_card__user_id=user.id).update(
                status=SShareContactRequests.DESTROYED
            )
            SShareContactRequests.objects.filter(by_card__user_id=user.id).update(
                status=SShareContactRequests.DESTROYED
            )

        def _disable_and_unlink_notifications(user: SUsers):
            SNotifications.objects.filter(user_id=user.id).update(
                user_id=None, status=SNotifications.DESTROYED
            )
            SNotifications.objects.filter(by_card__user_id=user.id).update(
                status=SNotifications.DESTROYED
            )
            SNotifications.objects.filter(target_card__user_id=user.id).update(
                status=SNotifications.DESTROYED
            )

        def _disable_and_unlink_company(user: SUsers):
            SCompanyUsers.objects.filter(user_id=user.id).update(
                user_id=None, status=SCompanyUsers.INACTIVE
            )

        _delete_cards(user, hard=False)

        _disable_and_unlink_contacts(user)
        _disable_and_unlink_contact_requests(user)
        _disable_and_unlink_share_contact_requests(user)
        _disable_and_unlink_notifications(user)
        _disable_and_unlink_company(user)

        _delete_account(user)

    return _hard_delete(user) if hard is True else _soft_delete(user)
