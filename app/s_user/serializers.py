from django.contrib.auth.models import User
from dynamic_rest.fields import DynamicRelationField
from dynamic_rest.serializers import DynamicModelSerializer
from rest_framework import serializers

from .models import MUserDevices, SUserRequests, SUsers


class UserSerializer(DynamicModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = (
            "id",
            "username",
            "first_name",
            "last_name",
            "email",
            "password",
            "last_login",
            "is_staff",
            "is_superuser",
        )

    def create(self, validated_data):
        user = User(**validated_data)
        user.set_password(validated_data["password"])
        user.save()
        return user

    def update(self, instance: User, validated_data: dict):
        User.objects.filter(pk=instance.pk).update(**validated_data)
        instance.refresh_from_db()

        if "password" in validated_data:
            instance.set_password(validated_data["password"])
            instance.save()

        return instance


class SUserLocationSerializer(DynamicModelSerializer):
    class Meta:
        models = SUsers
        fields = ("location_longitude", "location_latitude")


class SUserAuthInfoSerializer(DynamicModelSerializer):
    name = serializers.CharField(read_only=True)

    class Meta:
        model = SUsers
        fields = (
            "status",
            "platform",
            "id",
            "name",
        )


class SAppleUserAuthInfoSerializer(DynamicModelSerializer):
    name = serializers.CharField(read_only=True)

    class Meta:
        model = SUsers
        fields = (
            "status",
            "platform",
            "id",
            "name",
            "email",
        )


class SUserInfoSerializer(DynamicModelSerializer):
    name = serializers.CharField(read_only=True)
    email = serializers.CharField(read_only=True)

    class Meta:
        model = SUsers
        fields = "__all__"


class SUserBasicsInfoSerializer(SUserAuthInfoSerializer):
    class Meta:
        model = SUsers
        exclude = SUsers.PERSONAL_FIELDS


class SUserSerializer(DynamicModelSerializer):
    user_auth = DynamicRelationField(UserSerializer, embed=True)
    name = serializers.CharField(read_only=True)
    email = serializers.CharField(read_only=True)

    balance_personal_credit = serializers.IntegerField(read_only=True)
    balance_business_credit = serializers.IntegerField(read_only=True)

    class Meta:
        model = SUsers
        fields = "__all__"


class SUserRequestsSerializer(DynamicModelSerializer):
    user = DynamicRelationField(SUserSerializer, embed=False)

    class Meta:
        model = SUserRequests
        fields = "__all__"


class SUserDeviceSerializer(DynamicModelSerializer):
    class Meta:
        model = MUserDevices
        fields = "__all__"
