from django.db import transaction
from django.db.models.query_utils import Q
from django.http.response import Http404
from fcm_django.models import FCMDevice
from rest_framework.decorators import action

from custom_django.views.viewsets import SDynamicModelViewSet
from helper.exceptions import BadRequestException
from s_auth.logics.user_request import UserRequest
from s_notifications.base.notification import Notification
from s_notifications.email import UserMailNotifier
from s_notifications.self_notification import UserSelfNotification
from s_notifications.self_notification.auth import AuthSelfNotification
from .logics.user import LUser
from .models import SUserRequests, SUsers
from .serializers import SUserDeviceSerializer, SUserSerializer


class SUserViewSet(SDynamicModelViewSet):
    serializer_class = SUserSerializer
    model_class = SUsers
    queryset = SUsers.objects.all()

    def list(self, request):
        raise Http404

    def create(self, request):
        raise Http404

    @transaction.atomic
    def update(self, request, pk, partial=True):
        instance = self.queryset.get(id=pk)

        # Documentation said to explicitly call this method to check for permission
        self.check_object_permissions(request, instance)

        # Remove non updatable key
        non_updatable_keys = (
            "email",
            "password",
            "balance_personal_credit",
            "balance_business_credit",
        )
        for key in non_updatable_keys:
            request.data.pop(key, None)

        # save shake user record
        user_auth = request.user
        request.data["user_auth"] = user_auth
        s_user_ser = self.serializer_class(
            instance,
            data=request.data,
            context={"user_auth": user_auth},
            partial=partial,
        )
        s_user_ser.is_valid(raise_exception=True)
        s_user_ser.save()

        return {"s_users": s_user_ser.data}

    def destroy(self, request, pk):
        link = UserRequest.generate_url(
            UserRequest.generate_request(self.view.user, SUserRequests.DELETE_ACCOUNT)
        )
        UserMailNotifier().confirm_delete_account.send_to(self.user, link=link)
        UserSelfNotification(self.user, target_id=self.user.id).delete_user.by(
            self.user.get_first_card()
        ).notify()
        return "SUCCESS"

    @action(methods=["get"], detail=False)
    def info(self, request):
        return self.user

    @action(methods=["post"], detail=False, url_path="change-password")
    @transaction.atomic
    def change_password(self, request, partial=False):
        correct = self.user_auth.check_password(request.data["current_password"])
        if correct:
            self.user_auth.set_password(request.data["new_password"])
            self.user_auth.save()
            UserSelfNotification(self.user, target_id=self.user.id).updated_password.by(
                self.user.get_first_card()
            ).notify()
            return "SUCCESS"
        raise BadRequestException(errcode=400064, message="Wrong password")

    @action(methods=["patch"], detail=False)
    @transaction.atomic
    def location(self, request, partial=False):
        s_user: SUsers = SUsers.objects.get(user_auth=request.user)
        s_user.location_longitude = request.data["longitude"]
        s_user.location_latitude = request.data["latitude"]
        s_user.save()

    @action(methods=["post"], detail=False, url_path="push-token")
    @transaction.atomic
    def push_token(self, request, partial=False):
        registration_id = request.data["registration_id"]
        fcm_device = self.view.fcm.get_if_exist(device_id=request.auth.key)

        if fcm_device is None:
            FCMDevice.objects.create(
                registration_id=registration_id,
                type=request.data["type"],
                device_id=request.auth.key,
                user_id=request.user.id,
            )
        elif registration_id != fcm_device.registration_id:
            fcm_device.registration_id = registration_id
            fcm_device.save(update_fields=["registration_id"])

        if self.user.is_active is False:
            AuthSelfNotification(self.user).welcome.notify()
        return "SUCCESS"

    @action(methods=["post"], detail=True, url_path="hard-delete")
    @transaction.atomic
    def hard_delete(self, request, pk):
        if not self.user_auth.is_superuser:
            raise Http404

        if request.data.get("email", None) is not None:
            instance = self.queryset.get(user_auth__email=request.data["email"])
        else:
            instance = self.queryset.get(id=pk)

        # LUser(instance).delete()
        LUser(instance).delete(hard=True)

    @action(methods=["post", "get"], detail=False)
    def device(self, request):
        if request.method.upper() == "GET":
            return SUserDeviceSerializer(self.view.device.all(), many=True)

        data = request.data.copy()
        data["user"] = self.user.id
        user_device = self.view.device.get_if_exist(device_id=data["device_id"])
        if user_device is None:
            user_device_ser = SUserDeviceSerializer(data=data)
            user_device_ser.is_valid(raise_exception=True)
            user_device_ser.save()
        else:
            user_device_ser = SUserDeviceSerializer(user_device, data=data)
            user_device_ser.is_valid(raise_exception=True)
            user_device_ser.save()
        return user_device_ser

    @action(methods=["post"], detail=True)
    def test(self, request, pk, partial=False):

        # s_user = SUsers.objects.filter(
        #     ~Q(status=SUsers.ACTIVE), _created__lte=1628777426
        # )
        # print("s_user", s_user.query)

        # from s_notifications.email.auth import AuthMailNotifier
        # from s_notifications.notification.user import UserNotification

        # # user_list = s_user.first()
        # user_list = list(s_user.all())
        # print("user_list", user_list)
        # # AuthMailNotifier().welcome.send_to(*user_list)
        # UserNotification().welcome.notify(*user_list)
        0 / 0
        pass


# class SUserUploadsViewSet(SDynamicModelViewSet):

#     serializer_class = SUserUploadsSerializer
#     queryset = SUserUploads.objects.all()
#     parser_classes = (
#         MultiPartParser,
#         FormParser,
#     )
#     permission_classes = (
#         IsAuthenticated,
#         IsOwnerOrReadOnly,
#     )
#     authentication_classes = (TokenAuthentication,)
