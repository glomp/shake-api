from django.contrib.auth.models import User
from django.db import models
from django.db.models.query_utils import Q

from custom_django.models import SModel, fields
from helper.common.string import split_name


class SUsers(SModel):
    UNVERIFIED = "U"
    PASSWORD_REQUIRED = "P"
    CARD_REQUIRED = "C"
    ACTIVE = "A"
    INACTIVE = "I"
    STATUS = (
        (UNVERIFIED, "UNVERIFIED"),
        (PASSWORD_REQUIRED, "PASSWORD_REQUIRED"),
        (CARD_REQUIRED, "CARD_REQUIRED"),
        (ACTIVE, "ACTIVE"),
        (INACTIVE, "INACTIVE"),
    )

    MALE = "M"
    FEMALE = "F"
    OTHER = "O"
    GENDER = (
        (MALE, "MALE"),
        (FEMALE, "FEMALE"),
        (OTHER, "OTHER"),
    )

    PASSWORD = "P"
    GOOGLE = "G"
    FACEBOOK = "F"
    APPLE = "A"
    PLATFORM = (
        (PASSWORD, "PASSWORD"),
        (GOOGLE, "GOOGLE"),
        (FACEBOOK, "FACEBOOK"),
        (APPLE, "APPLE"),
    )

    user_auth = models.OneToOneField(User, on_delete=models.CASCADE)
    status = models.CharField(max_length=1, default=UNVERIFIED, choices=STATUS)
    platform = models.CharField(max_length=1, default=PASSWORD, choices=PLATFORM)

    # mobile = models.CharField(max_length=31, default=None, null=True, blank=True)
    # address = models.CharField(max_length=255, default=None, null=True, blank=True)
    # date_of_birth = fields.UnixTimeStampField(null=True, blank=True)
    # gender = models.CharField(
    #     max_length=1, blank=True, null=True, choices=GENDER
    # )

    location_longitude = models.FloatField(null=True)
    location_latitude = models.FloatField(null=True)

    personal_balance = models.IntegerField(default=0)
    business_balance = models.IntegerField(default=0)

    _created = fields.UnixTimeStampField(null=True, auto_now_add=True)
    _updated = fields.UnixTimeStampField(null=True, auto_now=True)

    PERSONAL_FIELDS = (
        "location_longitude",
        "location_latitude",
        "personal_balance",
        "business_balance",
        "user_auth",
    )

    @property
    def email(self):
        return self.user_auth.email

    @property
    def name(self):
        self.user_auth: User
        return f"{self.user_auth.first_name} {self.user_auth.last_name}".strip()

    @property
    def is_active(self):
        return self.status == self.ACTIVE

    @name.setter
    def name(self, value: str):
        first_name, last_name = split_name(value)

        if self.name != f"{first_name} {last_name}".strip():
            self.user_auth.first_name = first_name
            self.user_auth.last_name = last_name
            self.user_auth.save(update_fields=["first_name", "last_name"])

    def finished_create_card(self, card):
        if self.status == self.CARD_REQUIRED:
            self.status = self.ACTIVE
            self.save(update_fields=["status"])
        if self.icard is None:
            self.icard_logic.create_from_other_card(from_card=card)

    def finished_delete_card(self):
        from v_user.card import SCards, VUCard

        cnt = VUCard(self).all(~Q(card_type=SCards.TEMPLATE)).count()
        if cnt == 0:
            self.status = self.CARD_REQUIRED
            self.save(update_fields=["status"])

    def finished_verify_email(self):
        if self.status == self.UNVERIFIED:
            if self.platform == self.PASSWORD:
                self.status = self.PASSWORD_REQUIRED
                self.save()
            else:
                self.status = self.CARD_REQUIRED
                self.save()

    def get_first_card(self):
        from s_cards.models import SCards

        if (
            not hasattr(self, "_first_card")
            or self._first_card is None
            or self._first_card.card_type == SCards.IDENTITY
        ):
            from v_user.special import get_first_card2

            self._first_card = get_first_card2(self.id)
        return self._first_card

    @property
    def icard(self):
        if not hasattr(self, "_icard") or self._icard is None:
            from v_user.card import VUIdentityCard

            self._icard = VUIdentityCard(self).get_if_exist()

        return self._icard

    @property
    def icard_logic(self):
        if not hasattr(self, "_icard_logic"):
            from s_cards.logics.card import IdentityCard

            self._icard_logic = IdentityCard(self, self.icard)

        return self._icard_logic

    @property
    def view(self):
        from v_user.view import VUser

        return VUser(self)

    def get_readonly_fields(self, request, obj=None):
        return ["email", "user_auth", "platform"] if obj else []

    class Meta:
        managed = True
        db_table = "s_user"


class SUserRequests(SModel):
    VERIFY_ACCOUNT = "V"
    RESET_PASSWORD = "R"
    DELETE_ACCOUNT = "D"
    REQUEST = (
        (VERIFY_ACCOUNT, "VERIFY_ACCOUNT"),
        (RESET_PASSWORD, "RESET_PASSWORD"),
        (DELETE_ACCOUNT, "DELETE_ACCOUNT"),
    )

    ACTIVE = "A"
    INACTIVE = "I"
    STATUS = (
        (ACTIVE, "ACTIVE"),
        (INACTIVE, "INACTIVE"),
    )

    user = models.ForeignKey(SUsers, models.DO_NOTHING)

    token = models.CharField(max_length=63)
    request = models.CharField(max_length=1, choices=REQUEST)
    status = models.CharField(max_length=1, default=INACTIVE, choices=STATUS)

    _expiration = fields.UnixTimeStampField(null=True)
    _created = fields.UnixTimeStampField(null=True, auto_now_add=True)

    def get_readonly_fields(self, request, obj=None):
        return ["user", "token", "request"] if obj else []

    class Meta:
        managed = True
        db_table = "s_user_request"


class MUserDevices(SModel):
    ANDROID = "A"
    IOS = "I"
    WEB = "W"
    UNKNOWN = "U"
    PLATFORM = (
        (ANDROID, "ANDROID"),
        (IOS, "IOS"),
        (WEB, "WEB"),
        (UNKNOWN, "UNKNOWN"),
    )

    user = models.ForeignKey(SUsers, models.DO_NOTHING, related_name="devices")

    last_use = fields.UnixTimeStampField(null=True, auto_now=True)
    device_name = models.CharField(max_length=127, null=True, default=None, blank=True)
    device_id = models.CharField(max_length=127, null=True, default=None, blank=True)
    platform = models.CharField(max_length=1, default=UNKNOWN, choices=PLATFORM)
    os = models.CharField(max_length=127, default=None, null=True, blank=True)
    os_version = models.CharField(max_length=127, default=None, null=True, blank=True)
    app_version = models.CharField(max_length=127)

    _created = fields.UnixTimeStampField(null=True, auto_now_add=True)

    READONLY_FIELDS = ["user", "last_use"]

    class Meta:
        managed = True
        db_table = "s_user_device"


# class SUserLocations(SModel):
#     user = models.ForeignKey(SUsers, models.DO_NOTHING)
#     lng = models.FloatField()
#     lat = models.FloatField()
#     country_code = models.TextField(blank=True, null=True)
#     region = models.TextField(blank=True, null=True)
#     date_updated = models.DateTimeField(blank=True, null=True, auto_now=True)

#     def save(self, *args, **kwargs):
#         self.process_location()
#         super(SUserLocations, self).save(*args, **kwargs)

#     def process_location(self):
#         from geopy.geocoders import Nominatim

#         geolocator = Nominatim(user_agent="shake")
#         lat = str(self.lat)
#         lng = str(self.lng)

#         try:
#             location = geolocator.reverse("{}, {}".format(lat, lng))
#             region = ""
#             # location.raw.country_code
#             self.country_code = location.raw["address"]["country_code"]

#             if location.raw["address"].has_key("state"):
#                 region = location.raw["address"]["state"]
#             else:
#                 region = location.raw["address"]["region"]

#             self.region = region
#         except:
#             pass

#     class Meta:
#         managed = True
#         db_table = "s_user_locations"
#         unique_together = ("user")
