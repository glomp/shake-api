from custom_django.test.base import BaseTestCase
from custom_django.test.request_data import NoneRequest, TestRequestData
from s_cards.logics.card import ScanCard
from .models import SUsers
from .serializers import SUserSerializer
from .views import SUserViewSet


class UserTestCase(BaseTestCase):
    ViewSet = SUserViewSet

    # def test_retrieve(self):
    #     data = TestRequestData({}, self.user)
    #     assert (
    #         self.run_func("retrieve", data, pk=self.user.id) == self.SUCCESS
    #     ), self.last_run_result()

    # assert (
    #     self.run_func("retrieve", data, self.user.id + 1) == self.NOT_FOUND
    # ), self.last_run_result()

    def create_general_objects(self):
        _, self.media = self.helper.create_media()
        _, self.media2 = self.helper.create_media(filename="test_2")
        _, self.card = self.helper.create_card(
            self.suser, self.media, self.media2, card_class=ScanCard
        )

    def create_test_objects(self):
        self.test_data, self.test_obj = SUserSerializer(self.suser).data, self.suser

    def validate_result(self, origin, result) -> bool:
        return [origin["name"] == result["s_users"]["name"]]

    # def test_update(self):
    #     self.default_test_update(
    #         TestRequestData(
    #             {
    #                 "name": "ahihi",
    #             },
    #             self.user,
    #             method="POST",
    #         ),
    #         {
    #             "name": "ahihi",
    #         },
    #     )
    #     # data = TestRequestData(
    #     #     {
    #     #         "name": "ahihi",
    #     #         "mobile": 123456789,
    #     #         "adress": "unknown",
    #     #     },
    #     #     self.user,
    #     # )
    #     # assert (
    #     #     self.run_func("update", data, self.suser.id, partial=True) == self.SUCCESS
    #     # ), self.last_run_result()

    def test_location(self):
        request = TestRequestData({"longitude": 1, "latitude": 1}, self.user)
        assert (
            self.run_func("location", request) == self.SUCCESS
        ), self.last_run_result()

    def test_destroy(self):
        self.default_test_destroy()

    def test_push_token(self):
        class Auth:
            key = "test"

        request = TestRequestData(
            {
                "type": "android",
                "registration_id": (
                    "dnLgBR8WQM-mqyoDWiUhzi:"
                    "APA91bGGK8MtmzkqXtn6S0I5x1R3lEABugB4y13jVTFpqgOq21oDVXY1"
                    "S7C6WCw-zqqs7njN_Ea4VR_za_1P33cwOP6rD7-ZSU7JoVp6mpfEfIBI"
                    "gohxeiASG6dWaQ4TeMcwr9BAhUmD"
                ),
            },
            self.user,
        )
        self.suser.status = SUsers.CARD_REQUIRED
        request.auth = Auth()
        assert (
            self.run_func("push_token", request) == self.SUCCESS
        ), self.last_run_result()

    # @BaseTestCase.wrap_exception
    def test_device(self):
        data = {
            "device_name": "Samsung Note 9",
            "device_id": "alsdjaoisd",
            "platform": "A",
            "os": "Hello",
            "os_version": "12.123.3",
            "app_version": "1.12.1",
        }
        request = TestRequestData(data.copy(), user=self.user, method="post")
        assert self.run_func("device", request) == self.SUCCESS, self.last_run_result()

        data["app_version"] = "1.12.2"
        request = TestRequestData(data=data.copy(), user=self.user, method="post")
        assert self.run_func("device", request) == self.SUCCESS, self.last_run_result()
        assert self.compare_dict(
            request.data, self.last_result_data()
        ), self.debug_data()

        request = NoneRequest(user=self.user, method="get")
        assert self.run_func("device", request) == self.SUCCESS, self.last_run_result()

        result = self.last_result_data()
        assert isinstance(result, list), self.debug_data("Response must be a list")
        assert len(result) == 1, self.debug_data(f"Response must has length exact {1}")
        assert self.compare_list_dict(
            [data], result, index_key="device_id"
        ), self.debug_data()
