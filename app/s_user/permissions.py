from rest_framework import permissions

from .models import SUsers


class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """

    def has_object_permission(self, request, view, obj):
        if hasattr(obj, "user_auth"):  # obj is s_user
            user = obj.user_auth
            return user == request.user
        elif hasattr(obj, "user_id"):  # obj is card, contact
            user_id = obj.user_id
            s_user = SUsers.objects.get(user_auth_id=request.user.id)
            return user_id == s_user.id
        else:
            return False


class IsOwnerConnectecToUser(permissions.BasePermission):
    """
    TODO: Check if the owner is connected to the other user so the owner can view other user profile
    """

    pass
