from s_user.models import SUsers
from v_user.view import VUser
from ....models import SContacts


def move_note(user: SUsers, from_contact: SContacts, to_contact: SContacts):
    from_contact.validate()
    to_contact.validate()

    my = VUser(user)
    my.note.all(contact=from_contact).update(contact=to_contact)


def move_task(user: SUsers, from_contact: SContacts, to_contact: SContacts):
    from_contact.validate()
    to_contact.validate()

    my = VUser(user)
    my.task.all(contact=from_contact).update(contact=to_contact)
