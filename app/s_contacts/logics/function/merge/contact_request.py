from django.db.models.query_utils import Q

from s_cards.models import SCards
from s_user.models import SUsers
from v_user.view import VUser
from ....models import SShareContactRequests


def merge_contact_request_in_my_card(user: SUsers, from_card: SCards, to_card: SCards):
    # print("merge_contact_request_in_my_card")
    from_card.validate()
    to_card.validate()

    my = VUser(user)
    card_id_exists_in_contact = my.contact.all(user_card=to_card).values_list(
        "card_id", named="card_id"
    )
    card_id_exists_in_contact_request = my.contact_request.all(
        to_card=to_card
    ).values_list("from_card_id", named="card_id")
    card_id_exists = set(
        card_id_exists_in_contact.union(card_id_exists_in_contact_request).values_list(
            "card_id", flat=True
        )
    )

    # For new contact request
    new_requests = my.contact_request.all(
        # Can't direct update in one query, because mysql don't allow it
        id__in=set(
            my.contact_request.all(
                ~Q(from_card_id__in=card_id_exists),
                to_card_id=from_card.id,
            )
            .values_list("id", flat=True)
            .distinct()
        )
    )
    new_card_ids = set(new_requests.values_list("from_card_id", flat=True))
    new_requests.update(to_card_id=to_card.id)
    SShareContactRequests.objects.filter(
        to_card=to_card, from_card__in=new_card_ids, status=SShareContactRequests.HOLD
    ).update(status=SShareContactRequests.DESTROYED)

    # For exits contact request
    for exists_request in my.contact_request.all(
        from_card_id__in=card_id_exists,
        to_card=from_card,
    ):
        exists_request.cancel() and exists_request.refund()


def merge_contact_request_in_my_friend_card(
    user: SUsers, from_card: SCards, to_card: SCards
):
    # print("merge_contact_request_in_my_friend_card")
    from_card.validate()
    to_card.validate()

    my_friend = VUser(user).friend
    card_id_exists_in_contact = my_friend.contact.all(card=to_card).values_list(
        "user_card_id", named="card_id"
    )
    card_id_exists_in_contact_request = my_friend.contact_request.all(
        from_card=to_card
    ).values_list("to_card_id", named="card_id")
    card_id_exists = set(
        card_id_exists_in_contact.union(card_id_exists_in_contact_request).values_list(
            "card_id", flat=True
        )
    )

    # For new contact request
    new_requests = my_friend.contact_request.all(
        # Can't direct update in one query, because mysql don't allow it
        id__in=set(
            my_friend.contact_request.all(
                ~Q(to_card_id__in=card_id_exists),
                from_card=from_card,
            )
            .values_list("id", flat=True)
            .distinct()
        )
    )
    new_card_ids = set(new_requests.values_list("to_card_id", flat=True))
    new_requests.update(from_card=to_card)
    SShareContactRequests.objects.filter(
        from_card=to_card, to_card__in=new_card_ids, status=SShareContactRequests.HOLD
    ).update(status=SShareContactRequests.DESTROYED)

    # For exists contact request
    for exists_request in my_friend.contact_request.all(
        to_card_id__in=card_id_exists,
        from_card=from_card,
    ):
        exists_request.cancel() and exists_request.refund()


def merge_contact_request(user: SUsers, from_card: SCards, to_card: SCards):
    merge_contact_request_in_my_card(user, from_card=from_card, to_card=to_card)
    merge_contact_request_in_my_friend_card(user, from_card=from_card, to_card=to_card)
