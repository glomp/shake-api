from django.db.models.query_utils import Q

from s_cards.models import SCards
from s_user.models import SUsers
from v_user.view import VUser
from ....models import SShareContactRequests


def merge_contact_share_in_my_card(user: SUsers, from_card: SCards, to_card: SCards):
    # print("merge_contact_share_in_my_card")
    from_card.validate()
    to_card.validate()

    my = VUser(user)
    card_id_exists_in_contact = my.contact.all(user_card=to_card).values_list(
        "card_id", named="card_id"
    )
    card_id_exists_in_contact_request = my.contact_request.all(
        to_card=to_card
    ).values_list("from_card_id", named="card_id")
    card_id_exists_in_contact_share = SShareContactRequests.objects.filter(
        to_card=to_card, status=SShareContactRequests.HOLD
    ).values_list("from_card_id", named="card_id")
    card_id_exists = set(
        card_id_exists_in_contact.union(card_id_exists_in_contact_request)
        .union(card_id_exists_in_contact_share)
        .values_list("card_id", flat=True)
    )

    # Move not exists contact share only
    SShareContactRequests.objects.filter(
        id__in=set(
            SShareContactRequests.objects.filter(
                ~Q(from_card__in=card_id_exists),
                to_card=from_card,
                status=SShareContactRequests.HOLD,
            )
            .values_list("id", flat=True)
            .distinct()
        )
    ).update(to_card=to_card)
    # Destroy exists contact share
    SShareContactRequests.objects.filter(
        id__in=set(
            SShareContactRequests.objects.filter(
                from_card__in=card_id_exists,
                to_card=from_card,
                status=SShareContactRequests.HOLD,
            )
            .values_list("id", flat=True)
            .distinct()
        )
    ).update(status=SShareContactRequests.DESTROYED)


def merge_contact_share_by_my_card(user: SUsers, from_card: SCards, to_card: SCards):
    # print("merge_contact_share_by_my_card")
    from_card.validate()
    to_card.validate()

    SShareContactRequests.objects.filter(
        by_card=from_card, status=SShareContactRequests.HOLD
    ).update(by_card=to_card)


def merge_contact_share_in_my_friend_card(
    user: SUsers, from_card: SCards, to_card: SCards
):
    # print("merge_contact_share_in_my_friend_card")
    from_card.validate()
    to_card.validate()

    my_friend = VUser(user).friend
    card_id_exists_in_contact = my_friend.contact.all(card=to_card).values_list(
        "user_card_id", named="card_id"
    )
    card_id_exists_in_contact_request = my_friend.contact_request.all(
        from_card=to_card
    ).values_list("to_card_id", named="card_id")
    card_id_exists_in_contact_share = SShareContactRequests.objects.filter(
        from_card=to_card, status=SShareContactRequests.HOLD
    ).values_list("to_card_id", named="card_id")
    card_id_exists = set(
        card_id_exists_in_contact.union(card_id_exists_in_contact_request)
        .union(card_id_exists_in_contact_share)
        .values_list("card_id", flat=True)
    )

    # Move not exists contact share only
    SShareContactRequests.objects.filter(
        id__in=set(
            SShareContactRequests.objects.filter(
                ~Q(to_card__in=card_id_exists),
                from_card=from_card,
                status=SShareContactRequests.HOLD,
            )
            .values_list("id", flat=True)
            .distinct()
        )
    ).update(from_card=to_card)
    # Destroy exists contact share
    SShareContactRequests.objects.filter(
        id__in=set(
            SShareContactRequests.objects.filter(
                to_card__in=card_id_exists,
                from_card=from_card,
                status=SShareContactRequests.HOLD,
            )
            .values_list("id", flat=True)
            .distinct()
        )
    ).update(status=SShareContactRequests.DESTROYED)


def merge_contact_share(user: SUsers, from_card: SCards, to_card: SCards):
    merge_contact_share_in_my_card(user, from_card=from_card, to_card=to_card)
    merge_contact_share_by_my_card(user, from_card=from_card, to_card=to_card)
    merge_contact_share_in_my_friend_card(user, from_card=from_card, to_card=to_card)
