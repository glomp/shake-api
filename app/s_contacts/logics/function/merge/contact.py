from django.db import transaction
from django.db.models.query_utils import Q

from helper.common import print_query
from helper.serializer.to_dict import query_to_dict
from s_cards.models import SCards
from s_user.models import SUsers
from v_user.view import VUser
from ....models import SContacts, SShareContactRequests
from ..move.note_and_task import move_note, move_task


@transaction.atomic
def merge_contact_in_my_card(user: SUsers, from_card: SCards, to_card: SCards):
    # print("merge_contact_in_my_card")
    from_card.validate()
    to_card.validate()

    my = VUser(user)
    contact_exists = query_to_dict(my.contact.all(user_card=to_card), key="card_id")
    card_id_exists = contact_exists.keys()

    # For new contact
    new_contacts = my.contact.all(
        ~Q(card_id__in=card_id_exists), user_card=from_card
    ).all()
    new_card_ids = set(new_contacts.values_list("card_id", flat=True))
    new_contacts.update(user_card=to_card)

    for exists_request in my.contact_request.all(
        to_card=to_card, from_card__in=new_card_ids
    ):
        exists_request.cancel() and exists_request.refund()
    SShareContactRequests.objects.filter(
        to_card=to_card, from_card__in=new_card_ids, status=SShareContactRequests.HOLD
    ).update(status=SShareContactRequests.DESTROYED)

    # For exists contact
    for contact in my.contact.all(card_id__in=card_id_exists, user_card=from_card):
        exists_contact = contact_exists[contact.card_id]
        move_note(user, contact, exists_contact)
        move_task(user, contact, exists_contact)

        contact.status = SContacts.DESTROYED
        contact.save(update_fields=["status"])


@transaction.atomic
def merge_contact_in_my_friend_card(user: SUsers, from_card: SCards, to_card: SCards):
    # print("merge_contact_in_my_friend_card")
    from_card.validate()
    to_card.validate()

    my_friend = VUser(user).friend
    contact_exists = query_to_dict(
        my_friend.contact.all(card=to_card), key="user_card_id"
    )
    card_id_exists = contact_exists.keys()

    # For new contact
    new_contacts = my_friend.contact.all(
        ~Q(user_card_id__in=card_id_exists), card=from_card
    )
    new_card_ids = set(new_contacts.values_list("user_card_id", flat=True))
    new_contacts.update(card=to_card)
    for exists_request in my_friend.contact_request.all(
        from_card=to_card, to_card__in=new_card_ids
    ):
        exists_request.cancel() and exists_request.refund()
    SShareContactRequests.objects.filter(
        from_card=to_card, to_card__in=new_card_ids, status=SShareContactRequests.HOLD
    ).update(status=SShareContactRequests.DESTROYED)

    # For exists contact
    for contact in my_friend.contact.all(card_id__in=card_id_exists, card=from_card):
        exists_contact = contact_exists[contact.card_id]
        move_note(user, contact, exists_contact)
        move_task(user, contact, exists_contact)

        contact.status = SContacts.DESTROYED
        contact.save(update_fields=["status"])


@transaction.atomic
def merge_contact(user: SUsers, from_card: SCards, to_card: SCards):
    merge_contact_in_my_card(user, from_card=from_card, to_card=to_card)
    merge_contact_in_my_friend_card(user, from_card=from_card, to_card=to_card)
