from .copy import copy_contact
from .merge.contact import (
    merge_contact,
    merge_contact_in_my_card,
    merge_contact_in_my_friend_card,
)
from .merge.contact_request import (
    merge_contact_request,
    merge_contact_request_in_my_card,
    merge_contact_request_in_my_friend_card,
)
from .merge.contact_share import (
    merge_contact_share,
    merge_contact_share_by_my_card,
    merge_contact_share_in_my_card,
    merge_contact_share_in_my_friend_card,
)

__all__ = (
    "copy_contact",
    "merge_contact",
    "merge_contact_in_my_card",
    "merge_contact_in_my_friend_card",
    "merge_contact_request",
    "merge_contact_request_in_my_card",
    "merge_contact_request_in_my_friend_card",
    "merge_contact_share",
    "merge_contact_share_in_my_card",
    "merge_contact_share_by_my_card",
    "merge_contact_share_in_my_friend_card",
)
