from django.db import transaction

from s_cards.models import SCards
from s_user.models import SUsers
from v_user.view import VUser
from ..contact import CompanyContact, EmployeeContact, UserContact


@transaction.atomic
def copy_contact(user: SUsers, from_card: SCards, to_card: SCards) -> int:
    from_card.validate()
    to_card.validate()

    my = VUser(user)
    contacts = my.contact.all(user_card=from_card).select_related(
        "card", "card__company_user", "card__company_user__company"
    )
    for contact in contacts:
        kwargs = dict(
            own_card=to_card,
            target_card=contact.card,
            tier_number=contact.tier_number,
            raise_exception=False,
            keep_max_tier=True,
        )
        if to_card.card_type == SCards.BUSINESS:
            CompanyContact.create_or_get(**kwargs)
            EmployeeContact.create_or_get(**kwargs)
        else:
            UserContact.create_or_get(**kwargs)
