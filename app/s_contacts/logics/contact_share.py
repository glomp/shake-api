from typing import List, Tuple

from django.db.models.query_utils import Q

from helper.exceptions import BadRequestException
from s_cards.models import SCards
from s_notifications.notification.contact import ShareContactRequestNotification
from s_notifications.self_notification.contact import (
    ShareContactRequestSelfNotification,
)
from v_user.contact import VUContact
from v_user.contact_card import VUContactCard
from ..models import SContactRequests, SContacts, SShareContactRequests
from ..serializers import (
    SDetailedShareContactRequestsSerializer,
    SShareContactRequestsSerializer,
)


class ContactShare:
    @classmethod
    def get_cards_in_contact(cls, user, to_card_ids: list) -> List[SCards]:
        return VUContactCard(user).all(~Q(card_type=SCards.PRINTED), id__in=to_card_ids)

    @classmethod
    def create_or_get(
        cls,
        from_card: SCards,
        to_card: SCards,
        by_card: SCards,
        tier_number: int,
        raise_exception=True,
        **kwargs
    ) -> Tuple[SShareContactRequests, bool]:
        """Create or get the contact request

        Args:
            from_card (SCards): Card of user's friend
            to_card (SCards): Card of targeter
            by_card (SCards): Card of current user
            tier_number (int): number
            raise_exception (bool): if False than the result could be (None, False)

        Returns:
            Tuple[SContacts, bool]:
                None and False if validation false or contact is already existed
                Object and False if share request is already existed
                Object and True if share request is created
        """
        if not (
            from_card.validate(raise_exception, errcode=400094)
            or to_card.validate(raise_exception, errcode=400095)
            or by_card.validate(raise_exception, errcode=400096)
        ):
            return None, False
        v_contact = VUContact(to_card.user)
        if v_contact.get_if_exist(card=from_card, user_card=to_card):
            return None, False

        contact_share: SShareContactRequests = (
            SShareContactRequests.objects.get_if_exist(
                from_card_id=from_card.id,
                to_card=to_card.id,
                by_card=by_card.id,
                status=SShareContactRequests.HOLD,
            )
        )
        if contact_share is None:
            data = dict(
                from_card=from_card.id,
                to_card=to_card.id,
                by_card=by_card.id,
                user=from_card.user_id,
                tier_number=tier_number,
                message=kwargs.get("message", None),
            )
            serializer = SShareContactRequestsSerializer(data=data)
            serializer.is_valid(raise_exception=True)
            return serializer.save(), True
        else:
            contact_share.tier_number = tier_number
            contact_share.message = kwargs.get("message") or contact_share.message
            contact_share.save(update_fields=["tier_number", "message"])
        return contact_share, False

    @classmethod
    def destroy(cls, contact_share: SShareContactRequests, raise_exception=True):
        if contact_share.status == SShareContactRequests.DESTROYED:
            if raise_exception:
                raise BadRequestException(
                    errcode=400075,
                    message="This share request is destroyed.",
                    data={"share_contact_request_id": contact_share.id},
                )
            else:
                return False

        contact_share.status = SShareContactRequests.DESTROYED
        contact_share.save(update_fields=["status"])
        return True

    @classmethod
    def notify(
        cls,
        current_user,
        contact_share: SShareContactRequests,
        is_created: bool,
        **kwargs
    ):
        if contact_share is None or not is_created:
            if is_created:  # If user already have this share contact request
                pass
            else:  # Silence validation
                pass
            return
        from_card: SCards = kwargs.get("from_card", contact_share.from_card)
        to_card: SCards = kwargs.get("to_card", contact_share.to_card)
        by_card: SCards = kwargs.get("by_card", contact_share.by_card)
        contact_share_data = SDetailedShareContactRequestsSerializer(contact_share).data

        ShareContactRequestNotification(
            data=contact_share_data, target_id=contact_share.id
        ).created_share.by(by_card).notify(
            from_card.user, recipient=to_card.user_name, save=is_created
        )
        ShareContactRequestSelfNotification(
            current_user,
            data=contact_share_data,
            target_card=from_card,
            target_id=contact_share.id,
        ).created_share.by(by_card).notify(
            assignee=from_card.user_name,
            recipient=to_card.user_name,
            save=is_created,
        )


def create_contact_share(
    current_user,
    from_card: SCards,
    to_card: SCards,
    by_card: SCards,
    tier_number: int,
    raise_exception=True,
    **kwargs
) -> Tuple[SShareContactRequests, bool]:
    contact_share, is_created = ContactShare.create_or_get(
        from_card,
        to_card,
        by_card,
        tier_number,
        raise_exception=raise_exception,
        **kwargs
    )
    ContactShare.notify(
        current_user,
        contact_share,
        is_created,
        from_card=from_card,
        to_card=to_card,
        by_card=by_card,
    )
    return contact_share, is_created
