from typing import Tuple

from helper.exceptions import AppException, BadRequestException
from s_cards.models import SCards
from s_contacts.serializers import SContactsSerializer
from s_notifications.notification.contact import ContactRequestNotification
from s_notifications.self_notification.contact import (
    ContactRequestSelfNotification,
    ContactSelfNotification,
)
from ..models import SContactRequests, SContacts


class Contact:
    TYPE: SContacts.TYPE = None

    @classmethod
    def create_or_get(
        cls,
        own_card: SCards,
        target_card: SCards,
        tier_number: int,
        raise_exception=True,
        keep_max_tier=False,
    ) -> Tuple[SContacts, bool]:
        """Create or get the contact

        Args:
            own_card (SCards): Card of current user
            target_card (SCards): Card of targeter
            tier_number (int): number

        Returns:
            Tuple[SContacts, bool]:
                None and False if validation false
                Object and False if share request is already existed
                Object and True if share request is created
        """
        if not (
            own_card.validate(raise_exception, errcode=400097)
            or target_card.validate(raise_exception, errcode=400098)
        ):
            return None, False

        contact: SContacts = SContacts.objects.get_if_exist(
            type=cls.TYPE,
            card_id=target_card,
            user_card_id=own_card,
            status=SContacts.ACTIVE,
        )
        if contact is None:
            data = dict(
                type=cls.TYPE,
                card=target_card.id,
                related_user=target_card.user_id,
                user=own_card.user_id,
                user_card=own_card.id,
                company=(
                    own_card.company_user.company_id
                    if own_card.card_type == SCards.BUSINESS
                    else None
                ),
                tier_number=tier_number,
            )
            serializer = SContactsSerializer(data=data)
            serializer.is_valid(raise_exception=True)
            return serializer.save(), True
        elif (keep_max_tier and contact.tier_number < tier_number) or (
            not keep_max_tier and contact.tier_number != tier_number
        ):
            contact.tier_number = tier_number
            contact.save(update_fields=["tier_number"])
        return contact, False

    @classmethod
    def create_from_request(
        cls, contact_request: SContactRequests, raise_exception=True, accepted=False
    ) -> Tuple[SContacts, bool]:
        if not accepted and contact_request.status != SContactRequests.HOLD:
            raise AppException(
                errcode=500072,
                message="This contact request is accepted.",
                data={"contact_request_id": contact_request.id},
            )
        contact_request.status = SContactRequests.ACCEPTED
        contact_request.save(update_fields=["status"])

        return cls.create_or_get(
            own_card=contact_request.to_card,
            target_card=contact_request.from_card,
            tier_number=contact_request.tier_number,
            raise_exception=raise_exception,
        )

    @classmethod
    def destroy(cls, contact: SContacts, raise_exception=True) -> bool:
        if contact.status == SContacts.DESTROYED:
            if raise_exception:
                raise BadRequestException(
                    errcode=400073,
                    message="This contact is destroyed.",
                    data={"contact_id": contact.id},
                )
            else:
                return False

        contact.status = SContacts.DESTROYED
        contact.save(update_fields=["status"])
        return True


class UserContact(Contact):
    TYPE = SContacts.USER

    @classmethod
    def notify_by_confirm(
        cls,
        current_user,
        contact: SContacts,
        contact_request: SContactRequests,
        is_created: bool,
        **kwargs
    ):
        if contact is None or not is_created:
            return
        my_card: SCards = kwargs.get("user_card", contact.user_card)
        card: SCards = kwargs.get("card", contact.card)
        ContactRequestNotification(
            target_card=my_card, target_id=contact_request.id
        ).confirmed_request.by(my_card).notify(card.user, save=is_created)
        ContactSelfNotification(
            current_user, target_card=card, target_id=contact.id
        ).received.by(my_card).notify(
            card_name=card.card_name,
            sender=card.user_name,
            save=is_created,
        )


class EmployeeContact(Contact):
    TYPE = SContacts.EMPLOYEE

    @classmethod
    def notify_by_confirm(
        cls,
        current_user,
        contact: SContacts,
        contact_request: SContactRequests,
        is_created: bool,
        **kwargs
    ):
        if contact is None or not is_created:
            return
        my_card: SCards = kwargs.get("user_card", contact.user_card)
        card: SCards = kwargs.get("card", contact.card)
        ContactRequestNotification(
            target_card=my_card, target_id=contact_request.id
        ).confirmed_request.by(my_card).notify(card.user, save=is_created)
        ContactSelfNotification(
            current_user, target_card=card, target_id=contact.id
        ).received.by(my_card).notify(
            card_name=card.card_name,
            sender=card.user_name,
            save=is_created,
        )


class CompanyContact(Contact):
    TYPE = SContacts.COMPANY

    @classmethod
    def create_or_get(
        cls,
        own_card: SCards,
        target_card: SCards,
        tier_number: int,
        raise_exception=True,
        keep_max_tier=False,
    ) -> Tuple[SContacts, bool]:
        """Create or get the contact

        Args:
            own_card (SCards): Card of current user
            target_card (SCards): Card of targeter
            tier_number (int): number

        Returns:
            Tuple[SContacts, bool]: Contact and True if this is a new contact
        """
        if not (
            own_card.validate(raise_exception, errcode=400099)
            or target_card.validate(raise_exception, errcode=400100)
        ):
            return None, False

        contact: SContacts = SContacts.objects.get_if_exist(
            type=cls.TYPE,
            card_id=target_card.id,
            user_card_id=own_card.id,
        )
        if contact is None:
            data = dict(
                type=cls.TYPE,
                card_id=target_card.id,
                related_user_id=target_card.user_id,
                user_id=None,
                user_card_id=own_card.id,
                company_id=own_card.company_user.company_id,
                tier_number=tier_number,
            )
            return SContacts.objects.create(**data), True
        elif (keep_max_tier and contact.tier_number < tier_number) or (
            not keep_max_tier and contact.tier_number != tier_number
        ):
            contact.tier_number = tier_number
            contact.save(update_fields=["tier_number"])
        return contact, False

    @classmethod
    def notify_by_confirm(
        cls,
        current_user,
        contact: SContacts,
        contact_request: SContactRequests,
        is_created: bool,
        **kwargs
    ):
        pass
