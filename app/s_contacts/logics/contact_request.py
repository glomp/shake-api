import types
from typing import Tuple

from helper.exceptions import BadRequestException
from s_cards.models import SCards
from s_notifications.notification.contact import ContactRequestNotification
from s_notifications.self_notification.contact import ContactRequestSelfNotification
from v_user.contact import VUContact
from ..models import SContactRequests, SContacts, SShareContactRequests
from ..serializers import SContactRequestSerializer, SDetailedContactRequestsSerializer


class ContactRequest:
    CREDIT_TYPE: SContactRequests.CREDIT_TYPE = None

    @classmethod
    def create_or_get(
        cls,
        from_card: SCards,
        to_card: SCards,
        tier_number: int,
        credit=CREDIT_TYPE,
        raise_exception=True,
        **kwargs,
    ) -> Tuple[SContactRequests, bool]:
        """Create or get the contact request

        Args:
            from_card (SCards): Card of current user
            to_card (SCards): Card of targeter
            tier_number (int): number
            credit (SContactRequests.CREDIT_TYPE): Which credit is used
            raise_exception (bool): if False than the result could be (None, False)

        Returns:
            Tuple[SContacts, bool]:
                None and False if validation false or contact is already existed
                Object and False if share request is already existed
                Object and True if share request is created
        """
        if not (
            from_card.validate(raise_exception, errcode=400092)
            or to_card.validate(raise_exception, errcode=400093)
        ):
            return None, False
        v_contact = VUContact(to_card.user)
        if v_contact.get_if_exist(card=from_card, user_card=to_card):
            return None, False

        contact_request: SContactRequests = SContactRequests.objects.get_if_exist(
            from_card_id=from_card.id, to_card=to_card.id, status=SContactRequests.HOLD
        )
        if contact_request is None:
            credit = next(credit) if isinstance(credit, types.GeneratorType) else credit
            data = dict(
                credit_type=credit,
                from_card=from_card.id,
                to_card=to_card.id,
                user=to_card.user_id,
                tier_number=tier_number,
                **kwargs,
            )
            serializer = SContactRequestSerializer(data=data)
            serializer.is_valid(raise_exception=True)
            return serializer.save(), True
        else:
            contact_request.tier_number = tier_number
            contact_request.message = kwargs.get("message") or contact_request.message
            contact_request.save(update_fields=["tier_number", "message"])
        return contact_request, False

    @classmethod
    def create_from_request(
        cls,
        share_request: SShareContactRequests,
        credit=CREDIT_TYPE,
        raise_exception=True,
        **kwargs,
    ) -> Tuple[SContactRequests, bool]:
        assert (
            share_request.status == SShareContactRequests.HOLD
        ), "This share contact request is accepted."
        share_request.status = SShareContactRequests.ACCEPTED
        share_request.save(update_fields=["status"])

        return cls.create_or_get(
            from_card=share_request.from_card,
            to_card=share_request.to_card,
            tier_number=share_request.tier_number,
            credit=credit,
            raise_exception=raise_exception,
            **kwargs,
        )

    @classmethod
    def destroy(cls, contact_request: SContactRequests, raise_exception=True):
        if contact_request.status == SContactRequests.DESTROYED:
            if raise_exception:
                raise BadRequestException(
                    errcode=400074,
                    message="This contact request is destroyed.",
                    data={"contact_request_id": contact_request.id},
                )
            else:
                return False

        contact_request.status = SContactRequests.DESTROYED
        contact_request.save(update_fields=["status"])
        return True

    @classmethod
    def notify(
        cls, current_user, contact_request: SContactRequests, is_created: bool, **kwargs
    ):

        if contact_request is None or not is_created:
            if is_created:  # If user already have this contact
                pass
            else:  # Silence validation
                pass
            return
        from_card: SCards = kwargs.get("from_card", contact_request.from_card)
        to_card: SCards = kwargs.get("to_card", contact_request.to_card)
        contact_request_data = SDetailedContactRequestsSerializer(contact_request).data

        ContactRequestNotification(
            data=contact_request_data, target_id=contact_request.id
        ).created_request.by(from_card).notify(to_card.user)
        ContactRequestSelfNotification(
            current_user,
            data=contact_request_data,
            target_card=to_card,
            target_id=contact_request.id,
        ).created_request.by(from_card).notify(
            card_name=from_card.card_name,
            recipient=to_card.user_name,
            save=is_created,
        )


def create_contact_request(
    current_user,
    from_card: SCards,
    to_card: SCards,
    tier_number: int,
    credit: SContactRequests.CREDIT_TYPE,
    raise_exception=True,
    **kwargs,
) -> Tuple[SContactRequests, bool]:
    contact_request, is_created = ContactRequest.create_or_get(
        from_card,
        to_card,
        tier_number,
        credit=credit,
        raise_exception=raise_exception,
        **kwargs,
    )
    ContactRequest.notify(
        current_user, contact_request, is_created, from_card=from_card, to_card=to_card
    )
    return contact_request, is_created
