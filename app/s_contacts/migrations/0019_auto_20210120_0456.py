# Generated by Django 2.2.17 on 2021-01-20 04:56

from django.db import migrations, models
import django_mysql.models


class Migration(migrations.Migration):

    dependencies = [
        ('s_contacts', '0018_auto_20210120_0214'),
    ]

    operations = [
        migrations.AlterField(
            model_name='saggregatecontacts',
            name='company_ids',
            field=django_mysql.models.ListCharField(models.IntegerField(), default=[], max_length=255, size=8),
        ),
        migrations.AlterField(
            model_name='saggregatecontacts',
            name='tiers',
            field=django_mysql.models.ListCharField(models.IntegerField(), default=[], max_length=7, size=4),
        ),
    ]
