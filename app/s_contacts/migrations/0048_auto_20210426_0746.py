# Generated by Django 2.2.19 on 2021-04-26 07:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("s_contacts", "0047_custom_20210422_0337"),
    ]

    operations = [
        migrations.AlterField(
            model_name="scontacts",
            name="type",
            field=models.CharField(
                choices=[("U", "USER"), ("C", "COMPANY"), ("E", "COMPANY_EMPLOYEE")],
                max_length=1,
            ),
        ),
    ]
