# Generated by Django 2.2.17 on 2021-01-20 10:04

from django.db import migrations, models
import django_mysql.models


class Migration(migrations.Migration):

    dependencies = [
        ('s_contacts', '0020_auto_20210120_0733'),
    ]

    operations = [
        migrations.AddField(
            model_name='saggregatecontacts',
            name='belong_to',
            field=django_mysql.models.ListCharField(models.IntegerField(), default=[], max_length=511, size=8),
        ),
    ]
