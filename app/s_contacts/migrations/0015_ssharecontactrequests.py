# Generated by Django 2.2.17 on 2021-01-18 09:27

import custom_django.models.fields.unix_timestamp
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('s_user', '0005_auto_20210106_0242'),
        ('s_cards', '0005_scardsview'),
        ('s_contacts', '0014_saggregatecontacts'),
    ]

    operations = [
        migrations.CreateModel(
            name='SShareContactRequests',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(choices=[('H', 'HOLD'), ('A', 'ACCEPTED')], default='H', max_length=1)),
                ('_created', custom_django.models.fields.unix_timestamp.UnixTimeStampField(auto_now_add=True, null=True)),
                ('_updated', custom_django.models.fields.unix_timestamp.UnixTimeStampField(auto_now=True, null=True)),
                ('by_card', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='share_by', to='s_cards.SCards')),
                ('to_card', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='share_to', to='s_cards.SCards')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='s_user.SUsers')),
            ],
            options={
                'db_table': 's_share_contact_request',
                'managed': True,
                'unique_together': {('user', 'by_card', 'to_card')},
            },
        ),
    ]
