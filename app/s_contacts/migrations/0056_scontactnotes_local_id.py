# Generated by Django 2.2.19 on 2021-05-17 04:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('s_contacts', '0055_delete_saggregatecontactviews'),
    ]

    operations = [
        migrations.AddField(
            model_name='scontactnotes',
            name='local_id',
            field=models.BigIntegerField(default=0),
        ),
    ]
