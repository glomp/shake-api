# Generated by Django 2.2.17 on 2021-01-27 10:06

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('s_contacts', '0027_scontacts_user_card'),
    ]

    operations = [
        migrations.DeleteModel(
            name='SCompanyUserContacts',
        ),
    ]
