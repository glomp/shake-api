# Generated by Django 2.2.17 on 2021-01-28 03:17

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("s_contacts", "0029_auto_20210128_0317"),
    ]

    operations = [
        migrations.RunSQL(
            """
WITH 
	personal_card AS (
		SELECT
			ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY card_type) AS idx,
			user_id, card_type, id AS card_id
		FROM s_card sc 
		WHERE card_type not in ('B', 'T')
	),
	personal_contact AS (
		SELECT 
			ROW_NUMBER() OVER (PARTITION BY user_id, card_id ORDER BY user_id, card_id) AS idx2,
			sc.card_id, sc.user_card_id, sc.id AS contact_id, user_id 
		FROM s_contact sc
		WHERE sc.company_id is NULL
	)
UPDATE s_contact sc
	JOIN personal_contact pct ON sc.id = pct.contact_id
	JOIN personal_card pc ON pct.user_id = pc.user_id AND pct.idx2 = pc.idx
SET sc.user_card_id = pc.card_id
            """
        ),
        migrations.RunSQL(
            """
UPDATE s_contact sct
JOIN s_card sc ON sct.user_card_id = sc.id
SET sct.user_id = sc.user_id
            """
        ),
    ]
