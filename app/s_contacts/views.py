from typing import List

from django.conf import settings
from django.db import transaction
from django.db.models.query_utils import Q
from django.http.response import Http404
from rest_framework.decorators import action

from custom_django.views.viewsets import SDynamicModelViewSet
from helper.exceptions import AppException, BadRequestException, ForbiddenException
from s_balances.logics.balance_manager import BalanceManager
from s_cards.models import SCards
from s_cards.serializers import SPublicThoroughCardSerializer
from s_contacts.logics.contact_share import ContactShare, create_contact_share
from s_notifications.self_notification import ContactRequestSelfNotification
from v_user.contact import VUContact
from v_user.special import get_my_card_let_another_user_know_me
from .logics.contact import CompanyContact, Contact, EmployeeContact, UserContact
from .logics.contact_request import ContactRequest, create_contact_request
from .models import (
    SContactNotes,
    SContactRequests,
    SContacts,
    SContactTasks,
    SShareContactRequests,
)
from .serializers import (
    SContactNotesSerializer,
    SContactTasksSerializer,
    SDetailedContactRequestsSerializer,
    SDetailedShareContactRequestsSerializer,
    SPublicContactCardSerializer,
    SUserContactsSerializer,
)


class SContactsViewSet(SDynamicModelViewSet):
    """Manage contact"""

    serializer_class = SUserContactsSerializer
    model_class = SContacts
    queryset = SContacts.objects.all()

    def retrieve(self, request, pk):
        return self.view.contact.get(id=pk)

    def list(self, request):
        return self.view.contact.all()

    def create(self, request, *args, **kwargs):
        raise Http404

    def update(self, request, *args, **kwargs):
        raise Http404

    def destroy(self, request, pk):
        instance = self.view.contact_card.get(id=pk)
        if Contact.destroy(instance, raise_exception=False):
            return "SUCCESS"


class SContactCardsViewSet(SDynamicModelViewSet):
    serializer_class = SPublicContactCardSerializer
    model_class = SCards
    queryset = SCards.objects.all()

    def retrieve(self, request, pk):
        contact_card = self.view.contact_card.get(id=pk)
        return self._get_contact_card_dict(contact_card)

    def list(self, request):
        contact_cards = self.view.contact_card.all()
        contact_card_datas = []
        for contact_card in contact_cards:
            contact_card_datas.append(self._get_contact_card_dict(contact_card))
        return contact_card_datas

    def create(self, request, *args, **kwargs):
        raise Http404

    def update(self, request, *args, **kwargs):
        raise Http404

    def destroy(self, request, pk):
        raise Http404

    @action(methods=["GET", "POST"], detail=True)
    @transaction.atomic
    def notes(self, request, pk):
        if request.method == "GET":
            return SContactNotesSerializer(
                self.view.note.all(contact__card_id=pk), many=True
            )

        user_contact = self._get_contact_from_card(request, pk)
        data = request.data.copy()
        data.update(user=self.view.user.id, contact=user_contact.id)
        note_ser = SContactNotesSerializer(data=data)
        note_ser.is_valid(raise_exception=True)
        note_ser.save()
        return note_ser.data

    @action(methods=["GET", "POST"], detail=True)
    @transaction.atomic
    def tasks(self, request, pk):
        if request.method == "GET":
            return SContactTasksSerializer(
                self.view.task.all(contact__card_id=pk), many=True
            )

        user_contact = self._get_contact_from_card(request, pk)
        data = request.data.copy()
        data.update(user=self.view.user.id, contact=user_contact.id)
        task_ser = SContactTasksSerializer(data=data)
        task_ser.is_valid(raise_exception=True)
        task_ser.save()
        return task_ser.data

    @classmethod
    def _get_contact_card_dict(cls, contact_card: SCards):
        data = cls.serializer_class(contact_card).data

        properties = contact_card.get_properties_from_tiers(contact_card.tiers)
        properties_map = {_property.label: _property.value for _property in properties}
        data["mobile"] = (
            properties_map.get("mobile", None)
            or properties_map.get("work mobile", None)
            or ""
        )
        data["email"] = (
            properties_map.get("email", None)
            or properties_map.get("work email", None)
            or ""
        )
        data["address"] = properties_map.get("address", None) or ""
        return data

    def _get_contact_from_card(self, request, card_id):
        contacts = list(self.view.contact.all(card_id=card_id))
        if not contacts:
            raise BadRequestException(
                errcode=400051, message="You don't have this card!"
            )
        user_contact = None
        if len(contacts) > 2:
            user_card_id = request.data["from_card"]
            for contact in contacts:
                if contact.user_card_id == user_card_id:
                    user_contact = contact
                    break
            if user_contact is None:
                raise BadRequestException(
                    errcode=400050,
                    message="These 2 cards are not related to each other.",
                    data={"user_card_id": user_card_id},
                )
        else:
            user_contact = contacts[0]
        return user_contact

    @action(methods=["POST"], detail=False)
    @transaction.atomic
    def test(self, request):
        from .logics.function.move import move_contact

        from_card = self.view.card.get(id=request.data["from_card"])
        to_card = self.view.card.get(id=request.data["to_card"])
        move_contact(self.user, from_card, to_card)
        0 / 0


class SContactRequestsViewSet(SDynamicModelViewSet):
    """Create a contact request and manage it"""

    serializer_class = SDetailedContactRequestsSerializer
    model_class = SContactRequests
    queryset = SContactRequests.objects.all()

    def retrieve(self, request, pk, partial=False):
        return self.view.contact_request.get(id=pk)

    def list(self, request):
        return self.view.contact_request.all()

    @transaction.atomic
    def create(self, request):
        from_card = self.view.card.get(id=request.data["from_card_id"])

        if not isinstance(request.data["to_card_id"], list):
            raise BadRequestException(
                errcode=400037, message="'To_card' must be a list"
            )
        # TODO: Must be users who are allow to search (base on visibility)
        to_cards: List[SCards] = SCards.objects.filter(
            ~Q(user_id=self.user.id),
            searchable=True,
            status=SCards.ACTIVE,
            id__in=request.data["to_card_id"],
        )

        contact_requests = []
        balance_mgr = BalanceManager(self.user, from_card)
        for to_card in to_cards:
            contact_request, _ = create_contact_request(
                self.user,
                from_card=from_card,
                to_card=to_card,
                tier_number=request.data["tier_number"],
                credit=(
                    balance_mgr.withdraw_one_credit()
                    if settings.ENV == "TEST"
                    else SContactRequests.FREE
                ),
                raise_exception=False,
                message=request.data.get("message", None),
            )
            if contact_request is not None:
                contact_requests.append(contact_request)
        balance_mgr.save()

        return self.serializer_class(contact_requests, many=True)

    def update(self, request, *args, **kwargs):
        return Http404

    @transaction.atomic
    def destroy(self, request, pk):
        instance = self.view.contact_request.advanced.get(id=pk)
        if ContactRequest.destroy(instance):
            to_card: SCards = instance.to_card
            from_card: SCards = instance.from_card

            if to_card.card_type != SCards.PRINTED:
                ContactRequestSelfNotification(
                    self.user, target_card=from_card, target_id=instance.id
                ).declined_request.by(to_card).notify(
                    card_name=from_card.card_name, sender=from_card.user_name
                )
            instance.refund()

            return "SUCCESS"

    @action(methods=["post"], detail=True)
    @transaction.atomic
    def confirm(self, request, pk, partial=None):
        contact_request = self.view.contact_request.advanced.get(
            id=pk,
            prefetch_related=["from_card__sproperties_set"],
            select_related=[  # for validate card
                "from_card",
                "to_card",
                "from_card__company_user",
                "from_card__company_user__company",
                "to_card__company_user",
                "to_card__company_user__company",
            ],
        )
        to_card: SCards = contact_request.to_card
        from_card: SCards = contact_request.from_card
        if contact_request.status == SContactRequests.HOLD:
            if to_card.card_type == SCards.BUSINESS:
                ctc, i_c = CompanyContact.create_from_request(contact_request)
                CompanyContact.notify_by_confirm(
                    self.user,
                    contact=ctc,
                    contact_request=contact_request,
                    is_created=i_c,
                )

                ctc, i_c = EmployeeContact.create_from_request(
                    contact_request, accepted=True
                )
                EmployeeContact.notify_by_confirm(
                    self.user,
                    contact=ctc,
                    contact_request=contact_request,
                    is_created=i_c,
                )
            else:
                ctc, i_c = UserContact.create_from_request(contact_request)
                UserContact.notify_by_confirm(
                    self.user,
                    contact=ctc,
                    contact_request=contact_request,
                    is_created=i_c,
                )

        opposite_relationship = (
            self.view.friend.contact.all(card=to_card, user_card=from_card).first()
            or self.view.friend.contact_request.all(
                from_card=to_card, to_card=from_card
            ).first()
        )

        return {
            "send_card_back": opposite_relationship is None,
            "card": SPublicThoroughCardSerializer(from_card),
            "contact_card": SContactCardsViewSet._get_contact_card_dict(
                self.view.contact_card.get(id=from_card.id)
            ),
            "contact": self.serializer_class(contact_request),
        }

    @action(methods=["post"], detail=False, url_path="send-one-to-me")
    @transaction.atomic
    def send_one_to_me(self, request):
        card = self.user.get_first_card()
        return self._send_one_to_my_card(request=request, pk=card.id)

    @action(methods=["post"], detail=True, url_path="send-one-to-my-card")
    @transaction.atomic
    def send_one_to_my_card(self, request, pk=None):
        return self._send_one_to_my_card(request=request, pk=pk)

    def _send_one_to_my_card(self, request, pk):
        if self.user_auth.is_staff is False:
            raise ForbiddenException(errcode=403065, message="You are not our staff.")

        my_card = self.view.card.get(id=pk)
        all_requested_card = set(
            [
                contact["from_card_id"]
                for contact in self.view.contact_request.advanced.all()
                .filter(to_card_id=my_card.id)
                .values("from_card_id")
            ]
        )
        target_card = SCards.objects.filter(
            ~Q(id__in=all_requested_card),
            ~Q(user_id=self.user.id),
            searchable=True,
        ).first()
        if target_card is None:
            contact_request: SContactRequests = (
                self.view.contact_request.advanced.all(
                    status=SContacts.ACTIVE, to_card=my_card.id
                )
                .select_related("from_card")
                .order_by("-_created")
                .first()
            )
            SContacts.objects.get(
                card=contact_request.from_card_id, user_card=my_card.id
            ).delete()
            target_card = contact_request.from_card

        contact_request, _ = create_contact_request(
            self.user,
            from_card=target_card,
            to_card=my_card,
            tier_number=3,
            credit=SContactRequests.FREE,
            raise_exception=False,
            message=f"Hi {self.user.get_first_card().user_name}! Nice to meet you!",
        )
        return contact_request


class SShareContactRequestsViewSet(SDynamicModelViewSet):
    """Create a share contact request and manage it"""

    serializer_class = SDetailedShareContactRequestsSerializer
    model_class = SShareContactRequests
    queryset = SShareContactRequests.objects.all()

    def retrieve(self, request, pk):
        return self.view.contact_share.get(id=pk, status=SShareContactRequests.HOLD)

    def list(self, request):
        return self.view.contact_share.all(status=SShareContactRequests.HOLD)

    @transaction.atomic
    def create(self, request):
        from_card = self.view.contact_card.get(id=request.data["from_card"])
        if from_card is None:
            raise BadRequestException(
                errcode=400047,
                message="Some card is not in your contact list.",
                data={
                    "from_card": request.data["from_card"],
                    "current_user_id": self.user.id,
                },
            )
        if from_card.shareability == SCards.NOT_ALLOWED:
            raise BadRequestException(
                errcode=400079,
                message="This user is not allowed to share this card.",
                data={
                    "card_id": from_card.id,
                    "shareability": from_card.shareability,
                },
            )
        if from_card.card_type == SCards.PRINTED:
            raise BadRequestException(
                errcode=400080,
                message="You can't share this card.",
                data={
                    "card_id": from_card.id,
                    "card_type": from_card.card_type,
                },
            )

        to_contact_cards = ContactShare.get_cards_in_contact(
            self.user, set(request.data["to_card"])
        )
        by_card = get_my_card_let_another_user_know_me(
            self.view.user.id, from_card.user_id
        )

        share_requests = []
        for to_card in to_contact_cards:
            share_request, _ = create_contact_share(
                self.user,
                from_card=from_card,
                to_card=to_card,
                by_card=by_card,
                tier_number=from_card.tiers[0],
                raise_exception=False,
                message=request.data.get("message", None),
            )
            share_requests.append(share_request)

        return SDetailedShareContactRequestsSerializer(
            filter(None, share_requests), many=True
        )

    def update(self, request, *args, **kwargs):
        raise Http404

    @transaction.atomic
    def destroy(self, request, pk):
        instance = self.view.contact_share.get(id=pk, status=SShareContactRequests.HOLD)
        if ContactShare.destroy(instance):
            return "SUCCESS"

    @action(methods=["post"], detail=True)
    @transaction.atomic
    def confirm(self, request, pk, partial=None):
        share_request = (
            self.view.contact_share.all()
            .select_related(  # for validate card
                "from_card__company_user",
                "from_card__company_user__company",
                "to_card__company_user",
                "to_card__company_user__company",
            )
            .get(id=pk, status=SShareContactRequests.HOLD)
        )
        balance_mgr = BalanceManager(self.user, share_request.from_card)
        contact_request, is_created = ContactRequest.create_from_request(
            share_request=share_request,
            credit=balance_mgr.withdraw_one_credit(),
            message=request.data.get("message", None),
        )
        ContactRequest.notify(self.user, contact_request, is_created)
        balance_mgr.save()
        return SContactRequestsViewSet.serializer_class(contact_request)

    @action(methods=["post"], detail=True)
    def test(self, request, pk):
        from s_user.models import SUsers
        from v_user.view import VUser

        user = SUsers.objects.get(id=pk)
        view = VUser(user)

        return view.contact_share.all()


class SContactNotesViewSet(SDynamicModelViewSet):
    """Create a contact note and manage it"""

    serializer_class = SContactNotesSerializer
    model_class = SContactNotes
    queryset = SContactNotes.objects.all()

    def retrieve(self, request, pk: int):
        return self.view.note.get(id=pk)

    def list(self, request):
        return self.view.note.all()

    def _create(self, user, data: dict):
        data = data.copy()
        if data.get("contact", None):
            contact = self.view.contact.get_if_exist(id=data["contact"])
        else:
            contact = self.view.contact.get_if_exist(
                card_id=data["to_card"],
                user_card_id=data["from_card"],
            )
        if contact is None:
            raise BadRequestException(
                errcode=400048, message="This contact is not belong to you!"
            )

        data.update(user=user.id, contact=contact.id)
        note_ser = self.serializer_class(data=data)
        note_ser.is_valid(raise_exception=True)
        note_ser.save()
        return note_ser

    @transaction.atomic
    def create(self, request):
        if isinstance(request.data, list):
            return [self._create(self.user, note) for note in request.data]
        return self._create(self.user, request.data)

    @transaction.atomic
    def update(self, request, pk, partial=True):
        instance = self.view.note.get(id=pk)
        serializer = self.serializer_class(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        return serializer.save()

    @transaction.atomic
    def destroy(self, request, pk):
        instace = self.view.note.get(id=pk)
        cnt, _ = instace.delete()
        if cnt == 1:
            return "SUCCESS"


class SContactTasksViewSet(SDynamicModelViewSet):
    """Create a contact task and manage it"""

    serializer_class = SContactTasksSerializer
    model_class = SContactTasks
    queryset = SContactTasks.objects.all()

    def retrieve(self, request, pk: int):
        return self.view.task.get(id=pk)

    def list(self, request):
        return self.view.task.all()

    def _create(self, user, data):
        data = data.copy()
        if data.get("contact", None):
            contact = VUContact(user).get_if_exist(id=data["contact"])
        else:
            contact = VUContact(user).get_if_exist(
                card_id=data["to_card"],
                user_card_id=data["from_card"],
            )
        if contact is None:
            raise BadRequestException(
                errcode=400049, message="This contact is not belong to you!"
            )
        data.update(user=user.id, contact=contact.id)
        task_ser = self.serializer_class(data=data)
        task_ser.is_valid(raise_exception=True)
        task_ser.save()
        return task_ser

    def create(self, request):
        if isinstance(request.data, list):
            return [self._create(self.user, task) for task in request.data]
        return self._create(self.user, request.data)

    def update(self, request, pk, partial=True):
        instance = self.view.task.get(id=pk)
        serializer = self.serializer_class(
            instance,
            data=request.data,
            context={"user": self.view.user},
            partial=partial,
        )
        serializer.is_valid(raise_exception=True)
        return serializer.save()

    def destroy(self, request, pk):
        instance = self.view.task.get(id=pk)
        cnt, obj = instance.delete()
        if cnt == 1:
            return "SUCCESS"
        elif cnt > 1:
            raise AppException(
                errcode=500057,
                message="Can't perform this action",
                data={"pk": pk, "cnt": cnt, "obj": obj.__dict__},
                err_message="More than one objects are being deleted!",
            )
