from django.db import models

from custom_django.models import SModel, fields
from helper.exceptions import BadRequestException
from s_cards.models import SCards
from s_companies.models import SCompanies
from s_user.models import SUsers


class SContacts(SModel):
    ACTIVE = "A"
    INACTIVE = "I"
    HOLD = "H"
    DESTROYED = "D"
    STATUS = (
        (ACTIVE, "ACTIVE"),
        (INACTIVE, "INACTIVE"),
        (HOLD, "HOLD"),
        (DESTROYED, "DESTROYED"),
    )

    USER = "U"
    COMPANY = "C"
    EMPLOYEE = "E"
    TYPE = (
        (USER, "USER"),
        (COMPANY, "COMPANY"),
        (EMPLOYEE, "EMPLOYEE"),
    )

    card = models.ForeignKey(SCards, models.DO_NOTHING, related_name="related_contacts")
    related_user = models.ForeignKey(
        SUsers,
        models.DO_NOTHING,
        blank=True,
        null=True,
        related_name="related_contacts",
    )

    user_card = models.ForeignKey(
        SCards, models.DO_NOTHING, related_name="own_contacts"
    )
    user = models.ForeignKey(
        SUsers,
        models.DO_NOTHING,
        blank=True,
        null=True,
        # related_name="related_contacts",
    )
    company = models.ForeignKey(SCompanies, models.DO_NOTHING, blank=True, null=True)

    type = models.CharField(max_length=1, choices=TYPE)
    status = models.CharField(max_length=1, default=ACTIVE, choices=STATUS)
    tier_number = models.IntegerField()

    _created = fields.UnixTimeStampField(null=True, auto_now_add=True)
    _updated = fields.UnixTimeStampField(null=True, auto_now=True)

    PERSONAL_FIELDS = ("company", "type")

    READONLY_FIELDS = ["user", "company", "card", "tier_number"]

    def validate(self, raise_exception=True, errcode: int = None, debug: str = ""):
        if getattr(self, "_is_valid", None) is not None:
            return self._is_valid

        from .serializers import SContactsSerializer

        try:
            if self.status == self.DESTROYED:
                raise BadRequestException(
                    errcode=errcode or 400101,
                    message=f"This contact is destroyed.",
                    data={"debug": debug, "card": SContactsSerializer(self).data},
                )
            self._is_valid = True
        except BadRequestException as e:
            if raise_exception:
                raise e
            self._is_valid = False
        return self._is_valid

    class Meta:
        managed = True
        db_table = "s_contact"


class SContactNotes(SModel):
    local_id = models.BigIntegerField(default=0)

    contact = models.ForeignKey(SContacts, models.DO_NOTHING)
    user = models.ForeignKey(
        SUsers, models.DO_NOTHING, db_constraint=False, db_column="user_id"
    )

    name = models.CharField(max_length=127, default="", blank=True)
    content = models.TextField()

    _created = fields.UnixTimeStampField(null=True, auto_now_add=True)
    _updated = fields.UnixTimeStampField(null=True, auto_now=True)

    def get_readonly_fields(self, request, obj=None):
        return ["contact"] if obj else []

    class Meta:
        managed = True
        db_table = "s_contact_note"


class SContactTasks(SModel):
    local_id = models.BigIntegerField(default=0)

    contact = models.ForeignKey(SContacts, models.DO_NOTHING)
    user = models.ForeignKey(
        SUsers, models.DO_NOTHING, db_constraint=False, db_column="user_id"
    )

    name = models.CharField(max_length=127, default="", blank=True)
    content = models.TextField()

    start_time = fields.UnixTimeStampField(default=None, blank=True, null=True)
    end_time = fields.UnixTimeStampField(default=None, blank=True, null=True)
    remind = models.BooleanField(default=True)

    _created = fields.UnixTimeStampField(null=True, auto_now_add=True)
    _updated = fields.UnixTimeStampField(null=True, auto_now=True)

    def get_readonly_fields(self, request, obj=None):
        return ["contact"] if obj else []

    class Meta:
        managed = True
        db_table = "s_contact_task"


class SContactRequests(SModel):
    HOLD = "H"
    ACCEPTED = "A"
    DESTROYED = "D"
    STATUS = (
        (HOLD, "HOLD"),
        (ACCEPTED, "ACCEPTED"),
        (DESTROYED, "DESTROYED"),
    )

    FREE = "F"
    PERSONAL = "P"
    COMPANY = "C"
    CREDIT_TYPE = (
        (FREE, "FREE"),
        (PERSONAL, "PERSONAL"),
        (COMPANY, "COMPANY"),
    )

    user = models.ForeignKey(SUsers, models.DO_NOTHING, blank=True, null=True)
    from_card = models.ForeignKey(
        SCards, models.DO_NOTHING, related_name="contact_requests_from"
    )
    to_card = models.ForeignKey(
        SCards, models.DO_NOTHING, related_name="contact_requests_to"
    )
    tier_number = models.IntegerField()
    message = models.CharField(max_length=511, default=None, blank=True, null=True)

    status = models.CharField(max_length=1, default=HOLD, choices=STATUS)
    credit_type = models.CharField(max_length=1, choices=CREDIT_TYPE)

    _created = fields.UnixTimeStampField(null=True, auto_now_add=True)
    _updated = fields.UnixTimeStampField(null=True, auto_now=True)

    def get_readonly_fields(self, request, obj=None):
        return ["user", "from_card", "to_card", "tier_number"] if obj else []

    def cancel(self) -> bool:
        if self.status != self.HOLD:
            return False

        self.status = self.DESTROYED
        self.save(update_fields=["status"])
        return True

    def refund(self) -> bool:
        if self.credit_type != self.FREE:
            return False

        from s_balances.logics.balance_manager import BalanceManager

        balance_mgr = BalanceManager(self.from_card.user, self.from_card)
        balance_mgr.refund(self.credit_type)
        balance_mgr.save()
        return True

    class Meta:
        managed = True
        db_table = "s_contact_request"


class SShareContactRequests(SModel):
    HOLD = "H"
    ACCEPTED = "A"
    DESTROYED = "D"
    STATUS = (
        (HOLD, "HOLD"),
        (ACCEPTED, "ACCEPTED"),
        (DESTROYED, "DESTROYED"),
    )

    user = models.ForeignKey(SUsers, models.DO_NOTHING, blank=True, null=True)
    by_card = models.ForeignKey(SCards, models.DO_NOTHING, related_name="share_by")
    from_card = models.ForeignKey(SCards, models.DO_NOTHING, related_name="share_from")
    to_card = models.ForeignKey(SCards, models.DO_NOTHING, related_name="share_to")
    tier_number = models.IntegerField()
    message = models.CharField(max_length=511, default=None, blank=True, null=True)

    status = models.CharField(max_length=1, default=HOLD, choices=STATUS)

    _created = fields.UnixTimeStampField(null=True, auto_now_add=True)
    _updated = fields.UnixTimeStampField(null=True, auto_now=True)

    def get_readonly_fields(self, request, obj=None):
        return ["user", "by_card", "to_card"] if obj else []

    class Meta:
        managed = True
        db_table = "s_share_contact_request"
