from dynamic_rest.fields import DynamicRelationField
from dynamic_rest.serializers import DynamicModelSerializer
from rest_framework import serializers

from custom_django.serializers import fields
from s_cards.models import SCards
from s_cards.serializers import SPublicCardSerializer
from s_properties.serializers import (
    SPublicPropertySerializer,
    SPublicThoroughPropertySerializer,
)
from .models import (
    SContactNotes,
    SContactRequests,
    SContacts,
    SContactTasks,
    SShareContactRequests,
)


class SBasicContactsSerializer(DynamicModelSerializer):
    class Meta:
        model = SContacts
        exclude = SContacts.PERSONAL_FIELDS


class SContactsSerializer(DynamicModelSerializer):
    class Meta:
        model = SContacts
        fields = "__all__"


class SUserContactsSerializer(DynamicModelSerializer):
    card = DynamicRelationField(SPublicCardSerializer, embed=True)

    class Meta:
        model = SContacts
        fields = "__all__"


class SCompanyUserContactsSerializer(DynamicModelSerializer):
    employee_card = DynamicRelationField(
        SPublicCardSerializer, source="user_card", embed=True
    )

    class Meta:
        model = SContacts
        fields = "__all__"


class SContactRequestSerializer(DynamicModelSerializer):
    class Meta:
        model = SContactRequests
        fields = "__all__"


class SDetailed2ContactRequestsSerializer(DynamicModelSerializer):
    from_card = DynamicRelationField(SPublicCardSerializer, embed=True)

    class Meta:
        model = SContactRequests
        fields = "__all__"


class SDetailedContactRequestsSerializer(DynamicModelSerializer):
    from_card = DynamicRelationField(SPublicCardSerializer, embed=True)
    to_card = DynamicRelationField(SPublicCardSerializer, embed=True)

    class Meta:
        model = SContactRequests
        fields = "__all__"


class SShareContactRequestsSerializer(DynamicModelSerializer):
    class Meta:
        model = SShareContactRequests
        fields = "__all__"


class SDetailedShareContactRequestsSerializer(DynamicModelSerializer):
    by_card = DynamicRelationField(SPublicCardSerializer, embed=True)
    from_card = DynamicRelationField(SPublicCardSerializer, embed=True)
    to_card = DynamicRelationField(SPublicCardSerializer, embed=True)

    class Meta:
        model = SShareContactRequests
        fields = "__all__"


class SPublicContactCardSerializer(DynamicModelSerializer):
    tiers = fields.IntSeparatedField(child=serializers.IntegerField())
    belong_to_contacts = fields.IntSeparatedField(child=serializers.IntegerField())
    belong_to_cards = fields.IntSeparatedField(child=serializers.IntegerField())

    class Meta:
        model = SCards
        exclude = SCards.PERSONAL_FIELDS


class SContactNotesSerializer(DynamicModelSerializer):
    contact = DynamicRelationField(SBasicContactsSerializer, embed=True)

    class Meta:
        model = SContactNotes
        fields = "__all__"


class SContactTasksSerializer(DynamicModelSerializer):
    contact = DynamicRelationField(SBasicContactsSerializer, embed=True)

    class Meta:
        model = SContactTasks
        fields = "__all__"
