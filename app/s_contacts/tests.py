from typing import List

from custom_django.test.base import BaseTestCase
from custom_django.test.card_logic import FullCardBaseLogic
from custom_django.test.request_data import TestRequestData
from .views import (
    SContactCardsViewSet,
    SContactNotesViewSet,
    SContactRequestsViewSet,
    SContactTasksViewSet,
    SShareContactRequestsViewSet,
)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~ CONTACT CARD ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class ContactPersonalToPersonalTC(BaseTestCase, FullCardBaseLogic):
    ViewSet = SContactCardsViewSet

    def create_general_objects(self):
        _, self.suser2 = self.helper.create_user("user2@yopmail.com")

    def create_specific_objects(self):
        _, self.card = self.create_card_p(self.suser)
        _, self.card2 = self.create_card_p(self.suser2)

    def create_test_objects(self):
        self.test_data, self.test_obj = self.helper.create_user_contact(
            self.card, self.card2
        )

    def compare_result(self, origin, result) -> bool:
        return (
            result["id"] == origin["target_card"].id
            and result["belong_to_cards"][0] == origin["own_card"].id
            and result["tiers"][0] == origin["tier_number"]
        )

    def test_retrieve(self):
        assert (
            self.run_func("retrieve", self.request_data, self.test_obj.card.id)
            == self.SUCCESS
        ), self.last_run_result()

        result = self.last_result_data()
        assert isinstance(result, dict) and self.compare_result(
            self.test_data, result
        ), self.debug_data()

    def test_list(self):
        self.default_test_list()

    def test_notes(self):
        note_data = {"name": "note test", "content": "Test content"}
        data = TestRequestData(note_data, self.user, method="POST")
        assert (
            self.run_func("notes", data, self.card2.id) == self.SUCCESS
        ), self.last_run_result()

        result1 = self.last_result_data()
        assert (
            isinstance(result1, dict)
            and result1["name"] == note_data["name"]
            and result1["content"] == note_data["content"]
        ), f"{note_data} \n!= {result1}"

        data = TestRequestData({}, self.user)
        assert (
            self.run_func("notes", data, self.card2.id) == self.SUCCESS
        ), self.last_run_result()

        result2 = self.last_result_data()
        assert (
            isinstance(result2, list)
            and len(result2) == 1
            and result2[0]["name"] == note_data["name"]
            and result2[0]["content"] == note_data["content"]
        ), f"{note_data} \n!= {result2}"

    def test_tasks(self):
        task_data = {"name": "task test", "content": "Test content"}
        data = TestRequestData(task_data, self.user, method="POST")
        assert (
            self.run_func("tasks", data, self.card2.id) == self.SUCCESS
        ), self.last_run_result()

        result1 = self.last_result_data()
        assert (
            isinstance(result1, dict)
            and result1["name"] == task_data["name"]
            and result1["content"] == task_data["content"]
        ), f"{task_data} \n!= {result1}"

        data = TestRequestData({}, self.user)
        assert (
            self.run_func("tasks", data, self.card2.id) == self.SUCCESS
        ), self.last_run_result()

        result2 = self.last_result_data()
        assert (
            isinstance(result2, list)
            and len(result2) == 1
            and result2[0]["name"] == task_data["name"]
            and result2[0]["content"] == task_data["content"]
        ), f"{task_data} \n!= {result2}"


class ContactPersonalToScanTC(ContactPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_p(self.suser)
        _, self.card2 = self.create_card_s(self.suser2)


class ContactPersonalToBusinessTC(ContactPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_p(self.suser)
        _, self.card2 = self.create_card_b(self.suser2)


class ContactScanToPersonalTC(ContactPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_s(self.suser)
        _, self.card2 = self.create_card_p(self.suser2)


class ContactScanToScanTC(ContactPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_s(self.suser)
        _, self.card2 = self.create_card_s(self.suser2)


class ContactScanToBusinessTC(ContactPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_s(self.suser)
        _, self.card2 = self.create_card_b(self.suser2)


class ContactBusinessToPersonalTC(ContactPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_b(self.suser)
        _, self.card2 = self.create_card_p(self.suser2)


class ContactBusinessToScanTC(ContactPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_b(self.suser)
        _, self.card2 = self.create_card_s(self.suser2)


class ContactBusinessToBusinessTC(ContactPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_b(self.suser)
        _, self.card2 = self.create_card_b(self.suser2, company_name="Shake")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~ REQUEST ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class ContactRequestPersonalToPersonalTC(BaseTestCase, FullCardBaseLogic):
    ViewSet = SContactRequestsViewSet

    def create_general_objects(self):
        _, self.media = self.helper.create_media()
        _, self.media2 = self.helper.create_media(filename="test_2")

        _, self.suser2 = self.helper.create_user("user2@yopmail.com")
        _, self.card_22 = self.create_card_s(self.suser2)
        _, self.card_23 = self.create_card_s(self.suser2)

    def create_specific_objects(self):
        _, self.card = self.create_card_p(self.suser)
        _, self.card_21 = self.create_card_p(self.suser2)

    def create_test_objects(self):
        self.test_data_1, self.test_obj_1 = self.helper.create_contact_request(
            self.card_21, self.card, tier_number=2, message="Hi honey!"
        )
        self.test_data_2, self.test_obj_2 = self.helper.create_contact_request(
            self.card_22, self.card, tier_number=2, message="Hi honey!"
        )
        self.test_data_3, self.test_obj_3 = self.helper.create_contact_request(
            self.card_23, self.card, tier_number=2, message="Hi honey!"
        )
        self.test_data, self.test_obj = self.test_data_1, self.test_obj_1

    def create_related_objects(self):
        _, self.contact = self.helper.create_user_contact(self.card_22, self.card)
        _, self.contact_request = self.helper.create_contact_request(
            self.card, self.card_23
        )

    def validate_result(self, origin, result) -> List[bool]:
        return [
            result["from_card"]["id"] == origin["from_card"].id,
            result["to_card"]["id"] == origin["to_card"].id,
            result["user"] == origin["user"].id,
            result["tier_number"] == origin["tier_number"],
            result["message"] == origin["message"],
        ]

    def test_list(self):
        self.default_test_list(list_length=3)

    def test_retrieve(self):
        self.default_test_retrieve()

    def test_create(self):
        data = {
            "from_card_id": self.card.id,
            "to_card_id": [self.card_21.id],
            "tier_number": 3,
            "message": "Hi honey",
        }
        new_test_data = {
            "from_card": self.card,
            "to_card": self.card_21,
            "user": self.card_21.user,
            "tier_number": 3,
            "message": "Hi honey",
        }
        self.default_test_create_list(
            TestRequestData(data, self.user, method="POST"), new_test_data=new_test_data
        )

    def test_destroy(self):
        self.default_test_destroy()

    def test_confirm(self):
        self.test_data, self.test_obj = self.test_data_1, self.test_obj_1
        assert (
            self.run_func(
                "confirm",
                TestRequestData({}, self.user, method="POST"),
                self.test_obj.id,
            )
            == self.SUCCESS
        ), self.last_run_result()

        result = self.last_result_data()
        assert isinstance(result, dict), self.debug_data("Response must be a dict")
        self.results = [
            result["contact"]["from_card"]["user_name"] == self.card.user_name,
            result["contact"]["to_card"]["user_name"]
            == self.test_obj.from_card.user_name,
            result["card"]["id"] == self.test_obj.from_card.id,
        ]
        assert all(self.results), self.debug_data()
        assert self.compare_dict(
            {"send_card_back": True}, self.last_result_data()
        ), self.debug_data("This contact should send back card")

    def test_confirm_with_exists_contact(self):
        self.test_data, self.test_obj = self.test_data_2, self.test_obj_2
        assert (
            self.run_func(
                "confirm",
                TestRequestData({}, self.user, method="POST"),
                self.test_obj.id,
            )
            == self.SUCCESS
        ), self.last_run_result()

        result = self.last_result_data()
        assert isinstance(result, dict), self.debug_data("Response must be a dict")
        self.results = [
            result["contact"]["from_card"]["user_name"] == self.card.user_name,
            result["contact"]["to_card"]["user_name"]
            == self.test_obj.from_card.user_name,
            result["card"]["id"] == self.test_obj.from_card.id,
        ]
        assert all(self.results), self.debug_data()
        assert self.compare_dict(
            {"send_card_back": False}, self.last_result_data()
        ), self.debug_data("This contact should not send back card")

    def test_confirm_with_exists_contact_request(self):
        self.test_data, self.test_obj = self.test_data_3, self.test_obj_3
        assert (
            self.run_func(
                "confirm",
                TestRequestData({}, self.user, method="POST"),
                self.test_obj.id,
            )
            == self.SUCCESS
        ), self.last_run_result()

        result = self.last_result_data()
        assert isinstance(result, dict), self.debug_data("Response must be a dict")
        self.results = [
            result["contact"]["from_card"]["user_name"] == self.card.user_name,
            result["contact"]["to_card"]["user_name"]
            == self.test_obj.from_card.user_name,
            result["card"]["id"] == self.test_obj.from_card.id,
        ]
        assert all(self.results), self.debug_data()
        assert self.compare_dict(
            {"send_card_back": False}, self.last_result_data()
        ), self.debug_data("This contact should not send back card")


class ContactRequestPersonalToScanTC(ContactRequestPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_p(self.suser)
        _, self.card_21 = self.create_card_s(self.suser2)


class ContactRequestPersonalToBusinessTC(ContactRequestPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_p(self.suser)
        _, self.card_21 = self.create_card_b(self.suser2)
        _, self.card_22 = self.create_card_b(self.suser2)
        _, self.card_23 = self.create_card_b(self.suser2)

    def create_related_objects(self):
        super().create_related_objects(self)
        _, self.company_contact = self.helper.create_company_contact(
            self.card_22, self.card
        )


class ContactRequestScanToPersonalTC(ContactRequestPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_s(self.suser)
        _, self.card_21 = self.create_card_p(self.suser2)


class ContactRequestScanToScanTC(ContactRequestPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_s(self.suser)
        _, self.card_21 = self.create_card_s(self.suser2)


class ContactRequestScanToBusinessTC(ContactRequestPersonalToBusinessTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_s(self.suser)
        _, self.card_21 = self.create_card_b(self.suser2)
        _, self.card_22 = self.create_card_b(self.suser2)
        _, self.card_23 = self.create_card_b(self.suser2)


class ContactRequestBusinessToPersonalTC(ContactRequestPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_b(self.suser)
        _, self.card_21 = self.create_card_p(self.suser2)


class ContactRequestBusinessToScanTC(ContactRequestPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_b(self.suser)
        _, self.card_21 = self.create_card_s(self.suser2)


class ContactRequestBusinessToBusinessTC(ContactRequestPersonalToBusinessTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_b(self.suser)
        _, self.card_21 = self.create_card_b(self.suser2, company_name="Shake")
        _, self.card_22 = self.create_card_b(self.suser2)
        _, self.card_23 = self.create_card_b(self.suser2)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~ SHARE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class ShareContactPersonalToPersonalTC(BaseTestCase, FullCardBaseLogic):
    ViewSet = SShareContactRequestsViewSet

    def create_general_objects(self):
        _, self.suser2 = self.helper.create_user("user2@yopmail.com")
        _, self.suser3 = self.helper.create_user("user3@yopmail.com")

    def create_specific_objects(self):
        _, self.card = self.create_card_p(self.suser)
        _, self.card2 = self.create_card_p(self.suser2)
        _, self.card3 = self.create_card_p(self.suser3)

    def create_test_objects(self):
        _, self.contact21 = self.helper.create_user_contact(self.card, self.card2)
        _, self.contact31 = self.helper.create_user_contact(self.card, self.card3)
        self.test_data, self.test_obj = self.helper.create_share_contact_request(
            self.card2, self.card, self.card3, message="Hi baby!"
        )

    def validate_result(self, origin, result) -> List[bool]:
        return [
            result["by_card"]["id"] == origin["by_card"].id,
            result["from_card"]["id"] == origin["from_card"].id,
            result["to_card"]["id"] == origin["to_card"].id,
            result["message"] == origin["message"],
        ]

    def test_list(self):
        self.default_test_list()

    def test_retrieve(self):
        self.default_test_retrieve()

    def test_create(self):
        data = {
            "from_card": self.card2.id,
            "to_card": [self.card3.id],
            "message": "Hi new baby",
        }
        new_test_data = {
            "by_card": self.card,
            "from_card": self.card2,
            "to_card": self.card3,
            "message": "Hi new baby",
        }
        self.default_test_create_list(
            TestRequestData(data, self.user, method="POST"), new_test_data=new_test_data
        )

    def test_destroy(self):
        self.default_test_destroy()

    def test_confirm(self):
        assert (
            self.run_func(
                "confirm",
                TestRequestData({}, self.user, method="POST"),
                self.test_obj.id,
            )
            == self.SUCCESS
        ), self.last_run_result()

        result = self.last_result_data()

        assert isinstance(result, dict), self.debug_data("Response must be a dict")
        assert self.validate(
            result["from_card"]["id"] == self.card.id,
            result["to_card"]["id"] == self.card3.id,
        ), self.debug_data()


class ShareContactPersonalToScanTC(ShareContactPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_s(self.suser)
        _, self.card2 = self.create_card_p(self.suser2)
        _, self.card3 = self.create_card_s(self.suser3)


class ShareContactPersonalToBusinessTC(ShareContactPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_b(self.suser)
        _, self.card2 = self.create_card_p(self.suser2)
        _, self.card3 = self.create_card_b(self.suser3)


class ShareContactScanToPersonalTC(ShareContactPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_b(self.suser)
        _, self.card2 = self.create_card_s(self.suser2)
        _, self.card3 = self.create_card_p(self.suser3)


class ShareContactScanToScanTC(ShareContactPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_s(self.suser)
        _, self.card2 = self.create_card_s(self.suser2)
        _, self.card3 = self.create_card_s(self.suser3)


class ShareContactScanToBusinessTC(ShareContactPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_p(self.suser)
        _, self.card2 = self.create_card_s(self.suser2)
        _, self.card3 = self.create_card_b(self.suser3)


class ShareContactBusinessToPersonalTC(ShareContactPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_s(self.suser)
        _, self.card2 = self.create_card_b(self.suser2)
        _, self.card3 = self.create_card_p(self.suser3)


class ShareContactBusinessToScanTC(ShareContactPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_p(self.suser)
        _, self.card2 = self.create_card_b(self.suser2)
        _, self.card3 = self.create_card_s(self.suser3)


class ShareContactBusinessToBusinessTC1(ShareContactPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_b(self.suser)
        _, self.card2 = self.create_card_b(self.suser2)
        _, self.card3 = self.create_card_b(self.suser3, company_name="Shake")


class ShareContactBusinessToBusinessTC2(ShareContactPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_b(self.suser)
        _, self.card2 = self.create_card_b(self.suser2, company_name="Glomp")
        _, self.card3 = self.create_card_b(self.suser3)


class ShareContactBusinessToBusinessTC3(ShareContactPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_b(self.suser)
        _, self.card2 = self.create_card_b(self.suser2, company_name="Glomp")
        _, self.card3 = self.create_card_b(self.suser3, company_name="Shake")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~ Note ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class NotePersonalToPersonalTC(BaseTestCase, FullCardBaseLogic):
    ViewSet = SContactNotesViewSet

    def create_general_objects(self):
        _, self.suser2 = self.helper.create_user("user2@yopmail.com")

    def create_specific_objects(self):
        _, self.card = self.create_card_p(self.suser)
        _, self.card2 = self.create_card_p(self.suser2)

    def create_test_objects(self):
        _, self.contact = self.helper.create_user_contact(self.card, self.card2)
        self.test_data, self.test_obj = self.helper.create_note(
            self.suser, self.contact, "This is a test"
        )

    def validate_result(self, origin, result) -> bool:
        return [
            result["user"] == origin["user"].id,
            result["contact"]["id"] == origin["contact"].id,
            result["contact"]["card"] == self.card2.id,
            result["contact"]["user_card"] == self.card.id,
            result["name"] == origin.get("name", ""),
            result["content"] == origin["content"],
        ]

    def test_list(self):
        self.default_test_list()

    def test_retrieve(self):
        self.default_test_retrieve()

    def test_create(self):
        self.default_test_create(
            TestRequestData(
                {
                    "from_card": self.card.id,
                    "to_card": self.card2.id,
                    "content": "This is a new test",
                },
                self.user,
                method="POST",
            ),
            {"content": "This is a new test"},
        )

    def test_update(self):
        self.default_test_update(
            TestRequestData(
                {"content": "This is a updated test"},
                self.user,
                method="POST",
            ),
            {"content": "This is a updated test"},
        )

    def test_destroy(self):
        self.default_test_destroy()


class NotePersonalToScanTC(NotePersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_p(self.suser)
        _, self.card2 = self.create_card_s(self.suser2)


class NotePersonalToBusinessTC(NotePersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_p(self.suser)
        _, self.card2 = self.create_card_b(self.suser2)


class NoteScanToPersonalTC(NotePersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_s(self.suser)
        _, self.card2 = self.create_card_p(self.suser2)


class NoteScanToScanTC(NotePersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_s(self.suser)
        _, self.card2 = self.create_card_s(self.suser2)


class NoteScanToBusinessTC(NotePersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_s(self.suser)
        _, self.card2 = self.create_card_b(self.suser2)


class NoteBusinessToPersonalTC(NotePersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_b(self.suser)
        _, self.card2 = self.create_card_p(self.suser2)


class NoteBusinessToScanTC(NotePersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_b(self.suser)
        _, self.card2 = self.create_card_s(self.suser2)


class NoteBusinessToBusinessTC(NotePersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_b(self.suser)
        _, self.card2 = self.create_card_b(self.suser2, company_name="Shake")


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~ TASK ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class TaskPersonalToPersonalTC(BaseTestCase, FullCardBaseLogic):
    ViewSet = SContactTasksViewSet

    def create_general_objects(self):
        _, self.suser2 = self.helper.create_user("user2@yopmail.com")

    def create_specific_objects(self):
        _, self.card = self.create_card_p(self.suser)
        _, self.card2 = self.create_card_p(self.suser2)

    def create_test_objects(self):
        _, self.contact = self.helper.create_user_contact(self.card, self.card2)
        self.test_data, self.test_obj = self.helper.create_task(
            self.suser,
            self.contact,
            "This is a test",
            start_time=1614915.393,
            end_time=1614926.393,
            remind=False,
        )

    def validate_result(self, origin, result) -> bool:
        return [
            result["user"] == origin["user"].id,
            result["contact"]["id"] == origin["contact"].id,
            result["contact"]["card"] == self.card2.id,
            result["contact"]["user_card"] == self.card.id,
            result["name"] == origin.get("name", ""),
            result["content"] == origin["content"],
            result["start_time"] == origin["start_time"],
            result["end_time"] == origin["end_time"],
            result["remind"] == origin["remind"],
        ]

    def test_list(self):
        self.default_test_list()

    def test_retrieve(self):
        self.default_test_retrieve()

    def test_create(self):
        self.default_test_create(
            TestRequestData(
                {
                    "from_card": self.card.id,
                    "to_card": self.card2.id,
                    "content": "This is a new test",
                    "start_time": 2614915.393,
                    "end_time": 2614926.393,
                    "remind": False,
                },
                self.user,
                method="POST",
            ),
            {
                "content": "This is a new test",
                "start_time": 2614915.393,
                "end_time": 2614926.393,
            },
        )

    def test_update(self):
        self.default_test_update(
            TestRequestData(
                {
                    "content": "This is a updated test",
                    "start_time": 1514915.393,
                    "end_time": 1514926.393,
                    "remind": True,
                },
                self.user,
                method="POST",
            ),
            {
                "content": "This is a updated test",
                "start_time": 1514915.393,
                "end_time": 1514926.393,
                "remind": True,
            },
        )

    def test_destroy(self):
        self.default_test_destroy()


class TaskPersonalToScanTC(TaskPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_p(self.suser)
        _, self.card2 = self.create_card_s(self.suser2)


class TaskPersonalToBusinessTC(TaskPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_p(self.suser)
        _, self.card2 = self.create_card_b(self.suser2)


class TaskScanToPersonalTC(TaskPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_s(self.suser)
        _, self.card2 = self.create_card_p(self.suser2)


class TaskScanToScanTC(TaskPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_s(self.suser)
        _, self.card2 = self.create_card_s(self.suser2)


class TaskScanToBusinessTC(TaskPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_s(self.suser)
        _, self.card2 = self.create_card_b(self.suser2)


class TaskBusinessToPersonalTC(TaskPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_b(self.suser)
        _, self.card2 = self.create_card_p(self.suser2)


class TaskBusinessToScanTC(TaskPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_b(self.suser)
        _, self.card2 = self.create_card_s(self.suser2)


class TaskBusinessToBusinessTC(TaskPersonalToPersonalTC):
    def create_specific_objects(self):
        _, self.card = self.create_card_b(self.suser)
        _, self.card2 = self.create_card_b(self.suser2, company_name="Shake")
