from helper.exceptions import BadRequestException
from ...models import SCards
from ...serializers import SPublicCardSerializer


def check_card(
    card: SCards,
    support_type: list = {
        SCards.IDENTITY,
        SCards.PERSONAL,
        SCards.BUSINESS,
        SCards.SCAN,
        SCards.PRINTED,
        SCards.TEMPLATE,
    },
    errcode=400030,
    debug="",
):
    if card.card_type not in support_type:
        raise BadRequestException(
            errcode=errcode,
            message=f"We don't support this card type yet! [{card.card_type}]",
            data={
                "debug": debug,
                "card": SPublicCardSerializer(card),
            },
        )
