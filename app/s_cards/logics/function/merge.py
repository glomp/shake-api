from django.db import transaction

from s_balances.logics.function import move_personal_balance
from s_contacts.logics.function import (
    copy_contact,
    merge_contact,
    merge_contact_request,
    merge_contact_share,
)
from s_user.models import SUsers
from ...models import SCards
from .check import check_card
from .delete import delete_card


@transaction.atomic
def merge_card(
    user: SUsers, from_card: SCards, to_card: SCards, keep_card=False, hard_delete=False
):
    from_card.validate()
    to_card.validate()

    check_card(card=from_card, support_type={SCards.SCAN}, errcode=400091)

    if keep_card is False:
        # contact
        if to_card.card_type == SCards.BUSINESS:
            copy_contact(user, from_card=from_card, to_card=to_card)
            merge_contact(user, from_card=from_card, to_card=user.icard)
        else:
            merge_contact(user, from_card=from_card, to_card=to_card)

        merge_contact_request(user, from_card=from_card, to_card=to_card)
        merge_contact_share(user, from_card=from_card, to_card=to_card)

        # balance
        # must move balance after move contact request
        move_personal_balance(user, from_card=from_card, to_card=to_card)

        delete_card(card=from_card, hard=hard_delete)
    else:
        copy_contact(user, from_card=from_card, to_card=to_card)
