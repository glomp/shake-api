from typing import Tuple

from django.db import transaction
from django.db.models.query_utils import Q

from s_companies.models import SCompanyUsers
from s_contacts.models import SContactRequests, SContacts, SShareContactRequests
from s_medias.logics.media import LMedia
from s_notifications.models import SNotifications
from s_properties.logics.property import CardProperty
from s_properties.models import SProperties
from ...models import SCards, SCompanyInvitations, SPotentialUserInformation


@transaction.atomic
def delete_card(card: SCards, hard=False, unlink=False) -> Tuple[int, dict]:
    def _hard_delete(card: SCards):
        def _unlink_with_system(card: SCards):
            SPotentialUserInformation.objects.filter(card_id=card.id).update(
                card_id=None
            )

        def _delete_other_relation(card: SCards):
            def _delete_contact(card: SCards):
                def _delete_each_contact(contact: SContacts):
                    contact.scontactnotes_set.all().delete()
                    contact.scontacttasks_set.all().delete()
                    contact.delete()

                for contact in SContacts.objects.filter(
                    Q(Q(card_id=card.id) | Q(user_card_id=card.id))
                ).all():
                    _delete_each_contact(contact)

            def _delete_contact_request(card: SCards):
                def _delete_each_contact_request(contact_request: SContactRequests):
                    contact_request.cancel() and contact_request.refund()
                    contact_request.delete()

                for contact_request in SContactRequests.objects.filter(
                    Q(Q(from_card_id=card.id) | Q(to_card_id=card.id))
                ).all():
                    _delete_each_contact_request(contact_request)

            def _delete_share_contact(card: SCards):
                SShareContactRequests.objects.filter(
                    Q(Q(from_card_id=card.id) | Q(to_card_id=card.id))
                    | Q(by_card_id=card.id)
                ).delete()

            def _delete_notification(card: SCards):
                SNotifications.objects.filter(
                    Q(Q(by_card_id=card.id) | Q(target_card_id=card.id))
                ).delete()

            def _delete_invitation(card: SCards):
                SCompanyInvitations.objects.filter(card_id=card.id).delete()

            _delete_contact(card)
            _delete_contact_request(card)
            _delete_share_contact(card)
            _delete_notification(card)
            _delete_invitation(card)

        def _delete_properties(card: SCards):
            def _delete_property(_property: SProperties):
                if _property.media is not None:
                    LMedia(card.user, card.user.view, _property.media).delete()

            properties = card.sproperties_set.all()
            [_delete_property(_property) for _property in properties]
            properties.delete()

        _unlink_with_system(card)
        _delete_other_relation(card)
        _delete_properties(card)
        return card.delete()

    def _soft_delete(card: SCards):
        def _disable_with_employee_in_company(card: SCards):
            if card.card_type != SCards.BUSINESS:
                return

            company_user: SCompanyUsers = card.company_user
            if company_user:
                company_user.status = SCompanyUsers.INACTIVE
                company_user.save(update_fields=["status"])

        def _disable_other_relation(card: SCards):
            def _disbale_contact_request(contact_request: SContactRequests):
                contact_request.cancel() and contact_request.refund()

            SNotifications.objects.filter(
                Q(Q(by_card_id=card.id) | Q(target_card_id=card.id))
            ).update(status=SNotifications.DESTROYED)

            SShareContactRequests.objects.filter(
                Q(Q(from_card_id=card.id) | Q(to_card_id=card.id))
            ).update(status=SShareContactRequests.DESTROYED)
            SContacts.objects.filter(
                Q(Q(card_id=card.id) | Q(user_card_id=card.id))
            ).update(status=SContacts.DESTROYED)

            for contact_request in SContactRequests.objects.filter(
                Q(Q(from_card_id=card.id) | Q(to_card_id=card.id))
            ).all():
                _disbale_contact_request(contact_request)

        def _disable_propeties(card: SCards):
            properties = CardProperty(card)
            properties.base_attributes.update(searchable=False)
            properties.synchronize(
                properties.get_properties(), need_check=False, force_edit=True
            )

        def _delete(card: SCards):
            card.status = (
                SCards.UNAVAILABLE
                if card.card_type == SCards.BUSINESS
                else SCards.DESTROYED
            )
            card.searchable = False
            card.save(update_fields=["status", "searchable"])
            return 1, None

        _disable_other_relation(card)
        _disable_propeties(card)
        _disable_with_employee_in_company(card)
        return _delete(card)

    return _hard_delete(card) if hard is True else _soft_delete(card)
