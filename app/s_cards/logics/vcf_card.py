import vobject
from typing import List

from helper.common import files
from s_cards.models import SCards
from s_properties.models import SProperties


class VCFCard:
    PROPERTY_TO_KEY = {
        "name": "fn",
        "email": "email",
        "work email": "email",
        "mobile": "tel",
        "work mobile": "tel",
        "company name": "org",
        "job title": "title",
        "website": "url",
        "address": "adr",
    }

    @classmethod
    def create(cls, card: SCards, tier: int) -> vobject.vcard.VCard3_0:
        vcard = vobject.vCard()
        properties: List[SProperties] = card.get_properties_from_tier(tier)
        for property in properties:
            if property.label in cls.PROPERTY_TO_KEY:
                key = cls.PROPERTY_TO_KEY[property.label]
                pcard = vcard.add(key)
                pcard.value = property.value

                if property.label == "name":
                    name = property.value
                    split_name = name.find(" ")
                    split_name = len(name) if split_name == -1 else split_name
                    first_name = name[:split_name]
                    last_name = name[split_name + 1 :]
                    vcard.add("n").value = vobject.vcard.Name(
                        family=last_name, given=first_name
                    )
                elif property.label == "company name":
                    pcard.value = [f"{property.value} Company"]
                elif property.label == "address":
                    pcard.value = vobject.vcard.Address(street=property.value)
                    pcard.type_param = (
                        "home" if card.card_type == SCards.PERSONAL else "work"
                    )
                elif property.label in {"email", "mobile"}:
                    pcard.type_param = "home"
                elif property.label in {"work email", "work mobile"}:
                    pcard.type_param = "work"
        return vcard

    @staticmethod
    def _save(vcard: vobject.vcard.VCard3_0, filename) -> str:
        """save_card

        Args:
            vcard (vobject.vcard.VCard3_0): Card
            filename ([type]): file name

        Returns:
            str: file path
        """
        return files.save(vcard.serialize(), filename, write_type="w+")

    @classmethod
    def create_and_save(cls, card: SCards, tier: int) -> str:
        vcard = cls.create(card, tier)
        return cls._save(vcard, f"{card.user_name}.vcf")
