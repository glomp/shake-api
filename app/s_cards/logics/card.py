from time import time
from typing import List, Tuple, Union

from django.conf import settings
from django.db import transaction

from helper.common.string import serialize_name
from helper.exceptions import BadRequestException
from s_balances.logics.function import move_personal_balance
from s_cards.logics.function.check import check_card
from s_companies.models import SCompanies
from s_contacts.logics.contact import CompanyContact, EmployeeContact, UserContact
from s_contacts.models import SContacts
from s_notifications.notification import CardNotification
from s_notifications.self_notification import CardSelfNotification
from s_properties.bases import PropertyTypes
from s_properties.logics import CardProperty, TemplateCardProperty
from s_properties.serializers import SThoroughPropertySerializer
from s_templates.serializers import STemplatesSerializer
from s_user.models import SUsers
from v_user.view import VUser
from ..models import SCards, SCompanyInvitations
from ..serializers import SPublicCardSerializer, SThoroughCardSerializer
from .function import delete_card


class Card:
    DEFAULT_SHOW_KEY: list = []
    REQUIRED_KEY: list = []
    DEFAULT_NEVER_SHOW_KEY: set = set()
    DEFAULT_CHECK: set = None

    TIER_KEY = {"tier_1_view_key", "tier_2_view_key", "tier_3_view_key"}
    EDITABLE_FIELD = {"shareability", "visibility"}
    INFO_KEY = TIER_KEY | {"name", "status", "company_user"}

    CARD_TYPE = None
    CARD_TITLE = "Card"

    WITHOUT_USER = False

    NOTIFY_WHEN_UPDATED = True
    NOTIFY_WHEN_DELETED = True

    def __init__(self, user: SUsers, card: SCards = None):
        self.user = user
        self.card = card

        self._objects: list = []
        self.properties = CardProperty(self.card)
        self.my = VUser(user)

        self._has_just_actived = False

    def is_exist(self) -> bool:
        return bool(self.get())

    def get(self):
        if not self._objects:
            self._objects = self.my.card.all()
        return self._objects

    @transaction.atomic
    def create(
        self,
        data: dict,
        properties: List[dict],
        allow_null: bool = False,
        raise_exception: bool = True,
        without_user: bool = None,
    ) -> SCards:
        data = data.copy()
        data.update(self.base_card_data)
        card_ser = SThoroughCardSerializer(data=data)
        card_ser.is_valid(raise_exception=True)

        properties = properties.copy()
        self.properties = CardProperty(None)
        self.properties.validate_many(
            properties, allow_null=allow_null, raise_exception=raise_exception
        )

        self.card = card_ser.save()
        self.properties.card = self.card
        self.properties.synchronize(properties, need_check=False)

        if not allow_null and raise_exception and self.card.status != SCards.ACTIVE:
            is_change = True
            self._active(without_user=without_user)
        self._after_create(without_user=without_user)
        self._save_card()
        return self.card

    @transaction.atomic
    def update(
        self,
        data: dict,
        properties: List[dict] = None,
        allow_null: bool = False,
        raise_exception: bool = True,
        without_user: bool = None,
        no_delete: bool = False,
        force_edit: bool = None,
        notify: bool = True,
        required_keys: list = None,
    ) -> SCards:
        data = self.standardize_data(data)

        is_change = False
        change_fields = set([])
        for key, value in data.items():
            if getattr(self.card, key) != value:
                is_change = True
                change_fields.add(key)
                setattr(self.card, key, value)

        if properties is not None:
            properties = properties.copy()
            properties = self.properties.add_require_info_into_existed_properties(
                properties
            )
            self.properties.validate_many(
                properties,
                allow_null=allow_null,
                raise_exception=raise_exception,
            )
            self.properties.synchronize(
                properties,
                need_check=False,
                no_delete=no_delete,
                force_edit=force_edit,
            )

            properties_ser = list(
                SThoroughPropertySerializer(self.properties.all, many=True).data
            )
            self.validate_require_properties(
                properties_ser,
                required_keys if required_keys is not None else self.REQUIRED_KEY,
            )

            if not allow_null and self.card.status == SCards.REQUIRE_INFO:
                is_change = True
                self._active(without_user=without_user)

        self._after_update(without_user=without_user)
        if is_change or self.properties.is_change:
            if (change_fields and self.TIER_KEY) or self.properties.is_change:
                notify and self._notify_card_is_updated()
            self._save_card()
        return self.card

    @transaction.atomic
    def delete(self, hard: bool = False, force_edit: bool = None) -> Tuple[int, dict]:
        def _notify_card_is_deleted(self: Card):
            all_friends = self._get_all_friends()
            card_data = SPublicCardSerializer(self.card).data
            CardNotification(
                card_data, target_card=self.card, target_id=self.card.id
            ).deleted_card.notify(
                *all_friends, card_name=self.card.card_name, by_user=self.user.name
            )

            if isinstance(self.user, SUsers):
                CardSelfNotification(
                    self.user, card_data, target_card=self.card, target_id=self.card.id
                ).deleted_card.notify(
                    card_name=self.card.card_name, by_user=self.user.name
                )

        self._before_delete()
        result = delete_card(self.card, hard=hard)
        self.user.finished_delete_card()
        self.NOTIFY_WHEN_DELETED and _notify_card_is_deleted(self)
        return result

    def _active(self, without_user=None):
        self._has_just_actived = True
        self.card.status = SCards.ACTIVE

    def _after_create(self, without_user=None):
        pass

    def _after_update(self, without_user=None):
        pass

    def _before_delete(self, without_user=None):
        pass

    def _save_card(self) -> SCards:
        self.card.user_name = serialize_name(self.properties.get("name")) or ""
        self.card.avatar = (
            self.properties.get("avatar") or self.properties.get("scan 1") or ""
        )
        self.card.job_title = self.properties.get("job title") or ""
        self.card.company_name = self.properties.get("company name") or ""
        self.card.save()
        return self.card

    def _should_notify(self):
        return settings.ENV == "TEST" or time() - self.card._updated > 3600

    def _get_all_friends(self):
        return self.my.friend.user.know_me.all(scontacts__card_id=self.card.id)

    def _notify_card_is_updated(self):
        if self._should_notify():
            all_friends = self._get_all_friends()
            card_data = SPublicCardSerializer(self.card).data
            CardNotification(
                card_data, target_card=self.card, target_id=self.card.id
            ).updated_card.by(self.card).notify(
                *all_friends, card_name=self.card.card_name
            )

            if isinstance(self.user, SUsers) and self.card.card_type != SCards.PRINTED:
                CardSelfNotification(
                    self.user, card_data, target_card=self.card, target_id=self.card.id
                ).updated_card.by(self.card).notify(card_name=self.card.card_name)

    @property
    def default_card_data(self) -> dict:
        return {
            "name": self.CARD_TITLE,
            **self._default_card_tier(),
        }

    @property
    def base_card_data(self) -> dict:
        return {
            "user": self.user.id,
            "card_type": self.CARD_TYPE,
            "searchable": True,
        }

    @classmethod
    def standardize_tier_key(cls, view_key) -> list:
        if cls.DEFAULT_CHECK is None:
            cls.DEFAULT_CHECK = set(cls.DEFAULT_SHOW_KEY) | cls.DEFAULT_NEVER_SHOW_KEY

        standardized_key = cls.DEFAULT_SHOW_KEY.copy()
        for key in view_key:
            if key not in cls.DEFAULT_CHECK:
                standardized_key.append(key)

        return standardized_key

    @classmethod
    def standardize_data(cls, data) -> dict:
        standardized_data = {}
        if not hasattr(cls, "_editable_fields"):
            cls._editable_fields = cls.EDITABLE_FIELD | cls.INFO_KEY
        for key in data:
            if key not in cls._editable_fields:
                continue
            standardized_data[key] = (
                cls.standardize_tier_key(data[key])
                if key in cls.TIER_KEY
                else data[key]
            )
        return standardized_data

    @classmethod
    def _default_card_tier(cls) -> dict:
        return {
            "tier_1_view_key": [],
            "tier_2_view_key": [],
            "tier_3_view_key": [],
        }

    @classmethod
    def validate_require_properties(cls, card_properties: list, view_key: list = None):
        if cls.DEFAULT_CHECK is None:
            cls.DEFAULT_CHECK = set(cls.DEFAULT_SHOW_KEY) | cls.DEFAULT_NEVER_SHOW_KEY
        view_key = set(view_key.copy()) if view_key is not None else cls.DEFAULT_CHECK

        card_property_keys = {
            card_property["label"] for card_property in card_properties
        }

        for key in view_key:
            if key not in card_property_keys:
                raise BadRequestException(
                    errcode=400034,
                    message=f"Card is missing '{key}' property",
                )


class UserCard(Card):
    def _before_delete(self):
        self._move_relation_to_identity()

    def _after_create(self, without_user=None):
        without_user = self.WITHOUT_USER if without_user is None else without_user
        if not without_user:
            self.card.user = self.user
            self.user.finished_create_card(self)
            if self.user.icard.balance_total_personal > 0:
                self._move_remain_balance_back()

    def _after_update(self, without_user=None):
        without_user = self.WITHOUT_USER if without_user is None else without_user
        if not without_user:
            self.card.user = self.user
            self.user.finished_create_card(self)

    def _move_remain_balance_back(self):
        move_personal_balance(self.user, self.user.icard, self.card)

    def _move_relation_to_identity(
        self, allowed_types={SCards.PERSONAL, SCards.SCAN, SCards.BUSINESS}
    ):
        if self.card.card_type not in allowed_types:
            BadRequestException(
                errcode=400086,
                err_message="This card does not support moving relationships",
            )
        icard: SCards = self.user.icard
        SContacts.objects.filter(user_card_id=self.card.id).update(
            user_card_id=icard.id
        )


class PersonalCard(UserCard):
    REQUIRED_KEY = ["name", "avatar"]
    DEFAULT_NEVER_SHOW_KEY = {
        "_created",
        "_updated",
        "balance_personal_credit",
        "balance_business_credit",
    }

    CARD_TYPE = SCards.PERSONAL
    CARD_TITLE = "Personal Card"

    def get(self):
        if not self._objects:
            self._objects = self.my.card.personal.get_if_exist()
            self.card = self._objects
        return self._objects

    @transaction.atomic
    def create(self, allow_null=False, raise_exception=True, **kwargs) -> SCards:
        properties = kwargs.pop("properties")
        if self.is_exist():
            raise BadRequestException(
                errcode=400025,
                message=f"You only have a maximum of one {self.CARD_TITLE}.",
            )

        data = self.default_card_data
        data.update(self.standardize_data(kwargs))

        self.validate_require_properties(properties, self.REQUIRED_KEY)
        return super(PersonalCard, self).create(
            data,
            properties,
            allow_null=allow_null,
            raise_exception=raise_exception,
        )


class IdentityCard(Card):
    CARD_TYPE = SCards.IDENTITY
    CARD_TITLE = "Identity Card"

    def get(self):
        if not self._objects:
            self._objects = self.my.card.identity.get_if_exist()
            self.card = self._objects
        return self._objects

    def _generate_card_data_from_card(self, card_logic: Card) -> list:
        return [
            {
                "property_type": PropertyTypes.NAME,
                "label": "name",
                "value": self.user.name,
            },
            {
                "property_type": PropertyTypes.EMAIL,
                "label": "email",
                "value": self.user.email,
            },
            CardProperty.to_dict(card_logic.properties.map["avatar"]),
        ]

    @transaction.atomic
    def create_from_other_card(
        self,
        from_card: Union[SCards, Card],
        allow_null=False,
        raise_exception=True,
        **kwargs,
    ) -> SCards:
        if isinstance(from_card, SCards):
            card_logic = Card(self.user, from_card)
        else:
            card_logic = from_card
        card_logic.properties.get_map()
        properties = kwargs.pop("properties", [])
        properties.extend(self._generate_card_data_from_card(card_logic=card_logic))
        return self.create(
            properties=properties,
            allow_null=allow_null,
            raise_exception=raise_exception,
            **kwargs,
        )

    @transaction.atomic
    def create(self, allow_null=False, raise_exception=True, **kwargs) -> SCards:
        properties = kwargs.pop("properties")
        if self.is_exist():
            raise BadRequestException(
                errcode=400025,
                message=f"You only have a maximum of one {self.CARD_TITLE}.",
            )

        data = self.default_card_data
        data.update(self.standardize_data(kwargs))

        self.validate_require_properties(properties, self.REQUIRED_KEY)
        return super(IdentityCard, self).create(
            data,
            properties,
            allow_null=allow_null,
            raise_exception=raise_exception,
        )

    @transaction.atomic
    def update(self, *args, **kwargs) -> SCards:
        result = super().update(*args, **kwargs)
        if self.card.user_name != self.user.name:
            self.user.name = self.card.user_name
        return result

    def _notify_card_is_updated(self):
        pass

    @property
    def base_card_data(self) -> dict:
        return {
            "user": self.user.id,
            "card_type": self.CARD_TYPE,
            "searchable": False,
            "shareable": False,
            "visibility": SCards.SYSTEM,
        }


class BusinessCard(UserCard):
    REQUIRED_KEY = ["name", "avatar", "job title", "company name"]
    REQUIRED_KEY_WHEN_CREATE = []
    DEFAULT_NEVER_SHOW_KEY = {
        "_created",
        "_updated",
        "balance_personal_credit",
        "balance_business_credit",
    }

    CARD_TYPE = SCards.BUSINESS
    CARD_TITLE = "Business Card"

    def get(self):
        if not self._objects:
            self._objects = self.my.card.business.all()
        return self._objects

    def _active(self, without_user=None):
        super()._active(without_user=without_user)

        self.properties.base_attributes.update(searchable=True)
        all_properties = [
            CardProperty.to_dict(_property) for _property in self.properties.all
        ]
        self.properties.synchronize(all_properties, need_check=True, force_edit=True)
        self.card.searchable = True

        if without_user is False:
            invitation: SCompanyInvitations = self.card.invitation
            invitation.access_code = None
            invitation.save(update_fields=["access_code"])

    @transaction.atomic
    def create(
        self,
        company: SCompanies,
        template_card: SCards,
        allow_null: bool = True,
        raise_exception: bool = True,
        without_user: bool = True,
        **kwargs,
    ) -> SCards:
        self.properties = CardProperty(self.card)
        properties = template_card.sproperties_set.all()
        properties = [CardProperty.to_dict(_property) for _property in properties]

        new_properties: list = kwargs.pop("properties", [])
        data = self.default_card_data
        data.update(self.standardize_data(kwargs))
        data.update(
            template=template_card.template_id,
            company=company.id,
            status=SCards.REQUIRE_INFO,
            searchable=False,
        )
        self.card = super(BusinessCard, self).create(
            data,
            properties,
            allow_null=True,
            raise_exception=False,
            without_user=True,
        )
        if new_properties:
            new_properties = self.properties.add_require_info_into_existed_properties(
                new_properties
            )
            self.card = super(BusinessCard, self).update(
                data,
                new_properties,
                allow_null=allow_null,
                raise_exception=raise_exception,
                without_user=without_user,
                no_delete=True,
                notify=False,
                required_keys=self.REQUIRED_KEY_WHEN_CREATE
                if without_user
                else self.REQUIRED_KEY,
            )
        return self.card

    @transaction.atomic
    def update(self, data, *args, **kwargs) -> SCards:
        data["user"] = self.user.id
        self.card = super().update(data, *args, **kwargs)
        if self.card.searchable == False and self.card.status == SCards.ACTIVE:
            self._active()
        return self.card

    @property
    def base_card_data(self) -> dict:
        return {
            "user": None,
            "card_type": self.CARD_TYPE,
            "searchable": False,
        }


class ScanCard(PersonalCard):
    REQUIRED_KEY = ["name", "avatar", "job title", "company name"]

    CARD_TYPE = SCards.SCAN
    CARD_TITLE = "Scan Card"

    def get(self):
        if not self._objects:
            self._objects = self.my.card.scan.all()
        return self._objects

    @transaction.atomic
    def create(self, allow_null=False, raise_exception=True, **kwargs) -> SCards:
        properties = kwargs.pop("properties")
        data = self.default_card_data
        data.update(self.standardize_data(kwargs))

        self.validate_require_properties(properties, self.REQUIRED_KEY)
        return super(PersonalCard, self).create(
            data, properties, allow_null, raise_exception
        )


class PrintedCard(Card):
    REQUIRED_KEY = ["name"]

    CARD_TYPE = SCards.PRINTED
    CARD_TITLE = "Printed Card"

    WITHOUT_USER = True
    DEFAULT_TIER = 3

    def __init__(self, user: SUsers, card: SCards = None):
        super().__init__(user, card=card)
        self.properties = CardProperty(self.card, dict(searchable=False))

    def get(self):
        raise NotImplementedError

    @transaction.atomic
    def create(self, allow_null=False, raise_exception=True, **kwargs) -> SCards:
        properties = kwargs.pop("properties")

        data = self.default_card_data
        data.update(self.standardize_data(kwargs))

        self.validate_require_properties(properties, self.REQUIRED_KEY)
        new_card = super(PrintedCard, self).create(
            data, properties, allow_null, raise_exception
        )
        user_card = self.my.card.get(id=kwargs["to_card"])
        if user_card.card_type == SCards.BUSINESS:  # save to business card
            CompanyContact.create_or_get(
                own_card=user_card,
                target_card=new_card,
                tier_number=self.DEFAULT_TIER,
            )
            EmployeeContact.create_or_get(
                own_card=user_card,
                target_card=new_card,
                tier_number=self.DEFAULT_TIER,
            )
        else:
            UserContact.create_or_get(
                own_card=user_card,
                target_card=new_card,
                tier_number=self.DEFAULT_TIER,
            )
        return new_card

    def _get_all_friends(self):
        return self.my.friend.user.know_me.all()

    def _notify_card_is_updated(self):
        if self._should_notify():
            all_friends = self._get_all_friends()
            card_data = SPublicCardSerializer(self.card).data
            CardNotification(
                card_data, target_card=self.card, target_id=self.card.id
            ).updated_card.by(self.card).notify(
                self.user, *all_friends, card_name=self.card.card_name
            )
            # CardSelfNotification(
            #     self.user, card_data, target_card=self.card, target_id=self.card.id
            # ).updated_card.by(self.card).notify(card_name=self.card.card_name)

    @property
    def base_card_data(self) -> dict:
        return {
            "user": None,
            "card_type": self.CARD_TYPE,
            "searchable": False,
        }


class TemplateCard(Card):
    REQUIRED_KEY = ["name"]

    CARD_TYPE = SCards.TEMPLATE
    CARD_TITLE = "Template Card"

    WITHOUT_USER = True

    def __init__(self, user: SUsers, company: SCompanies, card: SCards = None):
        super().__init__(user, card=card)
        self.properties = TemplateCardProperty(self.card, dict(searchable=False))
        self.company = company

    def get(self):
        raise NotImplementedError

    def _create_company_template(self):
        template_ser = STemplatesSerializer(data={"company": self.company.id})
        template_ser.is_valid(raise_exception=True)
        return template_ser.save()

    @transaction.atomic
    def create(
        self, allow_null=False, without_user=True, raise_exception=True, **kwargs
    ) -> SCards:
        properties = kwargs.pop("properties")
        template = self._create_company_template()

        data = self.default_card_data
        data.update(self.standardize_data(kwargs), template=template.id)
        self.validate_require_properties(properties, self.REQUIRED_KEY)
        card = super(TemplateCard, self).create(
            data,
            properties,
            allow_null=allow_null,
            raise_exception=raise_exception,
            without_user=True,
        )
        return card

    def update(self, *args, without_user=True, **kwargs) -> SCards:
        return super().update(*args, without_user=True, **kwargs)

    def _notify_card_is_updated(self):
        pass

    def _save_card(self) -> SCards:
        self.card.company_name = self.properties.get("company name") or ""
        self.card.save()
        return self.card

    @property
    def base_card_data(self) -> dict:
        return {
            "user": None,
            "card_type": self.CARD_TYPE,
            "status": SCards.REQUIRE_INFO,
            "searchable": False,
            "visibility": SCards.HIDDEN,
        }


class UserCard(Card):
    def __init__(self, s_user):
        super().__init__(s_user)

        self.personal = PersonalCard(self.user)
        self.business = BusinessCard(self.user)


def get_card_logic(
    user: SUsers,
    card: SCards,
    support_type={
        SCards.IDENTITY,
        SCards.PERSONAL,
        SCards.BUSINESS,
        SCards.SCAN,
        SCards.PRINTED,
        SCards.TEMPLATE,
    },
    **kwargs,
) -> Union[PersonalCard, BusinessCard, ScanCard, PrintedCard, TemplateCard]:
    check_card(card, support_type=support_type, errcode=400030)

    if card.card_type == card.IDENTITY:
        card = IdentityCard(user, card)
    elif card.card_type == card.PERSONAL:
        card = PersonalCard(user, card)
    elif card.card_type == SCards.SCAN:
        card = ScanCard(user, card)
    elif card.card_type == SCards.PRINTED:
        card = PrintedCard(user, card)
    elif card.card_type == SCards.BUSINESS:
        card = BusinessCard(user, card)
    elif card.card_type == SCards.TEMPLATE:
        card = TemplateCard(user, kwargs["company"], card)
    return card
