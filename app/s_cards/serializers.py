from dynamic_rest.fields import DynamicRelationField
from dynamic_rest.serializers import DynamicModelSerializer
from rest_framework import serializers

from s_companies.serializers import SCompanyUserSerializer
from s_properties.serializers import (
    SMiniPropertySerializer,
    SPublicThoroughPropertySerializer,
    SThoroughPropertySerializer,
)
from s_templates.serializers import STemplatesSerializer
from .models import SCards, SCompanyInvitations, SPotentialUserInformation


class SCompanyInvitationsSerializer(DynamicModelSerializer):
    class Meta:
        model = SCompanyInvitations
        fields = "__all__"


class SPublicCardSerializer(DynamicModelSerializer):
    class Meta:
        model = SCards
        exclude = SCards.PERSONAL_FIELDS


class SCardSerializer(DynamicModelSerializer):
    tier_1_view_key = serializers.ListField(child=serializers.CharField())
    tier_2_view_key = serializers.ListField(child=serializers.CharField())
    tier_3_view_key = serializers.ListField(child=serializers.CharField())

    class Meta:
        model = SCards
        fields = "__all__"
        read_only_fields = SCards.SYSTEM_FIELDS


class SPublicThoroughCardSerializer(DynamicModelSerializer):
    # user_id = serializers.PKOnlyObject(pk="user")
    properties = serializers.ListSerializer(
        source="public_properties",
        child=SPublicThoroughPropertySerializer(),
        read_only=True,
    )

    class Meta:
        model = SCards
        exclude = SCards.PERSONAL_FIELDS


class SMiniThoroughCardSerializer(SCardSerializer):
    # user_id = serializers.PKOnlyObject(pk="user")
    properties = serializers.ListSerializer(
        source="sproperties_set",
        child=SMiniPropertySerializer(),
        read_only=True,
    )

    class Meta:
        model = SCards
        exclude = SCards.PERSONAL_FIELDS


class SThoroughCardSerializer(SCardSerializer):
    properties = serializers.ListSerializer(
        source="sproperties_set",
        child=SThoroughPropertySerializer(),
        read_only=True,
    )
    company_user = DynamicRelationField(SCompanyUserSerializer, embed=True)
    template = DynamicRelationField(STemplatesSerializer, embed=True)

    class Meta:
        model = SCards
        fields = "__all__"
        read_only_fields = SCards.SYSTEM_FIELDS


class SThoroughCompanyUserCardSerializer(SThoroughCardSerializer):
    company_user = DynamicRelationField(SCompanyUserSerializer, embed=True)

    class Meta:
        model = SCards
        fields = "__all__"
        read_only_fields = SCards.SYSTEM_FIELDS


class SPotentialUserInformationSerializer(DynamicModelSerializer):
    class Meta:
        model = SPotentialUserInformation
        fields = "__all__"
