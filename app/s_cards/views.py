from datetime import datetime
from typing import Tuple

from django.db import transaction
from django.db.models import Count
from django.db.models.query_utils import Q
from django.http.response import Http404
from rest_framework.decorators import action

from custom_django.views.viewsets import SDynamicModelViewSet
from helper import slogger
from helper.common import Search, files
from helper.exceptions import BadRequestException, ForbiddenException
from s_balances.logics.balance_manager import BalanceManager
from s_balances.logics.function import move_personal_balance
from s_companies.models import SCompanyUsers
from s_companies.serializers import SCompanyUserSerializer
from s_contacts.models import SContactRequests, SContacts
from s_contacts.serializers import (
    SDetailed2ContactRequestsSerializer,
    SUserContactsSerializer,
)
from s_medias.logics.store import SStorage
from s_notifications.email import CardMailNotifier
from s_notifications.message.card import CardMessageNotifier
from s_notifications.notifier import MailNotifier, MessageNotifier
from s_properties.models import SProperties
from s_templates.models import STemplates
from s_user.logics.helper import Distance
from s_user.models import SUsers
from .logics import (
    BusinessCard,
    IdentityCard,
    PersonalCard,
    PrintedCard,
    ScanCard,
    VCFCard,
    get_card_logic,
)
from .logics.function import merge_card
from .models import SCards, SCompanyInvitations
from .serializers import (
    SCardSerializer,
    SPotentialUserInformationSerializer,
    SPublicCardSerializer,
    SPublicThoroughCardSerializer,
    SThoroughCardSerializer,
)


class SCardsViewSet(SDynamicModelViewSet):
    """Handles creating, creating and updating companies."""

    serializer_class = SCardSerializer
    model_class = SCards
    queryset = SCards.objects.filter(
        ~Q(status=SCards.DESTROYED),
        ~Q(card_type=SCards.TEMPLATE),
    ).all()

    _prefetch_related = (
        "sproperties_set",
        "sproperties_set__media",
    )
    _select_related = (
        "template",
        "company_user",
    )

    def _get_any_card(self, pk: int) -> Tuple[SCards, bool]:
        """Get a card from anyone

        Args:
            pk ([int]): card id

        Returns:
            Tuple[SCards, bool]: Card and the full-access right
        """
        instance: SCards = (
            self.queryset.prefetch_related(*self._prefetch_related)
            .select_related("user", *self._select_related)
            .get(id=pk)
        )

        if instance is None:
            return None, False

        if instance.user_id == self.user.id or (
            instance.card_type == SCards.PRINTED
            and self.view.contact.all(card_id=instance.id).exists()
        ):
            return instance, True
        elif instance.check_read_permission(self.user):
            return instance, False
        else:
            return None, False

    def retrieve(self, request, pk):
        instance, full_permission = self._get_any_card(pk)
        if full_permission:
            return SThoroughCardSerializer(instance)
        else:
            return instance and SPublicThoroughCardSerializer(instance)

    def list(self, request):
        cards = (
            self.view.card.all()
            .prefetch_related(*self._prefetch_related)
            .select_related(*self._select_related)
        )
        return SThoroughCardSerializer(cards, many=True)

    @transaction.atomic
    def create(self, request):
        card_type = request.data["card_type"]
        if card_type == SCards.PERSONAL:
            card = PersonalCard(self.user)
        elif card_type == SCards.SCAN:
            card = ScanCard(self.user)
        elif card_type == SCards.PRINTED:
            card = PrintedCard(self.user)
        else:
            raise BadRequestException(
                errcode=400022,
                message=(f"Not support create card type '{card_type}' yet!"),
            )
        card_obj = card.create(**request.data)
        return SThoroughCardSerializer(card_obj)

    @transaction.atomic
    def update(self, request, pk, partial=False):
        instance, full_permission = self._get_any_card(pk)
        if not instance or not full_permission:
            raise ForbiddenException(
                errcode=403032, message="You don't have permission to edit this card."
            )

        card_logic = get_card_logic(
            self.user,
            instance,
            support={
                SCards.IDENTITY,
                SCards.PERSONAL,
                SCards.BUSINESS,
                SCards.SCAN,
                SCards.PRINTED,
            },
        )

        data = request.data.copy()
        properties = data.pop("properties", None)
        card_logic.update(data, properties)
        return SThoroughCardSerializer(card_logic.card)

    @transaction.atomic
    def destroy(self, request, pk):
        instance = self.view.card.get(id=pk)
        card_logic = get_card_logic(
            self.user,
            instance,
            support={SCards.PERSONAL, SCards.BUSINESS, SCards.SCAN},
        )
        move_personal_balance(
            self.user,
            from_card=instance,
            to_card=request.data.get("receiving_card", None),
        )

        cnt, _ = card_logic.delete()
        return "SUCCESS" if cnt == 1 else None

    @action(methods=["get"], detail=False)
    def all(self, request):
        cards = (
            self.view.card.advanced.all()
            .prefetch_related(*self._prefetch_related)
            .select_related(*self._select_related)
        )
        return SThoroughCardSerializer(cards, many=True)

    @action(methods=["get", "patch"], detail=False)
    def identity(self, request):
        instance = self.view.card.identity.get()

        if request.method == "GET":
            return SThoroughCardSerializer(instance)
        card_logic = IdentityCard(self.user, instance)
        data = request.data.copy()
        properties = data.pop("properties", None)
        card_logic.update(data, properties)
        return SThoroughCardSerializer(card_logic.card)

    @transaction.atomic
    @action(detail=False, methods=["post"])
    def access(self, request):
        invitation: SCompanyInvitations = SCompanyInvitations.objects.get(
            access_code=request.data["access_code"], status=SCompanyInvitations.HOLD
        )

        if invitation._expiration < datetime.utcnow().timestamp():
            raise BadRequestException(
                errcode=400027,
                data=invitation.__dict__,
                message="Your invitation is expired!",
            )

        if self.user.email != invitation.email:
            s_other_user = SUsers.objects.get_if_exist(
                ~Q(status=SUsers.INACTIVE), user_auth__email=invitation.email
            )
            if s_other_user is not None:
                personal_card = PersonalCard(s_other_user)
                personal_card.is_exist()
                raise ForbiddenException(
                    errcode=403028,
                    data=personal_card.__dict__,
                    message="This invitation for an other account!",
                )

        card: SCards = SCards.objects.get(id=invitation.card_id)
        template = STemplates.objects.get(id=card.template_id)
        company_user: SCompanyUsers = card.company_user
        if company_user is not None:
            company_user.user_id = self.user.id
            company_user.save(update_fields=["user_id"])
        else:
            company_user_ser = SCompanyUserSerializer(
                data={
                    "user": self.user.id,
                    "company": template.company_id,
                    "card": card.id,
                    "status": SCompanyUsers.ACTIVE,
                }
            )
            company_user_ser.is_valid(raise_exception=True)
            company_user = company_user_ser.save()

        card.user = self.user
        card.company_user = company_user
        card.save(update_fields=["company_user_id", "user_id"])

        # We will remove access_code when user finish ACTIVE their card.
        invitation.status = SCompanyInvitations.ACCEPTED
        invitation.save(update_fields=["status"])
        return SThoroughCardSerializer(card)

    @action(methods=["post"], detail=False)
    @transaction.atomic
    def merge(self, request):
        from_card = self.view.card.get(id=request.data["from_card"])
        to_card = self.view.card.get(id=request.data["to_card"])
        keep_card = request.data["keep_card"]
        hard_delete = request.data["hard_delete"]

        merge_card(
            user=self.user,
            from_card=from_card,
            to_card=to_card,
            keep_card=keep_card,
            hard_delete=hard_delete,
        )
        return "SUCCESS"

    @action(methods=["get"], detail=False)
    def search(self, request):
        key = Search.serialize_key(self.params["key"])
        offset = self.params["offset"]
        limit = self.params["limit"]

        # filters = Q()
        # for k in key:
        #     filters |= Q(sproperties__value__icontains=k)

        cards = (
            self.model_class.objects.filter(
                ~Q(user_id=self.user.id),
                searchable=True,
                sproperties__searchable=True,
                sproperties__value__icontains=key,
            )
            .filter(
                Q(visibility=SCards.EVERYONE)
                | Q(
                    Q(visibility=SCards.CONTACT_OF_CONTACT),
                    Q(
                        user_id__in=self.view.friend.contact.of_contact.all().values(
                            "user_id"
                        )
                    ),
                )
            )
            .distinct()[offset : offset + limit]
        )
        return SPublicCardSerializer(cards, many=True)

    @action(methods=["get"], detail=False, url_path="search-advanced")
    def search_advanced(self, request):
        offset = self.params["offset"]
        limit = self.params["limit"]
        self.params.all()

        search_filters = []
        name = self.params["name"]
        company = self.params["company"]
        location = self.params["location"]
        job_title = self.params["job_title"]
        mobile = self.params["mobile"]
        email = self.params["email"]
        if name is not None:
            search_filters.append(Q(label="name", value__icontains=name))
        if company is not None:
            search_filters.append(Q(label="company name", value__icontains=company))
        if location is not None:
            search_filters.append(Q(label="location", value__icontains=location))
        if job_title is not None:
            search_filters.append(Q(label="job title", value__icontains=job_title))
        if mobile is not None:
            search_filters.append(Q(property_type=SProperties.MOBILE, value=mobile))
        if email is not None:
            search_filters.append(Q(property_type=SProperties.EMAIL, value=email))
        if len(search_filters) == 0:
            return BadRequestException(
                errcode=400060,
                message="Missing search filter",
                data=self.params.all(),
                err_message=f"Key in [name, company, location, address, mobile, email]",
            )
        aggregate_filter = search_filters[0]
        for search_filter in search_filters[1:]:
            aggregate_filter |= search_filter

        all_properties = (
            SProperties.objects.values("card_id")
            .filter(aggregate_filter)
            .annotate(cnt=Count("card_id"))
            .filter(cnt=len(search_filters))
            .values("card_id")
        )
        # print("all_properties", all_properties)
        cards = (
            SCards.objects.filter(id__in=all_properties)
            .filter(
                ~Q(user_id=self.user.id),
                searchable=True,
            )
            .filter(
                Q(visibility=SCards.EVERYONE)
                | Q(
                    Q(visibility=SCards.CONTACT_OF_CONTACT),
                    Q(
                        user_id__in=self.view.friend.contact.of_contact.all().values(
                            "user_id"
                        )
                    ),
                )
            )
            .distinct()[offset : offset + limit]
        )
        # print("cards", cards)
        return SPublicCardSerializer(cards, many=True)

    @action(methods=["get"], detail=False, url_path="search-nearby")
    def search_nearby(self, request):
        offset = self.params["offset"]
        limit = self.params["limit"]
        distance = self.params.get("distance", 100)

        if self.user.location_latitude is None:
            return []

        long = self.params.get("long", self.user.location_longitude)
        lat = self.params.get("lat", self.user.location_latitude)
        lat_range, lon_range = Distance.get_range_from_center(distance, lat, long)
        slogger.debug(
            "search near-by in [%r] around %r : %r ",
            distance,
            (lat, long),
            (lat_range, lon_range),
        )

        cards = (
            self.model_class.objects.filter(
                ~Q(user_id=self.user.id),
                searchable=True,
            )
            .filter(
                user__location_latitude__range=lat_range,
                user__location_longitude__range=lon_range,
            )
            .filter(
                ~Q(visibility=SCards.CONTACT_OF_CONTACT)
                | Q(
                    user_id__in=self.view.friend.contact.of_contact.all().values(
                        "user_id"
                    )
                )
            )
            .order_by("user_id")
            .all()[offset : offset + limit]
        )

        return SPublicCardSerializer(cards, many=True)

    @action(methods=["post"], detail=True, url_path="send-to-email")
    def send_to_email(self, request, pk, partial=False):
        target_email = request.data["user_email"]
        instance = self.view.card.get(id=pk)

        vcard = VCFCard.create(instance, request.data["tier"])
        data = {
            "card": instance.id,
            "user_name": request.data.get("user_name", None),
            "email": target_email,
            "mobile": request.data.get("mobile", None),
            "message": request.data.get("message", None),
        }
        potential_user_ser = SPotentialUserInformationSerializer(data=data)
        potential_user_ser.is_valid(raise_exception=True)
        potential_user_ser.save()

        cnt = (
            CardMailNotifier()
            .send_vcf.by(instance)
            .send_to(
                target_email,
                files=[
                    (f"{instance.user_name}.vcf", vcard.serialize(), "text/x-vcard")
                ],
            )
        )
        if cnt > 0:
            return {"data": potential_user_ser.data, "message": "SUCCESS"}

    @action(methods=["post"], detail=True, url_path="send-to-mobile")
    def send_to_mobile(self, request, pk, partial=False):
        mobile = request.data["mobile"]
        instance = self.view.card.get(id=pk)

        vcard_path = VCFCard.create_and_save(instance, request.data["tier"])
        data = {
            "card": instance.id,
            "user_name": request.data.get("user_name", None),
            "email": request.data.get("email", None),
            "mobile": mobile,
            "message": request.data.get("message", None),
        }
        potential_user_ser = SPotentialUserInformationSerializer(data=data)
        potential_user_ser.is_valid(raise_exception=True)
        potential_user_ser.save()

        media = SStorage().upload(vcard_path, mime="text/vcard", keep_name=True)
        cnt = CardMessageNotifier().send_vcf.send_to(
            mobile, inviter_name=self.user.name, link=media.url
        )
        files.delete(vcard_path)
        if cnt > 0:
            return {"data": potential_user_ser.data, "message": "SUCCESS"}

    @action(methods=["get"], detail=True)
    def relations(self, request, pk):
        contacts = self.view.friend.contact.all(user_card_id=pk).select_related("card")
        contact_requests = self.view.friend.contact_request.all(
            status=SContactRequests.HOLD, to_card__id=pk
        ).select_related("from_card")
        return {
            "contacts": SUserContactsSerializer(contacts, many=True),
            "contact_requests": SDetailed2ContactRequestsSerializer(
                contact_requests, many=True
            ),
        }

    @action(methods=["post"], detail=False, url_path="import-csv-file")
    @transaction.atomic
    def import_csv_file(self, request):
        import csv
        import os
        import random

        from django.conf import settings
        from django.contrib.auth.models import User

        from s_medias.logics.store import SUserStorage
        from s_properties.logics.property import Property, PropertyTypes

        data_maper = {
            "first_name": "First Name",
            "last_name": "Last Name",
            "job_title": "Job Title",
            "website": "website",
            "email": "Email",
            "company_name": "Company Name",
        }
        with open(
            os.path.join(
                settings.ROOT_DIR,
                "tmp",
                "data",
                "hubspot-crm-exports-all-contacts-2021-06-23-edited.csv",
            ),
            mode="r",
        ) as csv_file:
            csv_reader = csv.DictReader(csv_file)
            exist_emails = set(User.objects.values_list("email", flat=True))
            avatars = os.listdir(os.path.join(settings.ROOT_DIR, "tmp", "avatar"))
            avatar_properties = SProperties.objects.filter(
                comment__in=list(avatars)
            ).all()
            avatar_map = {_p.comment: _p for _p in avatar_properties}
            exist_locations = list(
                SUsers.objects.filter(~Q(location_latitude=None)).values_list(
                    "location_latitude", "location_longitude"
                )
            )
            last_location = None

            def generate_location():
                nonlocal exist_locations, last_location
                while True:
                    location = random.choice(exist_locations)
                    if last_location is None:
                        break
                    if Distance.check(200, *location, *last_location) is False:
                        break
                lat_len, lon_len = Distance.get_side_length(100, location[0])
                # lat, lon = Distance.get_range_from_center(100, *location)
                lat, lon = location
                new_location = (
                    lat + lat_len * random.randrange(-20, 20, 1) / 20,
                    lon + lon_len * random.randrange(-20, 20, 1) / 20,
                )
                exist_locations.append(new_location)
                return new_location

            cnt = 0
            for row in csv_reader:
                if row[data_maper["email"]] in exist_emails:
                    continue

                user_auth_data = {
                    "email": row[data_maper["email"]],
                    "username": row[data_maper["email"]],
                    "first_name": row[data_maper["first_name"]],
                    "last_name": row[data_maper["last_name"]],
                }
                user_auth: User = User.objects.create(**user_auth_data)
                user_auth.set_password("123456")

                location = generate_location()
                user_data = {
                    "user_auth": user_auth,
                    "status": SUsers.ACTIVE,
                    "location_latitude": location[0],
                    "location_longitude": location[1],
                }
                user: SUsers = SUsers.objects.create(**user_data)

                user_avatar = random.choice(avatars)
                if user_avatar not in avatar_map:
                    media = SUserStorage(user).upload(
                        os.path.join(settings.ROOT_DIR, "tmp", "avatar", user_avatar),
                        keep_name=True,
                    )
                    property_data = {
                        "property_type": PropertyTypes.MEDIA,
                        "label": "avatar",
                        "media": media,
                        "comment": user_avatar,
                    }
                    avatar_map[user_avatar] = SProperties.objects.create(
                        **property_data
                    )

                card_data = {
                    "properties": [
                        {
                            "property_type": PropertyTypes.NAME,
                            "label": "name",
                            "value": user.name,
                        },
                        {
                            "property_type": PropertyTypes.EMAIL,
                            "label": "email",
                            "value": row[data_maper["email"]],
                        },
                        {
                            "property_type": PropertyTypes.CONTENT,
                            "label": "job title",
                            "value": row[data_maper["job_title"]],
                        },
                        {
                            "property_type": PropertyTypes.NAME,
                            "label": "company name",
                            "value": row[data_maper["company_name"]],
                        },
                        {
                            "property_type": PropertyTypes.URL,
                            "label": "website",
                            "value": row[data_maper["website"]],
                        },
                        Property.to_dict(avatar_map[user_avatar]),
                    ]
                }
                card = ScanCard(user).create(**card_data)
                cnt += 1
                cnt % 10 == 0 and print(cnt)
                # return user_data
                # return card

    @action(methods=["post"], detail=False)
    @transaction.atomic
    def test(self, request):
        from v_user.special import get_first_card, get_first_card2

        card = get_first_card(self.user)
        # print("------------------------")
        card2 = get_first_card2(self.user)
        return card2
