from typing import List

from custom_django.test.base import BaseTestCase
from custom_django.test.card_logic import (
    BusinessCardBaseLogic,
    PersonalCardBaseLogic,
    ScanCardBaseLogic,
)
from custom_django.test.request_data import NoneRequest, TestRequestData
from helper.common.debug import print_query
from m_company.tests import BaseManagerTestCase
from m_company.views.employees import CompanyEmployeesViewSet
from m_company.views.templates import CompanyTemplatesViewSet
from s_contacts.models import SShareContactRequests
from v_user.view import VUser
from .logics.card import PrintedCard, ScanCard
from .models import SCards
from .views import SCardsViewSet


class CardTestFunction:
    def validate_property(self, origin_property, current_property, name):
        origin_value = origin_property.get("value", None)
        origin_true_value = origin_property.get("true_value", None)
        current_value = current_property.get("value", None)
        origin_property_type = origin_property.get("property_type", None)
        if origin_property_type != "F":
            assert origin_value == current_value, (
                f"{name} different value: "
                f"\n{origin_property} \n!= {current_property}"
            )
        else:
            assert (
                "media" not in origin_property and "media_id" not in origin_property
            ) or origin_value != current_value, (
                f"{name} didn't change media value: \n"
                f"{origin_property} \n== {current_property}"
            )
            assert origin_true_value == current_value, (
                f"{name} different media value "
                f"\n{origin_property} \n!= {current_property}"
            )

        origin_mandatory = origin_property.get("mandatory", False)
        current_mandatory = current_property.get("mandatory", False)
        assert (
            origin_mandatory == current_mandatory
        ), f"{name} mandatory value \n{origin_property} \n!= {current_property}"

        origin_editable = origin_property.get("editable", True)
        current_editable = current_property.get("editable", True)
        assert (
            origin_editable == current_editable
        ), f"{name} editable value \n{origin_property} \n!= {current_property}"

        origin_searchable = origin_property.get(
            "searchable",
            origin_property_type not in {"F", "U", "O"},
        )
        current_searchable = current_property.get(
            "searchable",
            origin_property_type not in {"F", "U", "O"},
        )
        assert (
            origin_searchable == current_searchable
        ), f"{name} searchable value \n{origin_property} \n!= {current_property}"

    def validate_properties(
        self, origin_properties: List[dict], new_properties: List[dict]
    ):
        properties_map_origin = {
            _property["label"]: _property for _property in origin_properties
        }
        properties_map_current = {
            _property["label"]: _property for _property in new_properties
        }
        for key, origin in properties_map_origin.items():
            assert (
                key in properties_map_current
            ), f"Missing property {key} when create new card"
            current = properties_map_current[key]
            self.validate_property(origin, current, key)

    def validate_tier(self, origin_card: dict, new_card: dict):
        assert set(new_card["tier_1_view_key"]) == set(
            origin_card["tier_1_view_key"]
        ), (
            f"Wrong tier 1: {new_card['tier_1_view_key']} "
            f"({type(new_card['tier_1_view_key'])}) "
            f"!= {origin_card['tier_1_view_key']}"
            f"({type(origin_card['tier_1_view_key'])}) "
        )
        assert set(new_card["tier_2_view_key"]) == set(
            origin_card["tier_2_view_key"]
        ), (
            f"Wrong tier 2: {new_card['tier_2_view_key']}"
            f"({type(new_card['tier_2_view_key'])}) "
            f"!= {origin_card['tier_2_view_key']}"
            f"({type(origin_card['tier_2_view_key'])}) "
        )
        assert set(new_card["tier_3_view_key"]) == set(
            origin_card["tier_3_view_key"]
        ), (
            f"Wrong tier 3: {new_card['tier_3_view_key']} "
            f"!= {origin_card['tier_3_view_key']}"
        )

    def validate_card(self, origin_card: dict, new_card: dict):
        self.validate_tier(origin_card, new_card)
        visibility = origin_card.get("visibility", None)
        assert (
            visibility is None or visibility == new_card["visibility"]
        ), f"Wrong visibility: {visibility} != {new_card['visibility']}"
        shareability = origin_card.get("shareability", None)
        assert (
            shareability is None or shareability == new_card["shareability"]
        ), f"Wrong shareability: {shareability} != {new_card['shareability']}"


class ScanCardTC(BaseTestCase, CardTestFunction):
    ViewSet = SCardsViewSet
    CARD_TYPE = SCards.SCAN

    def create_general_objects(self):
        _, self.media = self.helper.create_media()
        _, self.media2 = self.helper.create_media(filename="test_2")

    def create_test_objects(self):
        _, card_data = self.generate_create_data(self)
        self.test_data, self.test_obj = self.helper.create_card(
            self.suser,
            self.media,
            self.media2,
            card_class=ScanCard,
            card_data=card_data,
        )

    def generate_create_data(self, **kwargs):
        properties = [
            {
                "label": "name",
                "value": "Chi",
                "property_type": "N",
            },
            {
                "label": "avatar",
                "media": self.media.id,
                "value": "None",
                "true_value": self.media.url,
                "property_type": "F",
            },
            {
                "label": "company name",
                "value": "h.@ctiv8",
                "property_type": "N",
            },
            {
                "label": "job title",
                "value": "Dev",
                "property_type": "C",
            },
            {
                "label": "work email",
                "value": "t1@g.com",
                "property_type": "E",
            },
            {
                "label": "mobile",
                "value": "127362947",
                "property_type": "M",
            },
            {
                "label": "mobile2",
                "value": "127323447",
                "property_type": "M",
            },
            {
                "label": "address",
                "value": "khong ai biet",
                "property_type": "A",
            },
            {
                "label": "media 1",
                "media": self.media2.id,
                "value": "None",
                "true_value": self.media2.url,
                "property_type": "F",
            },
        ]
        card = {
            "card_type": self.CARD_TYPE,
            "properties": properties,
            "tier_1_view_key": ["address"],
            "tier_2_view_key": ["work email"],
            "tier_3_view_key": ["mobile2"],
            "visibility": SCards.CONTACT_OF_CONTACT,
            "shareable": SCards.EVERYONE,
            **kwargs,
        }
        return properties, card

    def generate_update_data(self, **kwargs):
        properties = [
            {
                "label": "name",
                "value": "Chi 2",
            },
            {
                "label": "avatar",
                "media": self.media.id,
                "value": self.media2.url,
                "true_value": self.media.url,
            },
            {
                "label": "company name",
                "value": "h.@ctiv8 2",
            },
            {
                "label": "job title",
                "value": "Dev 2",
            },
            {
                "label": "work email",
                "value": "t2@g.com",
            },
            {
                "label": "mobile",
                "value": "2374623853",
            },
            {
                "label": "address",
                "value": "khong ai biet 2",
            },
            {
                "label": "media 1",
                "media": self.media.id,
                "value": self.media2.url,
                "true_value": self.media.url,
            },
        ]
        card = {
            "properties": properties,
            "tier_1_view_key": ["mobile"],
            "tier_2_view_key": ["address"],
            "tier_3_view_key": ["work email"],
            "visibility": "N",
            "shareable": "N",
            **kwargs,
        }
        return properties, card

    def compare_result(self, origin, result) -> bool:
        assert result["card_type"] == self.CARD_TYPE, "Wrong card type"
        assert "properties" in result, result
        self.validate_properties(origin["properties"], result["properties"])
        self.validate_card(origin, result)
        return True

    def test_retrieve(self):
        self.default_test_retrieve()

    def test_list(self):
        self.default_test_list()

    def test_create(self):
        _, card_data = self.generate_create_data()
        self.default_test_create(TestRequestData(card_data, self.user), card_data)

    def test_update(self):
        _, card_data = self.generate_update_data()
        self.default_test_update(TestRequestData(card_data, self.user), card_data)

    def test_destroy(self):
        self.default_test_destroy()

    @BaseTestCase.wrap_exception
    def test_send_message_to_email(self):
        data = {"user_email": "user2@yopmail.com", "tier": 2, "message": "to mail"}
        assert (
            self.run_func(
                "send_to_email",
                TestRequestData(data, self.user, method="POST"),
                pk=self.test_obj.id,
            )
            == self.SUCCESS
        ), self.last_run_result()

        assert self.last_result()["message"] == "SUCCESS", self.last_run_result()

        response = self.last_result_data()["data"]
        self.results = [
            response["card"] == self.test_obj.id,
            response["email"] == data["user_email"],
            response["message"] == data["message"],
        ]
        assert all(self.results), self.last_run_result()

    @BaseTestCase.wrap_exception
    def test_send_message_to_mobile(self):
        data = {"mobile": "+84793361234", "tier": 2, "message": "send to mobile"}
        assert (
            self.run_func(
                "send_to_mobile",
                TestRequestData(data, self.user, method="POST"),
                pk=self.test_obj.id,
            )
            == self.SUCCESS
        ), self.last_run_result()

        assert self.last_result()["message"] == "SUCCESS", self.last_run_result()

        response = self.last_result_data()["data"]
        self.results = [
            response["card"] == self.test_obj.id,
            response["mobile"] == data["mobile"],
            response["message"] == data["message"],
        ]
        assert all(self.results), self.last_run_result()


class PersonalCardTC(ScanCardTC):
    CARD_TYPE = SCards.PERSONAL

    def create_test_objects(self):
        _, card_data = self.generate_create_data(self)
        self.test_data, self.test_obj = self.helper.create_card(
            self.suser,
            self.media,
            self.media2,
            card_data=card_data,
        )

    def test_create(self):
        self.test_obj.status = SCards.DESTROYED
        self.test_obj.save()
        return super().test_create()


class PrintedCardForUserTC(ScanCardTC):
    CARD_TYPE = SCards.PRINTED

    def create_specific_objects(self):
        _, self.card = self.helper.create_card(self.suser, self.media, self.media2)

    def create_test_objects(self):
        _, card_data = self.generate_create_data(self)
        self.test_data, self.test_obj = self.helper.create_card(
            self.suser,
            self.media,
            self.media2,
            card_class=PrintedCard,
            card_data=card_data,
            auto_add_credit=False,
        )
        _, self.contact = self.helper.create_user_contact(self.test_obj, self.card)

    def generate_create_data(self, **kwargs):
        return ScanCardTC.generate_create_data(self, to_card=self.card.id, **kwargs)

    def compare_result(self, origin, result) -> bool:
        assert result["card_type"] == self.CARD_TYPE
        assert "properties" in result, result
        self.validate_properties(origin["properties"], result["properties"])
        return True

    def test_list(self):
        pass

    def test_destroy(self):
        pass

    def test_send_message_to_email(self):
        pass

    def test_send_message_to_mobile(self):
        pass


class BusinessCardTC(BaseManagerTestCase, ScanCardTC):
    CARD_TYPE = SCards.BUSINESS

    def create_general_objects(self):
        _, self.media = self.helper.create_media()
        _, self.media2 = self.helper.create_media(filename="test_2")
        self.init_base()
        self.init_company()
        self.init_admin()
        self.init_template()

    def create_test_objects(self):
        _, card_data = self.generate_create_data(self)
        self.test_data, self.test_obj = self.helper.create_business_card(
            self.suser,
            self.company,
            self.template_card,
            self.media,
            self.media2,
            without_user=False,
            name="Test business card",
            company_user=self.company_admin,
            card_data=card_data,
        )

    def generate_create_data(self, **kwargs):
        properties = [
            {
                "label": "name",
                "value": "Chi",
                "property_type": "N",
            },
            {
                "label": "avatar",
                "media": self.media.id,
                "value": "None",
                "true_value": self.media.url,
                "property_type": "F",
            },
            {
                "label": "company name",
                "value": "h.@ctiv8",
                "property_type": "N",
            },
            {
                "label": "job title",
                "value": "Dev",
                "property_type": "C",
            },
            {
                "label": "work email",
                "value": "t1@g.com",
                "property_type": "E",
            },
            {
                "label": "mobile",
                "value": "127362947",
                "property_type": "M",
            },
            {
                "label": "mobile2",
                "value": "127323447",
                "property_type": "M",
            },
            {
                "label": "address",
                "value": "khong ai biet",
                "property_type": "A",
            },
            {
                "label": "media 1",
                "media": self.media2.id,
                "value": "None",
                "true_value": self.media2.url,
                "property_type": "F",
            },
        ]
        card = {
            "email": "t1@g.com",
            "template_card_id": self.template_card.id,
            "card_type": self.CARD_TYPE,
            "properties": properties,
            "tier_1_view_key": ["address"],
            "tier_2_view_key": ["work email"],
            "tier_3_view_key": ["mobile"],
            "visibility": SCards.CONTACT_OF_CONTACT,
            "shareable": SCards.EVERYONE,
            **kwargs,
        }
        return properties, card

    def generate_update_data(self, **kwargs):
        properties = [
            {
                "label": "name",
                "value": "Chi 2",
            },
            {
                "label": "avatar",
                "media": self.media.id,
                "value": self.media2.url,
                "true_value": self.media.url,
            },
            {
                "label": "company name",
                "value": "h.@ctiv8 2",
            },
            {
                "label": "job title",
                "value": "Dev 2",
            },
            {
                "label": "work email",
                "value": "t2@g.com",
            },
            {
                "label": "mobile",
                "value": "2374623853",
            },
            {
                "label": "mobile2",
                "value": "2897298535",
            },
            {
                "label": "address",
                "value": "khong ai biet 2",
            },
            {
                "label": "media 1",
                "media": self.media.id,
                "value": self.media2.url,
                "true_value": self.media.url,
            },
        ]
        card = {
            "properties": properties,
            "tier_1_view_key": ["mobile"],
            "tier_2_view_key": ["address"],
            "tier_3_view_key": ["work email"],
            "visibility": "N",
            "shareable": "N",
            **kwargs,
        }
        return properties, card

    def test_create(self):
        pass


class TemplateCardTC(BaseManagerTestCase, CardTestFunction):
    CARD_TYPE = SCards.TEMPLATE

    def create_general_objects(self):
        _, self.media = self.helper.create_media()
        _, self.media2 = self.helper.create_media(filename="test_2")
        self.init_base()
        self.init_company()
        self.init_admin()

    def create_test_objects(self):
        _, card_data = self.generate_create_data(self)
        self.test_data, self.test_obj = self.helper.create_template_card(
            self.suser,
            self.company,
            self.media,
            self.media2,
            card_data=card_data,
        )

    def generate_create_data(self, **kwargs):
        properties = [
            {
                "label": "name",
                "mandatory": True,
                "property_type": "N",
            },
            {
                "label": "avatar",
                "mandatory": True,
                "property_type": "F",
            },
            {
                "label": "company name",
                "value": "h.@ctiv8",
                "property_type": "N",
                "editable": False,
            },
            {
                "label": "job title",
                "mandatory": True,
                "property_type": "C",
            },
            {
                "label": "work email",
                "mandatory": True,
                "property_type": "E",
            },
            {
                "label": "work mobile",
                "mandatory": True,
                "property_type": "M",
            },
            {
                "label": "address",
                "property_type": "A",
            },
            {
                "label": "media 1",
                "property_type": "F",
            },
        ]
        card = {
            "card_type": self.CARD_TYPE,
            "properties": properties,
            "tier_1_view_key": ["address"],
            "tier_2_view_key": ["work email"],
            "tier_3_view_key": ["mobile"],
            "visibility": SCards.HIDDEN,
            "shareable": SCards.EVERYONE,
            **kwargs,
        }
        return properties, card

    def generate_update_data(self, **kwargs):
        properties = [
            {"label": "name", "mandatory": True, "value": "Chi 2"},
            {
                "label": "avatar",
                "mandatory": True,
                "media": self.media.id,
                "value": self.media2.url,
                "true_value": self.media.url,
            },
            {"label": "job title", "mandatory": True, "value": "Dev"},
            {
                "label": "work email",
                "mandatory": True,
                "value": self.user.email,
            },
            {
                "label": "work mobile",
                "mandatory": True,
                "value": "124329850729",
            },
            {
                "label": "address",
                "value": "fcv,",
            },
            {
                "label": "media 1",
                "media": self.media.id,
                "value": self.media2.url,
                "true_value": self.media.url,
            },
        ]
        card = {
            "properties": properties,
            "tier_1_view_key": ["mobile"],
            "tier_2_view_key": ["address"],
            "tier_3_view_key": ["work email"],
            "visibility": "N",
            "shareable": "N",
            **kwargs,
        }
        return properties, card


class CompanyTemplateCardTestCase(BaseManagerTestCase, ScanCardTC):
    ViewSet = CompanyTemplatesViewSet
    CARD_TYPE = SCards.TEMPLATE

    generate_create_data = TemplateCardTC.generate_create_data
    generate_update_data = TemplateCardTC.generate_update_data

    def create_general_objects(self):
        _, self.media = self.helper.create_media()
        _, self.media2 = self.helper.create_media(filename="test_2")
        self.init_base()
        self.init_company()
        self.init_admin()

    def create_test_objects(self):
        _, card_data = self.generate_create_data(self)
        self.test_data, self.test_obj = self.helper.create_template_card(
            self.suser,
            self.company,
            self.media,
            self.media2,
            card_data=card_data,
        )

    def test_destroy(self):
        pass

    def test_send_message_to_email(self):
        pass

    def test_send_message_to_mobile(self):
        pass


class CompanyEmployeeCardTC(BaseManagerTestCase, ScanCardTC):
    ViewSet = CompanyEmployeesViewSet
    CARD_TYPE = SCards.BUSINESS

    generate_create_data = BusinessCardTC.generate_create_data
    generate_update_data = BusinessCardTC.generate_update_data

    def create_general_objects(self):
        _, self.media = self.helper.create_media()
        _, self.media2 = self.helper.create_media(filename="test_2")
        self.init_base()
        self.init_company()
        self.init_admin()
        self.init_template()

    def create_test_objects(self):
        _, card_data = self.generate_create_data(self)
        self.test_data, self.test_obj = self.helper.create_business_card(
            self.suser,
            self.company,
            self.template_card,
            self.media,
            self.media2,
            without_user=False,
            name="Test employee card",
            company_user=self.company_admin,
            card_data=card_data,
        )

    def test_send_message_to_email(self):
        pass

    def test_send_message_to_mobile(self):
        pass


class AccessBusinessCardTC(BaseManagerTestCase, CardTestFunction):
    ViewSet = SCardsViewSet
    CARD_TYPE = SCards.BUSINESS

    generate_create_data = BusinessCardTC.generate_create_data
    compare_result = BusinessCardTC.compare_result

    def create_general_objects(self):
        _, self.media = self.helper.create_media()
        _, self.media2 = self.helper.create_media(filename="test_2")
        self.init_base()
        self.init_company()
        self.init_admin()
        self.init_template()

    def create_specific_objects(self):
        self.user2, self.suser2 = self.helper.create_user("user2@yopmail")
        _, card_data = self.generate_create_data(self)
        _, self.business_card = self.helper.create_business_card(
            self.suser2,
            self.company,
            self.template_card,
            self.media,
            self.media2,
            without_user=True,
            name="Test employee card",
            company_user=self.company_admin,
            card_data=card_data,
            auto_add_credit=False,
            auto_invite=False,
        )

    def create_test_objects(self):
        self.test_data, self.test_obj = self.helper.create_invitation_code(
            "user2@yopmail.com", self.business_card
        )

    def test_access(self):
        _, card_data = self.generate_create_data()
        assert (
            self.run_func(
                "access",
                TestRequestData({"access_code": self.test_obj.access_code}, self.user2),
            )
            == self.SUCCESS
        ), self.last_run_result()

        self.compare_result(card_data, self.last_result_data())


class SearchCardTC(BaseTestCase):
    ViewSet = SCardsViewSet

    def create_general_objects(self):
        _, self.media = self.helper.create_media()
        _, self.media2 = self.helper.create_media(filename="test_2")
        _, self.suser2 = self.helper.create_user(
            "user2@yopmail.com",
            name="user 2",
            location_longitude=self.suser.location_longitude,
            location_latitude=self.suser.location_latitude,
        )
        _, self.suser3 = self.helper.create_user(
            "user3@yopmail.com",
            name="user 3",
            location_longitude=12,
            location_latitude=12,
        )
        _, self.card = self.helper.create_card(
            self.suser, self.media, self.media2, mobile="1234"
        )

    def create_test_objects(self):
        _, self.card3 = self.helper.create_card(
            self.suser3, self.media, self.media2, company_name="Shake"
        )
        self.test_data, self.test_obj = self.helper.create_card(
            self.suser2,
            self.media,
            self.media2,
            company_name="h.@ctiv8",
            mobile="23458123",
            address="Wasinton",
        )
        # _, self.contact = self.helper.create_user_contact(self.card, self.test_obj)

    @BaseTestCase.wrap_exception
    def test_search(self):
        self.request_data = TestRequestData(None, self.user, {"key": self.suser2.name})
        assert (
            self.run_func("search", self.request_data) == self.SUCCESS
        ), self.last_run_result()

        result = self.last_result_data()
        assert isinstance(result, list), self.debug_data("Response must be a list")
        assert len(result) > 0, self.debug_data("Response must has at least 1 result")
        assert result[0]["id"] == self.test_obj.id, self.debug_data("Not correct!")

        data = TestRequestData(None, self.user, {"key": self.suser3.name})
        assert self.run_func("search", data) == self.SUCCESS, self.last_run_result()

        result = self.last_result_data()
        assert isinstance(result, list), self.debug_data("Response must be a list")
        assert len(result) > 0, self.debug_data("Response must has at least 1 result")
        assert result[0]["id"] == self.card3.id, self.debug_data("Not correct!")

    @BaseTestCase.wrap_exception
    def test_search_nearby(self):
        assert (
            self.run_func("search_nearby", NoneRequest(user=self.user)) == self.SUCCESS
        ), self.last_run_result()

        result = self.last_result_data()
        assert isinstance(result, list), self.debug_data("Response must be a list")
        assert len(result) > 0, self.debug_data("Response must has at least 1 result")
        assert result[0]["id"] == self.test_obj.id, self.debug_data("Not correct!")

    @BaseTestCase.wrap_exception
    def test_search_advanced(self):
        self.request_data = TestRequestData(
            None,
            self.user,
            {
                "name": self.test_obj.user_name,
                "company_name": self.test_obj.company_name,
                "mobile": self.test_data["mobile"],
                "address": self.test_data["address"],
            },
        )
        assert (
            self.run_func("search_advanced", self.request_data) == self.SUCCESS
        ), self.last_run_result()

        result = self.last_result_data()
        assert isinstance(result, list), self.debug_data("Response must be a list")
        assert len(result) > 0, self.debug_data("Response must has at least 1 result")
        assert result[0]["id"] == self.test_obj.id, self.debug_data("Not correct!")

    @BaseTestCase.wrap_exception
    def test_relations__contact(self):
        self.helper.create_user_contact(self.test_obj, self.card)

        assert (
            self.run_func("relations", NoneRequest(user=self.user), pk=self.test_obj.id)
            == self.SUCCESS
        ), self.last_run_result()

        result = self.last_result_data()
        assert isinstance(result, dict), self.debug_data("Response must be a dict")
        assert isinstance(result["contacts"], list), self.debug_data("Must be a list")
        assert len(result["contacts"]) == 1, self.debug_data("Must has length exact 1")
        self.results = [
            result["contacts"][0]["card"]["id"] == self.card.id,
            result["contacts"][0]["user_card"] == self.test_obj.id,
            result["contacts"][0]["user"] == self.suser2.id,
            result["contacts"][0]["related_user"] == self.suser.id,
        ]
        assert all(self.results), self.debug_data()

    @BaseTestCase.wrap_exception
    def test_relations__contact_request(self):
        self.helper.create_contact_request(self.card, self.test_obj)

        assert (
            self.run_func("relations", NoneRequest(user=self.user), pk=self.test_obj.id)
            == self.SUCCESS
        ), self.last_run_result()

        result = self.last_result_data()
        assert isinstance(result, dict), self.debug_data("Response must be a dict")
        assert isinstance(result["contact_requests"], list), self.debug_data(
            "Must be a list"
        )
        assert len(result["contact_requests"]) == 1, self.debug_data(
            "Must has length exact 1"
        )
        self.results = [
            result["contact_requests"][0]["from_card"]["id"] == self.card.id,
            result["contact_requests"][0]["to_card"] == self.test_obj.id,
            result["contact_requests"][0]["from_card"]["user"] == self.suser.id,
            result["contact_requests"][0]["user"] == self.suser2.id,
        ]
        assert all(self.results), self.debug_data()


class MergeScanCardToPersonalCardTC(PersonalCardBaseLogic):
    ViewSet = SCardsViewSet

    def create_general_objects(self):
        _, self.suser2 = self.helper.create_user("user2@yopmail.com")
        _, self.card21 = PersonalCardBaseLogic._create_card(user=self.suser2)
        _, self.card22 = ScanCardBaseLogic._create_card(user=self.suser2)
        _, self.card23 = BusinessCardBaseLogic._create_card(user=self.suser2)
        _, self.card24 = ScanCardBaseLogic._create_card(user=self.suser2)
        _, self.card25 = ScanCardBaseLogic._create_card(user=self.suser2)

        _, self.suser3 = self.helper.create_user("user3@yopmail.com")
        _, self.card31 = PersonalCardBaseLogic._create_card(user=self.suser3)
        _, self.card32 = ScanCardBaseLogic._create_card(user=self.suser3)
        _, self.card33 = BusinessCardBaseLogic._create_card(user=self.suser3)
        _, self.card34 = ScanCardBaseLogic._create_card(user=self.suser3)

    def create_specific_objects(self):
        _, self.from_card = ScanCardBaseLogic._create_card(user=self.suser)
        self.test_data, self.test_obj = self.create_card()
        self.to_card = self.test_obj

    def create_test_objects(self):
        self.friend_connections = dict(
            contact_231=self.helper.create_user_contact(self.card21, self.card31),
            contact_232=self.helper.create_user_contact(self.card21, self.card32),
            contact_234=self.helper.create_user_contact(self.card25, self.card34),
            contact_21f=self.helper.create_user_contact(self.card21, self.from_card),
            contact_21t=self.helper.create_user_contact(self.card21, self.to_card),
            contact_231t=self.helper.create_user_contact(self.card23, self.to_card),
            contact_31f=self.helper.create_user_contact(self.card33, self.from_card),
            contact_31fc=self.helper.create_company_contact(
                self.card33, self.from_card
            ),
            contact_324=self.helper.create_user_contact(self.card33, self.card24),
            contact_324c=self.helper.create_company_contact(self.card33, self.card24),
            request_31=self.helper.create_contact_request(self.from_card, self.card32),
            request_212=self.helper.create_contact_request(self.from_card, self.card22),
            request_213=self.helper.create_contact_request(self.from_card, self.card23),
            share_32f=self.helper.create_share_contact_request(
                self.card33, self.card24, self.from_card
            ),
            share_123=self.helper.create_share_contact_request(
                self.from_card, self.card21, self.card31
            ),
            share_231t=self.helper.create_share_contact_request(
                self.card21, self.card31, self.to_card
            ),
            share_231f2=self.helper.create_share_contact_request(
                self.card25, self.card34, self.from_card
            ),
        )
        self.from_card_connections = dict(
            contact_121=self.helper.create_user_contact(self.from_card, self.card21),
            contact_125=self.helper.create_user_contact(self.from_card, self.card25),
            contact_131=self.helper.create_user_contact(self.from_card, self.card31),
            contact_133=self.helper.create_user_contact(self.from_card, self.card33),
            request_122=self.helper.create_contact_request(self.card22, self.from_card),
            # request_123=self.helper.create_contact_request(self.from_card, self.card23),
            request_13=self.helper.create_contact_request(self.card32, self.from_card),
            share_213f=self.helper.create_share_contact_request(
                self.card21, self.from_card, self.card31
            ),
            share_213f2=self.helper.create_share_contact_request(
                self.card21, self.from_card, self.card32
            ),
        )
        self.to_card_connections = dict(
            contact_123=self.helper.create_user_contact(self.to_card, self.card23),
            # existed
            contact_121=self.helper.create_user_contact(self.to_card, self.card21),
            contact_122=self.helper.create_user_contact(self.to_card, self.card22),
            contact_124=self.helper.create_user_contact(self.to_card, self.card24),
            # contact_132=self.helper.create_user_contact(self.to_card, self.card32),
            request_125=self.helper.create_contact_request(self.card25, self.to_card),
            request_134=self.helper.create_contact_request(self.card34, self.to_card),
            share_213t=self.helper.create_share_contact_request(
                self.card21, self.to_card, self.card31
            ),
        )

    def validate_before_merge(self):
        my = VUser(self.suser)
        assert (
            my.contact.all().count() == 8
        ), f"Not correct my contact: {my.contact.all().count()} != 8"
        assert (
            my.contact_request.all().count() == 4
        ), f"Not correct my contact_request: {my.contact_request.all().count()} != 4"
        assert (
            my.contact_share.all().count() == 3
        ), f"Not correct my contact_share: {my.contact_share.all().count()} != 3"

        assert (
            my.friend.contact.all().count() == 5
        ), f"Not correct my friend's contact: {my.friend.contact.all().count()} != 5"
        assert (
            my.friend.contact_request.all().count() == 3
        ), f"Not correct my friend's contact_request: {my.friend.contact_request.all().count()} != 3"
        assert (
            SShareContactRequests.objects.filter(
                status=SShareContactRequests.HOLD
            ).count()
            == 7
        ), (
            "Not correct all contact_share: %d != 7"
            % SShareContactRequests.objects.filter(
                status=SShareContactRequests.HOLD
            ).count()
        )

    def validate_after_merge(self):
        my = VUser(self.suser)
        assert (
            my.contact.all().count() == 7
        ), f"Not correct my contact: {my.contact.all().count()} != 7"
        assert (
            my.contact_request.all().count() == 2
        ), f"Not correct my contact_request: {my.contact_request.all().count()} != 2"
        assert (
            my.contact_share.all().count() == 1
        ), f"Not correct my contact_share: {my.contact_share.all().count()} != 1"

        assert (
            my.friend.contact.all().count() == 4
        ), f"Not correct my friend's contact: {my.friend.contact.all().count()} != 4"
        assert (
            my.friend.contact_request.all().count() == 2
        ), f"Not correct my friend's contact_request: {my.friend.contact_request.all().count()} != 2"
        assert (
            SShareContactRequests.objects.filter(
                status=SShareContactRequests.HOLD
            ).count()
            == 2
        ), (
            "Not correct all contact_share: %d != 2"
            % SShareContactRequests.objects.filter(
                status=SShareContactRequests.HOLD
            ).count()
        )

    def test_soft_merge_not_delete(self):
        data = TestRequestData(
            {
                "from_card": self.from_card.id,
                "to_card": self.to_card.id,
                "keep_card": True,
                "hard_delete": False,
            },
            self.user,
        )
        assert self.run_func("merge", data) == self.SUCCESS, self.last_run_result()

    def test_hard_merge_not_delete(self):
        data = TestRequestData(
            {
                "from_card": self.from_card.id,
                "to_card": self.to_card.id,
                "keep_card": False,
                "hard_delete": False,
            },
            self.user,
        )

        self.validate_before_merge()
        assert self.run_func("merge", data) == self.SUCCESS, self.last_run_result()
        self.validate_after_merge()

    def test_hard_merge_and_delete(self):
        data = TestRequestData(
            {
                "from_card": self.from_card.id,
                "to_card": self.to_card.id,
                "keep_card": False,
                "hard_delete": True,
            },
            self.user,
        )

        self.validate_before_merge()
        assert self.run_func("merge", data) == self.SUCCESS, self.last_run_result()
        self.validate_after_merge()
