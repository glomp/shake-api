from django.db import models
from django_mysql.models import ListCharField

from custom_django.models import SModel, fields
from helper import slogger
from helper.exceptions import BadRequestException
from s_companies.models import SCompanies, SCompanyUsers
from s_properties.bases import PropertyTypes
from s_templates.models import STemplates
from s_user.logics.helper import Distance
from s_user.models import SUsers


class SCards(SModel):
    ACTIVE = "A"
    REQUIRE_INFO = "R"
    UNAVAILABLE = "U"
    INACTIVE = "I"
    DESTROYED = "D"
    STATUS = (
        (ACTIVE, "ACTIVE"),
        (REQUIRE_INFO, "REQUIRE_INFO"),
        (UNAVAILABLE, "UNAVAILABLE"),
        (INACTIVE, "INACTIVE"),
        (DESTROYED, "DESTROYED"),
    )

    PERSONAL = "P"
    IDENTITY = "I"
    BUSINESS = "B"
    SCAN = "S"
    PRINTED = "C"
    TEMPLATE = "T"
    CARD_TYPE = (
        (PERSONAL, "PERSONAL"),
        (IDENTITY, "IDENTITY"),
        (BUSINESS, "BUSINESS"),
        (SCAN, "SCAN"),
        (PRINTED, "PRINTED"),
        (TEMPLATE, "TEMPLATE"),
    )

    EVERYONE = "E"
    CONTACT_OF_CONTACT = "C"
    NEARBY_ONLY = "N"
    HIDDEN = "H"
    SYSTEM = "S"
    VISIBILITY = (
        (EVERYONE, "EVERYONE"),
        (CONTACT_OF_CONTACT, "CONTACT_OF_CONTACT"),
        (NEARBY_ONLY, "NEARBY_ONLY"),
        (HIDDEN, "HIDDEN"),
        (SYSTEM, "SYSTEM"),
    )

    EVERYONE = "E"
    NOT_ALLOWED = "N"
    SHAREABLE = (
        (EVERYONE, "EVERYONE"),
        (NOT_ALLOWED, "NOT_ALLOWED"),
    )

    user = models.ForeignKey(SUsers, models.DO_NOTHING, null=True, blank=True)
    template = models.ForeignKey(STemplates, models.DO_NOTHING, default=None, null=True)
    company_user = models.OneToOneField(
        SCompanyUsers, models.DO_NOTHING, default=None, null=True, blank=True
    )

    status = models.CharField(max_length=1, default=ACTIVE, choices=STATUS)
    card_type = models.CharField(max_length=1, choices=CARD_TYPE)
    searchable = models.BooleanField(default=True)
    visibility = models.CharField(max_length=1, default=EVERYONE, choices=VISIBILITY)
    shareability = models.CharField(max_length=1, default=EVERYONE, choices=SHAREABLE)

    name = models.CharField(max_length=127)

    user_name = models.CharField(max_length=255, default="", null=True, blank=True)
    avatar = models.CharField(max_length=255, default="", null=True, blank=True)
    company_name = models.CharField(max_length=255, default="", null=True, blank=True)
    job_title = models.CharField(max_length=255, default="", null=True, blank=True)

    balance_total_personal = models.IntegerField(default=0)
    balance_total_company = models.IntegerField(default=0)
    balance_free_lock_until = fields.UnixTimeStampField(
        default=None,
    )

    tier_1_view_key = ListCharField(
        base_field=models.CharField(max_length=31),
        size=16,
        max_length=(16 * 32 - 1),
        default=[],
    )
    tier_2_view_key = ListCharField(
        base_field=models.CharField(max_length=31),
        size=16,
        max_length=(16 * 32 - 1),
        default=[],
    )
    tier_3_view_key = ListCharField(
        base_field=models.CharField(max_length=31),
        size=16,
        max_length=(16 * 32 - 1),
        default=[],
    )

    _created = fields.UnixTimeStampField(null=True, auto_now_add=True)
    _updated = fields.UnixTimeStampField(null=True, auto_now=True)

    # Example for property type
    PROS__MANDATORY_FIELDS = {
        "name": PropertyTypes.NAME,
        "email": PropertyTypes.EMAIL,
        "avatar": PropertyTypes.MEDIA,
        "background": PropertyTypes.MEDIA,
    }
    PROS__NORMAL_FIELDS = {
        "mobile": PropertyTypes.MOBILE,
        "address": PropertyTypes.ADDRESS,
        "font color": PropertyTypes.OTHER,
        "tag 1": PropertyTypes.TAG,
        "tag 2": PropertyTypes.TAG,
        "tag 3": PropertyTypes.TAG,
        "media 1": PropertyTypes.MEDIA,
        "media 2": PropertyTypes.MEDIA,
        "media 3": PropertyTypes.MEDIA,
        "social facebook": PropertyTypes.URL,
        "social twitter": PropertyTypes.URL,
        "social instagram": PropertyTypes.URL,
        "social linkedin": PropertyTypes.URL,
        "social others": PropertyTypes.URL,
        "about me": PropertyTypes.CONTENT,
        "did": PropertyTypes.MOBILE,
        "website": PropertyTypes.URL,
        "work mobile": PropertyTypes.MOBILE,
        "work email": PropertyTypes.EMAIL,
        "job title": PropertyTypes.CONTENT,
        "blank card": PropertyTypes.MEDIA,
    }

    # For serializer
    PERSONAL_FIELDS = [
        "tier_1_view_key",
        "tier_2_view_key",
        "tier_3_view_key",
        "balance_total_personal",
        "balance_total_company",
        "balance_free_lock_until",
    ]
    SYSTEM_FIELDS = [
        "balance_total_personal",
        "balance_total_company",
        "balance_free_lock_until",
    ]

    # For model
    READONLY_FIELDS = ["card_type"]

    # For fetch properties
    PROS__DEFAULT_SHOW_KEY = {
        "name",
        "avatar",
        "background",
        "job title",
        "company name",
        "about me",
        "media 1",
        "media 2",
        "media 3",
        "tag 1",
        "tag 2",
        "tag 3",
        "blank card",
        "company website",
        "company address",
    }
    PROS__TIER_KEYS = {
        "email",
        "work email",
        "mobile",
        "work mobile",
        "did",
        "address",
        "website",
        "linkedin",
        "facebook",
        "twitter",
        "instagram",
    }

    @property
    def card_name(self):
        return self.company_name if self.card_type != SCards.PERSONAL else "personal"

    _company_contact: models.OneToOneField = None

    @property
    def company_contact(self):
        return self._company_contact

    @property
    def public_properties(self):
        from threading import current_thread

        current_user = current_thread.user

        if current_user.id == self.user_id:
            return self.sproperties_set.all()

        contacts = list(self.related_contacts.all())
        my_contacts = [
            contact for contact in contacts if contact.user_id == current_user.id
        ]
        if my_contacts and self.card_type == self.PRINTED:
            return self.sproperties_set.all()

        if my_contacts:
            contact = max(my_contacts, key=lambda contact: contact.tier_number)
            show_key = self._get_property_label_from_tier(contact.tier_number)
        else:
            show_key = []

        properties = list(self.sproperties_set.all())
        properties_label = set(self.PROS__TIER_KEYS - set(show_key))
        allow_properties = [
            _property
            for _property in properties
            if _property.label not in properties_label
        ]
        return allow_properties

    @property
    def logic(self):
        if not hasattr(self, "_logic"):
            from .logics.card import Card, get_card_logic

            self._logic: Card = get_card_logic(self)

        return self._logic

    @property
    def get_card_basic_info(self):
        show_keys = [
            "name",
            "avatar",
            "background",
            "job title",
            "company name",
        ]

        return self.get_properties(show_keys)

    def check_read_permission(self, user: SUsers, distance: int = 100):
        if self.status == SCards.DESTROYED:
            return False
        if self.user_id == user.id:
            return True

        if self.visibility == SCards.HIDDEN or self.visibility == SCards.SYSTEM:
            return False
        if self.visibility == SCards.EVERYONE:
            return True
        related_contacts = self.related_contacts.all()
        related_user_ids = set([ctc.user_id for ctc in related_contacts])
        if user.id in related_user_ids:
            return True
        if self.visibility == SCards.CONTACT_OF_CONTACT:
            from v_friend.view import VFriend

            friend_view = VFriend(user)
            if self.user_id in friend_view.contact.of_contact.all().values_list(
                "user_id", flat=True
            ):
                return True
            else:
                return False
        elif self.visibility == SCards.NEARBY_ONLY:

            self.user: SUsers
            return Distance.check(
                distance,
                user.location_latitude,
                user.location_longitude,
                self.user.location_latitude,
                self.user.location_longitude,
            )

    def validate(
        self, raise_exception=True, errcode: int = None, debug: str = ""
    ) -> bool:
        if getattr(self, "_is_valid", None) is not None:
            return self._is_valid

        from .serializers import SPublicCardSerializer

        try:
            if self.status != SCards.ACTIVE:
                raise BadRequestException(
                    errcode=errcode or 400068,
                    message=f"This {self.card_name} card is not activated.",
                    data={"debug": debug, "card": SPublicCardSerializer(self).data},
                )
            if self.card_type == SCards.BUSINESS:
                company_user: SCompanyUsers = self.company_user
                if company_user is None or company_user.status != SCompanyUsers.ACTIVE:
                    raise BadRequestException(
                        errcode=errcode or 400069,
                        message=f"This {self.card_name} card is not activated.",
                        data={"debug": debug, "card": SPublicCardSerializer(self).data},
                    )
                company: SCompanies = company_user.company
                if company is None or company.status != SCompanies.ACTIVE:
                    from s_companies.serializers import SCompanySerializer

                    raise BadRequestException(
                        errcode=errcode or 400070,
                        message=f"Company {company.name} is not activated.",
                        data={
                            "debug": debug,
                            "card": SPublicCardSerializer(self).data,
                            "company": (
                                SCompanySerializer(company).data if company else None
                            ),
                        },
                    )
            self._is_valid = True
        except BadRequestException as e:
            if self.company_user:
                slogger.debug("company_user: %r", self.company_user.__dict__)
            if raise_exception:
                raise e
            slogger.debug("Card validate: %r", e)
            self._is_valid = False
        return self._is_valid

    def get_properties(self, properties_label: list):
        properties_label = set(self.PROS__TIER_KEYS - set(properties_label))
        properties = self.sproperties_set.all()
        if self.card_type != self.PRINTED:
            properties = [
                _property
                for _property in properties
                if _property.label not in properties_label
            ]
        return properties

    def get_properties_from_tier(self, tier_num: int = 0):
        if tier_num >= 1 and tier_num <= 3:
            show_keys = self._get_property_label_from_tier(tier_num)
            return self.get_properties(show_keys)
        return self.get_card_basic_info

    def get_properties_from_tiers(self, tiers: list):
        if isinstance(tiers, str):
            tiers = tiers.split(",")
        all_show_keys = set()
        for tier_num in tiers:
            show_keys = self._get_property_label_from_tier(tier_num)
            all_show_keys |= set(show_keys)
        # tier_num = max(tiers)
        # all_show_keys = self._get_property_label_from_tier(tier_num)

        return self.get_properties(all_show_keys)

    def get_readonly_fields(self, request, obj=None):
        return ["template", "card_type"] if obj else []

    def _get_property_label_from_tier(self, tier_num: int) -> list:
        tier_name = f"tier_{tier_num}_view_key"
        return getattr(self, tier_name)

    class Meta:
        managed = True
        db_table = "s_card"


class SCompanyInvitations(SModel):
    ACCEPTED = "A"
    HOLD = "H"
    STATUS = (
        (ACCEPTED, "ACCEPTED"),
        (HOLD, "HOLD"),
    )

    status = models.CharField(max_length=1, default=HOLD, choices=STATUS)

    card = models.OneToOneField(
        SCards, on_delete=models.DO_NOTHING, related_name="invitation"
    )

    email = fields.LowerCharField(max_length=127)
    access_code = models.CharField(max_length=6, unique=True, blank=True, null=True)

    inviter = models.ForeignKey(
        SCompanyUsers,
        on_delete=models.DO_NOTHING,
        default=None,
        null=True,
        blank=True,
        related_name="invitations",
    )

    _expiration = fields.UnixTimeStampField(null=True)
    _created = fields.UnixTimeStampField(null=True, auto_now_add=True)

    PERSONAL_FIELDS = ("access_code",)

    def get_readonly_fields(self, request, obj=None):
        return ["_created"] + (["card", "email", "access_code"] if obj else [])

    class Meta:
        managed = True
        db_table = "s_company_invitation"


class SPotentialUserInformation(SModel):
    card = models.ForeignKey(
        SCards, on_delete=models.DO_NOTHING, related_name="potential_users"
    )

    user_name = models.CharField(max_length=125, default=None, blank=True, null=True)
    email = models.CharField(max_length=512, default=None, blank=True, null=True)
    mobile = models.CharField(max_length=31, default=None, blank=True, null=True)

    message = models.CharField(max_length=125, default=None, blank=True, null=True)

    _created = fields.UnixTimeStampField(null=True, auto_now_add=True)

    class Meta:
        managed = True
        db_table = "s_potential_user_information"
