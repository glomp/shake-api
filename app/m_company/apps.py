from django.apps import AppConfig
from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from .views.balance import CompanyCreditLogsViewSet, CompanyTotalBalancesViewSet
from .views.contacts import CompanyContactCardsViewSet, CompanyContactsViewSet
from .views.employee.balance import EmployeeCreditLogsViewSet
from .views.employee.contacts import (
    EmployeeContactRequestsViewSet,
    EmployeeContactsViewSet,
)
from .views.employees import CompanyEmployeesViewSet
from .views.templates import CompanyTemplatesViewSet


class CompanyManagerConfig(AppConfig):
    name = "company_manager"


class CompanyManagerRouter:
    router = DefaultRouter()

    router.register("templates", CompanyTemplatesViewSet)

    router.register("contacts", CompanyContactsViewSet)
    router.register("contact-cards", CompanyContactCardsViewSet)

    router.register("employees", CompanyEmployeesViewSet)

    router.register("credit-logs", CompanyCreditLogsViewSet)
    router.register("total-balances", CompanyTotalBalancesViewSet)

    employee_router = DefaultRouter()

    employee_router.register("contacts", EmployeeContactsViewSet)
    employee_router.register("contact-requests", EmployeeContactRequestsViewSet)
    employee_router.register("credit-logs", EmployeeCreditLogsViewSet)

    urlpatterns = [
        url(r"^(?P<company_id>[\d]+)/", include(router.urls)),
        url(
            r"^(?P<company_id>[\d]+)/employees/(?P<employee_card_id>[\d]+)/",
            include(employee_router.urls),
        ),
    ]
