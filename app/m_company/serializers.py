from dynamic_rest.fields import DynamicRelationField
from dynamic_rest.serializers import DynamicModelSerializer
from rest_framework import serializers

import custom_django.serializers.fields as fields
from s_balances.models import SCreditLogs
from s_balances.serializers import CreditLogSerializer
from s_cards.models import SCards, SCompanyInvitations
from s_cards.serializers import (
    SCardSerializer,
    SPublicCardSerializer,
    SPublicThoroughCardSerializer,
)
from s_companies.models import SCompanyUsers
from s_companies.serializers import SCompanyUserSerializer
from s_contacts.models import SContacts
from s_contacts.serializers import SCompanyUserContactsSerializer
from s_properties.serializers import (
    SPublicPropertySerializer,
    SPublicThoroughPropertySerializer,
    SThoroughPropertySerializer,
)
from s_user.serializers import SUserSerializer


class SPublicDetailedCompanyUserSerializer(DynamicModelSerializer):
    card = DynamicRelationField(SPublicCardSerializer, embed=True, source="scards")

    class Meta:
        model = SCompanyUsers
        fields = "__all__"


class SPublicThoroughCompanyEmployeeContactSerializer(DynamicModelSerializer):
    card = DynamicRelationField(SPublicThoroughCardSerializer, embed=True)

    class Meta:
        model = SContacts
        fields = "__all__"


class SDetailedCompanyInvitationSerializer(DynamicModelSerializer):
    inviter = DynamicRelationField(SPublicDetailedCompanyUserSerializer, embed=True)

    class Meta:
        model = SCompanyInvitations
        fields = "__all__"


class SDetailedCompanyContactCardSerializer(SCardSerializer):
    receiver = fields.OnlyOneSerializer(
        child=SCompanyUserContactsSerializer(),
        source="company_contact",
    )

    tiers = fields.IntSeparatedField(child=serializers.IntegerField())
    belong_to_contacts = fields.IntSeparatedField(child=serializers.IntegerField())
    belong_to_cards = fields.IntSeparatedField(child=serializers.IntegerField())

    properties = serializers.ListSerializer(
        child=SPublicPropertySerializer(),
        read_only=True,
        source="sproperties_set",
    )

    class Meta:
        model = SCards
        fields = "__all__"
        read_only_fields = SCards.SYSTEM_FIELDS


class SThoroughCompanyContactCardSerializer(SDetailedCompanyContactCardSerializer):
    properties = serializers.ListSerializer(
        child=SThoroughPropertySerializer(),
        read_only=True,
        source="sproperties_set",
    )

    class Meta:
        model = SCards
        fields = "__all__"
        read_only_fields = SCards.SYSTEM_FIELDS


class SCompanyUserCardSerializer(SCardSerializer):
    company_user = DynamicRelationField(SCompanyUserSerializer, embed=True)
    properties = serializers.ListSerializer(
        source="sproperties_set",
        child=SPublicPropertySerializer(),
        read_only=True,
    )
    invitation = DynamicRelationField(SDetailedCompanyInvitationSerializer, embed=True)

    class Meta:
        model = SCards
        fields = "__all__"
        read_only_fields = SCards.SYSTEM_FIELDS


class SDetailedCompanyUserCardSerializer(SCompanyUserCardSerializer):
    contact_number = serializers.IntegerField()
    scan_card_number = serializers.IntegerField()
    contact_request_number = serializers.IntegerField()
    contact_request_success_number = serializers.IntegerField()

    class Meta:
        model = SCards
        fields = "__all__"
        read_only_fields = SCards.SYSTEM_FIELDS


class SThoroughCompanyUserCardSerializer(SDetailedCompanyUserCardSerializer):
    properties = serializers.ListSerializer(
        source="sproperties_set",
        child=SPublicThoroughPropertySerializer(),
        read_only=True,
    )

    class Meta:
        model = SCards
        fields = "__all__"
        read_only_fields = SCards.SYSTEM_FIELDS


class TotalBalanceSerializer(SCompanyUserCardSerializer):
    # card = DynamicRelationField(
    #     SCompanyUserCardSerializer,
    # )
    properties = None
    invitation = None

    total_company_credit_used = serializers.IntegerField(default=0, initial=0)
    total_personal_credit_used = serializers.IntegerField(default=0, initial=0)

    class Meta:
        model = SCards
        fields = "__all__"
        read_only_fields = SCards.SYSTEM_FIELDS
