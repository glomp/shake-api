from typing import List, Tuple

from custom_django.test.base import BaseTestCase
from custom_django.test.request_data import TestRequestData
from s_balances.models import SCreditLogs
from s_cards.models import SCards
from s_contacts.models import SContacts
from .views.balance import CompanyCreditLogsViewSet, CompanyTotalBalancesViewSet
from .views.contacts import CompanyContactCardsViewSet, CompanyContactsViewSet
from .views.employee.balance import EmployeeCreditLogsViewSet
from .views.employee.contacts import (
    EmployeeContactRequestsViewSet,
    EmployeeContactsViewSet,
)
from .views.employees import CompanyEmployeesViewSet
from .views.templates import CompanyTemplatesViewSet


class BaseManager:
    TIER_NUMBER = 2
    COMPANY_NAME = "Hactivate"

    @classmethod
    def init_base(cls, company_name=None, user=None):
        cls.helper = BaseTestCase.helper

        cls.init_company(company_name=company_name)
        cls._admin = user or cls.helper.create_user("user@yopmail.com")[1]
        _, cls.media = cls.helper.create_media()
        _, cls.media2 = cls.helper.create_media(filename="test_2")

    @classmethod
    def init_company(cls, company_name=None):
        _, cls.company = cls.helper.create_company(company_name or cls.COMPANY_NAME)
        return cls.company

    @classmethod
    def init_admin(cls):
        _, cls.company_admin = cls.helper.create_company_admin(cls._admin, cls.company)
        return cls.company_admin

    @classmethod
    def init_template(cls, admin=None):
        _, cls.template_card = cls.helper.create_template_card(
            admin or cls._admin, cls.company, cls.media, cls.media2
        )
        return cls.template_card

    @classmethod
    def init_admin_card(cls, template=None):
        _, cls.admin_card = cls.helper.create_business_card(
            cls._admin,
            cls.company,
            template or cls.template_card,
            cls.media,
            cls.media2,
            without_user=False,
            name=f"{cls.company.name} Admin",
            company_user=cls.company_admin,
        )
        return cls.admin_card

    @classmethod
    def init_employee(cls, user2=None):
        cls._employee = user2 or cls.helper.create_user("user2@yopmail.com")[1]
        _, cls.company_employee = cls.helper.create_company_employee(
            cls._employee, cls.company
        )
        return cls.company_employee

    @classmethod
    def init_employee_card(cls, template=None, company_user=None, **kwargs):
        data, cls.employee_card = cls.helper.create_business_card(
            cls._employee,
            cls.company,
            template or cls.template_card,
            cls.media,
            cls.media2,
            without_user=False,
            name=f"{cls._employee.name} Business card",
            company_user=company_user or cls.company_employee,
            **kwargs,
        )
        return data, cls.employee_card

    @classmethod
    def init_full(
        cls,
        company_name=None,
        user=None,
        admin=None,
        template_card=None,
        admin_card=None,
        user2=None,
        employee=None,
        employee_card=None,
        **kwargs,
    ):
        cls.init_base(company_name=company_name, user=user)
        cls.company_admin = admin or cls.init_admin()
        cls.template_card = template_card or cls.init_template()
        cls.admin_card = admin_card or cls.init_admin_card()
        cls.company_employee = employee or cls.init_employee(user2=user2)
        _, cls.employee_card = employee_card or cls.init_employee_card(**kwargs)

    @classmethod
    def _create_card(cls, user, company_name=None, **kwargs) -> Tuple[dict, SCards]:
        cls.init_base(company_name=company_name)
        cls.init_admin()
        cls.init_template()
        cls.init_employee(user2=user)
        return cls.init_employee_card(**kwargs)


class BaseManagerTestCase(BaseManager, BaseTestCase):
    pass


class EmloyeeContactTC(BaseManagerTestCase):
    ViewSet = EmployeeContactsViewSet
    TIER_NUMBER = 2

    def create_general_objects(cls):
        cls.init_full()

    def create_specific_objects(self):
        _, self.suser3 = self.helper.create_user("user3@yopmail.com")
        _, self.card3 = self.helper.create_card(self.suser3, self.media, self.media2)

    def create_test_objects(self):
        _, self.company_contact = self.helper.create_company_contact(
            own_card=self.admin_card,
            target_card=self.card3,
            tier_number=self.TIER_NUMBER,
        )

        (
            self.test_data,
            self.test_obj,
        ) = self.helper.create_company_employee_contact(self.employee_card, self.card3)

    def validate_result(self, origin, result) -> bool:
        return (
            result["card"]["id"] == origin["target_card"].id,
            result["user_card"] == origin["own_card"].id,
            result["tier_number"] == self.TIER_NUMBER,
            result["company"] == self.company.id,
        )

    def test_retrieve(self):
        self.default_test_retrieve()

    def test_list(self):
        self.default_test_list()

    def test_create(self):
        self.default_test_create(TestRequestData({"card_id": self.card3.id}, self.user))

    def test_destroy(self):
        return self.default_test_destroy()


class EmloyeeContactRequestTC(BaseManagerTestCase):
    ViewSet = EmployeeContactRequestsViewSet

    def create_general_objects(cls):
        cls.init_full()

    def create_specific_objects(self):
        _, self.suser3 = self.helper.create_user("user3@yopmail.com")
        _, self.card3 = self.helper.create_card(self.suser3, self.media, self.media2)

    def create_test_objects(self):
        (self.test_data, self.test_obj,) = self.helper.create_contact_request(
            self.employee_card, self.card3, self.TIER_NUMBER
        )

    def validate_result(self, origin, result) -> bool:
        return (
            result["user"] == origin["to_card"].user.id,
            result["from_card"]["id"] == origin["from_card"].id,
            result["to_card"]["id"] == origin["to_card"].id,
            result["from_card"]["company_name"] == self.COMPANY_NAME,
            result["tier_number"] == origin["tier_number"],
        )

    def test_retrieve(self):
        self.default_test_retrieve()

    def test_list(self):
        self.default_test_list()

    def test_destroy(self):
        return self.default_test_destroy()


class CompanyContactTC(BaseManagerTestCase):
    ViewSet = CompanyContactsViewSet

    def create_general_objects(self):
        self.init_full()

    def create_specific_objects(self):
        _, self.suser3 = self.helper.create_user("user3@yopmail.com")
        _, self.card3 = self.helper.create_card(self.suser3, self.media, self.media2)
        # _, self.card4 = self.helper.create_card(
        #     self.suser3, self.media2, self.media, ScanCard
        # )

    def create_test_objects(self):
        self.test_data, self.test_obj = self.helper.create_company_contact(
            self.employee_card, self.card3
        )

    def validate_result(self, origin, result) -> bool:
        return (
            result["id"] == self.test_obj.id,
            result["tier_number"] == origin["tier_number"],
            result["employee_card"]["id"] == self.employee_card.id,
            result["status"] == SContacts.ACTIVE,
        )

    def test_retrieve(self):
        return self.default_test_retrieve()

    def test_list(self):
        return self.default_test_list()


class CompanyContactCardTC(CompanyContactTC):
    ViewSet = CompanyContactCardsViewSet

    def create_test_objects(self):
        super(CompanyContactCardTC, self).create_test_objects(self)
        self.test_obj = self.card3

    def validate_result(self, origin, result) -> bool:
        return (
            origin["target_card"].id == result["id"],
            origin["tier_number"] in result["tiers"],
            origin["own_card"].id in result["belong_to_cards"],
        )


class CompanyEmployeeTC:
    ViewSet = CompanyEmployeesViewSet
    # This test is tested in card test cases.


class CompanyTemplateTC:
    ViewSet = CompanyTemplatesViewSet
    # This test is tested in card test cases.


class CompanyTotalBalanceTC(BaseManagerTestCase):
    ViewSet = CompanyTotalBalancesViewSet

    def create_test_objects(self):
        self.test_data, self.test_obj = self._create_card(
            self.suser, auto_add_credit=False
        )
        _, self.credit_log = self.helper.create_company_credit(self.test_obj, 123)

    def validate_result(self, origin, result) -> List[bool]:
        return [
            result["id"] == self.test_obj.id,
            result["balance_total_company"] == self.credit_log.amount,
            result["balance_total_personal"] == 0,
        ]

    def test_retrieve(self):
        return super().default_test_retrieve()

    def test_list(self):
        return super().default_test_list()


# class PersonalTotalBalanceTC(BaseManagerTestCase):
#     ViewSet = CompanyTotalBalancesViewSet

#     def create_test_objects(self):
#         self.test_data, self.test_obj = self.create_card(auto_add_credit=False)
#         _, self.credit_log = self.helper.create_personal_credit(self.test_obj, 123)

#     def validate_result(self, origin, result) -> List[bool]:
#         return [
#             result["id"] == self.test_obj.id,
#             result["balance_total_company"] == 0,
#             result["balance_total_personal"] == self.credit_log.amount,
#         ]


class CompanyCreditLogTC(BaseManagerTestCase):
    ViewSet = CompanyCreditLogsViewSet

    def create_test_objects(self):
        _, self.employee_card = self._create_card(self.suser, auto_add_credit=False)
        self.test_data, self.test_obj = self.helper.create_company_credit(
            self.employee_card, 123
        )

    def validate_result(self, origin, result) -> List[bool]:
        return [
            result["id"] == self.test_obj.id,
            result["card"] == self.employee_card.id,
            result["credit_type"] == SCreditLogs.COMPANY,
            result["direction"] == SCreditLogs.INCOMING,
            result["amount"] == origin["amount"],
        ]

    def test_retrieve(self):
        return super().default_test_retrieve()

    def test_list(self):
        return super().default_test_list()


class EmployeeCreditLogTC(CompanyCreditLogTC):
    ViewSet = EmployeeCreditLogsViewSet
