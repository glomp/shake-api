from django.db import transaction
from django.http.response import Http404
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from s_cards.logics.card import BusinessCard, get_card_logic
from s_cards.models import SCards
from s_cards.serializers import SThoroughCardSerializer
from s_companies.logics.members import CompanyMemberInvitation
from s_notifications.email.company import CompanyMailNotifier
from ..serializers import (
    SDetailedCompanyUserCardSerializer,
    SThoroughCompanyUserCardSerializer,
)
from .base_view_set import SDynamicModelManagerViewSet


class CompanyEmployeesViewSet(SDynamicModelManagerViewSet):
    """Handles company's employees."""

    serializer_class = SThoroughCardSerializer
    model_class = SCards
    queryset = SCards.objects.filter(status=SCards.ACTIVE).all()

    def retrieve(self, request, pk):
        return SThoroughCompanyUserCardSerializer(
            self.company_view.card.view.get(id=pk)
        )

    def list(self, request):
        return SDetailedCompanyUserCardSerializer(
            self.company_view.card.view.all(), many=True
        )

    @transaction.atomic
    def create(self, request):
        member_email = request.data["email"]
        template = self.company_view.card.get(id=request.data["template_card_id"])
        company_employee = CompanyMemberInvitation(
            self.user, self.company_admin, self.company, template
        )
        company_employee.check_template()
        company_employee.check_email(member_email)
        invitation = company_employee.make_invitation(member_email, request.data.copy())
        CompanyMailNotifier().invite_employee.send_to(
            invitation.email,
            access_code=invitation.access_code,
            company_name=self.company.name,
            admin_name=self.user.name,
        )
        return invitation.card

    @transaction.atomic
    def update(self, request, pk, partial=False):
        card = self.company_view.card.get(id=pk)
        data = request.data.copy()
        properties = data.pop("properties", None)
        card = BusinessCard(self.user, card).update(data, properties, allow_null=True)
        return card

    @transaction.atomic
    def destroy(self, request, pk):
        instance = self.view.card.get_if_exist(id=pk)
        card_logic = get_card_logic(
            self.user,
            instance,
            support={SCards.PERSONAL, SCards.BUSINESS, SCards.SCAN},
        )

        cnt, _ = card_logic.delete()
        return "SUCCESS" if cnt == 1 else None
