from custom_django.views.viewsets import SDynamicModelViewSet
from m_company.logics.permission import check_manager_permission
from s_companies.models import SCompanies, SCompanyUsers
from v_manager.view import VManager
from v_user.view import VUser


class SDynamicModelManagerViewSet(SDynamicModelViewSet):
    def extra_initial(self, request: object, *args, **kwargs):
        super(SDynamicModelManagerViewSet, self).extra_initial(request, *args, **kwargs)

        company_id = self.kwargs.pop("company_id")
        company, company_admin = check_manager_permission(self.user, company_id)

        self.company: SCompanies = company
        self.company_admin: SCompanyUsers = company_admin
        self.view = VUser(self.user)
        self.company_view = VManager(self.user, company, company_admin)
