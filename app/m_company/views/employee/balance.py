from django.db import transaction
from django.http.response import Http404
from rest_framework.decorators import action

from s_balances.models import SCreditLogs
from s_balances.serializers import CreditLogSerializer
from .base_view_set import SDynamicModelEmployeeViewSet


class EmployeeCreditLogsViewSet(SDynamicModelEmployeeViewSet):
    """Handles company's credit logs."""

    models_class = SCreditLogs
    serializer_class = CreditLogSerializer
    queryset = SCreditLogs.objects.all()

    def retrieve(self, request, pk):
        return self.employee_view.balance_log.company.get(id=pk)

    def list(self, request):
        filters = dict(
            _created__gte=self.params.get("from_date", None),
            _created__lte=self.params.get("to_date", None),
        )
        filters = {k: v for k, v in filters.items() if v is not None}
        return self.employee_view.balance_log.company.all(
            card__company_user__company_id=self.company.id, **filters
        )[self.pag_left : self.pag_right]

    def create(self, request):
        raise Http404

    def update(self, request, pk, partial=False):
        raise Http404

    def destroy(self, request, pk):
        raise Http404

    @action(methods=["get"], detail=False, url_path="by-month")
    def by_month(self, request):
        filters = dict(
            _created__gte=self.params.get("from_date", None),
            _created__lte=self.params.get("to_date", None),
        )
        filters = {k: v for k, v in filters.items() if v is not None}
        return self.employee_view.balance_log.company.by_month.all(
            card__company_user__company_id=self.company.id, **filters
        )[self.pag_left : self.pag_right]

    @action(methods=["get"], detail=False, url_path="by-year")
    def by_year(self, request):
        filters = dict(
            _created__gte=self.params.get("from_date", None),
            _created__lte=self.params.get("to_date", None),
        )
        filters = {k: v for k, v in filters.items() if v is not None}
        return self.employee_view.balance_log.company.by_year.all(
            card__company_user__company_id=self.company.id, **filters
        )[self.pag_left : self.pag_right]
