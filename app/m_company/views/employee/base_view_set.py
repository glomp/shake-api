from v_user.view import VUser
from ..base_view_set import SDynamicModelManagerViewSet


class SDynamicModelEmployeeViewSet(SDynamicModelManagerViewSet):
    def extra_initial(self, request: object, *args, **kwargs):
        super(SDynamicModelEmployeeViewSet, self).extra_initial(
            request, *args, **kwargs
        )
        employee_card_id = self.kwargs.pop("employee_card_id")
        self.employee_card = self.company_view.card.employee.get(id=employee_card_id)
        self.employee_view = VUser(self.employee_card.user)
