from django.db import transaction
from django.http.response import Http404

from helper.exceptions import AppException
from s_contacts.logics.contact import EmployeeContact
from s_contacts.models import SContactRequests, SContacts
from s_contacts.serializers import SDetailedContactRequestsSerializer
from ...serializers import SPublicThoroughCompanyEmployeeContactSerializer
from .base_view_set import SDynamicModelEmployeeViewSet


class EmployeeContactsViewSet(SDynamicModelEmployeeViewSet):
    """Handles employee's contacts."""

    serializer_class = SPublicThoroughCompanyEmployeeContactSerializer
    model_class = SContacts
    queryset = SContacts.objects.all()

    def retrieve(self, request, pk):
        return self.employee_view.contact.business.get(
            company_id=self.company.id, id=pk
        )

    def list(self, request):
        return self.employee_view.contact.business.all(company_id=self.company.id)

    @transaction.atomic
    def create(self, request):
        company_contact = self.company_view.contact.company.get(
            card_id=request.data["card_id"]
        )
        obj, _ = EmployeeContact.create_or_get(
            own_card=self.employee_card,
            target_card=company_contact.card,
            tier_number=company_contact.tier_number,
        )
        return obj

    def update(self, request, pk, partial=False):
        raise Http404

    @transaction.atomic
    def destroy(self, request, pk):
        instance = self.company_view.contact.employee.get_if_exist(
            id=pk, user_id=self.employee_card.user_id
        )
        if instance is None:
            return
        cnt, obj = instance.delete()
        if cnt == 1:
            return "SUCCESS"
        elif cnt > 1:
            raise AppException(
                errcode=500058,
                message="Can't perform this action",
                data={"pk": pk, "cnt": cnt, "obj": obj.__dict__},
                err_message="More than one objects are being deleted!",
            )


class EmployeeContactRequestsViewSet(SDynamicModelEmployeeViewSet):
    """Handles employee's contacts."""

    serializer_class = SDetailedContactRequestsSerializer
    model_class = SContactRequests
    queryset = SContactRequests.objects.all()

    def retrieve(self, request, pk):
        return self.company_view.contact_request.by_employee_card(
            self.employee_card
        ).get(id=pk)

    def list(self, request):
        return self.company_view.contact_request.by_employee_card(
            self.employee_card
        ).all()

    def create(self, request):
        raise Http404

    def update(self, request, pk, partial=False):
        raise Http404

    @transaction.atomic
    def destroy(self, request, pk):
        instance = self.company_view.contact_request.by_employee_card(
            self.employee_card
        ).get_if_exist(id=pk, status=SContactRequests.HOLD)
        if instance is None:
            return
        cnt, obj = instance.delete()
        if cnt == 1:
            return "SUCCESS"
        elif cnt > 1:
            raise AppException(
                errcode=500059,
                message="Can't perform this action",
                data={"pk": pk, "cnt": cnt, "obj": obj.__dict__},
                err_message="More than one objects are being deleted!",
            )
