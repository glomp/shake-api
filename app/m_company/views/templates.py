from django.db import transaction
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from s_cards.logics import TemplateCard
from s_cards.models import SCards
from s_cards.serializers import SThoroughCardSerializer
from .base_view_set import SDynamicModelManagerViewSet


class CompanyTemplatesViewSet(SDynamicModelManagerViewSet):
    """Handles company's templates."""

    models_class = SCards
    serializer_class = SThoroughCardSerializer
    queryset = SCards.objects.filter(status=SCards.ACTIVE).all()

    def retrieve(self, request, pk):
        return self.company_view.card.template.get(id=pk)

    def list(self, request):
        return self.company_view.card.template.all()

    @transaction.atomic
    def create(self, request):
        return TemplateCard(self.user, self.company).create(
            allow_null=True, **request.data
        )

    @transaction.atomic
    def update(self, request, pk, partial=False):
        card = self.company_view.card.template.get(id=pk)
        data = request.data.copy()
        properties = data.pop("properties", {})
        return TemplateCard(self.user, self.company, card).update(
            data,
            properties,
            allow_null=True,
            without_user=True,
            force_edit=True,
        )

    @transaction.atomic
    def destroy(self, request, pk):
        card = self.company_view.card.template.get_if_exist(pk)
        cnt, _ = TemplateCard(self.user, self.company, card).delete(
            soft=False, force_edit=True
        )
        if cnt == 1:
            return "SUCCESS"
