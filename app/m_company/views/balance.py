from django.db import transaction
from django.http.response import Http404
from rest_framework.decorators import action

from s_balances.models import SCreditLogs
from s_balances.serializers import CreditLogSerializer
from s_cards.models import SCards
from ..serializers import TotalBalanceSerializer
from .base_view_set import SDynamicModelManagerViewSet


class CompanyCreditLogsViewSet(SDynamicModelManagerViewSet):
    """Handles company's credit logs."""

    models_class = SCreditLogs
    serializer_class = CreditLogSerializer
    queryset = SCreditLogs.objects.all()

    def retrieve(self, request, pk):
        return self.company_view.balance_log.company.get(id=pk)

    def list(self, request):
        filters = dict(
            _created__gte=self.params.get("from_date", None),
            _created__lte=self.params.get("to_date", None),
        )
        filters = {k: v for k, v in filters.items() if v is not None}
        return self.company_view.balance_log.company.all(**filters)[
            self.pag_left : self.pag_right
        ]

    def create(self, request):
        raise Http404

    def update(self, request, pk, partial=False):
        raise Http404

    def destroy(self, request, pk):
        raise Http404

    @action(methods=["get"], detail=False, url_path="by-month")
    def by_month(self, request):
        filters = dict(
            _created__gte=self.params.get("from_date", None),
            _created__lte=self.params.get("to_date", None),
        )
        filters = {k: v for k, v in filters.items() if v is not None}
        return self.company_view.balance_log.company.by_month.all(**filters)[
            self.pag_left : self.pag_right
        ]

    @action(methods=["get"], detail=False, url_path="by-year")
    def by_year(self, request):
        filters = dict(
            _created__gte=self.params.get("from_date", None),
            _created__lte=self.params.get("to_date", None),
        )
        filters = {k: v for k, v in filters.items() if v is not None}
        return self.company_view.balance_log.company.by_year.all(**filters)[
            self.pag_left : self.pag_right
        ]


class CompanyTotalBalancesViewSet(SDynamicModelManagerViewSet):
    models_class = SCards
    serializer_class = TotalBalanceSerializer
    queryset = SCards.objects.all()

    def retrieve(self, request, pk):
        return self.company_view.total_balance.get(id=pk)

    def list(self, request):
        filters = dict(
            _created__gte=self.params.get("from_date", None),
            _created__lte=self.params.get("to_date", None),
        )
        filters = {k: v for k, v in filters.items() if v is not None}
        return self.company_view.total_balance.all(**filters)[
            self.pag_left : self.pag_right
        ]
        # seralizers = self.serializer_class(
        #     data=self.company_view.total_balance.all(**filters)[
        #         self.pag_left : self.pag_right
        #     ],
        #     many=True,
        # )
        # seralizers.is_valid(raise_exception=False)
        # return seralizers.data

    def create(self, request):
        raise Http404

    def update(self, request, pk, partial=False):
        raise Http404

    def destroy(self, request, pk):
        raise Http404

    @action(methods=["get"], detail=False, url_path="by-month")
    def by_month(self, request):
        filters = dict(
            _created__gte=self.params.get("from_date", None),
            _created__lte=self.params.get("to_date", None),
        )
        filters = {k: v for k, v in filters.items() if v is not None}
        return self.company_view.total_balance.by_month.all(**filters)[
            self.pag_left : self.pag_right
        ]
