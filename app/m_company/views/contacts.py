from django.db import transaction
from django.http.response import Http404
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from s_cards.models import SCards
from s_contacts.models import SContacts
from s_contacts.serializers import SCompanyUserContactsSerializer
from ..serializers import (
    SDetailedCompanyContactCardSerializer,
    SThoroughCompanyContactCardSerializer,
)
from .base_view_set import SDynamicModelManagerViewSet


class CompanyContactsViewSet(SDynamicModelManagerViewSet):
    """Handles company's contacts."""

    serializer_class = SCompanyUserContactsSerializer
    model_class = SContacts
    queryset = SContacts.objects.all()

    def retrieve(self, request, pk):
        return self.company_view.contact.get(id=pk)

    def list(self, request):
        return self.company_view.contact.all()

    def create(self, request):
        raise Http404

    def update(self, request, pk, partial=False):
        raise Http404

    def destroy(self, request, pk):
        raise Http404


class CompanyContactCardsViewSet(SDynamicModelManagerViewSet):
    """Handles company's contact cards."""

    serializer_class = SDetailedCompanyContactCardSerializer
    model_class = SCards
    queryset = SCards.objects.filter(status=SCards.ACTIVE).all()

    def retrieve(self, request, pk):
        return SThoroughCompanyContactCardSerializer(
            self.company_view.contact_card.get(id=pk)
        )

    def list(self, request):
        query = self.company_view.contact_card.all()
        data = SDetailedCompanyContactCardSerializer(query, many=True).data
        return data

    def create(self, request):
        raise Http404

    def update(self, request, pk, partial=False):
        raise Http404

    def destroy(self, request, pk):
        raise Http404
