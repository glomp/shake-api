# Generated by Django 2.2.17 on 2021-01-27 04:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("m_company", "0010_auto_20210121_0733"),
        ("s_cards", "0008_scards_company_user_id"),
    ]

    operations = [
        migrations.RemoveField(model_name="scompanyusers", name="card",),
    ]
