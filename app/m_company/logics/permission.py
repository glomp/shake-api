from django.core.exceptions import ObjectDoesNotExist
from django.http.response import Http404

from helper.exceptions import ForbiddenException, NotFoundException
from s_companies.models import SCompanies, SCompanyUsers
from s_user.models import SUsers


def check_manager_permission(user, company_id):
    try:
        company_admin: SCompanyUsers = SCompanyUsers.objects.select_related(
            "company"
        ).get(company_id=company_id, user_id=user.id, role=SCompanyUsers.ADMIN)
    except ObjectDoesNotExist:
        raise ForbiddenException(
            errcode=403052,
            message="You are not a company admin!",
            data={"user_id": user.id, "company_id": company_id},
        )
    company = company_admin.company
    return company, company_admin


def check_manager_permission_request(func):
    def _check_manager_permission(self, request, **kwargs):
        user = request.user.susers
        company_id = kwargs.pop("company_id")

        company, company_admin = check_manager_permission(user, company_id)
        return func(self, request, company=company, admin=company_admin, **kwargs)

    return _check_manager_permission


class CompanyPermission:
    check_permission = check_manager_permission_request

    def __init__(self, user: SUsers, company: SCompanies):
        self.user = user
        self.company = company

    # def card_permission(self, card: SCards) -> int:
    #     company_user_contact = SCompanyUserContacts.objects.get_if_exist(
    #         company_user_id=self.company_user.id,
    #         contact__company_id=self.company.id,
    #         contact__card_id=card.id,
    #     )
    #     return (
    #         company_user_contact.tier_number
    #         if company_user_contact is None
    #         else 0
    #     )
