from dynamic_rest.serializers import DynamicModelSerializer
from rest_framework import serializers

from .models import MAppleAccount, SCompanyOtherInformation, SCompanyRegistration


class SAppleAccountSerializer(DynamicModelSerializer):
    class Meta:
        model = MAppleAccount
        fields = "__all__"


class SCompanyOtherInformationSerializer(DynamicModelSerializer):
    class Meta:
        model = SCompanyOtherInformation
        fields = "__all__"


class SCompanyRegistrationSerializer(DynamicModelSerializer):
    class Meta:
        model = SCompanyRegistration
        fields = "__all__"


class SThoroughCompanyRegistrationSerializer(DynamicModelSerializer):
    others = serializers.ListSerializer(child=SCompanyOtherInformationSerializer())

    class Meta:
        model = SCompanyRegistration
        fields = "__all__"
