from django.contrib.auth.models import User
from django.db import models

from custom_django.models import SModel, fields


class MAppleAccount(SModel):
    id = models.CharField(max_length=255, primary_key=True)

    user_auth = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name="apple"
    )

    class Meta:
        managed = True
        db_table = "s_apple_account"


class SCompanyRegistration(SModel):
    company_name = models.CharField(max_length=255)

    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    job_title = models.CharField(max_length=255)
    email = fields.LowerCharField(max_length=512)
    phone = models.CharField(max_length=127)

    _created = fields.UnixTimeStampField(null=True, auto_now_add=True)

    class Meta:
        managed = True
        db_table = "s_company_registration"


class SCompanyOtherInformation(SModel):
    company_registration = models.ForeignKey(
        SCompanyRegistration, models.DO_NOTHING, related_name="others"
    )

    label = models.CharField(max_length=63)
    value = models.CharField(max_length=1023)

    class Meta:
        managed = True
        db_table = "s_company_other_information"
