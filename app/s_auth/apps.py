from django.apps import AppConfig
from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from .views import (
    AccountInfoViewSet,
    AppleAccountInfoViewSet,
    AppleRegisterViewSet,
    LoginViewSet,
    LogoutViewSet,
    RegisterForCompanyViewSet,
    RegisterViewSet,
    ResetPasswordViewSet,
    ReverifyAccountViewSet,
    SetPasswordViewSet,
    ThirdPartyRegisterViewSet,
    VerifyTokenViewSet,
)


class SAuthConfig(AppConfig):
    name = "s_auth"


class SAuthRouter:
    router = DefaultRouter()
    router.register("login", LoginViewSet, basename="login")
    router.register("logout", LogoutViewSet, basename="logout")
    router.register("register", RegisterViewSet, basename="register")
    router.register(
        "third-party-register",
        ThirdPartyRegisterViewSet,
        basename="third-party register",
    )
    router.register("apple-register", AppleRegisterViewSet, basename="apple register")
    router.register(
        "register-for-company",
        RegisterForCompanyViewSet,
        basename="register for company",
    )
    router.register(
        "resend-verify-email",
        ReverifyAccountViewSet,
        basename="reverify account",
    )
    router.register(
        "reset-password",
        ResetPasswordViewSet,
        basename="reset password",
    )
    router.register("set-password", SetPasswordViewSet, basename="set password")

    verify_token = VerifyTokenViewSet.as_view({"get": "get"})
    account_status = AccountInfoViewSet.as_view({"get": "get"})
    apple_account_status = AppleAccountInfoViewSet.as_view({"get": "get"})

    urlpatterns = [
        url(r"verify/(?P<token>[\w]+)", verify_token, name="verify user"),
        url(
            r"apple-account-status/(?P<apple_id>\w+\.\w+\.\w+)",
            apple_account_status,
            name="apple account status",
        ),
        url(
            r"account-status/(?P<email>[\w_+@.\-\"\ \!\%]+)",
            account_status,
            name="account status",
        ),
        url(r"", include(router.urls)),
    ]
