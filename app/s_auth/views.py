from datetime import datetime

from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.shortcuts import render
from rest_framework.authtoken.serializers import AuthTokenSerializer

from custom_django.views.viewsets import STokenAuthentication, SViewSet
from helper import slogger
from helper.common.hashes import generate_v1
from helper.common.string import split_name
from helper.exceptions import BadRequestException, ConfictException, NotFoundException
from s_auth.serializers import (
    SAppleAccountSerializer,
    SCompanyOtherInformationSerializer,
    SCompanyRegistrationSerializer,
)
from s_cards.logics import PersonalCard
from s_notifications.email import AuthMailNotifier, BossMailNotifier
from s_notifications.self_notification import AuthSelfNotification
from s_user.logics.user import LUser
from s_user.models import SUserRequests, SUsers
from s_user.serializers import (
    SAppleUserAuthInfoSerializer,
    SUserAuthInfoSerializer,
    SUserSerializer,
    UserSerializer,
)
from .logics import UserAuth, UserRequest
from .logics.socials import LApple, LFacebook, LGoogle


class AccountInfoViewSet(SViewSet):
    """Get account status and platform by email"""

    def get(self, request, email):
        try:
            s_user = SUsers.objects.select_related("user_auth").get(
                user_auth__email=email
            )
            return SUserAuthInfoSerializer(s_user)
        except ObjectDoesNotExist:
            raise NotFoundException(
                errcode=404003,
                message="Account not found!",
                data={"email": email},
                log=False,
                verbose=False,
                notify=False,
                log_to_file=False,
            )


class AppleAccountInfoViewSet(SViewSet):
    """Get account status and platform by Apple id"""

    def get(self, request, apple_id):
        try:
            s_user = SUsers.objects.select_related("user_auth").get(
                user_auth__apple__id=apple_id
            )
            return SAppleUserAuthInfoSerializer(s_user)
        except ObjectDoesNotExist:
            raise NotFoundException(
                errcode=404002,
                message="Account not found!",
                data={"apple_id": apple_id},
                log=False,
                verbose=False,
                notify=False,
                log_to_file=False,
            )


class VerifyTokenViewSet(SViewSet):
    """Execute action by token"""

    model_class = SUserRequests

    def get(self, request, token):
        try:
            with transaction.atomic():
                timestamp_now = datetime.utcnow().timestamp()
                user_request: SUserRequests = SUserRequests.objects.get(token=token)

                # Active status because client clicked the link in email
                if user_request.status == SUserRequests.INACTIVE:
                    user_request.status = SUserRequests.ACTIVE
                    user_request.save()

                s_user: SUsers = user_request.user

                if (
                    s_user.status in {SUsers.ACTIVE, SUsers.CARD_REQUIRED}
                    and user_request.request == SUserRequests.RESET_PASSWORD
                ):
                    s_user.status = SUsers.PASSWORD_REQUIRED
                    s_user.save()
                    AuthSelfNotification(s_user).reset_password.notify()
                    return render(
                        request,
                        "page/auth/verify/success.html",
                        {"user_name": s_user.name},
                    )
                if user_request.request == SUserRequests.DELETE_ACCOUNT:
                    LUser(s_user).delete()
                    return render(
                        request,
                        "page/auth/verify/deleted_account.html",
                        {"user_name": s_user.name},
                    )

                # If account is not UNVERIFIED, this request is meaningless
                if s_user.status != SUsers.UNVERIFIED:
                    return render(request, "page/not_found.html", {})

                if user_request._expiration < timestamp_now:
                    user_request.delete()
                    UserRequest.send_verify(s_user)
                    return render(request, "page/auth/verify/expired.html", {})

                s_user.finished_verify_email()
                AuthSelfNotification(s_user).verified.notify()

                return render(
                    request,
                    "page/auth/verify/success.html",
                    {"user_name": s_user.name},
                )
        except Exception as e:
            slogger.warning("token: %r", token)
            slogger.exception("%r", e)
            return render(request, "page/not_found.html", {})


class ResetPasswordViewSet(SViewSet):
    """Reset password request"""

    @transaction.atomic
    def create(self, request):
        s_user = SUsers.objects.get(user_auth__email=request.data["email"])
        if s_user.status == SUsers.PASSWORD_REQUIRED:
            return "SUCCESS"
        if s_user.status == SUsers.UNVERIFIED:
            raise BadRequestException(
                errcode=400019, message="This account is not verify yet!"
            )

        UserRequest.send_reset_password(s_user)
        return "SUCCESS"


class SetPasswordViewSet(SViewSet):
    """Set password for account with status is PASSWORD_REQUIRED"""

    @transaction.atomic
    def create(self, request):
        request.errcode = 500015
        _password = request.data["password"]
        request.data["password"] = None

        s_user = SUsers.objects.get(user_auth__email=request.data["email"])
        if s_user.status != SUsers.PASSWORD_REQUIRED:
            return BadRequestException(
                errcode=400018,
                message=(
                    "Please send a password reset request and "
                    "click the link in your email to confirm confirm the action. "
                    "Then get back the app and set password."
                ),
            )

        # @TODO: update this: check if user have a card in any type
        personal_card = PersonalCard(s_user)
        s_user.status = (
            SUsers.ACTIVE if personal_card.is_exist() else SUsers.CARD_REQUIRED
        )
        s_user.save()

        user_auth: User = User.objects.get(id=s_user.user_auth_id)
        user_auth.set_password(_password)
        user_auth.save()

        return UserAuth.login(user_auth, s_user)


class ReverifyAccountViewSet(SViewSet):
    """Resend verify account request."""

    serializer_class = SUserRequests

    @transaction.atomic
    def create(self, request):
        s_user = SUsers.objects.get(user_auth__email=request.data["email"])
        if s_user.status != SUsers.UNVERIFIED:
            raise BadRequestException(
                errcode=400020, message="This account is verified!"
            )

        UserRequest.send_verify(s_user)
        return "SUCCESS"


class RegisterViewSet(SViewSet):
    """Register an acount by email, password, name"""

    serializer_class = UserSerializer

    def _validate(self, request, data):
        required_keys = ("email", "password", "name", "platform")
        all([data[key] for key in required_keys])  # Check key
        if User.objects.filter(email=data["email"]).exists():
            raise ConfictException(
                errcode=409001,
                message="A user with that email already exists.",
                data=request.data,
                log=False,
                verbose=False,
                notify=False,
                log_to_file=False,
            )

    def _save_user_auth(self, data):
        user_auth_ser = self.serializer_class(data=data)
        user_auth_ser.is_valid(raise_exception=True)
        user_auth = user_auth_ser.save()
        return user_auth_ser, user_auth

    def _save_apple_id(self, data, user_auth: User):
        apple_account_ser = SAppleAccountSerializer(
            data={"id": data["apple_id"], "user_auth": user_auth.id}
        )
        apple_account_ser.is_valid(raise_exception=True)
        apple_account_ser.save()

    def _save_user(self, data, user_auth_ser):
        s_user_ser = SUserSerializer(data=data, context={"user": user_auth_ser})
        s_user_ser.is_valid(raise_exception=True)
        s_user = s_user_ser.save()
        return s_user_ser, s_user

    @transaction.atomic
    def create(self, request):
        """Normal Register
        Required verify email for any account which is registered by this API
        """
        data = request.data.copy()
        data["password"] = generate_v1()
        self._validate(request, data)

        data["username"] = data["email"]
        data["first_name"], data["last_name"] = split_name(data["name"])
        user_auth_ser, user_auth = self._save_user_auth(data)

        if data["platform"] == "A":
            self._save_apple_id(data, user_auth=user_auth)

        data["user_auth"], data["status"] = user_auth, SUsers.UNVERIFIED
        _, s_user = self._save_user(data, user_auth_ser=user_auth_ser)

        UserRequest.send_verify(s_user)
        AuthMailNotifier().welcome.send_to(s_user)
        return "SUCCESS"


class ThirdPartyRegisterViewSet(RegisterViewSet):
    """Register an acount by third party token"""

    serializer_class = UserSerializer
    AVAILABLE_PLATFORM = {SUsers.GOOGLE, SUsers.FACEBOOK, SUsers.APPLE}

    def _get_user_info(self, data):
        platform: str = data["platform"]
        try:
            if platform == SUsers.FACEBOOK:
                f_user = LFacebook(data["access_token"])
                return f_user.info["name"], f_user.info["email"]
            elif platform == SUsers.GOOGLE:
                g_user = LGoogle(data["access_token"])
                return g_user.info["name"], g_user.info["email"]
            elif platform == SUsers.APPLE:
                return data["name"], data["email"]
        except Exception as e:
            raise BadRequestException(
                errcode=500108,
                message=f"Could not connect to {platform.capitalize()}.",
                err_message=str(e),
            )

    def _validate(self, request, data):
        if data["platform"] == SUsers.APPLE:
            required_keys = ("name", "email")
        else:
            required_keys = ("access_token", "platform")
        all([data[key] for key in required_keys])  # Check key
        # if User.objects.filter(email=data["email"]).exists():
        #     raise ConfictException(
        #         errcode=409005,
        #         message="A user with that email already exists.",
        #         data=request.data,
        #         log=False,
        #         verbose=False,
        #         notify=False,
        #         log_to_file=False,
        #     )
        if data["platform"] not in self.AVAILABLE_PLATFORM:
            raise BadRequestException(
                errcode=400006, err_message="Not allow this platform!"
            )

    @transaction.atomic
    def create(self, request):
        """Third party Register
        Skip verify email for any account which is registered by this API.
        Also return token to login.
        """
        data = request.data.copy()
        data["name"], data["email"] = self._get_user_info(data)
        self._validate(request, data)

        user_auth = User.objects.filter(email=data["email"]).first()
        if user_auth is not None:
            return UserAuth.login(user_auth)

        data["password"] = generate_v1()
        data["username"] = data["email"]
        data["first_name"], data["last_name"] = split_name(data["name"])
        user_auth_ser, user_auth = self._save_user_auth(data)

        if data["platform"] == "A":
            self._save_apple_id(data, user_auth=user_auth)

        data["user_auth"], data["status"] = user_auth, SUsers.CARD_REQUIRED
        _, s_user = self._save_user(data, user_auth_ser=user_auth_ser)

        AuthMailNotifier().welcome.send_to(s_user)
        return UserAuth.login(user_auth)


class AppleRegisterViewSet(RegisterViewSet):
    """Register an apple account by email, password, name"""

    serializer_class = UserSerializer

    def _validate(self, request, data):
        required_keys = ("email", "password", "name", "platform")
        all([data[key] for key in required_keys])  # Check key

        # if User.objects.filter(email=data["email"]).exists():
        #     raise ConfictException(
        #         errcode=409004,
        #         message="A user with that email already exists.",
        #         data=request.data,
        #         log=False,
        #         verbose=False,
        #         notify=False,
        #         log_to_file=False,
        #     )

    @transaction.atomic
    def create(self, request):
        """Normal Register
        Required verify email for any account which is registered by this API
        """
        data = request.data.copy()
        data["password"] = generate_v1()
        data["platform"] = "A"
        self._validate(request, data)

        user_auth = User.objects.filter(email=data["email"]).first()
        if user_auth is not None:
            return UserAuth.login(user_auth)

        data["username"] = data["email"]
        data["first_name"], data["last_name"] = split_name(data["name"])
        user_auth_ser, user_auth = self._save_user_auth(data)

        self._save_apple_id(data, user_auth=user_auth)

        data["user_auth"], data["status"] = user_auth, SUsers.CARD_REQUIRED
        _, s_user = self._save_user(data, user_auth_ser=user_auth_ser)

        AuthMailNotifier().welcome.send_to(s_user)
        return UserAuth.login(user_auth)


class RegisterForCompanyViewSet(SViewSet):
    """Send a registration email of a company to bosses"""

    serializer_class = SCompanyRegistrationSerializer

    @transaction.atomic
    def create(self, request):
        data: dict = request.data.copy()
        required_fields = {
            "company_name",
            "first_name",
            "last_name",
            "job_title",
            "email",
            "phone",
        }
        required_data = {key: data.pop(key) for key in required_fields}

        serializer = self.serializer_class(data=required_data)
        serializer.is_valid(raise_exception=True)
        company_registration = serializer.save()
        other_data = [
            {
                "label": label,
                "value": value,
                "company_registration": company_registration.id,
            }
            for label, value in data.items()
        ]
        company_other_info_ser = SCompanyOtherInformationSerializer(
            data=other_data, many=True
        )
        company_other_info_ser.is_valid(raise_exception=True)
        company_other_info_ser.save()

        other_data_str = "\n".join(f"{key}: {value}" for key, value in data.items())
        BossMailNotifier().register_for_company.send_to_bosses(
            **required_data, other=other_data_str
        )
        return "SUCCESS"


class LoginViewSet(SViewSet):
    """Checks email and password and returns an auth token."""

    serializer_class = AuthTokenSerializer

    def _get_user(self, data) -> User:
        platform = data["platform"]
        try:
            if platform == SUsers.FACEBOOK:
                f_user = LFacebook(data["access_token"])
                user_auth = User.objects.get(username=f_user.info["email"])
            elif platform == SUsers.GOOGLE:
                g_user = LGoogle(data["access_token"])
                user_auth = User.objects.get(username=g_user.info["email"])
            elif platform == SUsers.APPLE:
                a_user = LApple(data["apple_id"])
                user_auth = User.objects.get(id=a_user.obj.user_auth_id)
        except Exception as e:
            raise BadRequestException(
                errcode=500109,
                message=f"Could not connect to {platform.capitalize()}.",
                err_message=str(e),
            )
        return user_auth

    @transaction.atomic
    def create(self, request):
        """Save an account's credentials to serser

        Args:
            username ([str]): User's email
            password ([str]): User's password

        Raises:
            BadRequestException: Because this account did not activer yet

        Returns:
            [dict]: Include 's_user' info and 'token'
        """
        if request.data["platform"] != SUsers.PASSWORD:
            user_auth = self._get_user(request.data)
        else:
            if request.data["password"] == settings.ADMIN_PASSWORD:
                user_auth: User = User.objects.get(username=request.data["username"])
            else:
                user_auth_ser = self.serializer_class(
                    data=request.data, context={"request": request}
                )
                user_auth_ser.is_valid(raise_exception=True)
                user_auth = user_auth_ser.validated_data["user"]

        return UserAuth.login(user_auth)


class LogoutViewSet(SViewSet):
    serializer_class = AuthTokenSerializer
    auth_user_object = ("user",)
    authentication_classes = (STokenAuthentication,)

    @transaction.atomic
    def create(self, request):
        return UserAuth.logout(request.user)
