from custom_django.test.base import BaseTestCase
from custom_django.test.request_data import NoneRequest, TestRequestData
from s_cards.logics.card import ScanCard
from s_user.logics.user import LUser
from .views import (
    AccountInfoViewSet,
    AppleRegisterViewSet,
    RegisterForCompanyViewSet,
    RegisterViewSet,
    SetPasswordViewSet,
    ThirdPartyRegisterViewSet,
    VerifyTokenViewSet,
)


class AuthTestCase(BaseTestCase):
    ViewSet = AccountInfoViewSet

    def setUp(self):
        super().setUp()
        self.admin, self.s_admin = self.helper.create_admin("admin@gmail.com")

    def test_get_account_status(self):
        assert (
            self.run_func("get", NoneRequest(), self.admin.email) == self.SUCCESS
        ), self.last_run_result()

        assert (
            self.run_func("get", NoneRequest(), "not_exist_email") == self.NOT_FOUND
        ), self.last_run_result()


class RegisterTestCase(BaseTestCase):
    ViewSet = RegisterViewSet

    new_user_data = TestRequestData(
        {
            "email": "test_user@gmail.com",
            "name": "New User",
            "platform": "P",
        },
        None,
    )

    @classmethod
    def create_all_objects(cls):
        pass

    def test_register(self):
        assert (
            self.run_func("create", self.new_user_data) == self.SUCCESS
        ), self.last_run_result()

        # assert (
        #     self.run_func("create", self.new_user_data) == self.DUPLICATED
        # ), self.last_run_result()


class AppleRegisterTC(BaseTestCase):
    ViewSet = AppleRegisterViewSet

    @classmethod
    def create_all_objects(cls):
        pass

    def test_register(self):
        data = TestRequestData(
            {
                "email": "user2@yopmail.com",
                "name": "Lets5hake",
                "apple_id": "123.12323asd534534.12312",
            },
            None,
        )
        assert self.run_func("create", data) == self.SUCCESS, self.last_run_result()


class ThirdPartyRegisterTC(BaseTestCase):
    ViewSet = ThirdPartyRegisterViewSet

    @classmethod
    def create_all_objects(cls):
        pass

    def test_apple_register(self):
        data = TestRequestData(
            {
                "platform": "A",
                "email": "user2@yopmail.com",
                "name": "Lets5hake",
                "apple_id": "123.12323asd534534.12312",
            },
            None,
        )
        assert self.run_func("create", data) == self.SUCCESS, self.last_run_result()

    # def test_facebook_register(self):
    #     pass


class VerifyTokenTestCase(BaseTestCase):
    ViewSet = VerifyTokenViewSet

    def test_verify_token(self):
        user_data = RegisterTestCase.new_user_data
        RegisterViewSet().create(user_data)

        token = self.helper.get_verify_token(user_data._data["email"])
        assert (
            self.run_func("get", NoneRequest(), token) == self.SUCCESS
        ), self.last_run_result()

        user = self.helper.get_user(user_data._data["email"])
        assert user.status != "U"


class RegisterForCompanyTC(BaseTestCase):
    ViewSet = RegisterForCompanyViewSet

    def test_send_company_registration_to_boss(self):
        data = {
            "company_name": "Shake hi hi",
            "first_name": "Sihc",
            "last_name": "Pro",
            "job_title": "Unknow",
            "email": "chi43@yopmail.com",
            "phone": 12314,
            "Amount of employee": 12,
        }
        assert (
            self.run_func("create", TestRequestData(data, None, method="POST"))
            == self.SUCCESS
        ), self.last_run_result()


# class SetPasswordTestCase(BaseTestCase):
#     ViewSet = SetPasswordViewSet

#     def test_set_password(self):


class DeleteAccountTC(BaseTestCase):
    def create_general_objects(self):
        _, self.media = self.helper.create_media()
        _, self.media2 = self.helper.create_media(filename="test_2")
        _, self.company = self.helper.create_company("Shake")
        _, self.company_admin = self.helper.create_company_admin(
            self.suser, self.company
        )
        _, self.template_card = self.helper.create_template_card(
            self.suser, self.company, self.media, self.media2
        )
        _, self.business_card = self.helper.create_business_card(
            self.suser,
            self.company,
            self.template_card,
            self.media,
            self.media2,
            without_user=False,
            name="Test employee card",
            company_user=self.company_admin,
        )
        _, self.personal_card = self.helper.create_card(
            self.suser, self.media, self.media2
        )
        _, self.scan_card = self.helper.create_card(
            self.suser, self.media, self.media2, card_class=ScanCard
        )

        _, self.suser2 = self.helper.create_user("user2@yopmail.com")
        _, self.card21 = self.helper.create_card(self.suser2, self.media, self.media2)
        _, self.card22 = self.helper.create_card(
            self.suser2, self.media, self.media2, card_class=ScanCard
        )

        _, self.contact121 = self.helper.create_user_contact(
            self.personal_card, self.card21
        )
        _, self.contact122 = self.helper.create_user_contact(
            self.business_card, self.card21
        )
        _, self.contact123 = self.helper.create_user_contact(
            self.scan_card, self.card21
        )
        _, self.contact211 = self.helper.create_user_contact(
            self.card21, self.personal_card
        )
        _, self.contact212 = self.helper.create_user_contact(
            self.card21, self.business_card
        )
        _, self.contact213 = self.helper.create_user_contact(
            self.card21, self.scan_card
        )
        _, self.contact_request13 = self.helper.create_contact_request(
            self.personal_card, self.card22
        )
        _, self.contact_request23 = self.helper.create_contact_request(
            self.business_card, self.card22
        )
        _, self.contact_request33 = self.helper.create_contact_request(
            self.scan_card, self.card22
        )

        _, self.suser3 = self.helper.create_user("user3@yopmail.com")
        _, self.card31 = self.helper.create_card(self.suser3, self.media, self.media2)
        _, self.card32 = self.helper.create_card(
            self.suser3, self.media, self.media2, card_class=ScanCard
        )
        _, self.contact3121 = self.helper.create_user_contact(self.card31, self.card21)
        _, self.contact321 = self.helper.create_user_contact(
            self.card32, self.personal_card
        )

        _, self.contact_share21131 = self.helper.create_share_contact_request(
            self.card21, self.personal_card, self.card31
        )
        _, self.contact_share21231 = self.helper.create_share_contact_request(
            self.card21, self.business_card, self.card31
        )
        _, self.contact_share21331 = self.helper.create_share_contact_request(
            self.card21, self.scan_card, self.card31
        )

        _, self.contact_share12131 = self.helper.create_share_contact_request(
            self.personal_card, self.card21, self.card32
        )
        _, self.contact_share22131 = self.helper.create_share_contact_request(
            self.business_card, self.card21, self.card32
        )
        _, self.contact_share32131 = self.helper.create_share_contact_request(
            self.scan_card, self.card21, self.card32
        )

        _, self.contact_share21311 = self.helper.create_share_contact_request(
            self.card21, self.card31, self.personal_card
        )
        _, self.contact_share21312 = self.helper.create_share_contact_request(
            self.card21, self.card31, self.business_card
        )
        _, self.contact_share21313 = self.helper.create_share_contact_request(
            self.card21, self.card31, self.scan_card
        )

        _, self.note = self.helper.create_note(
            self.suser, self.contact121, "This is a test"
        )
        _, self.task = self.helper.create_task(
            self.suser, self.contact121, "This is a test"
        )

        self.helper.create_device(self.suser)
        self.helper.create_android_purchase(self.suser)
        self.helper.create_apple_account(self.suser)

    def test_delete_account(self):
        LUser(self.suser).delete()


class HardDeleteAccountTC(DeleteAccountTC):
    def test_delete_account(self):
        LUser(self.suser).delete(hard=True)
