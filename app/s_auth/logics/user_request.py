from datetime import datetime, timedelta

from django.core.exceptions import ObjectDoesNotExist

from helper.common import hashes, urls
from helper.exceptions import BadRequestException
from s_notifications.email.auth import AuthMailNotifier
from s_notifications.notifier import MailNotifier
from s_user.models import SUserRequests
from s_user.serializers import SUserRequestsSerializer


class UserRequest:
    WAITING_TIME = timedelta(seconds=30)
    NAX_REQUEST_PER_DAY = 10

    @classmethod
    def generate_request(
        cls,
        s_user,
        request_type,
        status=SUserRequests.INACTIVE,
        period: timedelta = None,
    ) -> SUserRequests:
        token = hashes.generate_v1(63)
        user_request_ser = SUserRequestsSerializer(
            data={
                "user": s_user.id,
                "token": token,
                "request": request_type,
                "status": status,
                "_expiration": (
                    datetime.utcnow() + (period or timedelta(days=1))
                ).timestamp(),
            }
        )
        user_request_ser.is_valid(raise_exception=True)
        user_request = user_request_ser.save()
        return user_request

    @classmethod
    def generate_url(cls, user_request: SUserRequests) -> str:
        return urls.generate_url("api", "auth", "verify", user_request.token)

    @classmethod
    def _check_max_request_perday(cls, s_user):
        request_count = SUserRequests.objects.filter(
            _created__gte=(datetime.utcnow() - timedelta(days=1)).timestamp(),
            user_id=s_user.id,
        ).count()
        if request_count > cls.NAX_REQUEST_PER_DAY:
            raise BadRequestException(
                errcode=400021,
                message="This account has reached maximum request today",
            )

    @classmethod
    def _send_verify(cls, s_user):
        user_request = cls.generate_request(s_user, SUserRequests.VERIFY_ACCOUNT)

        AuthMailNotifier().verify_account.send_to(
            s_user, link=urls.generate_url("api", "auth", "verify", user_request.token)
        )
        return user_request

    @classmethod
    def send_verify(cls, s_user):
        try:
            user_request: SUserRequests = SUserRequests.objects.get(
                _created__gte=(datetime.utcnow() - cls.WAITING_TIME).timestamp(),
                user_id=s_user.id,
                request=SUserRequests.VERIFY_ACCOUNT,
            )
            return user_request.token
        except ObjectDoesNotExist:
            cls._check_max_request_perday(s_user)
            return cls._send_verify(s_user)

    @classmethod
    def _send_reset_password(cls, s_user):
        user_request = cls.generate_request(s_user, SUserRequests.RESET_PASSWORD)

        AuthMailNotifier().reset_password.send_to(
            s_user, link=urls.generate_url("api", "auth", "verify", user_request.token)
        )
        return user_request

    @classmethod
    def send_reset_password(cls, s_user):
        try:
            user_request: SUserRequests = SUserRequests.objects.get(
                _created__gte=(datetime.utcnow() - cls.WAITING_TIME).timestamp(),
                user_id=s_user.id,
                request=SUserRequests.RESET_PASSWORD,
            )
            return user_request.token
        except Exception:
            cls._check_max_request_perday(s_user)
            return cls._send_reset_password(s_user)
