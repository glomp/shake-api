from rest_framework.authtoken.models import Token

from helper.exceptions import BadRequestException
from s_notifications.self_notification import AuthSelfNotification
from s_user.models import SUsers
from s_user.serializers import SUserSerializer


class UserAuth:
    ALLOW_STATUS = {SUsers.ACTIVE, SUsers.CARD_REQUIRED}

    @classmethod
    def validate_account(cls, s_user: SUsers):
        if s_user.status not in cls.ALLOW_STATUS:
            raise BadRequestException(
                errcode=400016,
                message="This account did not active yet!",
                data={"s_user_email": s_user.email},
            )

    @classmethod
    def login(cls, user_auth, s_user: SUsers = None):
        s_user = s_user or SUsers.objects.get(user_auth=user_auth)
        cls.validate_account(s_user)

        token, created = Token.objects.get_or_create(user=user_auth)
        # if not created:
        #     AuthSelfNotification(s_user).re_login.notify()
        #     # token = Token.objects.create(user=user_auth)

        return {
            "s_users": SUserSerializer(s_user).data,
            "token": token.key,
        }

    @classmethod
    def logout(cls, user_auth):
        token = Token.objects.get(user=user_auth)
        return token.delete()
