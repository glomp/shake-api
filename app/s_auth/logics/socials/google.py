import requests
from google.auth.transport import requests
from google.oauth2 import id_token

from django.conf import settings

from helper.exceptions import BadRequestException


class LGoogle:
    CLIENT_ID = settings.SOCIAL_AUTH_GOOGLE_CLIENT_ID

    def __init__(self, token):
        self.info = self.get_user_profile(token)

    @classmethod
    def get_user_profile(cls, token) -> dict:
        try:
            # Specify the CLIENT_ID of the app that accesses the backend:
            return id_token.verify_oauth2_token(
                token, requests.Request(), cls.CLIENT_ID
            )

            # Or, if multiple clients access the backend server:
            # idinfo = id_token.verify_oauth2_token(token, requests.Request())
            # if idinfo['aud'] not in [CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]:
            #     raise ValueError('Could not verify audience.')

            # If auth request is from a G Suite domain:
            # if idinfo['hd'] != GSUITE_DOMAIN_NAME:
            #     raise ValueError('Wrong hosted domain.')

            # ID token is valid. Get the user's Google Account ID from the decoded token.
        except Exception as e:
            raise BadRequestException(errcode=400017, message="Token is not correct")
