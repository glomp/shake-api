from s_auth.serializers import SAppleAccountSerializer
from ...models import MAppleAccount


class LApple:
    def __init__(self, apple_id: str):
        self.obj = self.get_user_from_database(apple_id)
        self.info = SAppleAccountSerializer(self.obj).data

    @classmethod
    def get_user_profile(cls, token) -> dict:
        pass

    @classmethod
    def get_user_from_database(cls, apple_id: str) -> MAppleAccount:
        return MAppleAccount.objects.get(id=apple_id)
