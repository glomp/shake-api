import requests
from json import loads

from django.conf import settings

from helper.exceptions import BadRequestException


class LFacebook:
    KEY = settings.SOCIAL_AUTH_FACEBOOK_KEY
    SECRET = settings.SOCIAL_AUTH_FACEBOOK_SECRET

    def __init__(self, token):
        self.info = self.get_user_profile(token)

    @classmethod
    def get_user_profile(cls, token) -> dict:
        response = requests.get(
            "https://graph.facebook.com/v8.0/me",
            params={
                "fields": ",".join(
                    [
                        "id",
                        "email",
                        "name",
                        "first_name",
                        "last_name",
                        "picture",
                    ]
                ),
                "access_token": token,
            },
        )

        if response.status_code == 200:
            content = loads(response.content)
            return content
        else:
            raise BadRequestException(
                errcode=400017,
                message="Token is not correct",
                data={
                    "status": response.status_code,
                    # "response": response.content,
                },
            )
