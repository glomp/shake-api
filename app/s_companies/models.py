from django.db import models

from custom_django.models import SModel, fields
from s_user.models import SUsers


class SCompanies(SModel):
    ACTIVE = "A"
    INACTIVE = "I"
    STATUS = (
        (ACTIVE, "ACTIVE"),
        (INACTIVE, "INACTIVE"),
    )

    name = models.CharField(max_length=127)
    status = models.CharField(max_length=1, default=ACTIVE, choices=STATUS)

    _created = fields.UnixTimeStampField(null=True, auto_now_add=True)
    _updated = fields.UnixTimeStampField(null=True, auto_now=True)

    class Meta:
        managed = True
        db_table = "s_company"


class SCompanyUsers(SModel):
    PENDING = "P"
    ACTIVE = "A"
    INACTIVE = "I"
    HOLD = "H"
    STATUS = (
        (PENDING, "PENDING"),
        (ACTIVE, "ACTIVE"),
        (HOLD, "HOLD"),
        (INACTIVE, "INACTIVE"),
    )

    ADMIN = "A"
    MEMBER = "M"
    ROLE = (
        (ADMIN, "ADMIN"),
        (MEMBER, "MEMBER"),
    )

    user = models.ForeignKey(SUsers, on_delete=models.CASCADE, null=True, blank=True)
    company = models.ForeignKey(SCompanies, on_delete=models.CASCADE)

    status = models.CharField(max_length=1, default=PENDING, choices=STATUS)
    role = models.CharField(max_length=1, default=MEMBER, choices=ROLE)

    balance_maximum = models.IntegerField(default=0)
    balance = models.IntegerField(default=0)

    _created = fields.UnixTimeStampField(null=True, auto_now_add=True)
    _updated = fields.UnixTimeStampField(null=True, auto_now=True)

    def get_readonly_fields(self, request, obj=None):
        return ["user", "company"] if obj else []

    class Meta:
        managed = True
        db_table = "s_company_user"
