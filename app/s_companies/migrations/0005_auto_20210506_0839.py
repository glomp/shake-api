# Generated by Django 2.2.19 on 2021-05-06 08:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('s_companies', '0004_auto_20210214_0158'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scompanyusers',
            name='user',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='s_user.SUsers'),
        ),
    ]
