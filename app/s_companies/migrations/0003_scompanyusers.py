# Generated by Django 2.2.17 on 2021-01-27 05:03

import django.db.models.deletion
from django.db import migrations, models

import custom_django.models.fields.unix_timestamp


class Migration(migrations.Migration):

    dependencies = [
        ("s_user", "0005_auto_20210106_0242"),
        ("s_companies", "0002_scompanies_name"),
    ]

    operations = [
        migrations.CreateModel(
            name="SCompanyUsers",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "status",
                    models.CharField(
                        choices=[
                            ("P", "PENDING"),
                            ("A", "ACTIVE"),
                            ("H", "HOLD"),
                            ("I", "INACTIVE"),
                        ],
                        default="P",
                        max_length=1,
                    ),
                ),
                (
                    "role",
                    models.CharField(
                        choices=[("A", "ADMIN"), ("M", "MEMBER")],
                        default="M",
                        max_length=1,
                    ),
                ),
                ("balance_maximum", models.IntegerField(default=0)),
                ("balance", models.IntegerField(default=0)),
                (
                    "_created",
                    custom_django.models.fields.unix_timestamp.UnixTimeStampField(
                        auto_now_add=True, null=True
                    ),
                ),
                (
                    "_updated",
                    custom_django.models.fields.unix_timestamp.UnixTimeStampField(
                        auto_now=True, null=True
                    ),
                ),
                (
                    "company",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="s_companies.SCompanies",
                    ),
                ),
                (
                    "user",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="s_user.SUsers",
                    ),
                ),
            ],
            options={"db_table": "s_company_user_copy", "managed": True,},
        ),
        migrations.RunSQL(
            """
INSERT INTO s_company_user_copy
(id, status, `role`, balance_maximum, balance, `_created`, `_updated`, company_id, user_id)

SELECT id, status, `role`, balance_maximum, balance, `_created`, `_updated`, company_id, user_id
from s_company_user scu 
            """
        ),
    ]
