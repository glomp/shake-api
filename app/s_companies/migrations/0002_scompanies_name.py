# Generated by Django 2.2.17 on 2020-12-28 09:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('s_companies', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='scompanies',
            name='name',
            field=models.CharField(default='Hactivate', max_length=127),
            preserve_default=False,
        ),
    ]
