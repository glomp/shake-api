from datetime import datetime

from django.db import transaction
from django.http.response import Http404
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated

from custom_django.views.viewsets import SDynamicModelViewSet
from helper.common import Search
from helper.exceptions import ForbiddenException
from s_user.models import SUsers
from .models import SCompanies, SCompanyUsers
from .serializers import SCompanySerializer, SCompanyUserSerializer


class SCompaniesViewSet(SDynamicModelViewSet):
    """Show all companies."""

    serializer_class = SCompanySerializer
    model_class = SCompanies
    queryset = SCompanies.objects.all()

    def retrieve(self, request, pk):
        return self.view.company.get(id=pk)

    def list(self, request):
        return self.view.company.all()

    @transaction.atomic
    def create(self, request):
        company_data = {"name": request.data["name"]}
        employee_data = {
            "user": request.data["user_id"],
            "role": request.data["role"],
        }
        if self.user_auth.is_superuser == False:
            return ForbiddenException(errcode=403062, message="You are not an admin")

        serializer = self.serializer_class(data=company_data)
        serializer.is_valid(raise_exception=True)
        instance = serializer.save()

        employee_data["company"] = instance.id
        employee_ser = SCompanyUserSerializer(data=employee_data)
        employee_ser.is_valid(raise_exception=True)
        employee_ser.save()
        return instance

    @transaction.atomic
    def update(self, request, pk, partial=True):
        company_data = {"name": request.data["name"]}
        if self.user_auth.is_superuser == False:
            return ForbiddenException(errcode=403063, message="You are not an admin")

        instance = self.view.company.get(id=pk)
        serialiser = self.serializer_class(instance, data=company_data)
        serialiser.is_valid(raise_exception=True)
        return serialiser.save()

    def destroy(self, request, pk):
        raise Http404


class SManagedCompaniesViewSet(SDynamicModelViewSet):
    """Show all managed companies."""

    serializer_class = SCompanySerializer
    model_class = SCompanies
    queryset = SCompanies.objects.all()

    def retrieve(self, request, pk):
        return self.view.company.i_managed.get(id=pk)

    def list(self, request):
        return self.view.company.i_managed.all()


# class SUserCreditsViewSet(SDynamicModelViewSet):
#     """Handles creating, creating and updating companies."""

#     serializer_class = SUserCreditsSerializer
#     queryset = {}
#     # queryset = SUserCredits.objects.all()
#     permission_classes = (
#         IsAuthenticated,
#         IsOwnerOrReadOnly,
#     )
#     authentication_classes = (TokenAuthentication,)

#     def update(self, request, pk=None, partial=False):

#         instance = self.queryset.get(pk=pk)

#         try:
#             add_value = request.data["remaining_credits"]
#             request.data["remaining_credits"] = int(
#                 request.data["remaining_credits"]
#             ) + int(instance.remaining_credits)

#             notification = {
#                 "status": "SUCCESS",
#                 "message": "You have succesfully recieved your credit point(s). Your new credit point(s) is "
#                 + str(request.data["remaining_credits"]),
#                 "recipients": instance.user.id,
#             }

#             notif_object = {
#                 "user": request.user.id,
#                 "notification": json.dumps(notification),
#                 "type": "add_credits",
#             }

#             s_notification = SUserNotificationsSerializer(data=notif_object)
#             s_notification.is_valid(raise_exception=True)
#             s_notification.save()

#             # add record to user credit logs
#             credit_log = {
#                 "label": "normal_credit",
#                 "value": add_value,
#                 "user_credit": instance.user.id,
#             }

#             s_credit_logs = SUserCreditLogsSerializer(data=credit_log)
#             s_credit_logs.is_valid(raise_exception=True)
#             s_credit_logs.save()

#         except:
#             pass

#         try:

#             add_value = request.data["rewards_claimed"]views
#             request.data["rewards_claimed"] = int(
#                 request.data["rewards_claimed"]
#             ) + int(instance.rewards_claimed)

#             notification = {
#                 "status": "SUCCESS",
#                 "message": "You have succesfully recieved your reward point(s). Your new reward point(s) is "
#                 + str(request.data["rewards_claimed"]),
#                 "recipients": instance.user.id,
#             }

#             notif_object = {
#                 "user": request.user.id,
#                 "notification": json.dumps(notification),
#                 "type": "add_rewards_claimed",
#             }

#             s_notification = SUserNotificationsSerializer(data=notif_object)
#             s_notification.is_valid(raise_exception=True)
#             s_notification.save()

#             # add record to user credit logs
#             credit_log = {
#                 "label": "rewards_claimed",
#                 "value": add_value,
#                 "user_credit": instance.user.id,
#             }

#             s_credit_logs = SUserCreditLogsSerializer(data=credit_log)
#             s_credit_logs.is_valid(raise_exception=True)
#             s_credit_logs.save()
#         except:
#             pass

#         try:

#             add_value = request.data["rewards_balance"]
#             request.data["rewards_balance"] = int(
#                 request.data["rewards_balance"]
#             ) + int(instance.rewards_balance)
#             notification = {
#                 "status": "SUCCESS",
#                 "message": "You have succesfully recieved your reward point(s). Your new reward balance is "
#                 + str(request.data["rewards_balance"]),
#                 "recipients": instance.user.id,
#             }

#             notif_object = {
#                 "user": request.user.id,
#                 "notification": json.dumps(notification),
#                 "type": "add_rewards_balance",
#             }

#             s_notification = SUserNotificationsSerializer(data=notif_object)
#             s_notification.is_valid(raise_exception=True)
#             s_notification.save()

#             # add record to user credit logs
#             credit_log = {
#                 "label": "rewards_balance",
#                 "value": add_value,
#                 "user_credit": instance.user.id,
#             }

#             s_credit_logs = SUserCreditLogsSerializer(data=credit_log)
#             s_credit_logs.is_valid(raise_exception=True)
#             s_credit_logs.save()

#         except:
#             pass

#         s_credits = self.serializer_class(instance, data=request.data)
#         s_credits.is_valid(raise_exception=True)
#         s_credits.save()

#         return Response(s_credits.data)


# class SUserCreditLogsViewSet(, SDynamicModelViewSet):
#     """Handles creating, creating and updating profiles."""

#     serializer_class = SUserCreditLogsSerializer
#     queryset = SCompanyUserCreditLogs.objects.all()
#     permission_classes = (
#         IsAuthenticated,
#         IsOwnerOrReadOnly,
#     )
#     authentication_classes = (TokenAuthentication,)
