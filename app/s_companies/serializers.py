from dynamic_rest.fields import DynamicRelationField
from dynamic_rest.serializers import DynamicModelSerializer

from .models import SCompanies, SCompanyUsers


class SCompanySerializer(DynamicModelSerializer):
    class Meta:
        model = SCompanies
        fields = "__all__"


class SCompanyUserSerializer(DynamicModelSerializer):
    class Meta:
        model = SCompanyUsers
        fields = "__all__"
