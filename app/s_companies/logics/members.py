from datetime import datetime, timedelta

from helper.common import hashes
from helper.exceptions import BadRequestException
from s_cards.logics import BusinessCard
from s_cards.models import SCards, SCompanyInvitations
from s_cards.serializers import SCompanyInvitationsSerializer
from s_notifications.notifier import MailNotifier
from s_properties.logics.property import CardProperty
from s_properties.models import SProperties
from s_user.models import SUsers
from ..models import SCompanies, SCompanyUsers


class CompanyMemberInvitation:
    ACCESS_CODE_LIVING_PERIOD = timedelta(days=3)

    def __init__(
        self,
        user: SUsers,
        company_admin: SCompanyUsers,
        company: SCompanies,
        template_card: SCards = None,
    ):
        self.user = user
        self.company_admin = company_admin
        self.company = company
        self.template_card = template_card

    def check_template(self, raise_exception: bool = True):
        if self.template_card is None:
            raise BadRequestException(
                errcode=400024,
                message="This template is not belong to this company!",
                data={"admin" "company_id": self.company.id},
            )

    def check_email(self, email):
        invitation_user = SUsers.objects.get_if_exist(user_auth__email=email)
        if (
            invitation_user is not None
            and SCompanyUsers.objects.filter(
                user=invitation_user.id, company=self.company.id
            ).exists()
        ):
            raise BadRequestException(
                errcode=400031,
                message="This email has already been registered to this company",
            )

        # @TODO: move this check to other place
        # email_property = SProperties.objects.get_if_exist(
        #     ~Q(card__user_id=self.user.id),
        #     property_type=SProperties.EMAIL,
        #     value=email,
        # )
        # if (
        #     email_property is not None
        #     and email_property.card.user_id is not None
        # ):
        #     raise ForbiddenException(
        #         errcode=403029,
        #         message="This email has already been registered by another user",
        #     )

    def make_invitation(
        self, email, data: dict, access_code: str = None
    ) -> SCompanyInvitations:
        access_code = (
            self.generate_invite_code() if access_code is None else access_code
        )

        invitation: SCompanyInvitations = SCompanyInvitations.objects.get_if_exist(
            email=email, card__template__company_id=self.company.id
        )
        if invitation is not None:
            invitation._expiration = (
                datetime.utcnow() + self.ACCESS_CODE_LIVING_PERIOD
            ).timestamp()
            invitation.access_code = access_code
            invitation.save()
        else:
            properties = data.pop("properties", [])
            if properties:
                properties_map = CardProperty.get_map_from_list_dict(properties)
                if "work email" not in properties_map:
                    properties.append(
                        {
                            "label": "work email",
                            "value": email,
                            "property_type": SProperties.EMAIL,
                            "mandatory": True,
                            "editable": False,
                            "searchable": True,
                        }
                    )

            new_company_user = SUsers
            new_company_user.id = None
            new_business_card = BusinessCard(new_company_user).create(
                self.company,
                self.template_card,
                raise_exception=False,
                properties=properties,
                **data
            )
            invitation_data = {
                "card": new_business_card.id,
                "email": email,
                "access_code": access_code,
                "inviter": self.company_admin.id,
                "_expiration": (
                    datetime.utcnow() + self.ACCESS_CODE_LIVING_PERIOD
                ).timestamp(),
            }
            invitation_ser = SCompanyInvitationsSerializer(data=invitation_data)
            invitation_ser.is_valid(raise_exception=True)
            invitation = invitation_ser.save()
        return invitation

    @classmethod
    def generate_invite_code(cls):
        all_codes = SCompanyInvitations.objects.distinct().values("access_code")
        all_codes = {row["access_code"] for row in all_codes}

        access_code = hashes.generate_v2()
        while access_code in all_codes:
            access_code = hashes.generate_v2()
        return access_code
