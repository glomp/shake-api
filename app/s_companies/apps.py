# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class SCompaniesConfig(AppConfig):
    name = 's_companies'
