from custom_django.test.base import BaseTestCase
from custom_django.test.request_data import NoneRequest, TestRequestData
from s_companies.models import SCompanyUsers
from .views import SCompaniesViewSet, SManagedCompaniesViewSet


class CompanyTestCase(BaseTestCase):
    ViewSet = SCompaniesViewSet
    FOR_ADMIN = True

    def create_test_objects(self):
        self.test_data, self.test_obj = self.helper.create_company("Hactivate")
        _, self.company_employee = self.helper.create_company_employee(
            self.suser, self.test_obj
        )

    def compare_result(self, origin, result) -> bool:
        return origin["name"] == result["name"]

    def test_retrieve(self):
        self.default_test_retrieve()

    def test_list(self):
        self.default_test_list()

    def test_create(self):
        self.default_test_create(
            TestRequestData(
                {
                    "name": "Shake",
                    "user_id": self.suser.id,
                    "role": SCompanyUsers.ADMIN,
                },
                self.user,
            ),
            {"name": "Shake", "user_id": self.suser.id, "role": SCompanyUsers.ADMIN},
        )

    def test_update(self):
        self.default_test_update(
            TestRequestData({"name": "Shake"}, self.user), {"name": "Shake"}
        )


class ManagedCompanyTestCase(BaseTestCase):
    ViewSet = SManagedCompaniesViewSet

    def create_test_objects(self):
        self.test_data, self.test_obj = self.helper.create_company("Hactivate")
        _, self.company_admin = self.helper.create_company_admin(
            self.suser, self.test_obj
        )

    def compare_result(self, origin, result) -> bool:
        return origin["name"] == result["name"]

    def test_retrieve(self):
        self.default_test_retrieve()

    def test_list(self):
        self.default_test_list()
