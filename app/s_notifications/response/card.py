from ..base.response import Response


class CardResponse(Response):
    @property
    def send_card(self):
        self.success = "Card sent."
