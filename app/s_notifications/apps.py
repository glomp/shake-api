from django.apps import AppConfig


class SNotificationsConfig(AppConfig):
    name = 's_notifications'
