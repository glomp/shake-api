from __future__ import annotations

from typing import List

from helper.exceptions import AppException
from s_cards.models import SCards
from s_companies.models import SCompanies
from s_user.models import SUsers
from ..models import SNotifications
from ..notifier import FCMNotifier
from .record import Record


class Notification:
    DEFAULT_RECORD_TYPE = SNotifications.SYSTEM
    DEFAULT_RECORD_SUBJECT = "{by_user}"

    DEFAULT_IS_SAVE = False
    DEFAULT_IS_NOTIFY = False
    DEFAULT_ANDROID_CHANNEL_ID = "AppChannel"

    DEFAULT_TARGET_MODEL: str = "notification"

    NEED_CARD = {SNotifications.USER, SNotifications.MYSELF}

    def __init__(
        self, data: dict = None, target_card: SCards = None, target_id: int = None
    ):
        self.data = data
        self.target_card = target_card
        self.target_model = self.DEFAULT_TARGET_MODEL
        self.target_id = target_id

        self.subject: str = self.DEFAULT_RECORD_SUBJECT
        self.message: str = None

        self.is_save: bool = self.DEFAULT_IS_SAVE
        self.is_notify: bool = self.DEFAULT_IS_NOTIFY
        self._record: Record = None
        # must define this if this is a special notification
        self.android_channel_id: str = self.DEFAULT_ANDROID_CHANNEL_ID

        self.action: str = None
        self._by_card: SCards = None

        # For serialize message
        self.default_kwargs: dict = {}

    def _check_data(self):
        check_map = {
            "Missing subject": self.subject is not None,
            "Missing message": self.message is not None,
            "Missing action": self.action is not None,
            "Missing by_card": self.DEFAULT_RECORD_TYPE not in self.NEED_CARD
            or self._by_card is not None,
        }
        if not all(check_map.values()):
            errors = [key for key, value in check_map.items() if value is False]
            raise AppException(
                errcode=500084,
                err_message="Data is not correct.",
                data={"errors": errors},
            )

    def _check_users(self, *to_users):
        if not self._by_card:
            return
        for user in to_users:
            if user.id == self._by_card.user_id:
                raise AppException(
                    errcode=500083,
                    err_message="Must send notification to the other user.",
                    data={"user_id": user.id, "self._by_card_id": self._by_card.id},
                )

    def by(self, card: SCards) -> Notification:
        if self.target_card is None and self._record is not None:
            self.target_card = card
            self._record.target_card = card
        self._by_card = card
        return self

    def serialized_data(self) -> dict:
        return {"action": self.action, "data": self.data}

    def save_record(self, message=None, subject=None, type=None):
        self.is_save = True
        self._record = Record(
            subject=subject or self.DEFAULT_RECORD_SUBJECT,
            message=message or self.message,
            type=type or self.DEFAULT_RECORD_TYPE,
            action=self.action,
            target_card=self.target_card or self._by_card,
            target_model=self.target_model,
            target_id=self.target_id,
        )

    def notify(
        self, *to_users: List[SUsers], save: bool = None, notify: bool = None, **kwargs
    ) -> int:
        self._check_data()
        self._check_users(*to_users)

        message_notifier = FCMNotifier(
            self.subject, self.message, self.serialized_data()
        )

        android_channel_id = kwargs.get("android_channel_id", self.android_channel_id)
        is_save = save if save is not None else self.is_save
        is_silent = not (notify if notify is not None else self.is_notify)

        # We only notify silent notification (data notification)
        # when that notification is saved
        kwargs["data_only"] = kwargs.get("data_only", is_save and is_silent)
        if is_save:
            self._record.by(self._by_card).save(
                *to_users, default_kwargs=self.default_kwargs, **kwargs
            )
        return message_notifier.by(self._by_card).send_to(
            *to_users,
            default_kwargs=self.default_kwargs,
            fcm_kwargs=dict(
                android_channel_id=android_channel_id,
            ),
            **kwargs
        )


class SelfNotification(Notification):
    DEFAULT_RECORD_TYPE = SNotifications.MYSELF
    DEFAULT_RECORD_SUBJECT = "You"

    def __init__(
        self,
        user: SUsers,
        data: dict = None,
        target_card: SCards = None,
        target_id: int = None,
    ):
        self.user = user
        super().__init__(data=data, target_card=target_card, target_id=target_id)

    def _check_users(self, *to_users):
        if not self._by_card:
            return
        for user in to_users:
            if user.id != self._by_card.user_id:
                raise AppException(
                    errcode=500085,
                    err_message="Must send notification to the current user.",
                    data={"user_id": user.id, "self._by_card_id": self._by_card.id},
                )

    def by(self, card: SCards) -> SelfNotification:
        return super().by(card)

    def notify(self, save: bool = None, notify: bool = None, **kwargs) -> int:
        return super().notify(self.user, save=save, notify=notify, **kwargs)


class CompanyNotification(Notification):
    DEFAULT_RECORD_TYPE = SNotifications.COMPANY

    def __init__(
        self,
        company: SCompanies,
        data: dict = None,
        target_card: SCards = None,
        target_id: int = None,
    ):
        self.company = company
        super().__init__(data=data, target_card=target_card, target_id=target_id)

    def _check_users(self, *to_users):
        pass

    def by(self, card: SCards) -> CompanyNotification:
        return super().by(card)

    def notify(self, save: bool = None, notify: bool = None, **kwargs) -> int:
        return super().notify(save=save, notify=notify, company=self.company, **kwargs)
