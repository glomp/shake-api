from __future__ import annotations

from typing import List

from django.conf import settings

from s_cards.models import SCards
from s_user.models import SUsers


class Notifier:
    IS_TEST = settings.ENV == "TEST"

    def __init__(self, subject: str, message: str):
        self.subject = subject
        self.message = message
        self._by_card: SCards = None

        self.default_kwargs: dict = {"link_down_app": settings.LINK_DOWNLOAD_APP}

    def by(self, card: SCards) -> Notifier:
        self._by_card = card
        return self

    def send_to(self, *to_users: List[SUsers], save=True) -> int:
        return sum(
            [
                self.send(
                    to_user,
                    self.subject,
                    self.message,
                    by_card=self._by_card,
                    save=save,
                )
                for to_user in to_users
            ]
        )

    @classmethod
    def send(
        cls,
        to_user: SUsers,
        subject: str,
        message: str,
        *args,
        by_card: SCards = None,
        default_kwargs: dict = None,
        **kwargs
    ) -> int:
        return 0
