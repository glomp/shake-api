from typing import Tuple, Union

from s_cards.models import SCards
from s_user.models import SUsers


def serialize_string(
    message: str,
    default_kwargs: dict = None,
    to_user: Union[SUsers, str, Tuple[str, str]] = None,
    by_card: Union[SCards, str] = None,
    **kwargs
) -> str:
    keyword = default_kwargs.copy() if default_kwargs is not None else dict()
    if to_user is not None:
        keyword["to_user"] = (
            to_user.name
            if isinstance(to_user, SUsers)
            else to_user[0]
            if isinstance(to_user, tuple)
            else to_user
        )
    if by_card is not None:
        keyword["by_user"] = (
            by_card.user_name if isinstance(by_card, SCards) else by_card
        )

    kwargs = {key: value for key, value in kwargs.items() if value is not None}
    keyword.update(kwargs)
    return message.format(**keyword)
