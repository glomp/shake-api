from __future__ import annotations

from typing import List

from s_cards.models import SCards
from s_companies.models import SCompanies
from s_user.models import SUsers
from ..models import SNotifications
from ..notifier import FCMNotifier
from ..serializers import SFullNotificationSerializer
from .common import serialize_string


class Record:
    def __init__(
        self,
        subject: str,
        message: str,
        type: str,
        action: str = "NO_ACTION",
        noti_objs: List[SNotifications] = None,
        target_card: SCards = None,
        target_model: str = None,
        target_id: int = None,
    ):
        self.subject = subject
        self.message = message
        self.type = type
        self.action = action

        self.target_card = target_card
        self.target_model = target_model
        self.target_id = target_id

        self.objects = noti_objs or []
        self._by_card: SCards = None

    def by(self, by_card: SCards) -> Record:
        self._by_card = by_card
        return self

    def save(
        self,
        *to_users: List[SUsers],
        company: SCompanies = None,
        default_kwargs=None,
        **kwargs,
    ):
        if self.type == SNotifications.COMPANY:
            return self.save_to_company(
                company, default_kwargs=default_kwargs, **kwargs
            )
        elif self.type == SNotifications.USER:
            return self.save_to_user(
                *to_users, company=company, default_kwargs=default_kwargs, **kwargs
            )
        else:
            if len(to_users) == 0:
                to_users = [None]
            return self.save_by_system(
                *to_users, company=company, default_kwargs=default_kwargs, **kwargs
            )

    def save_to_company(
        self,
        company: SCompanies = None,
        default_kwargs=None,
        **kwargs,
    ):
        assert company is not None, f"Company can't be None"
        noti = self._save(
            None, company=company, default_kwargs=default_kwargs, **kwargs
        )
        self.objects.append(noti)
        return noti

    def save_to_user(
        self,
        *to_users: List[SUsers],
        company: SCompanies = None,
        default_kwargs=None,
        **kwargs,
    ):
        assert self._by_card is not None, f"By user can't be None"
        return self.save_by_system(
            *to_users, company=company, default_kwargs=default_kwargs, **kwargs
        )

    def save_by_system(
        self,
        *to_users: List[SUsers],
        company: SCompanies = None,
        default_kwargs=None,
        **kwargs,
    ):
        notis = []
        for to_user in to_users:
            notis.append(
                self._save(
                    to_user, company=company, default_kwargs=default_kwargs, **kwargs
                )
            )
        self.objects.extend(notis)
        return notis

    def _save(
        self,
        to_users: SUsers,
        company: SCompanies = None,
        default_kwargs=None,
        **kwargs,
    ):
        subject, message = self._serialize(
            None, default_kwargs=default_kwargs, **kwargs
        )
        return SNotifications.objects.create(
            subject=subject,
            content=message,
            user=to_users,
            company=company,
            noti_type=self.type,
            action=self.action,
            target_card=self.target_card,
            target_model=self.target_model,
            target_id=self.target_id,
            by_card=self._by_card,
        )

    def _serialize(self, to_user, default_kwargs=None, **kwargs):
        subject = serialize_string(
            self.subject,
            default_kwargs=default_kwargs,
            to_user=to_user,
            by_card=self._by_card,
            **kwargs,
        )
        message = serialize_string(
            self.message,
            default_kwargs=default_kwargs,
            to_user=to_user,
            by_card=self._by_card,
            **kwargs,
        )
        return subject, message
