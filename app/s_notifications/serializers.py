from dynamic_rest.fields import DynamicRelationField
from dynamic_rest.serializers import DynamicModelSerializer
from rest_framework import serializers

from s_cards.serializers import SPublicCardSerializer
from .models import SNotifications


class SViewNotificationSerializer(DynamicModelSerializer):
    class Meta:
        model = SNotifications
        exclude = SNotifications.PERSONAL_FIELDS


class SNotificationSerializer(DynamicModelSerializer):
    class Meta:
        model = SNotifications
        fields = "__all__"


class SFullNotificationSerializer(DynamicModelSerializer):
    by_card = DynamicRelationField(SPublicCardSerializer, embed=True)

    class Meta:
        model = SNotifications
        fields = "__all__"
