from .auth import AuthSelfNotification
from .card import CardSelfNotification
from .contact import ContactRequestSelfNotification
from .user import UserSelfNotification

__all__ = (
    "AuthSelfNotification",
    "UserSelfNotification",
    "CardSelfNotification",
    "ContactRequestSelfNotification",
)
