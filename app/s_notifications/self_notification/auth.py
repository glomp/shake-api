from ..base.notification import SelfNotification
from ..models import SNotifications


class AuthSelfNotification(SelfNotification):
    DEFAULT_RECORD_TYPE = SNotifications.SYSTEM
    DEFAULT_TARGET_MODEL = "auth"

    @property
    def verified(self) -> SelfNotification:
        self.action = "ACCOUNT_VERIFIED"
        self.subject = "Let's Shake"
        self.message = "Hi {to_user}! Your account is verified. Let's Shake."
        return self

    @property
    def activated(self) -> SelfNotification:
        self.action = "ACCOUNT_ACTIVATED"
        self.subject = "Let's Shake"
        self.message = "Hi {to_user}! Your account is activated. Let's Shake."
        return self

    @property
    def reset_password(self) -> SelfNotification:
        self.action = "ACCOUNT_RESET_PASSWORD"
        self.subject = "Let's Shake"
        self.message = (
            "Hi {to_user}! Your account is required a new password. Let's Shake."
        )
        return self

    @property
    def re_login(self) -> SelfNotification:
        self.action = "ACCOUNT_RE_LOGIN"
        self.subject = "Warning"
        self.message = "Hi {to_user}! Your account is signed in on another device."
        return self

    @property
    def welcome(self) -> SelfNotification:
        self.action = "WELCOME"
        self.subject = "Welcome to Shake"
        self.message = """Thanks for signing up to shake and welcome! shake makes exchanging of cards easy. Its time saving productivity features and the ease of sending your digital cards to new and existing contacts will be a great benefit to you.
To get started simply create your personal card or scan your business card then start sharing your card!
To see how it works, simply take the tour now.
Enjoy!"""
        return self
