from ..base.notification import SelfNotification, SNotifications


class CardSelfNotification(SelfNotification):
    DEFAULT_TARGET_MODEL = "card"

    @property
    def updated_card(self):
        self.action = "MY_CARD_UPDATED"
        self.message = "Have updated your {card_name} card."
        # self.default_kwargs = {"card_name": "personal"}

        self.save_record()
        return self

    @property
    def delete_card(self):
        self.action = "MY_CARD_DELETE"
        self.message = (
            "A confirmation email has been sent to you. "
            "Please verify to complete the deletion."
        )

        self.save_record()
        return self

    @property
    def deleted_card(self):
        self.DEFAULT_RECORD_TYPE = SNotifications.SYSTEM

        self.action = "MY_CARD_DELETED"
        self.message = "Have deleted your {card_name} card."

        self.save_record(type=SNotifications.SYSTEM)
        return self
