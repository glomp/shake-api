from ..base.notification import SelfNotification


class ContactSelfNotification(SelfNotification):
    DEFAULT_TARGET_MODEL = "contact"

    @property
    def received(self):
        self.target_model = "contact"
        self.action = "MY_CONTACT_RECEIVED"
        self.message = "Received a {card_name} card from {sender}."
        # self.default_kwargs = {"card_name": "personal", "sender": "another user"}

        self.save_record()
        return self


class ContactRequestSelfNotification(SelfNotification):
    DEFAULT_TARGET_MODEL = "contact_request"

    @property
    def created_request(self):
        self.action = "MY_CONTACT_REQUEST_CREATED"
        self.message = "Have sent your {card_name} card to {recipient}."
        # self.default_kwargs = {"card_name": "personal", "recipient": "another user"}

        self.save_record()
        return self

    # @property
    # def confirmed_request(self):
    #     self.action = "MY_CONTACT_REQUEST_CONFIRMED"
    #     self.message = "Received a {card_name} card from {sender}."
    #     # self.default_kwargs = {"card_name": "personal", "sender": "another user"}

    #     self.save_record()
    #     return self

    @property
    def declined_request(self):
        self.action = "MY_CONTACT_REQUEST_DECLINED"
        self.message = "Declined a {card_name} card from {sender}."
        # self.default_kwargs = {"card_name": "personal", "sender": "another user"}

        self.save_record()
        return self


class ShareContactRequestSelfNotification(SelfNotification):
    DEFAULT_TARGET_MODEL = "share_contact_request"

    @property
    def created_share(self):
        self.action = "MY_SHARE_CONTACT_REQUEST_CREATED"
        self.message = "Have share {assignee}'s card to {recipient}."
        # self.default_kwargs = {"assignee": "a user", "recipient": "another user"}

        self.save_record()
        return self
