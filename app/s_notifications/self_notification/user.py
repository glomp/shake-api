from ..base.notification import SelfNotification


class UserSelfNotification(SelfNotification):
    DEFAULT_TARGET_MODEL = "user"

    @property
    def updated_user(self):
        self.action = "MY_USER_UPDATED"
        self.message = "Have updated your personal infomation."

        self.save_record()
        return self

    @property
    def delete_user(self):
        self.action = "MY_USER_DELETE"
        self.message = (
            "A confirmation email has been sent to you. "
            "Please verify to complete the account closure."
        )

        self.save_record()
        return self

    @property
    def updated_password(self):
        self.action = "MY_PASSWORD_UPDATED"
        self.message = "Have updated your password."

        self.save_record()
        return self
