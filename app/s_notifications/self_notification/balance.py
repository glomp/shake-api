from ..base.notification import SelfNotification


class BalanceSelfNotification(SelfNotification):
    DEFAULT_TARGET_MODEL = "log"

    @property
    def added_credits(self):
        self.action = "MY_BALANCE_ADDED"
        self.message = "Have added {amount} {credit} to your {card_name} card."

        self.save_record()
        return self

    @property
    def added_company_credits(self):
        self.action = "MY_BALANCE_BUSINESS_ADDED"
        self.message = (
            "Have added {amount} company's {credit} to your {card_name} card."
        )

        self.save_record()
        return self
