from ..email_template.letter import UserLetterTemplate
from ..notifier import SendgridMailNotifier


class CompanyMailNotifier(SendgridMailNotifier):
    TEMPLATE = UserLetterTemplate

    @property
    def invite_employee(self) -> SendgridMailNotifier:
        self.subject = (
            "{company_name} is now using Shake for better contact management."
        )
        self.message = """
{Shake} is a premium contact exchange and storage app platform that allows you to \
quickly and easily exchange your business card digitally with business partners \
and clients either offline or online. Making it easier for you to save and access \
your contacts, {Shake} also means that you’ll always have your contacts updated \
information should they change their phone number, etc. {Shake} is easy to use and \
will mean that you will never be caught without your business card.

{Shake} is now {company_name}’s preferred method of business card exchange so please \
<a href="{link_down_app}">click here</a> to download and activate your account.

Your access code is {access_code}.

{admin_name} {admin_job_title}
"""
        self.default_kwargs.update(
            company_name="Your company",
            admin_name="",
            admin_job_title="Administrator",
        )
        return self
