from ..email_template.letter import UserLetterTemplate
from ..notifier import SendgridMailNotifier


class InvitationNotifier(SendgridMailNotifier):
    TEMPLATE = UserLetterTemplate
    #     @property
    #     def invited_by_partner_conference(self) -> SendgridMailNotifier:
    #         self.subject = "[LetsShake] Welcome"
    #         self.message = """
    # Thanks for signing up to Shake and welcome. Shake makes exchanging of \
    # cards easy. It's time saving productivity features and the ease of sending your \
    # digital cards to new and existing contacts will be a great benefit to you and \
    # your company.

    # Simply scan your printed card to get started then start sharing your card with \
    # anyone you meet at this conference for free.

    # To see how it works, simply take the tour now.

    # Enjoy.
    # """
    #         return self

    #     @property
    #     def invited_by_partner_conference_is_not_yet_client(self) -> SendgridMailNotifier:
    #         self.subject = "[LetsShake] Welcome"
    #         self.message = """
    # We trust that you’ve enjoyed using the Shake app, it's time saving productivity \
    # features and the ease of sending your digital cards to new and existing contacts.

    # Now that the conference is over, we hope you continue to use Shake as a \
    # paying customer. However we would like to allow you to continue to send your \
    # cards for free for 30 days or until your company signs up. All we ask is that \
    # you share with us, the name, email and phone number of your company \
    # colleague who currently organizes your printed cards so that we can introduce \
    # Shake to your entire company. Click <a href="{link}">here</a> to submit.
    # """

    #     @property
    #     def invited_by_user_is_not_yet_client(self) -> SendgridMailNotifier:
    #         self.subject = "[LetsShake] Welcome"
    #         self.message = """
    # We trust that you’ve enjoyed using the Shake app, it’s time saving productivity \
    # features and the ease of sending your digital cards to new and existing contacts.

    # You have now used almost all of your free Shake business card credits. \
    # However, we would like to allow you to continue to send your cards for free for \
    # 30 days or until your company signs up. All we ask is that you share with us, \
    # the name, email and phone number of your company colleague who currently \
    # organizes your printed cards so that we can introduce Shake to your entire \
    # company. And once they sign up, your cards will be paid for by the company \
    # as it should be. Click <a href="{link}">here</a> to submit.
    # """

    #     @property
    #     def invited_by_user_is_not_yet_client(self) -> SendgridMailNotifier:
    #         self.subject = "[LetsShake] Welcome"
    #         self.message = """
    # We trust that you’ve enjoyed using the Shake app, it’s time saving productivity \
    # features and the ease of sending your digital cards to new and existing contacts.

    # You have now used almost all of your free Shake business card credits. However, \
    # we would like to allow you to continue to send your cards for free for 30 days \
    # or until your company signs up. All we ask is that you share with us the name, email \
    # and phone number of your company colleague who currently organizes your printed cards \
    # so that we can introduce Shake to your entire company. And once they sign up, \
    # your cards will be paid for by the company as it should be. \
    # Click <a href="{link}">here</a> to submit.
    # """

    @property
    def invited_by_user_is_not_yet_client(self) -> SendgridMailNotifier:
        self.subject = "Continue to use your Shake business card for free."
        self.message = """
We trust that you’ve enjoyed using the {Shake} app, it's time saving productivity \
features and the ease of sending your digital cards to new and existing contacts.

You have now used almost all of your free {Shake} business card credits \
however, we would like to allow you to continue to send your cards for free for \
30 days or until your company signs up. All we ask is that you share with us, \
the name, email and phone number of your company colleague who currently \
organizes your printed cards so that we can introduce {Shake} to your entire \
company. And once they sign up, your cards will be paid for by the company \
as it should be.

Simply submit the details <a href="{link}">here</a>. We look forward to continuing to serve you.


Your access code is {access_code}.

{admin_name}
{admin_job_title}
"""
