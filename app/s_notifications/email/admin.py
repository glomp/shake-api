from django.conf import settings

from ..notifier import SendgridMailNotifier


class AdminMailNotifier(SendgridMailNotifier):
    HTML_MESSAGE = False

    @property
    def exception(self) -> SendgridMailNotifier:
        self.subject = (
            f"{settings.EMAIL_SUBJECT_PREFIX} Application Error {{err_brief}}"
        )
        self.message = """
env: {env}
version: {version}
release_time: {release_time}
--------------------------

git_branch: {git_branch}
git_hash_short: {git_hash_short}
git_hash: {git_hash}
git_commit_message: {git_commit_message}
--------------------------

err_code: {errcode}
err_class: {err_class}
err_message: {err_message}
--------------------------

request_data: {request_data}
--------------------------

data: {data}

--------------------------
traceback:
{traceback}
"""
        return self
