from __future__ import annotations

from django.conf import settings

from ..email_template.letter import UserLetterTemplate
from ..notifier import MailNotifier, SendgridMailNotifier


class AuthMailNotifier(SendgridMailNotifier):
    TEMPLATE = UserLetterTemplate

    @property
    def verify_account(self) -> MailNotifier:
        self.subject = "Verify Shake account"
        self.message = """
Thank you for choosing {Shake}.
Please verify your account by clicking <a href="{link}">here</a>.
"""
        return self

    @property
    def reset_password(self) -> MailNotifier:
        self.subject = "Reset password"
        self.message = """
A password request was made for your {Shake} account.
Simply <a href="{link}">click here</a> to reset your password.”
"""
        return self

    @property
    def welcome(self) -> MailNotifier:
        self.subject = "Welcome to Shake"
        self.message = """
Thanks for signing up to {Shake} and welcome! {Shake} makes exchanging cards easy. It's time saving productivity features and the ease of sending your digital cards to new and existing contacts will be a great benefit to you and your company. To get started simply scan your business card then start sharing your card!

If you want to use it for personal reasons, create your card and start sharing.

To see how it works, simply <a href="{walkthrough_link}">take the tour</a> now.

Enjoy!
"""
        return self


class BossMailNotifier(SendgridMailNotifier):
    @property
    def register_for_company(self) -> BossMailNotifier:
        self.subject = "[Shake] [Business] {company_name}'s registration"
        self.message = """
This is a registration from a company.

Company name: {company_name}
First name: {first_name}
Last name: {last_name}
Job title: {job_title}
Email: {email}
Phone: {phone}
{other}
"""
        self.default_kwargs.update(other="")
        return self

    def send_to_bosses(self, **kwargs):
        super(BossMailNotifier, self).send_to(*settings.BOSS_EMAILS, **kwargs)
