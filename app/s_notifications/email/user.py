from ..email_template.letter import UserLetterTemplate
from ..notifier import SendgridMailNotifier


class UserMailNotifier(SendgridMailNotifier):
    TEMPLATE = UserLetterTemplate

    @property
    def confirm_delete_account(self) -> SendgridMailNotifier:
        self.subject = "Confirm delete account"
        self.message = """
We’ve received a request to delete your account. \
Please verify to complete the deletion by clicking <a href="{link}">here</a>.
"""
        return self
