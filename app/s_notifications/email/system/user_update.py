from ...notifier import MailNotifier


class UserUpdateNotifier(MailNotifier):
    @property
    def update(self) -> MailNotifier:
        self.subject = "Your monthly update"
        self.message = """
Hi {user_name},

We hope you enjoyed a great month both professionally and socially and that \
Shake, had a helping hand with that.

The Shake community has been growing rapidly and in the past month, we saw a \
growth of {past_month_growth}%. In total, {total_past_month_business_cards_sent} \
business cards were sent by Shake users as well as \
{total_past_month_personal_cards_sent} personal cards. Users not only exchanged \
cards when meeting in person but many were connected via our search and \
recommendations.

As for your own usage in the month of {past_month}:

{user_past_month_business_cards_sent} business cards sent.
{user_past_month_business_cards_received} business cards received.
{user_past_month_personal_cards_sent} personal cards sent.
{user_past_month_personal_cards_received} business cards received.
{user_past_month_rewards_points_earned} reward points earned.

Thanks for your support in {past_month} and we hope you continue to enjoy \
your experience with Shake.

Best,

The Shake team
"""
        return self
