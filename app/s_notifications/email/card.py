from ..email_template.letter import UserLetterTemplate
from ..notifier import SendgridMailNotifier


class CardMailNotifier(SendgridMailNotifier):
    TEMPLATE = UserLetterTemplate

    @property
    def send_vcf(self) -> SendgridMailNotifier:
        self.subject = "You have new contact"
        self.message = """
{by_user} has shared their contact details to you via {Shake}. \
{Shake} is a premium contact exchange and storage app platform \
that allows you to quickly and easily exchange your phone number \
and email or more with people you meet either offline or online.

Fast and easy, use {Shake} to exchange contact details. Simply open the app, \
{Shake} your phones (like a handshake) and in a blink of an eye you’re connected. \
Then whenever you want to, you can contact each other again. And share as much \
or as little information about yourself as you like. Click here to download and join.
"""
        return self

    @property
    def delete_card(self) -> SendgridMailNotifier:
        self.subject = "Deletion confirmation"
        self.message = """
We’ve received a request to delete your {card_name} card. \
Please verify to complete the deletion by clicking <a href="{link}">here</a>..
"""
        return self
