from ..base.notification import Notification, SNotifications


class CardNotification(Notification):
    DEFAULT_RECORD_TYPE = SNotifications.USER
    DEFAULT_TARGET_MODEL = "card"

    @property
    def updated_card(self) -> Notification:
        self.action = "CARD_UPDATED"
        self.subject = "A card is updated."
        self.message = "{by_user}'s card has been updated."
        # self.default_kwargs = {"by_user": "Another user", "card_name": "personal"}

        self.save_record("Has updated their {card_name} card.")
        return self

    @property
    def deleted_card(self) -> Notification:
        self.DEFAULT_RECORD_TYPE = SNotifications.SYSTEM

        self.action = "CARD_DELETED"
        self.subject = "A card is deleted."
        self.message = "{by_user}'s card has been deleted."
        # self.default_kwargs = {"by_user": "Another user", "card_name": "personal"}

        self.save_record(
            "Has deleted their {card_name} card.", type=SNotifications.SYSTEM
        )
        return self
