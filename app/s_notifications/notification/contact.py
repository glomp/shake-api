from s_notifications.models import SNotifications
from ..base.notification import Notification


class ContactRequestNotification(Notification):
    DEFAULT_RECORD_TYPE = SNotifications.USER
    DEFAULT_TARGET_MODEL = "contact_request"

    @property
    def created_request(self) -> Notification:
        self.action = "NEW_CONTACT_RECEIVED"
        self.subject = "New card received"
        self.message = "You received a new card from {by_user}."
        # self.default_kwargs = {"by_user": "another user"}

        self.android_channel_id = "ContactReceiveChannel"
        self.is_notify = True
        self.save_record("Sent a card to you")
        return self

    @property
    def confirmed_request(self) -> Notification:
        self.action = "CONTACT_CONFIRMED"
        self.subject = "Your card is accepted"
        self.message = "Your card is accepted by {by_user}."
        # self.default_kwargs = {"by_user": "another user"}

        self.save_record("Received your card")
        return self

    @property
    def declined_request(self) -> Notification:
        self.action = "CONTACT_DECLINED"
        self.subject = "Your card is declined"
        self.message = "Your card is declined by {by_user}."
        # self.default_kwargs = {"by_user": "another user"}

        self.save_record("Declined your card")
        return self


class ShareContactRequestNotification(Notification):
    DEFAULT_RECORD_TYPE = SNotifications.USER
    DEFAULT_TARGET_MODEL = "share_contact_request"

    @property
    def created_share(self) -> Notification:
        self.action = "SHARE_CONTACT_REQUESTED"
        self.subject = "Your card is shared"
        self.message = "{by_user} want to share your card to {recipient}."
        # self.default_kwargs = {"by_user": "Someone", "recipient": "another one"}

        self.save_record("Want to share your card")
        self.is_notify = True
        return self
