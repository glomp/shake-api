from ..base.notification import Notification
from ..models import SNotifications


class AuthNotification(Notification):
    DEFAULT_RECORD_TYPE = SNotifications.SYSTEM
    DEFAULT_TARGET_MODEL = "auth"

    @property
    def deleted_account(self) -> Notification:
        self.action = "DELETE_ACCOUNT"
        self.subject = "An account is deleted"
        self.message = "{by_user}'s account has been deleted."
        # self.default_kwargs = {"by_user": "Another user"}

        self.save_record("Has deleted their account.")
        return self
