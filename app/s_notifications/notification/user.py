from ..base.notification import Notification
from ..models import SNotifications


class UserNotification(Notification):
    @property
    def welcome(self) -> Notification:
        self.action = "WELCOME"
        self.subject = "Welcome to Shake"
        self.message = """Thanks for signing up to shake and welcome! shake makes exchanging of cards easy. Its time saving productivity features and the ease of sending your digital cards to new and existing contacts will be a great benefit to you.
To get started simply create your personal card or scan your business card then start sharing your card!
To see how it works, simply take the tour now.
Enjoy!"""
        return self
