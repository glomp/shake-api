from ..base.notification import CompanyNotification


class BalanceCompanyNotification(CompanyNotification):
    DEFAULT_TARGET_MODEL = "log"

    @property
    def added_personal_credits(self):
        self.action = "PERSONAL_BALANCE_ADDED"
        self.message = "Have added {amount} {credit} to {card_name} card."

        self.save_record()
        return self

    @property
    def added_company_credits(self):
        self.action = "COMPANY_BALANCE_ADDED"
        self.message = "Have added {amount} {credit} to {card_name} card."

        self.save_record()
        return self
