# Generated by Django 2.2.19 on 2021-04-15 04:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('s_notifications', '0005_auto_20210324_0405'),
    ]

    operations = [
        migrations.AlterField(
            model_name='snotifications',
            name='noti_type',
            field=models.CharField(choices=[('S', 'SYSTEM'), ('U', 'USER'), ('O', 'OTHER'), ('M', 'MYSELF')], default='S', max_length=1),
        ),
    ]
