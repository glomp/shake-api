# Generated by Django 2.2.19 on 2021-05-18 07:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('s_notifications', '0006_auto_20210415_0419'),
    ]

    operations = [
        migrations.AlterField(
            model_name='snotifications',
            name='noti_type',
            field=models.CharField(choices=[('S', 'SYSTEM'), ('U', 'USER'), ('M', 'MYSELF'), ('C', 'COMPANY'), ('O', 'OTHER')], default='S', max_length=1),
        ),
    ]
