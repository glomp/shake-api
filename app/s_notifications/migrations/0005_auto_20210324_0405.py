# Generated by Django 2.2.17 on 2021-03-24 04:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('s_notifications', '0004_auto_20210324_0320'),
    ]

    operations = [
        migrations.RenameField(
            model_name='snotifications',
            old_name='object_id',
            new_name='target_id',
        ),
    ]
