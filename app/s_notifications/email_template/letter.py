from pathlib import Path


def load_file(file_name: str) -> str:
    with open(Path(__file__).parent.joinpath(file_name), "r+") as f:
        return f.read()


class LetterTemplate:
    LETTER_TEMPLATE = """
<html>
    <head>
        {header}
    </head>
    <body class="shake-body">
        {body}
    </body>
</html>
"""
    _HEADER = load_file("header.html")
    _LOGO = load_file("body_logo.html")
    _SIGNATURE = load_file("body_signature.html")

    def __init__(self, body: str, header: str = None):
        self._body = body
        self._header = header

    @classmethod
    def _build_letter(cls, body: str, header: str = None) -> str:
        return cls.LETTER_TEMPLATE.format(
            header=cls._HEADER if header is None else header,
            body=body,
        )

    def to_str(self) -> str:
        letter = self._build_letter(body=self._body, header=self._header)
        return letter


class UserLetterTemplate(LetterTemplate):
    BODY_TEMPLATE = """
<div class="shake-logo">
    {logo}
</div>

<div class="shake-letter">
    <p class="shake-letter-subject">
        {subject}
    </p>

    <p class="shake-letter-message">
        {message}
    </p>
</div>

<div class="shake-signature">
    {signature}
</div>
"""

    def __init__(self, subject: str, message: str):
        self._subject = subject
        self._message = message
        super().__init__(body=self._build_body(subject, message))

    @classmethod
    def _build_body(cls, subject: str, message: str) -> str:
        return cls.BODY_TEMPLATE.format(
            logo=cls._LOGO,
            subject=subject,
            message=message.replace("\n", "<br>"),
            signature=cls._SIGNATURE,
        )
