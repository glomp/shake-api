from ..notifier import MessageNotifier


class CardMessageNotifier(MessageNotifier):
    @property
    def send_vcf(self) -> MessageNotifier:
        self.message = """
{inviter_name}’s has shared their contact details to you via Shake. \
Shake is a great new app that allows you to quickly and easily exchange your \
phone number and email or more with people you meet either offline or online. \
Instead of making it a chore, just Shake and swap contact details. You both \
open the app, Shake your phones like a handshake and in a blink of an eye you’re \
connected. No worries about giving too much information about your friends, \
what you did last weekend, etc. Just so that if you so wish to, you can contact \
others again. To download and join click: {link}
"""
        return self
