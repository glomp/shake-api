from __future__ import annotations

import base64
import sendgrid
from email.mime.text import MIMEText
from sendgrid.helpers.mail import Attachment, Mail
from typing import List, Union

from django.conf import settings
from django.utils.html import strip_tags

from helper.logger_handler import slogger
from s_user.models import SUsers
from ..email_template.letter import LetterTemplate
from .email import MailNotifier


class SendgridMailNotifier(MailNotifier):
    FROM_EMAIL: str = sendgrid.Email(settings.EMAIL_HOST_USER, "Shake App")
    HTML_MESSAGE: bool = True
    TEMPLATE: LetterTemplate = None

    sg = sendgrid.SendGridAPIClient(api_key=settings.SENDGRID_API_KEY)

    @classmethod
    def send_to_email(
        cls,
        to_user_email: Union[str, tuple],
        subject: str,
        message: str,
        files: list = None,
        from_email: str = None,
        is_html: bool = HTML_MESSAGE,
    ) -> int:
        slogger.debug(f"Mail to: {to_user_email} about: '{subject}'")
        if cls.IS_TEST:
            return 1

        mail = cls._create_email(
            to_user_email, subject, message, files, from_email, is_html
        )
        response = cls.sg.send(mail)
        return response.status_code == 200

    @classmethod
    def _create_email(
        cls,
        to_user_email: Union[str, tuple],
        subject: str,
        message: str,
        files: list = None,
        from_email: str = FROM_EMAIL,
        is_html: bool = HTML_MESSAGE,
    ) -> Mail:
        from_email = from_email or cls.FROM_EMAIL
        assert to_user_email, "Email can't be empty"

        if is_html:
            html_message = MIMEText(message, "html")
            html_message, message = message, strip_tags(html_message)
            email = Mail(
                from_email=from_email,
                to_emails=[to_user_email],
                subject=subject,
                plain_text_content=message,
                html_content=html_message,
            )
        else:
            email = Mail(
                from_email=from_email,
                to_emails=[to_user_email],
                subject=subject,
                plain_text_content=message,
            )

        if files is not None:
            for _file in files:
                if isinstance(_file, str):
                    attachment = Attachment(
                        file_content=base64.b64encode(bytes(_file, "utf-8")).decode()
                    )
                elif isinstance(_file, tuple) and len(_file) == 3:
                    attachment = Attachment(
                        file_content=base64.b64encode(
                            bytes(_file[1], "utf-8")
                        ).decode(),
                        file_name=_file[0],
                        file_type=_file[2],
                    )
                else:
                    attachment = _file
                email.attachment = attachment
        return email
