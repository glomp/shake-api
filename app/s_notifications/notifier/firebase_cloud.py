from typing import List

from fcm_django.models import FCMDevice

from helper import slogger
from helper.encoder import CustomEncoder
from s_cards.models import SCards
from s_user.models import SUsers
from v_user.fcm import VUFCM
from ..base.common import serialize_string
from ..base.notifier import Notifier


class FCMNotifier(Notifier):
    def __init__(self, subject: str, message: str, data: dict = None, **kwargs):
        super().__init__(subject, message, **kwargs)
        self.data = data

    def send_to(
        self, *users: List[SUsers], default_kwargs=None, fcm_kwargs: dict, **kwargs
    ) -> int:
        return sum(
            [
                self.send(
                    user,
                    self.subject,
                    self.message,
                    by_card=self._by_card,
                    default_kwargs=default_kwargs or self.default_kwargs,
                    data=self.data,
                    fcm_kwargs=fcm_kwargs,
                    **kwargs
                )
                for user in users
            ]
        )

    @classmethod
    def send(
        cls,
        user: SUsers,
        subject: str,
        message: str,
        *args,
        by_card: SCards = None,
        default_kwargs: dict = None,
        data: dict = None,
        data_only: bool = False,
        fcm_kwargs: dict = None,
        **kwargs
    ) -> int:
        message = serialize_string(
            message,
            default_kwargs=default_kwargs,
            to_user=user,
            by_card=by_card,
            **kwargs
        )
        devices = list(VUFCM(user).with_badge.all()[:2])
        fcm_kwargs = fcm_kwargs or {}
        badge = devices[0].badge if (len(devices) > 0) else 0
        if data_only:
            return sum(
                [cls.send_data_to_fcm(device, data, **fcm_kwargs) for device in devices]
            )
        else:
            return sum(
                [
                    cls.send_to_fcm(
                        device, subject, message, data, badge=badge, **fcm_kwargs
                    )
                    for device in devices
                ]
            )

    @classmethod
    def send_to_fcm(
        cls, device: FCMDevice, subject: str, message: str, data: dict = None, **kwargs
    ) -> int:
        slogger.debug("Send-to-fcm [%r]: %r > %r", device, data["action"], kwargs)
        if cls.IS_TEST:
            return 1

        result = device.send_message(title=subject, body=message, data=data, **kwargs)
        if result["success"]:
            return 1
        else:
            # Should we delete it?
            # device.delete()
            return 0

    @classmethod
    def send_data_to_fcm(cls, device: FCMDevice, data: dict = None, **kwargs) -> int:
        slogger.debug("Send-data-to-fcm [%r]: %r > %r", device, data["action"], kwargs)
        if cls.IS_TEST:
            return 1

        result = device.send_data_message(data_message=data, json_encoder=CustomEncoder)
        if result["success"]:
            return 1
        else:
            # device.delete()
            return 0
