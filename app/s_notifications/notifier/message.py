# Download the helper library from https://www.twilio.com/docs/python/install
from typing import List

from django.conf import settings
from twilio.base.exceptions import TwilioRestException
from twilio.rest import Client
from twilio.rest.api.v2010.account.message import MessageInstance

from helper.exceptions import BadRequestException
from helper.logger_handler import slogger
from s_cards.models import SCards
from s_medias.logics.store import SStorage
from s_user.models import SUsers
from ..base.common import serialize_string
from ..base.notifier import Notifier


class MessageNotifier(Notifier):
    MESSAGE_SERVICE_SID = "MGb973f6746b2a587878d7a5f40cead0d1"
    FROM = "+1 209 780 6007"

    # MESSAGE_SERVICE_SID = "MG03c9b51474b90829b2ce1820f6bb1146"
    # FROM = "+19785925185"

    # Your Account Sid and Auth Token from twilio.com/console
    # and set the environment variables. See http://twil.io/secure
    _CLIENT = (
        settings.TWILIO_ACCOUNT_SID
        and settings.TWILIO_AUTH_TOKEN
        and Client(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)
    )

    def __init__(
        self,
        message: str = None,
        from_: str = FROM,
        service_id: str = MESSAGE_SERVICE_SID,
        **kwargs,
    ):
        super().__init__("", message, **kwargs)
        self.from_ = from_
        self.service_id = service_id

    @property
    def client(self) -> Client:
        return self._CLIENT

    def send_to(self, *mobiles: List[str], **kwargs) -> List[MessageInstance]:
        assert self.message is not None, "Missing twilio message!"
        return sum(
            [
                self.send(
                    mobile,
                    self.message,
                    from_=self.from_,
                    service_id=self.service_id,
                    by_card=self._by_card,
                    default_kwargs=self.default_kwargs,
                    **kwargs,
                )
                for mobile in mobiles
            ]
        )

    @classmethod
    def send(
        cls,
        to_mobile: str,
        message: str,
        *media,
        from_: str = FROM,
        service_id: str = MESSAGE_SERVICE_SID,
        by_card: SCards = None,
        default_kwargs: dict = None,
        **kwargs,
    ) -> int:
        message = serialize_string(
            message, default_kwargs=default_kwargs, by_card=by_card, **kwargs
        )
        slogger.debug(f"Send message to {to_mobile}: {message[1:50]}")
        if cls.IS_TEST:
            return 1

        try:
            twilio_message: MessageInstance = cls._CLIENT.messages.create(
                messaging_service_sid=service_id,
                from_=from_,
                media_url=media,
                body=message,
                to=to_mobile,
                # to="+84935177244",
            )
            return int(twilio_message.status == "accepted")
        except TwilioRestException as e:
            raise BadRequestException(
                errcode=400061,
                message=f"Can't send message to this phone number: {to_mobile}",
                data=e.__dict__,
                err_message=e.msg,
                notify=BadRequestException.NOTIFY_EXC,
            )

    @classmethod
    def send_media(cls, to_mobile: str, message: str, file, mime=None, **kwargs) -> int:
        media = SStorage().upload(file, mime=mime, keep_name=True)
        return cls.send(to_mobile, message, media.url, **kwargs)

    @classmethod
    def send_card(cls, to_mobile: str, message: str, file, **kwargs) -> int:
        return cls.send_media(to_mobile, message, file, mime="text/vcard", **kwargs)
