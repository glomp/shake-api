from __future__ import annotations

from email.mime.text import MIMEText
from email.utils import formataddr
from typing import List, Tuple, Union

from django.conf import settings
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.utils.html import strip_tags

from helper.logger_handler import slogger
from s_user.models import SUsers
from ..base.common import serialize_string
from ..base.notifier import Notifier
from ..email_template.letter import LetterTemplate


class MailNotifier(Notifier):
    FROM_EMAIL: str = formataddr(("Shake App", settings.EMAIL_HOST_USER))
    HTML_MESSAGE: bool = True
    TEMPLATE: LetterTemplate = None

    def __init__(self, subject: str = None, message: str = None):
        super().__init__(subject, message)
        self.default_kwargs.update(
            Shake="<span class='shake-bold'>Shake</span>",
            walkthrough_link=settings.LINK_WALKTHROUGH_PAGE,
        )

    def by(self, card) -> MailNotifier:
        return super().by(card)

    def send_to(self, *users: List[Union[SUsers, str]], **kwargs) -> int:
        assert self.subject is not None, "Missing email subject!"
        assert self.message is not None, "Missing email message!"
        return sum(
            [
                self.send(
                    user,
                    self.subject,
                    self.message,
                    by_card=self._by_card,
                    default_kwargs=self.default_kwargs,
                    **kwargs,
                )
                for user in users
            ]
        )

    @classmethod
    def send(
        cls,
        user: Union[SUsers, str, Tuple[str, str]],
        subject: str = None,
        message: str = None,
        files: list = None,
        from_email: str = None,
        is_html: bool = HTML_MESSAGE,
        **kwargs,
    ) -> int:
        subject = serialize_string(subject, to_user=user, **kwargs)
        message = serialize_string(message, to_user=user, **kwargs)
        message = message.replace("\n", "<br>")
        letter = cls._build_letter(subject, message)
        return (
            cls.send_to_email(
                user.email,
                subject,
                letter,
                files,
                from_email=from_email,
                is_html=is_html,
            )
            if isinstance(user, SUsers)
            else cls.send_to_email(
                user,
                subject,
                letter,
                files,
                from_email=from_email,
                is_html=is_html,
            )
        )

    @classmethod
    def send_to_email(
        cls,
        to_user_email: Union[str, tuple],
        subject: str,
        message: str,
        files: list = None,
        from_email: str = None,
        is_html: bool = HTML_MESSAGE,
    ) -> int:
        slogger.debug(f"Mail to: {to_user_email} about: '{subject}'")
        if cls.IS_TEST:
            return 1

        email = cls._create_email(
            to_user_email, subject, message, files, from_email, is_html
        )
        return email.send()

    @classmethod
    def _create_email(
        cls,
        to_user_email: Union[str, tuple],
        subject: str,
        message: str,
        files: list = None,
        from_email: str = FROM_EMAIL,
        is_html: bool = HTML_MESSAGE,
    ) -> EmailMessage:
        from_email = from_email or cls.FROM_EMAIL
        assert to_user_email, "Email can't be empty"

        if is_html:
            html_message = MIMEText(message, "html")
            html_message, message = message, strip_tags(html_message)
            email = EmailMultiAlternatives(
                subject, message, from_email, [to_user_email]
            )
            email.attach_alternative(html_message, "text/html")
        else:
            email = EmailMessage(subject, message, from_email, [to_user_email])

        if files is not None:
            for _file in files:
                if isinstance(_file, str):
                    email.attach_file(_file)
                else:
                    email.attach(*_file)
        return email

    @classmethod
    def _build_letter(cls, subject: str, message: str) -> str:
        if cls.TEMPLATE is None:
            return message
        letter: LetterTemplate = cls.TEMPLATE(subject, message)
        return letter.to_str()
