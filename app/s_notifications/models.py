from django.db import models

from custom_django.models import fields
from s_cards.models import SCards
from s_companies.models import SCompanies
from s_medias.models import SModel
from s_user.models import SUsers


class SNotifications(SModel):
    UNREAD = "U"
    READ = "R"
    DESTROYED = "D"
    STATUS = (
        (UNREAD, "UNREAD"),
        (READ, "READ"),
        (DESTROYED, "DESTROYED"),
    )

    SYSTEM = "S"
    USER = "U"
    MYSELF = "M"
    COMPANY = "C"
    OTHER = "O"
    NOTI_TYPE = (
        (SYSTEM, "SYSTEM"),
        (USER, "USER"),
        (MYSELF, "MYSELF"),
        (COMPANY, "COMPANY"),
        (OTHER, "OTHER"),
    )

    user = models.ForeignKey(
        SUsers,
        models.DO_NOTHING,
        default=None,
        null=True,
        blank=True,
        related_name="notifications",
    )
    by_card = models.ForeignKey(
        SCards,
        models.DO_NOTHING,
        default=None,
        null=True,
        blank=True,
        related_name="notifications",
    )
    company = models.ForeignKey(
        SCompanies, models.DO_NOTHING, default=None, null=True, blank=True
    )

    status = models.CharField(max_length=1, choices=STATUS, default=UNREAD)
    noti_type = models.CharField(max_length=1, choices=NOTI_TYPE, default=SYSTEM)

    action = models.CharField(max_length=63)
    subject = models.CharField(max_length=127)
    content = models.CharField(max_length=255)

    target_card = models.ForeignKey(
        SCards,
        models.DO_NOTHING,
        default=None,
        null=True,
        blank=True,
        related_name="related_notifications",
    )
    target_model = models.CharField(max_length=63, default=None, null=True, blank=True)
    target_id = models.BigIntegerField(default=None, null=True, blank=True)

    _created = fields.UnixTimeStampField(null=True, auto_now_add=True)
    _updated = fields.UnixTimeStampField(null=True, auto_now=True)

    PERSONAL_FIELDS = (
        "by_card",
        "company",
    )

    def get_readonly_fields(self, request, obj=None):
        return ["_created", "_updated"] + (
            ["user", "company", "noti_type", "content"] if obj else []
        )

    class Meta:
        managed = True
        db_table = "s_notification"
