from django.db import transaction
from django.db.models.query_utils import Q
from django.http.response import Http404
from rest_framework.decorators import action

from custom_django.views.viewsets import SDynamicModelViewSet
from helper.common.search import Search
from .email import (
    AuthMailNotifier,
    CardMailNotifier,
    CompanyMailNotifier,
    UserMailNotifier,
)
from .models import SNotifications
from .serializers import SFullNotificationSerializer


class SNotificationsViewSet(SDynamicModelViewSet):
    """Manage notification"""

    serializer_class = SFullNotificationSerializer
    model_class = SNotifications
    queryset = SNotifications.objects.all()

    def retrieve(self, request, pk):
        return self.view.notification.get(id=pk)

    def list(self, request):
        return self.view.notification.all()

    def create(self, request):
        raise Http404

    @transaction.atomic
    def update(self, request, pk, partial=True):
        instance = self.view.notification.get(id=pk)
        serializer = self.serializer_class(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        return serializer.save()

    @transaction.atomic
    def destroy(self, request, pk):
        instace = self.view.notification.get(id=pk)
        cnt, _ = instace.delete()
        if cnt == 1:
            return "SUCCESS"

    @action(methods=["GET"], detail=False, url_path="search-advanced")
    def search_advanced(self, request: SDynamicModelViewSet.SRequest):
        offset = self.params["offset"]
        limit = self.params["limit"]

        search_filters = {}
        name = Search.serialize_key(self.params["name"])
        company = Search.serialize_key(self.params["company"])
        from_date = self.params.get("from_date", _type=float)
        to_date = self.params.get("to_date", _type=float)
        if name is not None:
            search_filters.update(by_card__user_name__icontains=name)
        if company is not None:
            search_filters.update(by_card__company_name__icontains=company)
        if from_date is not None:
            search_filters.update(_created__gte=from_date)
        if to_date is not None:
            search_filters.update(_created__lte=to_date)
        return (
            self.view.notification.all(**search_filters)
            .select_related("by_card")
            .distinct()[offset : offset + limit]
        )

    @action(methods=["get"], detail=False)
    def status(self, request):
        data = self.view.notification.status()
        return data

    @action(methods=["patch"], detail=False, url_path="mark-all-as-read")
    def mark_all_as_read(self, request):
        self.view.notification.all().update(status=SNotifications.READ)
        return "SUCCESS"

    @action(methods=["POST"], detail=False, url_path="test-email")
    def test_email(self, request):
        email = request.data["email"]
        by_card = self.user.get_first_card()

        AuthMailNotifier().verify_account.send_to(email, link="letsshake.app")
        AuthMailNotifier().reset_password.send_to(email, link="letsshake.app")
        CardMailNotifier().send_vcf.by(by_card).send_to(email)
        CardMailNotifier().delete_card.send_to(
            email, card_name="Personal", link="letsshake.app"
        )
        CompanyMailNotifier().invite_employee.send_to(
            email,
            access_code="999999",
            company_name="h.@ctiv8",
            admin_name="Chi",
            admin_job_title="Backend",
        )
        UserMailNotifier().confirm_delete_account.send_to(email, link="letsshake.app")

    @action(methods=["POST"], detail=False)
    def test(self, request):
        # from .message.auth import AuthMessage

        # AuthMessage().re_login.notify(self.view.user)

        # from .notifier import MessageNotifier

        # result = MessageNotifier("Hello Sihc").send_to("+84793361231")
        # print("result", result[0]._properties)
        # return result[0]._properties

        from .notification.user import UserNotification

        UserNotification().welcome.notify(self.user)
        # pass
