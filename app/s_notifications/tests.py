from typing import List

from custom_django.test.base import BaseTestCase, TestCase
from custom_django.test.card_logic import (
    BusinessCardBaseLogic,
    PersonalCardBaseLogic,
    PrintedCardBaseLogic,
    ScanCardBaseLogic,
)
from custom_django.test.request_data import NoneRequest, TestRequestData
from helper.exceptions import AppException
from s_balances.views import SBalanceViewSet
from s_cards.models import SCards
from s_cards.views import SCardsViewSet
from s_contacts.views import SContactRequestsViewSet, SShareContactRequestsViewSet
from s_user.views import SUserViewSet
from .base.notification import Notification
from .models import SNotifications
from .notification.card import CardNotification
from .notification.contact import (
    ContactRequestNotification,
    ShareContactRequestNotification,
)
from .self_notification.balance import BalanceSelfNotification
from .self_notification.card import CardSelfNotification
from .self_notification.contact import (
    ContactRequestSelfNotification,
    ContactSelfNotification,
    ShareContactRequestSelfNotification,
)
from .self_notification.user import UserSelfNotification
from .views import SNotificationsViewSet


class NotificationTC(BaseTestCase):
    ViewSet = SNotificationsViewSet

    def create_general_objects(self):
        _, self.suser2 = self.helper.create_user("user2@yopmail.com")

    def create_specific_objects(self):
        _, self.card = self.helper.create_card(self.suser, auto_add_credit=False)
        _, self.card2 = self.helper.create_card(self.suser2, auto_add_credit=False)

    def create_test_objects(self):
        self.helper.create_notification(
            self.card2,
            self.suser,
            "test",
            "this is a test",
            status=SNotifications.UNREAD,
        )
        self.test_data, self.test_obj = self.helper.create_notification(
            self.card2,
            self.suser,
            "test 2",
            "this is a test 2",
            status=SNotifications.UNREAD,
        )

    def validate_result(self, origin, result) -> bool:
        return (
            result["by_card"]["id"] == origin["by_card"].id,
            result["user"] == origin["user"].id,
            result["subject"] == origin["subject"],
            result["content"] == origin["content"],
            result["action"] == origin["action"],
            result["status"] == origin["status"],
        )

    def test_retrieve(self):
        self.default_test_retrieve()

    def test_list(self):
        self.default_test_list(list_length=2)

    def test_update(self):
        self.default_test_update(
            TestRequestData({"status": SNotifications.READ}, self.user),
            {"status": SNotifications.READ},
        )

    def test_destroy(self):
        self.default_test_destroy()

    def test_search_advanced(self):
        data = TestRequestData(
            None,
            self.user,
            {
                "name": self.card2.user_name,
                "company_name": self.card2.company_name,
                "from_date": self.test_obj._created,
                "to_date": self.test_obj._created,
            },
        )
        assert (
            self.run_func("search_advanced", data) == self.SUCCESS
        ), self.last_run_result()

        assert self.validate_result(
            self.test_data, self.last_result_data()[0]
        ), self.debug_data()

    def test_status(self):
        assert self.run_func("status", NoneRequest(user=self.user)) == self.SUCCESS
        assert self.compare_dict({"read": 0, "unread": 2}, self.last_result_data())

    def test_mark_all_as_read(self):
        assert (
            self.run_func("mark_all_as_read", NoneRequest(user=self.user))
            == self.SUCCESS
        )
        self.run_func("status", NoneRequest(user=self.user))
        assert self.compare_dict({"read": 2, "unread": 0}, self.last_result_data())


# ==========================  Self Notification


class BaseSelfNotificateTestCase(BaseTestCase):
    ViewSet = SNotificationsViewSet

    def validate_result(self, origin: Notification, result) -> List[bool]:
        self.target_card: SCards
        return [
            result["action"] == origin.action,
            result["subject"] == origin.subject,
            result["target_model"] == origin.target_model,
            result["target_id"] is not None,
            result["target_id"] is not None
            and (
                result["target_id"] == self.target_card.id
                if origin.target_model == "card"
                else result["target_id"] != self.target_card.id
            ),
            result["target_card"] is not None,
            result["target_card"] is not None
            and result["target_card"] == self.target_card.id,
        ]

    def _test_result(self, origin, noti_len=1):
        assert (
            self.run_func("list", NoneRequest(user=self.user)) == self.SUCCESS
        ), self.last_run_result()

        result = self.last_result_data()
        assert isinstance(result, list), self.debug_data("Response must be a list")
        assert len(result) == noti_len, self.debug_data(
            f"Response must has length exact {noti_len}"
        )
        assert self.validate(
            *self.validate_result(origin, result[0])
        ), self.debug_data()


class PersonalSelfNotificationTC(PersonalCardBaseLogic, BaseSelfNotificateTestCase):
    def create_general_objects(self):
        self.user2, self.suser2 = self.helper.create_user(
            "user2@yopmail.com", name="test user 2"
        )
        _, self.card2 = self.helper.create_card(self.suser2)
        self.user3, self.suser3 = self.helper.create_user(
            "user3@yopmail.com", name="test user 3"
        )
        _, self.card3 = self.helper.create_card(self.suser3)

    def create_test_objects(self):
        _, self.card = self._create_card(self.suser, auto_add_credit=False)
        self.target_card = self.card

    # ==========================  BALANCE

    def test_added_credits(self):
        assert (
            self.run_func(
                "verify_token",
                TestRequestData(
                    data={
                        "token": "cohcdjkemppn",
                        "product_id": "sku_20_credits",
                    },
                    user=self.user,
                    method="POST",
                ),
                self.card.id,
                ViewSet=SBalanceViewSet,
            )
            == self.SUCCESS
        ), self.last_run_result()

        self._test_result(BalanceSelfNotification(None).added_credits)

    # ==========================  CARD

    def test_updated_card(self):
        assert (
            self.run_func(
                "update",
                TestRequestData(
                    data={"visibility": "C"}, user=self.user, method="POST"
                ),
                self.card.id,
                ViewSet=SCardsViewSet,
            )
            == self.SUCCESS
        ), self.last_run_result()

        self._test_result(CardSelfNotification(None).updated_card)

    # ===========================  CONTACT

    def test_received(self):
        self.target_card = self.card2
        self.helper.create_personal_credit(self.card, 1)
        _, ctc_request = self.helper.create_contact_request(self.card2, self.card)

        assert (
            self.run_func(
                "confirm",
                NoneRequest(user=self.user, method="POST"),
                ctc_request.id,
                ViewSet=SContactRequestsViewSet,
            )
            == self.SUCCESS
        ), self.last_run_result()

        self._test_result(ContactSelfNotification(None).received, noti_len=2)

    # ==========================  CONTACT REQUEST

    def test_created_request(self):
        self.target_card = self.card2
        self.helper.create_personal_credit(self.card, 1)

        assert (
            self.run_func(
                "create",
                TestRequestData(
                    data={
                        "from_card_id": self.card.id,
                        "to_card_id": [self.card2.id],
                        "tier_number": 1,
                    },
                    user=self.user,
                    method="POST",
                ),
                ViewSet=SContactRequestsViewSet,
            )
            == self.SUCCESS
        ), self.last_run_result()

        self._test_result(
            ContactRequestSelfNotification(None).created_request, noti_len=2
        )

    def test_declined_request(self):
        self.target_card = self.card2
        self.helper.create_personal_credit(self.card, 1)
        _, ctc_request = self.helper.create_contact_request(self.card2, self.card)

        assert (
            self.run_func(
                "destroy",
                NoneRequest(user=self.user, method="POST"),
                ctc_request.id,
                ViewSet=SContactRequestsViewSet,
            )
            == self.SUCCESS
        ), self.last_run_result()

        self._test_result(
            ContactRequestSelfNotification(None).declined_request, noti_len=2
        )

    # ==========================  CONTACT SHARE

    def test_created_share(self):
        self.target_card = self.card2
        self.helper.create_user_contact(self.card, self.card2)
        self.helper.create_user_contact(self.card, self.card3)

        assert (
            self.run_func(
                "create",
                TestRequestData(
                    data={"from_card": self.card2.id, "to_card": [self.card3.id]},
                    user=self.user,
                    method="POST",
                ),
                ViewSet=SShareContactRequestsViewSet,
            )
            == self.SUCCESS
        ), self.last_run_result()

        self._test_result(ShareContactRequestSelfNotification(None).created_share)

    # ==========================  USER

    def test_updated_password(self):
        assert (
            self.run_func(
                "change_password",
                TestRequestData(
                    data={"current_password": "123456", "new_password": "654321"},
                    user=self.user,
                    method="POST",
                ),
                ViewSet=SUserViewSet,
            )
            == self.SUCCESS
        ), self.last_run_result()

        self._test_result(UserSelfNotification(None).updated_password)

    def test_delete_user(self):
        assert (
            self.run_func(
                "destroy",
                NoneRequest(user=self.user, method="POST"),
                self.user.id,
                ViewSet=SUserViewSet,
            )
            == self.SUCCESS
        ), self.last_run_result()

        self._test_result(UserSelfNotification(None).delete_user)


class ScanSelfNotificationTC(ScanCardBaseLogic, PersonalSelfNotificationTC):
    pass


class BusinessSelfNotificationTC(BusinessCardBaseLogic, PersonalSelfNotificationTC):
    def test_added_company_credits(self):
        assert (
            self.run_func(
                "add",
                TestRequestData(
                    data={"type": "C", "amount": 10}, user=self.user, method="POST"
                ),
                self.card.id,
                ViewSet=SBalanceViewSet,
            )
            == self.SUCCESS
        ), self.last_run_result()

        self._test_result(BalanceSelfNotification(None).added_company_credits)


# class PrintedCardSelfNotificateTC(PrintedCardBaseLogic, PersonalSelfNotificationTC):
#     def test_added_credits(self):
#         pass

#     def test_received(self):
#         pass

#     def test_created_request(self):
#         pass

#     def test_declined_request(self):
#         pass

#     def test_created_share(self):
#         pass

#     def test_updated_password(self):
#         pass

#     def test_delete_user(self):
#         pass


# ========================  Notification


class BaseNotificateTestCase(BaseTestCase):
    ViewSet = SNotificationsViewSet

    def validate_result(self, origin: Notification, result) -> List[bool]:
        self.target_card: SCards
        return [
            result["action"] == origin.action,
            result["subject"] == self.target_card.user_name,
            result["target_model"] == origin.target_model,
            result["target_id"] is not None,
            result["target_id"] is not None
            and (
                result["target_id"] == self.target_card.id
                if origin.target_model == "card"
                else result["target_id"] != self.target_card.id
            ),
            result["target_card"] is not None,
            result["target_card"] is not None
            and result["target_card"] == self.target_card.id,
        ]

    def _test_result(self, origin, noti_len=1):
        assert (
            self.run_func("list", NoneRequest(user=self.user)) == self.SUCCESS
        ), self.last_run_result()

        result = self.last_result_data()
        assert isinstance(result, list), self.debug_data("Response must be a list")
        assert len(result) == noti_len, self.debug_data(
            f"Response must has length exact {noti_len}"
        )
        assert self.validate(
            *self.validate_result(origin, result[0])
        ), self.debug_data()


class PersonalNotificationTC(PersonalCardBaseLogic, BaseNotificateTestCase):
    def create_general_objects(self):
        self.user2, self.suser2 = self.helper.create_user(
            "user2@yopmail.com", name="test user 2"
        )
        _, self.card2 = self.helper.create_card(self.suser2)
        self.user3, self.suser3 = self.helper.create_user(
            "user3@yopmail.com", name="test user 3"
        )
        _, self.card3 = self.helper.create_card(self.suser3)

    def create_test_objects(self):
        _, self.card = self._create_card(self.suser, auto_add_credit=False)
        self.target_card = self.card

        _, self.contact12 = self.helper.create_user_contact(self.card, self.card2)

    # ========================  CARD

    def test_updated_card(self):
        self.target_card = self.card2

        assert (
            self.run_func(
                "update",
                TestRequestData(
                    data={"visibility": "C"}, user=self.user2, method="POST"
                ),
                self.card2.id,
                ViewSet=SCardsViewSet,
            )
            == self.SUCCESS
        ), self.last_run_result()

        self._test_result(CardNotification().updated_card)

    def test_deleted_card(self):
        pass

    # ========================  CONTACT REQUEST

    def test_confirmed_request(self):
        self.target_card = self.card2
        self.helper.create_personal_credit(self.card, 1)
        _, ctct_rqst = self.helper.create_contact_request(self.card, self.card2)

        assert (
            self.run_func(
                "confirm",
                NoneRequest(user=self.user2, method="POST"),
                ctct_rqst.id,
                ViewSet=SContactRequestsViewSet,
            )
            == self.SUCCESS
        ), self.last_run_result()

        self._test_result(ContactRequestNotification().confirmed_request, noti_len=2)

    # def test_declined_request(self):
    #     self.target_card = self.card2
    #     self.helper.create_personal_credit(self.card, 1)
    #     _, ctct_rqst = self.helper.create_contact_request(self.card, self.card2)

    #     assert (
    #         self.run_func(
    #             "destroy",
    #             NoneRequest(user=self.user2, method="POST"),
    #             ctct_rqst.id,
    #             ViewSet=SContactRequestsViewSet,
    #         )
    #         == self.SUCCESS
    #     ), self.last_run_result()

    #     self._test_result(ContactRequestNotification().confirmed_request, noti_len=2)

    # ======================  SHARE CONTACT REQUEST

    def test_created_share(self):
        self.target_card = self.card2
        self.helper.create_user_contact(self.card2, self.card)
        self.helper.create_user_contact(self.card2, self.card3)

        assert (
            self.run_func(
                "create",
                TestRequestData(
                    data={"from_card": self.card.id, "to_card": [self.card3.id]},
                    user=self.user2,
                    method="POST",
                ),
                ViewSet=SShareContactRequestsViewSet,
            )
            == self.SUCCESS
        ), self.last_run_result()

        self._test_result(ShareContactRequestNotification().created_share)


class ScanNotificationTC(ScanCardBaseLogic, PersonalNotificationTC):
    pass


class BusinessNotificationTC(BusinessCardBaseLogic, PersonalNotificationTC):
    pass


class PrintedCardNotificateTC(PrintedCardBaseLogic, PersonalNotificationTC):
    def create_general_objects(self):
        _, self.media = self.helper.create_media()
        # self.user2, self.suser2 = self.helper.create_user(
        #     "user2@yopmail.com", name="test user 2"
        # )
        self.user3, self.suser3 = self.helper.create_user(
            "user3@yopmail.com", name="test user 3"
        )
        _, self.card3 = self.helper.create_card(self.suser3, media=self.media)

    def create_test_objects(self):
        _, self.card = self.helper.create_card(
            self.suser, media=self.media, auto_add_credit=False
        )
        _, self.card2 = self._create_card(
            self.suser, to_card=self.card, auto_add_credit=False
        )
        self.target_card = self.card
        self.suser2, self.user2 = self.suser, self.user

        # _, self.contact12 = self.helper.create_user_contact(self.card, self.card2)

    def test_confirmed_request(self):
        pass

    def test_created_share(self):
        pass


class ExceptionTest(TestCase):
    def test_send_email_to_admin(self):
        try:
            0 / 0
        except Exception as e:
            app_e = AppException(
                errcode=000000,
                data={"message": "This is a test"},
                err_message=str(e),
                log=False,
                verbose=False,
                notify=False,
                log_to_file=False,
                error=e,
            )
            app_e.send_email_to_admin()

    def test_send_to_sentry(self):
        try:
            0 / 0
        except Exception as e:
            app_e = AppException(
                errcode=000000,
                data={"message": "This is a test"},
                err_message=str(e),
                log=False,
                verbose=False,
                notify=False,
                log_to_file=False,
                error=e,
            )
            app_e.make_sentry_context()
