#!/usr/bin/env python
import os
import sys

import django
from django.conf import settings
from django.test.utils import get_runner

log_file = settings.PATH_LOG_FILE
exception_log_file = settings.PATH_EXCEPTION_LOG_FILE


def clean_log():
    try:
        os.remove(log_file)
        os.remove(exception_log_file)
    except Exception as e:
        print("Can't clean logs!")
        # raise e


def show_exception():
    with open(exception_log_file, "r+") as exception_logs:
        print(exception_logs.read())


if __name__ == "__main__":
    clean_log()
    os.environ.setdefault("SERVER_ENV", "TEST")
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "app.settings.test")
    django.setup()
    TestRunner = get_runner(settings)
    test_runner = TestRunner(keepdb=True, verbosity=2)

    full_modules = [
        "s_auth",
        "s_user",
        "s_medias",
        "s_companies",
        "s_templates",
        "s_cards",
        "s_contacts",
        "s_notifications",
        "s_balances",
        "m_company",
    ]
    filter_module = sys.argv[1] if len(sys.argv) > 1 else ""
    modules = [module for module in full_modules if filter_module in module]
    print("Test modules:", modules)

    failures = test_runner.run_tests(modules)
    failure = bool(failures)
    if failure is True:
        show_exception()

    sys.exit(failure)
