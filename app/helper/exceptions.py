import sentry_sdk
import sys
import traceback
from datetime import datetime, timedelta

from django.conf import settings

from .logger_handler import file_logger, slogger
from .responses import AppResponse, json_dumps


class AppException(Exception):
    request: dict = None

    exception_holder = {}

    STATUS_CODE = 500
    MESSAGE = "Application Error"
    LOG_EXC = True
    VERBOSE_LOG = settings.ENV != "PRODUCTION"
    VERBOSE_EXC = settings.ENV != "PRODUCTION"
    NOTIFY_EXC = settings.ENV in {"PRODUCTION", "STAGING"}

    def __init__(
        self,
        errcode: int,
        message: str = None,
        data: dict = None,
        err_message: str = None,
        log: bool = None,
        verbose: bool = None,
        notify: bool = None,
        log_to_file: bool = True,
        error: Exception = None,
    ):
        self.errcode = errcode
        self.data = data if data is not None else dict()
        self.message = message or self.MESSAGE
        self.err_message = err_message or self.message

        if error is not None:
            self.__traceback__ = error.__traceback__
        else:
            super(AppException, self).__init__(self.message)

        if log if log is not None else self.LOG_EXC:
            slogger.warning(self.name)
        if verbose if verbose is not None else self.VERBOSE_EXC:
            file_logger.error("%s", "\t".join(self.logs))
            slogger.exception("%r", self)
        elif log_to_file:
            file_logger.error("%s", "\t".join(self.logs))
            file_logger.exception("%r", self)
        if notify if notify is not None else self.NOTIFY_EXC:
            self.notify_exception()

    def notify_exception(self):
        self.make_sentry_context()
        self.send_email_to_admin()

    @property
    def name(self):
        return f"[{self.errcode}] {self.err_message}"

    @property
    def logs(self):
        if hasattr(self, "_logs"):
            return self._logs
        self._logs = (
            f"[{self.errcode}] {self.message}",
            f"Request data: {self.request_data}",
            f"Error: {str(self)}",
            f"Error message: {self.err_message}",
            f"Error data: {self.data}",
        )
        return self._logs

    @property
    def request_data(self):
        if hasattr(self, "_request_data"):
            return self._request_data

        data = {}
        try:
            data["request"] = {
                "url": self.request._request.path,
                "method": self.request._request.method,
            }
        except Exception:
            pass
        try:
            data["user"] = {
                "id": self.request.user.id,
                "email": self.request.user.email,
            }
        except Exception:
            pass
        try:
            data["request_data"] = self.request.data
        except Exception:
            pass
        try:
            data["param"] = dict(self.request.GET.items())
        except Exception:
            pass
        try:
            data["device"] = self.request._request.environ["HTTP_USER_AGENT"]
        except Exception:
            data["device"] = "Unknown"

        self._request_data = data
        return self._request_data

    @property
    def request_data_str(self):
        if hasattr(self, "_request_data_str"):
            return self._request_data_str

        self._request_data_str = json_dumps(self.request_data)
        return self._request_data_str

    @property
    def data_str(self):
        if hasattr(self, "_data_str"):
            return self._data_str

        self._data_str = json_dumps(self.data)
        return self._data_str

    @property
    def body(self) -> dict:
        return {
            "errcode": self.errcode,
            "data": {
                "request data": self.request_data,
                "error data": self.data,
                "error message": self.err_message,
            },
            "message": self.message,
        }

    def send_email_to_admin(self):
        exception_id = f"[{self.errcode}] {self.err_message[:20]}"
        if self.exception_holder.get(exception_id, None) is None:
            self.exception_holder[exception_id] = datetime.utcnow()
        elif self.exception_holder[exception_id] > datetime.utcnow() - timedelta(
            hours=1
        ):
            return

        from s_notifications.email.admin import AdminMailNotifier

        traceback_message = "".join(list(traceback.format_exception(*sys.exc_info())))
        kwargs = dict(
            env=settings.ENV,
            version=settings.VERSION,
            release_time=settings.RELEASE_TIME,
            git_branch=settings.GIT_BRANCH,
            git_hash_short=settings.GIT_HASH_SHORT,
            git_hash=settings.GIT_HASH,
            git_commit_message=settings.GIT_COMMIT_MESSAGE,
            err_brief=exception_id,
            errcode=self.errcode,
            err_class=self.__class__.__name__,
            err_message=self.err_message,
            request_data=self.request_data_str,
            data=self.data_str,
            traceback=traceback_message,
        )
        slogger.debug("Notify to admin email")
        AdminMailNotifier().exception.send_to(*settings.ADMINS, **kwargs)

    def make_sentry_context(self):
        slogger.debug("Notify sentry")
        sentry_sdk.set_tag("env", settings.ENV)
        sentry_sdk.set_tag("app_version", settings.VERSION)
        sentry_sdk.set_tag("release_time", settings.RELEASE_TIME)

        sentry_sdk.set_tag("git_branch", settings.GIT_BRANCH)
        sentry_sdk.set_tag("git_hash_short", settings.GIT_HASH_SHORT)
        sentry_sdk.set_tag("git_hash", settings.GIT_HASH)
        sentry_sdk.set_tag("git_commit_message", settings.GIT_COMMIT_MESSAGE)

        sentry_sdk.set_tag("err_code", self.errcode)
        sentry_sdk.set_tag("message", self.message)
        sentry_sdk.set_tag("err_message", self.err_message)
        sentry_sdk.set_tag("err_class", self.__class__.__name__)
        sentry_sdk.set_extra("request_data", self.request_data_str)
        sentry_sdk.set_extra("data", self.data_str)
        sentry_sdk.set_context("app", {"app_version": settings.VERSION})
        sentry_sdk.set_context(
            "device",
            {"name": self.request_data["device"]},
        )
        sentry_sdk.capture_exception(self)

    @property
    def resp(self) -> AppResponse:
        return AppResponse(body=self.body, status=self.STATUS_CODE)


class BadRequestException(AppException):
    STATUS_CODE = 400
    MESSAGE = "Bad request"
    LOG_EXC = settings.ENV != "TEST"
    VERBOSE_EXC = settings.ENV not in {"PRODUCTION", "TEST"}
    NOTIFY_EXC = False


class UnauthorizedException(AppException):
    STATUS_CODE = 401
    MESSAGE = "Unauthorized"
    LOG_EXC = settings.ENV != "TEST"
    VERBOSE_EXC = settings.ENV not in {"PRODUCTION", "TEST"}
    NOTIFY_EXC = False


class ForbiddenException(AppException):
    STATUS_CODE = 403
    MESSAGE = "Forbidden"
    LOG_EXC = settings.ENV != "TEST"
    VERBOSE_EXC = settings.ENV not in {"PRODUCTION", "TEST"}
    NOTIFY_EXC = False


class NotFoundException(AppException):
    STATUS_CODE = 404
    MESSAGE = "Item not found"
    LOG_EXC = settings.ENV != "TEST"
    VERBOSE_EXC = settings.ENV not in {"PRODUCTION", "TEST"}
    NOTIFY_EXC = False


class ConfictException(AppException):
    STATUS_CODE = 409
    MESSAGE = "Item exists"
    LOG_EXC = settings.ENV != "TEST"
    VERBOSE_EXC = settings.ENV not in {"PRODUCTION", "TEST"}
    NOTIFY_EXC = settings.ENV == "STAGING"
