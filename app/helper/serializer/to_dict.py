from typing import List


def query_to_dict(queryset: list, key: str) -> dict:
    result = {}
    for item in queryset:
        result[getattr(item, key)] = item
    return result


def list_to_dict(list_dict: List[dict], key: str) -> dict:
    result = {}
    for item in list_dict:
        result[item[key]] = item
    return result
