from os.path import join as join_path

from django.conf import settings


def generate_url(*url):
    return join_path(settings.CUSTOM_DOMAIN, *url)
