def print_query(queryset: list, query_name: str):
    print("---------->  DEBUG:   ", query_name)
    for item in queryset:
        data = item.__dict__
        data.pop("_state", None)
        print(data)
    print("---------")
