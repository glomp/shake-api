import re
import urllib
from urllib.parse import unquote, urlparse


class Search:
    KEY_SEARCH_REGEX = r"\w_\+@."
    RE = f"[^{KEY_SEARCH_REGEX}]"  # Get all charactor not match

    @classmethod
    def pre_serialize(cls, key: str) -> str:
        if key is None:
            return key
        key = unquote(key)
        return key

    @classmethod
    def serialize_email(cls, key: str) -> str:
        if key is None:
            return key
        key = cls.pre_serialize(key)
        key = re.sub(cls.RE, "", key)
        return key

    @classmethod
    def serialize_key(cls, key: str) -> str:
        if key is None:
            return key
        key = cls.pre_serialize(key)
        key = key.strip()
        return key

    @classmethod
    def serialize_search_key(cls, key: str) -> str:
        if key is None:
            return key
        key = cls.pre_serialize(key)
        key = "".join(list(filter(lambda x: x != "", key.split(" "))))
        return key
