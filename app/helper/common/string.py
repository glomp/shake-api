from typing import Tuple


def serialize_name(name: str) -> str:
    name = name.strip()
    splited_name = list(filter(lambda x: x != "", name.split(" ")))
    return " ".join(splited_name).title()


def split_name(name: str) -> Tuple[str, str]:
    splited_name = serialize_name(name).split(" ")
    first_name = splited_name[0]
    last_name = " ".join(splited_name[1:])

    return first_name, last_name
