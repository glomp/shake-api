import random
import string


def generate_v1(_length=63) -> str:
    return "".join([random.choice(string.hexdigits) for i in range(_length)])


def generate_v2(_length=6) -> str:  # only number
    return "".join([random.choice(string.digits) for i in range(_length)])
