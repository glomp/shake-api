import hashlib
import os


def get_name(filename):
    result = filename.split("/")
    return result[-1]


def get_extension(filename):
    result = filename.split(".")
    if len(result) <= 1:
        raise ValueError(f"File {filename} has no extension")
    return result[-1]


def get_hash(afile, blocksize=65536):
    hasher = hashlib.md5()

    def get_hash(file_byte, blocksize):
        buf = file_byte.read(blocksize)
        while len(buf) > 0:
            hasher.update(buf)
            buf = file_byte.read(blocksize)
        return hasher.hexdigest()

    if isinstance(afile, str):
        with open(afile, "rb") as file_byte:
            return get_hash(file_byte, blocksize)
    else:
        return get_hash(afile, blocksize)


def save(file_content, filename, write_type="wb+") -> str:
    file_path = f"/tmp/{filename}"

    with open(file_path, write_type) as tmp_video_file:
        tmp_video_file.write(file_content)
    return file_path


def delete(*filenames):
    [os.unlink(filename) for filename in filenames]
