import csv
import io
import logging
import sys
from datetime import datetime
from traceback import TracebackException


class CsvFormatter(logging.Formatter):
    def __init__(self):
        super().__init__()
        self.output = io.StringIO()
        self.writer = csv.writer(self.output, quoting=csv.QUOTE_ALL)

    def format(self, record):
        # print(" record.msg", record.msg)
        # print(" record.args", record.args)
        message = getattr(record, "message", record.msg % record.args)
        exc_info = getattr(record, "exc_info", None)
        if exc_info:
            if isinstance(exc_info, Exception):
                exc_info = (type(exc_info), exc_info, exc_info.__traceback__)
            elif not isinstance(exc_info, tuple):
                exc_info = sys.exc_info()
            exc_message = "".join(list(TracebackException(*exc_info).format()))
            # print("exc_info", exc_message)
            # print("message", getattr(record, "message", record.msg % record.args))
            message += f"\n{exc_message}"
        self.writer.writerow(
            [
                str(datetime.utcnow().date()),
                str(datetime.utcnow().time()),
                # settings.VERSION,
                # settings.RELEASE_TIME,
                record.levelname,
                record.name,
                record.module,
                record.process,
                record.thread,
                record.pathname,
                record.lineno,
                message,
            ]
        )
        data = self.output.getvalue()
        self.output.truncate(0)
        self.output.seek(0)
        return data.strip()
