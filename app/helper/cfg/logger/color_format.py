import logging
import sys

from django.core.management.color import color_style


class DjangoColorsFormatter(logging.Formatter):
    def __init__(self, *args, **kwargs):
        super(DjangoColorsFormatter, self).__init__(*args, **kwargs)
        self.style = self.configure_style(color_style())

    def configure_style(self, style):
        style.DEBUG = style.HTTP_NOT_MODIFIED
        style.INFO = style.HTTP_SUCCESS
        style.WARNING = style.HTTP_NOT_FOUND
        style.ERROR = style.NOTICE
        style.CRITICAL = style.HTTP_SERVER_ERROR
        return style

    def format(self, record):
        message = logging.Formatter.format(self, record)
        if isinstance(message, str):
            # message = message.encode("utf-8")
            colorizer = getattr(self.style, record.levelname, self.style.HTTP_SUCCESS)
            messages = [
                colorizer(line_of_message) for line_of_message in message.split("\n")
            ]
            return "\n         ".join(messages)


class DjangoConsoleColorsFormatter(DjangoColorsFormatter):
    def configure_style(self, style):
        style.DEBUG = style.MIGRATE_HEADING
        style.INFO = style.HTTP_INFO
        style.WARNING = style.WARNING
        style.ERROR = style.ERROR
        style.CRITICAL = style.HTTP_SERVER_ERROR
        return style


class ConsoleColorsFormatter(DjangoColorsFormatter):
    def format(self, record):
        message = logging.Formatter.format(self, record)
        if isinstance(message, str):
            # message = message.encode("utf-8")
            colorizer = getattr(self.style, record.levelname, self.style.HTTP_SUCCESS)
            messages = [
                colorizer(line_of_message) for line_of_message in message.split("\n")
            ]
            levelname = colorizer("%-7s> " % record.levelname)
            return levelname + "\n         ".join(messages)
