from django.db.models import Model
from django.db.models.query import QuerySet
from django.http import JsonResponse
from django.http.response import HttpResponseBase
from dynamic_rest.serializers import (
    DynamicListSerializer,
    DynamicModelSerializer,
)
from dynamic_rest.viewsets import DynamicModelViewSet
from rest_framework.response import Response

from .exception_handler import exception_handler
from .exceptions import AppException
from .responses import AppResponse


class ResponseHandler(AppResponse):
    @classmethod
    def response(cls, resp, viewset: DynamicModelViewSet = None):
        if isinstance(
            resp, (DynamicListSerializer, DynamicModelSerializer, Response)
        ):
            return AppResponse(data=resp.data)
        if isinstance(resp, (JsonResponse, AppResponse, HttpResponseBase)):
            return resp
        if isinstance(resp, (dict, list)):
            return AppResponse(data=resp)
        if isinstance(resp, str):
            return AppResponse(data={}, message=resp)
        if viewset and isinstance(resp, Model):
            return AppResponse(data=viewset.serializer_class(resp))
        if viewset and isinstance(resp, QuerySet):
            return AppResponse(data=viewset.serializer_class(resp, many=True))
        if isinstance(resp, AppException):
            return resp.resp
        return AppResponse(data={})

    @classmethod
    def handler(cls, func_or_viewset):
        viewset: DynamicModelViewSet = None

        def _handler(func):
            def run_fun(self, request, *args, **kwargs):
                AppException.request = request
                try:
                    resp = func(self, request, *args, **kwargs)
                    return cls.response(resp, viewset)
                except Exception as exc:
                    exception = exception_handler(exc)
                    return exception.resp

            return run_fun

        if isinstance(func_or_viewset, DynamicModelViewSet):
            viewset = func_or_viewset
            return _handler

        return _handler(func_or_viewset)
