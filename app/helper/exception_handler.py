from python_http_client.exceptions import HTTPError

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.http.response import Http404
from rest_framework.exceptions import APIException, ValidationError

from .exceptions import (
    AppException,
    BadRequestException,
    ConfictException,
    ForbiddenException,
    NotFoundException,
    UnauthorizedException,
)
from .logger_handler import slogger


class DjangoExceptionHandler:
    EXCEPTION_MAP = {
        400: BadRequestException,
        401: UnauthorizedException,
        403: ForbiddenException,
        404: NotFoundException,
        409: ConfictException,
        500: AppException,
    }

    @classmethod
    def from_validation_error(
        cls, errcode: int, e: ValidationError, data: dict = None
    ) -> AppException:
        detail = e.detail if isinstance(e.detail, dict) else e.detail[0]
        first_key = list(detail.keys())[0]
        first_error = detail[first_key][0]

        message = str(first_error)
        ExceptionHandler: AppException = None

        conflict_codes = {"unique"}
        badrequest_codes = {"invalid", "required", "max_length", "blank"}
        unauthorized_codes = {"authorization"}

        if first_error.code in conflict_codes:
            ExceptionHandler = ConfictException
        elif first_error.code in badrequest_codes:
            ExceptionHandler = BadRequestException
        elif first_error.code in unauthorized_codes:
            ExceptionHandler = UnauthorizedException
            message = "Wrong email or password."
        else:
            slogger.warning("----------------> %r is not controled", first_error.code)
            return cls.from_api_exception(errcode=errcode, e=e, data=data)

        message = message.replace("username", "email")
        message = message.replace("This field", f"{first_key.capitalize()} field")
        message = message.replace("this field", f"{first_key} field")

        return ExceptionHandler(
            errcode=errcode or 500996, message=message, data=data or e.detail, error=e
        )

    @classmethod
    def from_api_exception(
        cls, errcode: int, e: APIException, data: dict = None
    ) -> AppException:
        ExceptionHandler = (
            cls.EXCEPTION_MAP[e.status_code]
            if e.status_code in cls.EXCEPTION_MAP
            else AppException
        )

        return ExceptionHandler(
            errcode=errcode or 500995, data=data, err_message=e.detail, error=e
        )

    @classmethod
    def from_http_exception(cls, errcode: int, e: HTTPError, data: dict = None):
        ExceptionHandler = (
            cls.EXCEPTION_MAP[e.status_code]
            if e.status_code in cls.EXCEPTION_MAP
            else AppException
        )

        return ExceptionHandler(
            errcode=errcode or 500994, data=data, err_message=e.body, error=e
        )


def get_exception_from_other_error(
    errcode: int, e: Exception, data: dict = None
) -> AppException:
    message = f"Something went wrong!"

    # Fail to get a key in dict
    if isinstance(e, KeyError):
        return BadRequestException(
            errcode=errcode or 400993, message=f"Missing field {str(e)}", error=e
        )
    # Item not found in objects.get
    if isinstance(e, ObjectDoesNotExist):
        return NotFoundException(
            errcode=errcode or 404992,
            data=data,
            err_message=f"Item not found in {e.__module__}",
            error=e,
        )
    if isinstance(e, Http404):
        return NotFoundException(
            errcode=errcode or 404989,
            data=data,
            message=f"This request is not supported.",
            error=e,
        )

    # Assert failt
    if isinstance(e, AssertionError):
        return BadRequestException(
            errcode=errcode or 400991, data=data, err_message=str(e), error=e
        )

    return AppException(
        errcode=errcode or 500990,
        message=message,
        data=data,
        err_message=str(e),
        error=e,
    )


def exception_handler(e: Exception, errcode: int = None) -> AppException:
    if isinstance(e, AppException):
        return e
    if isinstance(e, ValidationError):
        return DjangoExceptionHandler.from_validation_error(errcode=errcode, e=e)
    if isinstance(e, APIException):
        return DjangoExceptionHandler.from_api_exception(errcode=errcode, e=e)
    if isinstance(e, HTTPError):
        return DjangoExceptionHandler.from_http_exception(errcode=errcode, e=e)
    if isinstance(e, Exception):
        return get_exception_from_other_error(errcode=errcode, e=e)
