import logging
from json import dumps

from django.http import HttpResponse

from .encoder import CustomEncoder


def json_dumps(body, **kwargs):
    return dumps(body, cls=CustomEncoder, **kwargs)


class AppResponse(HttpResponse):
    content_type = "application/json"
    headers = ""
    accepted_renderer = False

    def __init__(
        self,
        body: dict = None,
        data: dict = None,
        message: str = None,
        status: int = 200,
        dumps=json_dumps,
        **kwargs,
    ):
        if body is None:
            _data = data if data is not None else {}
            body = {
                "data": _data,
                "message": message
                or (isinstance(_data, dict) and _data.get("message", "OK"))
                or "OK",
            }
        body = dumps(body, **kwargs)
        super().__init__(
            content=body,
            status=status,
            # headers=self.headers,
            content_type=self.content_type,
        )
