import logging

slogger = logging.getLogger("shake_console")
file_logger = logging.getLogger("shake_file")
exception_file_logger = logging.getLogger("shake_exception_file")
