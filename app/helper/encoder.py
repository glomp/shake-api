from django.core.serializers.json import DjangoJSONEncoder
from rest_framework.serializers import BaseSerializer

from uuid import UUID


class CustomEncoder(DjangoJSONEncoder):
    def default(self, obj):
        if isinstance(obj, UUID):
            # if the obj is uuid, we simply return the value of uuid
            return str(obj)
        if isinstance(obj, BaseSerializer):
            return obj.data
        try:
            return super().default(obj)
        except Exception:
            return str(obj)
