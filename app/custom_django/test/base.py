import json
from builtins import classmethod
from typing import List

from django.db.models.base import Model
from django.test import TestCase

from custom_django.views.params import Params
from custom_django.views.viewsets import SDynamicModelViewSet, SViewSet
from helper.exceptions import AppException
from helper.response_handler import ResponseHandler
from helper.responses import AppResponse
from m_company.views.base_view_set import SDynamicModelManagerViewSet
from m_company.views.employee.base_view_set import SDynamicModelEmployeeViewSet
from s_user.models import User
from .functions import BaseFunction, Comparator
from .request_data import TestRequestData
from .status import TestStatus


class CustomTestCase(TestStatus, Comparator, TestCase):
    handler = ResponseHandler.handler
    helper: BaseFunction = BaseFunction()
    ViewSet: SViewSet = None

    @classmethod
    def create_request_data(
        cls, data: dict, user: User = None, get: dict = None
    ) -> TestRequestData:
        return TestRequestData(data=data, user=user, get=get)

    def init_viewset(self, ViewSet, request, *args, **kwargs):
        viewset_obj = ViewSet()
        if isinstance(viewset_obj, SDynamicModelViewSet):
            viewset_obj.params = Params(request.GET, viewset_obj.DEFAULT_PARAMS)
            viewset_obj.pag_left = viewset_obj.params["offset"]
            viewset_obj.pag_right = (
                viewset_obj.params["offset"] + viewset_obj.params["limit"]
            )

            if isinstance(viewset_obj, SDynamicModelEmployeeViewSet):
                kwargs.update(
                    company_id=self.company.id,
                    employee_card_id=self.employee_card.id,
                )
            elif isinstance(viewset_obj, SDynamicModelManagerViewSet):
                kwargs.update(
                    company_id=self.company.id,
                )
            viewset_obj.kwargs = kwargs
            viewset_obj.extra_initial(request, *args, **kwargs)
            self.handler = ResponseHandler.handler(viewset_obj)
        return viewset_obj

    def get_func(
        self,
        func_name,
        request: TestRequestData,
        *args,
        ViewSet=ViewSet,
        **kwargs,
    ):
        ViewSet = ViewSet or self.ViewSet
        viewset_obj = self.init_viewset(ViewSet, request, *args, **kwargs)
        viewset_obj.action = func_name
        func = getattr(ViewSet, func_name)
        self.request_data = request
        AppException.request = request

        def wrapper():
            return func(viewset_obj, request, *args, **kwargs)

        return wrapper

    def run_func(
        self,
        func_name,
        request: TestRequestData,
        *args,
        ViewSet=ViewSet,
        **kwargs,
    ) -> int:
        ViewSet = ViewSet or self.ViewSet
        viewset_obj = self.init_viewset(ViewSet, request, *args, **kwargs)
        viewset_obj.action = func_name
        func = self.handler(getattr(ViewSet, func_name))
        self.request_data = request
        AppException.request = request
        self.last_run: AppResponse = func(viewset_obj, request, *args, **kwargs)
        return self.last_run.status_code

    def last_result(self) -> dict:
        return json.loads(self.last_run.content)

    def last_result_data(self) -> dict:
        return self.last_result()["data"]

    def last_run_result(self) -> str:
        return f"{self.last_run.content} -> {self.last_run.status_code}"


class BaseTestCase(CustomTestCase, TestStatus, TestCase):
    FOR_ADMIN = False

    def create_general_objects(self):
        pass

    def create_specific_objects(self):
        pass

    def create_test_objects(self):
        pass

    def create_related_objects(self):
        pass

    @classmethod
    def create_all_objects(cls):
        cls.user, cls.suser = (
            cls.helper.create_user(
                "user@yopmail.com", location_longitude=10, location_latitude=10
            )
            if cls.FOR_ADMIN is False
            else cls.helper.create_admin(
                "admin@yopmail.com", location_longitude=10, location_latitude=10
            )
        )
        cls.request_data = TestRequestData({}, cls.user, method="GET")
        cls.test_data: dict = None
        cls.test_obj = None
        cls.create_general_objects(cls)
        cls.create_specific_objects(cls)
        cls.create_test_objects(cls)
        cls.create_related_objects(cls)

    @classmethod
    def setUpTestData(cls):
        cls.helper.reset()
        cls.create_all_objects()

    def validate(self, *results) -> bool:
        self.results = results
        return all(results)

    def validate_result(self, origin, result) -> List[bool]:
        return []

    def compare_result(self, origin, result) -> bool:
        raise NotImplementedError

    def _compare_result(self, origin, result) -> bool:
        try:
            results = self.validate_result(origin, result)
            if len(results) > 0:
                self.results = results
                return all(results)
            else:
                return self.compare_result(origin, result)
        except KeyError as e:
            self.err_message = f"Missing key {e}"
        except Exception as e:
            self.err_message = str(e)
        return False

    @classmethod
    def wrap_exception(cls, func):
        def wrapper(self):
            try:
                return func(self)
            except KeyError as e:
                cls.err_message = f"Missing key {e}"
            except AssertionError as e:
                raise e
            except Exception as e:
                cls.err_message = str(e)
            assert False, cls.debug_data(self)

        return wrapper

    def debug_data(
        self,
        message=None,
        request_data=None,
        test_data=None,
        test_obj=None,
        result=None,
    ) -> str:
        try:
            return (
                f"{message or getattr(self, 'results', getattr(self, 'err_message', ''))}\n"
                f"--->   env: { {key: value.id for key, value in self.__class__.__dict__.items() if isinstance(value, Model)}}\n"
                f"--->   request_data: {request_data or self.request_data.data}\n"
                f"--->   test_data: {test_data or self.test_data}\n"
                f"--->   test_obj: {test_obj or self.test_obj and self.test_obj.__dict__}\n"
                f"--->   result: {result or self.last_result_data()}"
            )
        except Exception:
            return message or getattr(self, "results", getattr(self, "err_message", ""))

    def default_test_list(self, list_length=1):
        assert (
            self.run_func("list", self.request_data) == self.SUCCESS
        ), self.last_run_result()

        result = self.last_result_data()
        assert isinstance(result, list), self.debug_data("Response must be a list")
        assert len(result) == list_length, self.debug_data(
            f"Response must has length exact {list_length}"
        )
        assert self._compare_result(self.test_data, result[0]), self.debug_data()

    def default_test_retrieve(self):
        assert (
            self.run_func("retrieve", self.request_data, self.test_obj.id)
            == self.SUCCESS
        ), self.last_run_result()

        result = self.last_result_data()
        assert isinstance(result, dict), self.debug_data("Response must be a dict")
        assert self._compare_result(self.test_data, result), self.debug_data()

    def default_test_create(self, new_request_data, new_test_data=None):
        self.request_data = new_request_data
        assert (
            self.run_func("create", self.request_data) == self.SUCCESS
        ), self.last_run_result()

        result = self.last_result_data()
        test_data = self.test_data.copy()
        if new_test_data is not None:
            test_data.update(new_test_data)
        assert isinstance(result, dict), self.debug_data("Response must be a dict")
        assert self._compare_result(test_data, result), self.debug_data(
            test_data=test_data
        )

    def default_test_create_list(self, new_request_data, new_test_data=None):
        self.request_data = new_request_data
        assert (
            self.run_func("create", self.request_data) == self.SUCCESS
        ), self.last_run_result()

        result = self.last_result_data()
        test_data = self.test_data.copy()
        if new_test_data is not None:
            test_data.update(new_test_data)
        assert isinstance(result, list), self.debug_data("Response must be a dict")
        assert len(result) > 0, self.debug_data("Response must has length at least 1")
        assert self._compare_result(test_data, result[0]), self.debug_data(
            test_data=test_data
        )

    def default_test_update(self, new_request_data, new_test_data=None):
        self.request_data = new_request_data
        assert (
            self.run_func("update", self.request_data, self.test_obj.id) == self.SUCCESS
        ), self.last_run_result()

        result = self.last_result_data()
        test_data = self.test_data.copy()
        if new_test_data is not None:
            test_data.update(new_test_data)
        assert isinstance(result, dict), self.debug_data("Response must be a dict")
        assert self._compare_result(test_data, result), self.debug_data(
            test_data=test_data
        )

    def default_test_destroy(self):
        self.request_data.method = "POST"
        assert (
            self.run_func("destroy", self.request_data, self.test_obj.id)
            == self.SUCCESS
        ), self.last_run_result()

        result = self.last_result()
        assert isinstance(result, dict), self.debug_data("Response must be a dict")
        assert result["message"] == "SUCCESS", self.debug_data()
