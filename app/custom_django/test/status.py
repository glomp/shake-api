class TestStatus:
    SUCCESS = 200
    BAD_REQUEST = 400
    NOT_FOUND = 404
    DUPLICATED = 409
