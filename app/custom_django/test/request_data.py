from django.http import HttpRequest
from rest_framework.request import Request

from s_user.models import User


class NoneRequest(HttpRequest):
    def __init__(
        self,
        data: dict = None,
        get: dict = None,
        user: User = None,
        method: str = "GET",
    ):
        super().__init__()

        self.user = user
        self.data = data or {}
        self.POST = data
        self.GET = get or dict()
        self.content_type = "application/json"
        self.method = method
        self.action_map = ["retrieve", "list", "create", "update", "delete"]
        self.environ = {}
        self.path = "test"


class TestRequestData(Request):
    def __init__(
        self,
        data: dict,
        user: User,
        get: str = "",
        file=None,
        method: str = "GET",
    ):
        request = NoneRequest(data, get=get, user=user, method=method)
        self._request = request

        super().__init__(request)

        self._data: dict = data
        self.GET: dict = request.GET
        self._files: list = file
        self._user = user
        self._full_data = data
        self.user = user
        self.method = method
