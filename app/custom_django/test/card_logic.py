from typing import Tuple

from custom_django.test.base import BaseTestCase
from m_company.tests import BaseManagerTestCase
from s_cards.logics.card import BusinessCard, PrintedCard, ScanCard
from s_cards.models import SCards


class PersonalCardBaseLogic(BaseTestCase):
    @classmethod
    def _create_card(cls, *args, **kwargs) -> Tuple[dict, SCards]:
        return cls.helper.create_card(*args, **kwargs)

    @classmethod
    def create_card(cls, *args, **kwargs) -> Tuple[dict, SCards]:
        return cls._create_card(cls.suser, *args, **kwargs)


class ScanCardBaseLogic(BaseTestCase):
    @classmethod
    def _create_card(cls, *args, **kwargs) -> Tuple[dict, SCards]:
        return cls.helper.create_card(*args, card_class=ScanCard, **kwargs)

    @classmethod
    def create_card(cls, *args, **kwargs) -> Tuple[dict, SCards]:
        return cls._create_card(cls.suser, *args, **kwargs)


class BusinessCardBaseLogic(BaseManagerTestCase):
    @classmethod
    def create_card(cls, *args, **kwargs) -> Tuple[dict, SCards]:
        return cls._create_card(cls.suser, *args, **kwargs)


class PrintedCardBaseLogic(BaseTestCase):
    @classmethod
    def _create_card(
        cls, *args, to_card: SCards = None, **kwargs
    ) -> Tuple[dict, SCards]:
        media = kwargs.pop("media", cls.helper.create_media()[1])
        if to_card is None:
            _, cls.personal_card = cls.helper.create_card(*args, media=media, **kwargs)
            to_card = cls.personal_card
        return cls.helper.create_card(
            *args, media=media, card_class=PrintedCard, to_card=to_card.id, **kwargs
        )

    @classmethod
    def create_card(cls, *args, **kwargs) -> Tuple[dict, SCards]:
        return cls._create_card(cls.suser, *args, **kwargs)


class FullCardBaseLogic:
    @classmethod
    def create_card_p(cls, *args, **kwargs) -> Tuple[dict, SCards]:
        return PersonalCardBaseLogic._create_card(*args, **kwargs)

    @classmethod
    def create_card_s(cls, *args, **kwargs) -> Tuple[dict, SCards]:
        return ScanCardBaseLogic._create_card(*args, **kwargs)

    @classmethod
    def create_card_b(cls, *args, **kwargs) -> Tuple[dict, SCards]:
        return BusinessCardBaseLogic._create_card(*args, **kwargs)

    @classmethod
    def create_card_c(cls, *args, **kwargs) -> Tuple[dict, SCards]:
        return PrintedCardBaseLogic._create_card(*args, **kwargs)
