from s_properties.bases import PropertyTypes


def sample_properties(
    user,
    media,
    media2,
    email=None,
    company_name="h.@ctiv8",
    mobile="127362947",
    work_mobile="127323447",
    address="khong ai biet",
    tag="girl",
    did="827461238",
    business=False,
    **kwargs,
):
    return list(
        filter(
            None,
            [
                {
                    "label": "name",
                    "value": user.name,
                    "property_type": PropertyTypes.NAME,
                },
                {
                    "label": "company name",
                    "value": company_name,
                    "property_type": PropertyTypes.NAME,
                },
                {
                    "label": "job title",
                    "value": "Dev",
                    "property_type": PropertyTypes.CONTENT,
                },
                {
                    "label": ("email" if business is False else "work email"),
                    "value": email or user.email,
                    "property_type": PropertyTypes.EMAIL,
                },
                {
                    "label": "mobile",
                    "value": mobile,
                    "property_type": PropertyTypes.MOBILE,
                },
                {
                    "label": "work mobile",
                    "value": work_mobile,
                    "property_type": PropertyTypes.MOBILE,
                },
                {
                    "label": "address",
                    "value": address,
                    "property_type": PropertyTypes.ADDRESS,
                },
                {
                    "label": "tag",
                    "value": tag,
                    "property_type": PropertyTypes.TAG,
                },
                {
                    "label": "social linkedin",
                    "value": "http://sihc.org",
                    "property_type": PropertyTypes.URL,
                },
                {
                    "label": "did",
                    "value": did,
                    "property_type": PropertyTypes.MOBILE,
                },
                {
                    "label": "about me",
                    "value": "I have nothing to hide",
                    "property_type": PropertyTypes.CONTENT,
                },
                {
                    "label": "avatar",
                    "media": media.id,
                    "value": "None",
                    "true_value": media.url,
                    "property_type": PropertyTypes.MEDIA,
                },
                media2
                and {
                    "label": "media 1",
                    "media": media2.id,
                    "value": "None",
                    "true_value": media2.url,
                    "property_type": PropertyTypes.MEDIA,
                },
                media2
                and {
                    "label": "background",
                    "media": media2.id,
                    "value": "None",
                    "true_value": media2.url,
                    "property_type": PropertyTypes.MEDIA,
                },
            ],
        )
    )


def sample_card(
    user, media, media2, card_type="P", searchable=True, status="A", **kwargs
):
    from .functions import BaseFunction

    media = media or BaseFunction().create_media(filename="default")[1]

    data = {
        "card_type": card_type,
        "properties": sample_properties(user, media, media2, **kwargs),
        "tier_1_view_key": ["address"],
        "tier_2_view_key": ["email"],
        "tier_3_view_key": ["mobile"],
        "searchable": searchable,
        "status": status,
    }
    return data
