from datetime import datetime
from typing import List, Tuple

from fcm_django.models import FCMDevice

from custom_django.test.sample_data import sample_card
from helper.common.string import split_name
from helper.serializer.to_dict import list_to_dict
from s_auth.models import MAppleAccount
from s_balances.logics.balance import BusinessBalance, CompanyBalance, PersonalBalance
from s_balances.logics.balance_manager import BalanceManager
from s_balances.models import MAndroidPurchase, SCreditLogs
from s_cards.logics.card import BusinessCard, PersonalCard, TemplateCard
from s_cards.models import SCards, SCompanyInvitations
from s_companies.logics.members import CompanyMemberInvitation
from s_companies.models import SCompanies, SCompanyUsers
from s_contacts.logics.contact import CompanyContact, EmployeeContact, UserContact
from s_contacts.models import (
    SContactNotes,
    SContactRequests,
    SContacts,
    SContactTasks,
    SShareContactRequests,
)
from s_medias.models import SMedias
from s_notifications.models import SNotifications
from s_user.models import MUserDevices, SUserRequests, SUsers, User


class BaseFunction:
    MEDIA_STORE = {}
    USER_STORE = {}
    COMPANY_STORE = {}

    def reset(self):
        self.__class__.MEDIA_STORE = {}
        self.__class__.USER_STORE = {}
        self.__class__.COMPANY_STORE = {}

    def get_user(self, email) -> SUsers:
        return SUsers.objects.get(user_auth__email=email)

    def get_verify_token(self, email):
        return SUserRequests.objects.get(
            request=SUserRequests.VERIFY_ACCOUNT, user__user_auth__email=email
        ).token

    def get_invitation_code(self, email):
        return SCompanyInvitations.objects.get(email=email).access_code

    def get_all_card(self):
        cards = SCards.objects.all()
        for card in cards:
            print(f"-------> card {card.id}: ", card.__dict__)

    def get_all_contact(self):
        contacts = SContacts.objects.all()
        for contact in contacts:
            print(f"-------> contact {contact.id}: ", contact.__dict__)

    defautl_user_data = {
        "password": "123456",
        "first_name": "test",
        "last_name": "user",
    }

    def create_user(self, email, user_data: dict = None, **data) -> Tuple[User, SUsers]:
        if email in self.USER_STORE:
            return self.USER_STORE[email]
        new_user_data = self.defautl_user_data.copy()
        user_data and new_user_data.update(user_data)
        new_user_data.update(email=email, username=email)
        if "name" in data:
            name = data.pop("name")
            first_name, last_name = split_name(name)
            new_user_data.update(first_name=first_name, last_name=last_name)
        user: User = User.objects.create(**new_user_data)
        user.set_password(new_user_data.pop("password"))
        FCMDevice.objects.create(
            registration_id=f"dT6v8LetR6m_UY_9EizA9X:{user.id}",
            type="test",
            device_id=f"dT6v8LetR6m_UY_9EizA9X:{user.id}",
            user_id=user.id,
        )
        data.update(user_auth=user)
        s_user = SUsers.objects.create(**data)
        self.USER_STORE[email] = user, s_user
        return self.USER_STORE[email]

    defautl_admin_data = {
        "password": "123456",
        "first_name": "test",
        "last_name": "admin",
        "is_staff": True,
        "is_superuser": True,
    }

    def create_admin(
        self, email, user_data: dict = None, **data
    ) -> Tuple[User, SUsers]:
        admin_data = self.defautl_admin_data.copy()
        user_data and admin_data.update(user_data)
        return self.create_user(email, user_data=admin_data, **data)

    def create_media(self, filename="test", **extra_data) -> Tuple[dict, SMedias]:
        if filename in self.MEDIA_STORE:
            return self.MEDIA_STORE[filename]
        data = dict(
            filename=filename,
            size=0,
            mime="jpg",
            storage_index=1,
            bucket_location="test",
            bucket_folder="test",
        )
        data.update(extra_data)
        media = SMedias.objects.create(**data)
        self.MEDIA_STORE[filename] = data, media
        return self.MEDIA_STORE[filename]

    def create_card(
        self,
        user,
        media=None,
        media2=None,
        card_class: PersonalCard = PersonalCard,
        card_data: dict = None,
        auto_add_credit: bool = True,
        **extra_data,
    ) -> Tuple[dict, SCards]:
        card_data = (
            sample_card(
                user,
                media,
                media2,
                card_type=card_class.CARD_TYPE,
                **extra_data,
            ).copy()
            if card_data is None
            else card_data
        )
        card_data.update(**extra_data)
        # print("card_data", card_data)
        # not auto_add_credit and card_data.update(balance_total_personal=0)
        card: SCards = card_class(user).create(**card_data)
        auto_add_credit and self.create_personal_credit(card, 1)
        return card_data, card

    def create_company(self, name, **extra_data) -> Tuple[dict, SCompanies]:
        if name in self.COMPANY_STORE:
            return self.COMPANY_STORE[name]
        data = {"name": name}
        data.update(**extra_data)
        company = SCompanies.objects.create(**data)
        self.COMPANY_STORE[name] = data, company
        return self.COMPANY_STORE[name]

    def create_company_admin(
        self, user, company, **extra_data
    ) -> Tuple[dict, SCompanyUsers]:
        data = {
            "user": user,
            "company": company,
            "role": SCompanyUsers.ADMIN,
            "status": SCompanyUsers.ACTIVE,
        }
        data.update(**extra_data)
        return data, SCompanyUsers.objects.create(**data)

    def create_company_employee(
        self, user, company, **extra_data
    ) -> Tuple[dict, SCompanyUsers]:
        data = {
            "user": user,
            "company": company,
            "role": SCompanyUsers.MEMBER,
            "status": SCompanyUsers.ACTIVE,
        }
        data.update(**extra_data)
        obj = SCompanyUsers.objects.create(**data)
        return data, obj

    def create_template_card(
        self, user, company, media, media2, card_data=None, **extra_data
    ) -> Tuple[dict, SCards]:
        data = (
            sample_card(
                user,
                media,
                media2,
                company_name=company.name,
                card_type="T",
                business=True,
                **extra_data,
            ).copy()
            if card_data is None
            else card_data
        )
        tmp_user = SUsers
        tmp_user.id = None
        return (
            data,
            TemplateCard(tmp_user, company).create(allow_null=True, **data),
        )

    def create_business_card(
        self,
        user: SUsers,
        company,
        template,
        media,
        media2,
        without_user=False,
        card_data=None,
        company_user=None,
        allow_null: bool = False,
        auto_add_credit: bool = True,
        auto_invite: bool = True,
        **extra_data,
    ) -> Tuple[dict, SCards]:
        data = (
            sample_card(
                user,
                media,
                media2,
                company_name=company.name,
                card_type="B",
                business=True,
            )
            if card_data is None
            else card_data
        )
        data.update(extra_data)
        company_user = company_user or SCompanyUsers.objects.create(
            user=user, company=company, role=SCompanyUsers.MEMBER
        )
        data["company_user"] = company_user
        card_logic = BusinessCard(user)
        card = card_logic.create(company, template, allow_null=allow_null, **data)
        auto_invite and self.create_invitation_code(email=user.email, card=card)
        card.user = user
        not without_user and card_logic._active(without_user=without_user)
        card_logic._save_card()
        user.finished_create_card(card)
        auto_add_credit and self.create_company_credit(card, 2)
        return (data, card)

    def create_user_contact(
        self, own_card, target_card, tier_number=3, **extra_data
    ) -> Tuple[dict, SContacts]:
        data = {
            "own_card": own_card,
            "target_card": target_card,
            "tier_number": tier_number,
        }
        data.update(extra_data)
        obj, _ = UserContact.create_or_get(**data)
        return data, obj

    def create_company_contact(
        self, own_card, target_card, tier_number=3, **extra_data
    ) -> Tuple[dict, SContacts]:
        data = {
            "own_card": own_card,
            "target_card": target_card,
            "tier_number": tier_number,
        }
        obj, _ = CompanyContact.create_or_get(**data)
        return data, obj

    def create_company_employee_contact(
        self, own_card, target_card, tier_number=3, **extra_data
    ) -> Tuple[dict, SContacts]:
        data = {
            "own_card": own_card,
            "target_card": target_card,
            "tier_number": tier_number,
        }
        data.update(extra_data)
        obj, _ = EmployeeContact.create_or_get(**data)
        return data, obj

    def create_note(self, user, contact, **extra_data) -> Tuple[dict, SContactNotes]:
        data = {
            "user": user,
            "contact": contact,
        }
        data.update(extra_data)
        return data, SContactNotes.objects.create(**data)

    def create_task(self, user, contact, **extra_data) -> Tuple[dict, SContactTasks]:
        data = {
            "user": user,
            "contact": contact,
        }
        data.update(extra_data)
        return data, SContactTasks.objects.create(**data)

    def create_contact_request(
        self, from_card, to_card, tier_number=1, **extra_data
    ) -> Tuple[dict, SContactRequests]:
        data = {
            "from_card": from_card,
            "to_card": to_card,
            "user": to_card.user,
            "tier_number": tier_number,
        }
        data.update(extra_data)
        return data, SContactRequests.objects.create(**data)

    def create_share_contact_request(
        self, by_card, from_card, to_card, tier_number=1, **extra_data
    ) -> Tuple[dict, SShareContactRequests]:
        data = {
            "by_card": by_card,
            "from_card": from_card,
            "to_card": to_card,
            "user": from_card.user,
            "tier_number": tier_number,
        }
        data.update(extra_data)
        return data, SShareContactRequests.objects.create(**data)

    def create_note(
        self, user, contact, content, **extra_data
    ) -> Tuple[dict, SContactNotes]:
        data = {
            "user": user,
            "contact": contact,
            "content": content,
        }
        data.update(extra_data)
        return data, SContactNotes.objects.create(**data)

    def create_task(
        self, user, contact, content, **extra_data
    ) -> Tuple[dict, SContactTasks]:
        data = {
            "user": user,
            "contact": contact,
            "content": content,
        }
        data.update(extra_data)
        return data, SContactTasks.objects.create(**data)

    def create_invitation_code(
        self, email, card, access_code=None, _expiration=None, **extra_data
    ) -> Tuple[dict, SCompanyInvitations]:
        data = {
            "access_code": CompanyMemberInvitation.generate_invite_code()
            if access_code is None
            else access_code,
            "_expiration": (
                (
                    datetime.utcnow()
                    + CompanyMemberInvitation.ACCESS_CODE_LIVING_PERIOD
                ).timestamp()
                if _expiration is None
                else _expiration
            ),
            "email": email,
            "card": card,
        }
        data.update(extra_data)
        return data, SCompanyInvitations.objects.create(**data)

    def create_notification(
        self, by_card, user, subject, content, action="TEST", **extra_data
    ) -> Tuple[dict, SNotifications]:
        data = {
            "by_card": by_card,
            "user": user,
            "subject": subject,
            "content": content,
            "action": action,
            **extra_data,
        }
        return data, SNotifications.objects.create(**data)

    def create_personal_credit(
        self, card, amount, **extra_data
    ) -> Tuple[dict, SCreditLogs]:
        data = {
            "card": card,
            "user": card.user,
            "amount": amount,
        }
        return data, BalanceManager(card.user, card).personal.add(amount=amount)

    def create_company_credit(
        self, card, amount, **extra_data
    ) -> Tuple[dict, SCreditLogs]:
        data = {
            "card": card,
            "user": card.user,
            "amount": amount,
        }
        return data, CompanyBalance(data["user"], card).add(amount)

    def create_android_purchase(self, user, amount=1, **extra_data):
        data = {
            "user": user,
            "amount": amount,
            "purchaseTimeMillis": 1627633304270.0,
            "purchaseState": 1,
            "consumptionState": 0,
            "developerPayload": "",
            "orderId": "GPA.3357-1361-2574-53416",
            "purchaseType": 0,
            "acknowledgementState": 1,
            "kind": "androidpublisher#productPurchase",
            "regionCode": "SG",
        }
        data.update(extra_data)
        return data, MAndroidPurchase.objects.create(**data)

    def create_apple_account(self, user, **extra_data):
        data = {
            "user_auth": user.user_auth,
            "id": "123",
        }
        data.update(extra_data)
        return data, MAppleAccount.objects.create(**data)

    def create_device(self, user, **extra_data):
        data = {
            "user": user,
            "device_name": "Samsung Note 9",
            "device_id": "alsdjaoisd",
            "platform": "A",
            "os": "Hello",
            "os_version": "12.123.3",
            "app_version": "1.12.1",
        }
        data.update(extra_data)
        return data, MUserDevices.objects.create(**data)


class Comparator:
    def compare_list(cls, this: list, that: list):
        if len(this) != len(that):
            cls.results = f"len: {len(this)} != {len(that)}\n{this} != {that}"
            return False
        if set(this) != set(that):
            cls.results = f"set: {this} != {that}\n{this} != {that}"
            return False
        return True

    @classmethod
    def compare_dict(cls, this: dict, that: dict, same_key=None):
        same_keys = same_key or (set(this.keys()) & set(that.keys()))
        for key in same_keys:
            if this[key] != that[key]:
                cls.results = f"{key}: {this[key]} != {that[key]}"
                return False
        return True

    @classmethod
    def compare_list_dict(cls, this: List[dict], that: List[dict], index_key: str):
        if len(this) != len(that):
            cls.results = f"len: {len(this)} != {len(that)}\n{this} != {that}"
            return False
        if len(this) == 0:
            return True

        same_keys = set(this[0].keys()) & set(that[0].keys())

        this_dict = list_to_dict(this, key=index_key)
        that_dict = list_to_dict(that, key=index_key)
        if set(this_dict.keys()) != set(that_dict.keys()):
            return False
        for key in this_dict.keys():
            if cls.compare_dict(this_dict[key], that_dict[key], same_keys) is False:
                return False
        return True
