from typing import Dict, List, Tuple, Type, Union

from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models.query import QuerySet

from s_user.models import SUsers


class SortQuery:
    model: models.Model = None

    _prefetch_related: Tuple = None
    _select_related: Tuple = None

    _annotate: Dict = None
    _extra: Dict = None

    _order: List = None
    _distinct: bool = False

    def get(
        self, *args, prefetch_related=None, select_related=None, **kwargs
    ) -> Type[model]:
        if self.model is None:
            raise NotImplementedError
        return (
            self.model.objects.prefetch_related(
                *(
                    self.prefetch_related
                    if prefetch_related is None
                    else prefetch_related
                )
            )
            .select_related(
                *(self.select_related if select_related is None else select_related)
            )
            .filter(*self.filter_args, **self.filters)
            .annotate(**self.annotate)
            .extra(**self.extra)
            .get(*args, **kwargs)
        )

    def get_if_exist(self, *args, **kwargs) -> Type[model]:
        """Get instance if it's exist.
        If there are no instance then return None.
        But if there are more than 1 instances then raise error.
        That is the different point with .first()

        Returns:
            object: Object or None
        """
        try:
            return self.get(*args, **kwargs)
        except ObjectDoesNotExist:
            return None

    def all(
        self, *args, prefetch_related=None, select_related=None, **kwargs
    ) -> Union[QuerySet, List[Type[model]]]:
        if self.model is None:
            raise NotImplementedError
        # print(
        #     "select_related",
        #     self.select_related if select_related is None else select_related,
        # )
        result = (
            self.model.objects.prefetch_related(
                *(
                    self.prefetch_related
                    if prefetch_related is None
                    else prefetch_related
                )
            )
            .select_related(
                *(self.select_related if select_related is None else select_related)
            )
            .filter(*self.filter_args, **self.filters)
            .annotate(**self.annotate)
            .extra(**self.extra)
            .filter(*args, **kwargs)
            .order_by(*self.order)
        )
        return result.distinct() if self._distinct else result

    @property
    def filters(self) -> dict:
        return {}

    @property
    def filter_args(self) -> list:
        return []

    @property
    def prefetch_related(self) -> dict:
        return self._prefetch_related or []

    @property
    def select_related(self) -> list:
        return self._select_related or []

    @property
    def annotate(self) -> dict:
        return self._annotate or {}

    @property
    def extra(self) -> dict:
        return self._extra or {}

    @property
    def order(self) -> list:
        return self._order or []
