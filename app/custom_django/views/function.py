from .params import Params
from .viewsets import SDynamicModelViewSet


def get_params(func):
    DEFAULTS = {
        "offset": 0,
        "limit": 10,
    }

    def request(self: SDynamicModelViewSet, request, *args, **kwargs):
        self.params = Params(request.GET or dict(), defaults=DEFAULTS)
        return func(self, request, *args, **kwargs)

    return request
