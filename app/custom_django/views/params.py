from typing import Any

from dynamic_rest.viewsets import QueryParams

from ..classes import imdict


class NULL:
    pass


class Params:
    def __init__(self, _dict: dict, defaults: imdict = None):
        assert isinstance(
            _dict, (dict, QueryParams)
        ), f"_dict must be Dict or QueryParams, not {type(_dict)}"
        assert isinstance(
            defaults, dict
        ), f"Defaults must be a Dict, not {type(defaults)}"

        self._dict = _dict.copy()
        self._defaults = defaults or imdict()

    def get(self, name, default=NULL, _type=NULL) -> Any:
        default = default if default is not NULL else self._defaults.get(name, None)
        _type = _type if _type is not NULL else self._get_type(default)

        value = self._dict.get(name, NULL)
        if value is NULL:
            return default
        else:
            if _type is NULL:
                return value
            else:
                return _type(value)

    def set_defaults(self, _defaults: dict = None, **kwargs):
        self._defaults = _defaults.copy()
        self._defaults.update(**kwargs)

    @classmethod
    def _get_type(cls, *values):
        _type = NULL
        for value in values:
            if value is NULL or value is None:
                continue
            _type = value.__class__
        return _type

    def __getitem__(self, name: str) -> Any:
        default_value = self._defaults.get(name, NULL)
        _type = self._get_type(default_value)

        return self.get(name, default_value, _type)

    def all(self):
        return self._dict
