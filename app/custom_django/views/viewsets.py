from threading import current_thread

from django.contrib.auth.models import User
from dynamic_rest.viewsets import DynamicModelViewSet
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request
from rest_framework.viewsets import ViewSet

from helper import slogger
from helper.exception_handler import exception_handler
from helper.exceptions import AppException
from helper.response_handler import ResponseHandler
from v_user.view import VUser
from ..auth import STokenAuthentication
from ..classes import imdict
from .params import Params


class CustomeHandler:
    def initial(self, request, *args, **kwargs):
        AppException.request = request
        return super(CustomeHandler, self).initial(request, *args, **kwargs)

    def finalize_response(self, request, response, *args, **kwargs):
        """Handle response

        Args:
            request ([any]): User request
            response ([any]): App reqponse

        Returns:
            [AppReponse]: Wrap response in control
        """
        slogger.debug(
            "user: %r %r",
            getattr(request.user, "id", "None"),
            getattr(request.user, "email", "None"),
        )
        return ResponseHandler.response(response, self)

    def handle_exception(self, exc):
        """Handle exception

        Args:
            exc ([Exception]): App Exception

        Returns:
            [AppReponse]: Wrap exception in control
        """
        exception = exception_handler(exc)
        return exception.resp


class SViewSet(CustomeHandler, ViewSet):
    pass


class SDynamicModelViewSet(CustomeHandler, DynamicModelViewSet):
    DEFAULT_PARAMS = imdict(
        {
            "offset": 0,
            "limit": 50,
        }
    )

    permission_classes = (IsAuthenticated,)
    authentication_classes = (STokenAuthentication,)

    class SRequest(Request):
        params: Params

    def extra_initial(self, request: SRequest, *args, **kwargs):
        if hasattr(request, "user") and request.user is not None:
            user = request.user.susers
            self.view = VUser(user)
            self.user = self.view.user
            self.user_auth: User = request.user
            current_thread.user = self.user

    def initial(self, request: SRequest, *args, **kwargs):
        result = super().initial(request, *args, **kwargs)
        self.extra_initial(request, *args, **kwargs)
        self.params = Params(request.GET, self.DEFAULT_PARAMS)
        self.pag_left = self.params["offset"]
        self.pag_right = self.params["offset"] + self.params["limit"]
        return result
