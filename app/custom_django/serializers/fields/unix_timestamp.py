import datetime
import time

from django.db import models
from rest_framework import serializers


class UnixTimestampField(serializers.DateTimeField):
    def to_representation(self, value):
        """ Return epoch time for a datetime object or ``None``"""
        try:
            return int(time.mktime(value.timetuple()) * 1000)
        except (AttributeError, TypeError):
            return None

    # def to_native(self, value):
    #     """ Return epoch time for a datetime object or ``None``"""
    #     print("--------------------................................")
    #     try:
    #         return int(time.mktime(value.timetuple()))
    #     except (AttributeError, TypeError):
    #         return None

    # def from_native(self, value):
    #     print("................................")
    #     return datetime.datetime.fromtimestamp(int(value))
