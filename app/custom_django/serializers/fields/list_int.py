# https://gist.github.com/jpadilla/8792723

from rest_framework.serializers import ListSerializer


class IntSeparatedField(ListSerializer):
    """
    A field that separates a string with a given separator into
    a native list and reverts a list into a string separated with a given
    separator.
    """

    def __init__(self, *args, **kwargs):
        self.separator = kwargs.pop("separator", ",")
        super(IntSeparatedField, self).__init__(*args, **kwargs)

    def to_representation(self, data: str):
        return [
            self.child.to_representation(item)
            for item in data.split(self.separator)
        ]
