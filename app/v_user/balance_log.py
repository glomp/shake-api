from typing import List, Union

from django.db.models.aggregates import Sum
from django.db.models.expressions import RawSQL
from django.db.models.query import QuerySet
from django.db.models.query_utils import Q

from s_balances.models import SCreditLogs
from s_cards.models import SCards
from .base import VUBase


class VUBalanceLogBase(VUBase):
    _prefetch_related = []
    _select_related = ("card",)
    _order = ("-_created",)

    model = SCreditLogs

    def get(self, *args, **kwargs) -> SCreditLogs:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> SCreditLogs:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SCreditLogs]]:
        return super().all(*args, **kwargs)


class VUBalanceLogByMonthBase(VUBalanceLogBase):
    TYPE = SCreditLogs.COMPANY

    # _annotate = dict(
    #     month=RawSQL(
    #         f"DATE_FORMAT(FROM_UNIXTIME(`{SCreditLogs._meta.db_table}`._created), '%%Y-%%m')",
    #         [],
    #     ),
    # )
    # _values = ["month"]
    _annotate = dict(
        year=RawSQL(
            f"YEAR(FROM_UNIXTIME(`{SCreditLogs._meta.db_table}`._created))",
            [],
        ),
        month=RawSQL(
            f"MONTH(FROM_UNIXTIME(`{SCreditLogs._meta.db_table}`._created))",
            [],
        ),
    )
    _values = ["year", "month"]
    _annotate2 = dict(
        total_company_credit_used=Sum(
            "amount",
            filter=Q(credit_type=SCreditLogs.COMPANY),
        ),
        total_personal_credit_used=Sum(
            "amount",
            filter=Q(credit_type=SCreditLogs.PERSONAL),
        ),
    )
    _values2 = [
        *_values,
        "total_company_credit_used",
        "total_personal_credit_used",
    ]
    _extra_filter_args = [
        Q(Q(total_company_credit_used__gt=0) | Q(total_personal_credit_used__gt=0)),
    ]

    @property
    def filters(self):
        return dict(
            credit_type=self.TYPE,
        )

    def get(self, *args, **kwargs) -> SCreditLogs:
        raise NotImplementedError()

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SCreditLogs]]:
        return list(
            super()
            .all(*args, **kwargs)
            .values(*self._values)
            .annotate(**self._annotate2)
            .values(*self._values2)
            .filter(*self._extra_filter_args)
        )


class VUBalanceLogByYearBase(VUBalanceLogByMonthBase):
    _annotate = dict(
        year=RawSQL(
            f"YEAR(FROM_UNIXTIME(`{SCreditLogs._meta.db_table}`._created))",
            [],
        ),
    )
    _values = ["year"]
    _values2 = [
        *_values,
        "total_company_credit_used",
        "total_personal_credit_used",
    ]


class VUPersonalBalanceLogBase(VUBalanceLogBase):
    TYPE = SCreditLogs.PERSONAL

    @property
    def filters(self):
        return dict(
            credit_type=self.TYPE,
            card__user_id=self.user.id,
        )

    @property
    def filter_args(self) -> list:
        return [
            ~Q(card__card_type=SCards.BUSINESS),
        ]


class VUBusinessBalanceLogBase(VUBalanceLogBase):
    TYPE = SCreditLogs.PERSONAL

    @property
    def filters(self):
        return dict(
            credit_type=self.TYPE,
            card__user_id=self.user.id,
            card__card_type=SCards.BUSINESS,
        )


class VUCompanyBalanceLogBase(VUBusinessBalanceLogBase):
    TYPE = SCreditLogs.COMPANY


class VUPersonalBalanceLogByMonth(VUBalanceLogByMonthBase, VUPersonalBalanceLogBase):
    pass


class VUPersonalBalanceLogByYear(VUBalanceLogByYearBase, VUPersonalBalanceLogBase):
    pass


class VUPersonalBalanceLog(VUBalanceLogBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.by_month = VUPersonalBalanceLogByMonth(*args, **kwargs)
        self.by_year = VUPersonalBalanceLogByYear(*args, **kwargs)


class VUBusinessBalanceLogByMonth(VUBalanceLogByMonthBase, VUBusinessBalanceLogBase):
    pass


class VUBusinessBalanceLogByYear(VUBalanceLogByYearBase, VUBusinessBalanceLogBase):
    pass


class VUBusinessBalanceLog(VUBalanceLogBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.by_month = VUBusinessBalanceLogByMonth(*args, **kwargs)
        self.by_year = VUBusinessBalanceLogByYear(*args, **kwargs)


class VUCompanyBalanceLogByMonth(VUBalanceLogByMonthBase, VUCompanyBalanceLogBase):
    pass


class VUCompanyBalanceLogByYear(VUBalanceLogByYearBase, VUCompanyBalanceLogBase):
    pass


class VUCompanyBalanceLog(VUBalanceLogBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.by_month = VUCompanyBalanceLogByMonth(*args, **kwargs)
        self.by_year = VUCompanyBalanceLogByYear(*args, **kwargs)


class VUBalanceLog(VUBalanceLogBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.personal = VUPersonalBalanceLog(*args, **kwargs)
        self.business = VUBusinessBalanceLog(*args, **kwargs)
        self.company = VUCompanyBalanceLog(*args, **kwargs)

    @property
    def filters(self):
        return dict(
            card__user_id=self.user.id,
        )
