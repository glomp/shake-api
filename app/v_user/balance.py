from typing import List, Union

from django.db.models import Sum
from django.db.models.query_utils import Q
from django_mysql.models.query import QuerySet

from s_balances.models import SCreditLogs
from s_cards.models import SCards
from .card import VUCard


class VUTotalBalance(VUCard):
    model = SCards

    _select_related = ("company_user",)
    _prefetch_related = ("sproperties_set",)
    _annotate = dict(
        total_company_credit_used=Sum(
            "credit_logs__amount",
            filter=Q(credit_logs__credit_type=SCreditLogs.COMPANY),
        ),
        total_personal_credit_used=Sum(
            "credit_logs__amount",
            filter=Q(credit_logs__credit_type=SCreditLogs.PERSONAL),
        ),
    )
    _extra_filter_args = [
        Q(Q(total_company_credit_used__gt=0) | Q(total_personal_credit_used__gt=0)),
    ]

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SCards]]:
        return super().all(*args, *self._extra_filter_args, **kwargs)
