from typing import List, Union

from django.db.models.query import QuerySet
from rest_framework.authtoken.models import Token

from .base import VUBase


class VUAuthtoken(VUBase):
    model = Token

    @property
    def filters(self) -> dict:
        return dict(
            user_id=self.user.user_auth.id,
        )

    def get(self, *args, **kwargs) -> Token:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> Token:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[Token]]:
        return super().all(*args, **kwargs)
