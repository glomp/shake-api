from typing import List, Union

from django.db.models.query import QuerySet
from django.db.models.query_utils import Q

from s_user.models import MUserDevices
from .base import VUBase


class VUDevice(VUBase):
    model = MUserDevices

    _order = ("-last_use",)

    @property
    def filters(self) -> dict:
        return dict(
            user_id=self.user.id,
        )

    def get(self, *args, **kwargs) -> MUserDevices:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> MUserDevices:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[MUserDevices]]:
        return super().all(*args, **kwargs)
