from typing import List, Union

from django.db.models.query import QuerySet
from django.db.models.query_utils import Q

from s_contacts.models import SContactRequests
from s_user.models import SUsers
from v_user.base import VUBase


class VUBaseContactRequest(VUBase):
    model = SContactRequests

    _select_related = (
        "from_card",
        "to_card",
    )

    def get(self, *args, **kwargs) -> SContactRequests:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> SContactRequests:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SContactRequests]]:
        return super().all(*args, **kwargs)


class VUAdvancedContactRequest(VUBaseContactRequest):
    @property
    def filter_args(self) -> list:
        return [
            ~Q(status=SContactRequests.DESTROYED),
        ]

    @property
    def filters(self) -> dict:
        return dict(
            user_id=self.user.id,
        )


class VUContactRequest(VUBaseContactRequest):
    def __init__(self, user: SUsers):
        super().__init__(user)
        self.advanced = VUAdvancedContactRequest(user)

    @property
    def filters(self) -> dict:
        return dict(
            status=SContactRequests.HOLD,
            user_id=self.user.id,
        )
