from typing import List, Union

from django.db.models.expressions import F
from django.db.models.query import QuerySet

from custom_django.models.functions.group_concat import GroupConcat
from s_cards.models import SCards
from .base import VUBase


class VUContactCard(VUBase):
    model = SCards

    _prefetch_related = ("sproperties_set",)
    _annotate = dict(
        tiers=GroupConcat(
            F("related_contacts__tier_number"),
            "tiers",
            separator=",",
        ),
        belong_to_contacts=GroupConcat(
            F("related_contacts__id"),
            separator=",",
        ),
        belong_to_cards=GroupConcat(
            F("related_contacts__user_card_id"),
            separator=",",
        ),
    )

    @property
    def filters(self) -> dict:
        return dict(
            status=SCards.ACTIVE,
            related_contacts__user_id=self.user.id,
        )

    def get(self, *args, **kwargs) -> SCards:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> SCards:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SCards]]:
        return super().all(*args, **kwargs)
