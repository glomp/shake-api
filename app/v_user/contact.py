from typing import List, Union

from django.db.models.query import QuerySet
from django.db.models.query_utils import Q

from s_cards.models import SCards
from s_contacts.models import SContacts
from s_user.models import SUsers
from v_user.contact_card import VUContactCard
from .base import VUBase


class VUContactBase(VUBase):
    model = SContacts

    def get(self, *args, **kwargs) -> SContacts:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> SContacts:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SContacts]]:
        return super().all(*args, **kwargs)

    @property
    def filter_args(self) -> list:
        return [
            ~Q(status=SContacts.DESTROYED),
        ]


class VUPersonalContact(VUContactBase):
    @property
    def filters(self) -> dict:
        return dict(
            user_id=self.user.id,
            user_card__card_type__in=[SCards.PERSONAL, SCards.SCAN],
        )


class VUBusinessContact(VUContactBase):
    @property
    def filters(self) -> dict:
        return dict(
            user_id=self.user.id,
            user_card__card_type=SCards.BUSINESS,
        )


class VUContact(VUContactBase):
    def __init__(self, user: SUsers):
        super().__init__(user)
        self.personal = VUPersonalContact(user)
        self.business = VUBusinessContact(user)
        self.card = VUContactCard(user)

    @property
    def filters(self) -> dict:
        return dict(
            user_id=self.user.id,
        )
