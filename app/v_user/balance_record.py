from typing import List, Union

from django.db.models.query import QuerySet

from s_cards.models import SCards
from s_contacts.models import SContactRequests
from s_user.models import SUsers
from .base import VUBase


class VUBalanceRecordBase(VUBase):
    model = SContactRequests

    def get(self, *args, **kwargs) -> SContactRequests:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> SContactRequests:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SContactRequests]]:
        return super().all(*args, **kwargs)


class VUFreeBalanceRecord(VUBalanceRecordBase):
    REQUEST_TYPE = SContactRequests.FREE

    @property
    def filters(self):
        return dict(
            from_card__user_id=self.user.id,
            credit_type=self.REQUEST_TYPE,
        )


class VUPersonalBalanceRecord(VUBalanceRecordBase):
    REQUEST_TYPE = SContactRequests.PERSONAL
    CARD_TYPE = [SCards.PERSONAL, SCards.SCAN]

    _prefetch_related = ("from_card",)

    @property
    def filters(self):
        return dict(
            user_id=self.user.id,
            credit_type=self.REQUEST_TYPE,
            from_card__card_type__in=self.CARD_TYPE,
        )


class VUBusinessBalanceRecord(VUBalanceRecordBase):
    REQUEST_TYPE = SContactRequests.PERSONAL
    CARD_TYPE = SCards.BUSINESS

    _prefetch_related = ("from_card",)

    @property
    def filters(self):
        return dict(
            user_id=self.user.id,
            credit_type=self.REQUEST_TYPE,
            from_card__card_type=self.CARD_TYPE,
        )


class VUCompanyBalanceRecord(VUBalanceRecordBase):
    REQUEST_TYPE = SContactRequests.COMPANY

    @property
    def filters(self):
        return dict(
            user_id=self.user.id,
            credit_type=self.REQUEST_TYPE,
        )


class VUBalanceRecord(VUBalanceRecordBase):
    def __init__(self, user: SUsers):
        super().__init__(user)
        self.free = VUFreeBalanceRecord(user)
        self.personal = VUPersonalBalanceRecord(user)
        self.business = VUBusinessBalanceRecord(user)
        self.company = VUCompanyBalanceRecord(user)

    @property
    def filters(self):
        return dict(
            user_id=self.user.id,
        )
