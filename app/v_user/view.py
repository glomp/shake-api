from s_user.models import SUsers
from v_friend.view import VFriend
from .android_purchase import VUAndroidPurchase
from .authtoken import VUAuthtoken
from .balance import VUTotalBalance
from .balance_log import VUBalanceLog
from .balance_record import VUBalanceRecord
from .base import VUBase
from .card import VUCard
from .company import VUCompany
from .company_user import VUCompanyUser
from .contact import VUContact
from .contact_card import VUContactCard
from .contact_request import VUContactRequest
from .contact_share import VUContactShare
from .device import VUDevice
from .fcm import VUFCM
from .note_task import VUNote, VUTask
from .notification import VUNotification
from .request import VURequest


class VUser(VUBase):
    def __init__(self, user: SUsers):
        super().__init__(user)

        self.card = VUCard(user)

        self.company = VUCompany(user)
        self.company_user = VUCompanyUser(user)

        self.contact = VUContact(user)
        self.contact_request = VUContactRequest(user)
        self.contact_card = VUContactCard(user)
        self.contact_share = VUContactShare(user)

        self.note = VUNote(user)
        self.task = VUTask(user)

        self.notification = VUNotification(user)

        self.total_balance = VUTotalBalance(user)
        self.balance_log = VUBalanceLog(user)
        self.balance_record = VUBalanceRecord(user)

        self.android_purchase = VUAndroidPurchase(user)

        self.fcm = VUFCM(user)
        self.authtoken = VUAuthtoken(user)
        self.request = VURequest(user)
        self.device = VUDevice(user)

        self.friend = VFriend(user)
