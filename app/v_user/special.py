from s_cards.models import SCards
from s_contacts.models import SContacts


def get_me_from_another_user_contact(my_user_id, another_user_id) -> SContacts:
    return SContacts.objects.filter(
        user_id=my_user_id, related_user_id=another_user_id, status=SContacts.ACTIVE
    ).first()


def get_another_user_in_my_contact(my_user_id, another_user_id) -> SContacts:
    return SContacts.objects.filter(
        user_id=another_user_id, related_user_id=my_user_id, status=SContacts.ACTIVE
    ).first()


def get_first_card(user_id) -> SCards:
    return (
        SCards.objects.filter(
            user_id=user_id, card_type=SCards.PERSONAL, status=SCards.ACTIVE
        ).first()
        or SCards.objects.filter(
            user_id=user_id, card_type=SCards.BUSINESS, status=SCards.ACTIVE
        ).first()
        or SCards.objects.filter(
            user_id=user_id, card_type=SCards.SCAN, status=SCards.ACTIVE
        ).first()
    )


def get_first_card2(user_id) -> SCards:
    return (
        SCards.objects.extra(
            select={
                "priority": (
                    "CASE `card_type` "
                    f"WHEN '{SCards.PERSONAL}' THEN 1 "
                    f"WHEN '{SCards.BUSINESS}' THEN 2 "
                    f"WHEN '{SCards.SCAN}' THEN 3 "
                    "ELSE 4 "
                    "END"
                )
            }
        )
        .filter(user_id=user_id, status=SCards.ACTIVE)
        .order_by("priority")
        .first()
    )


def get_my_card_let_another_user_know_me(my_user_id, another_user_id) -> SCards:
    contact = (
        None
        or get_me_from_another_user_contact(my_user_id, another_user_id)
        or get_another_user_in_my_contact(my_user_id, another_user_id)
    )

    if contact is not None:
        if contact.user_id == my_user_id:
            return SCards.objects.get(id=contact.user_card_id)
        else:
            return SCards.objects.get(id=contact.card_id)
    else:
        return get_first_card2(my_user_id)
