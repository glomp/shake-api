from typing import List, Union

from django.db.models.query import QuerySet

from s_contacts.models import SContactNotes, SContactTasks
from .base import VUBase


class VUNote(VUBase):
    model = SContactNotes

    @property
    def filters(self) -> dict:
        return dict(
            user_id=self.user.id,
        )

    def get(self, *args, **kwargs) -> SContactNotes:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> SContactNotes:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SContactNotes]]:
        return super().all(*args, **kwargs)


class VUTask(VUBase):
    model = SContactTasks

    @property
    def filters(self) -> dict:
        return dict(
            user_id=self.user.id,
        )

    def get(self, *args, **kwargs) -> SContactTasks:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> SContactTasks:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SContactTasks]]:
        return super().all(*args, **kwargs)
