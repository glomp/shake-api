from typing import List, Union

from django.db.models.aggregates import Count, Sum
from django.db.models.query import QuerySet
from django.db.models.query_utils import Q

from s_notifications.models import SNotifications
from .base import VUBase


class VUNotificationBase(VUBase):
    model = SNotifications

    _order = ("-_created",)

    def get(self, *args, **kwargs) -> SNotifications:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> SNotifications:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SNotifications]]:
        return super().all(*args, **kwargs)


class VUNotification(VUNotificationBase):
    @property
    def filter_args(self) -> list:
        return [
            ~Q(status=SNotifications.DESTROYED),
        ]

    @property
    def filters(self) -> dict:
        return dict(
            user_id=self.user.id,
        )

    def status(self, *args, **kwargs) -> QuerySet:
        return (
            SNotifications.objects.filter(**self.filters)
            .filter(*args, **kwargs)
            .values("user_id")
            .annotate(
                read=Count("user_id", filter=Q(status=SNotifications.READ)),
                unread=Count("user_id", filter=Q(status=SNotifications.UNREAD)),
            )
            .get()
        )
