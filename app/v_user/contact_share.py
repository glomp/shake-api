from typing import List, Union

from django.db.models.query import QuerySet
from django.db.models.query_utils import Q

from s_contacts.models import SShareContactRequests
from s_user.models import SUsers
from v_user.base import VUBase


class VUContactShare(VUBase):
    model = SShareContactRequests
    _prefetch_related = ("from_card", "to_card")

    def get(self, *args, **kwargs) -> SShareContactRequests:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> SShareContactRequests:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SShareContactRequests]]:
        return super().all(*args, **kwargs)


class VUAdvancedContactShare(VUContactShare):
    @property
    def filter_args(self) -> list:
        return [
            ~Q(statuts=SShareContactRequests.DESTROYED),
        ]

    @property
    def filters(self) -> dict:
        return dict(
            user_id=self.user.id,
        )


class VUContactShare(VUContactShare):
    def __init__(self, user: SUsers):
        super().__init__(user)
        self.advanced = VUAdvancedContactShare(user)

    @property
    def filters(self) -> dict:
        return dict(
            status=SShareContactRequests.HOLD,
            user_id=self.user.id,
        )
