from typing import List, Union

from django.db.models.query import QuerySet

from s_balances.models import MAndroidPurchase
from .base import VUBase


class VUAndroidPurchase(VUBase):
    model = MAndroidPurchase

    _order = ("-_created",)

    def get(self, *args, **kwargs) -> MAndroidPurchase:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> MAndroidPurchase:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[MAndroidPurchase]]:
        return super().all(*args, **kwargs)

    @property
    def filters(self) -> dict:
        return dict(
            user_id=self.user.id,
        )
