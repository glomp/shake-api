from typing import List, Union

from django.db.models.query import QuerySet

from s_companies.models import SCompanies, SCompanyUsers
from .base import VUBase


class CompanyBase(VUBase):
    model = SCompanies

    def get(self, *args, **kwargs) -> SCompanies:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> SCompanies:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SCompanies]]:
        return super().all(*args, **kwargs)


class VUManagedCompany(CompanyBase):
    @property
    def filters(self) -> dict:
        return dict(
            status=SCompanies.ACTIVE,
            scompanyusers__user_id=self.user.id,
            scompanyusers__status=SCompanyUsers.ACTIVE,
            scompanyusers__role=SCompanyUsers.ADMIN,
        )


class VUCompany(CompanyBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.i_managed = VUManagedCompany(*args, **kwargs)

    @property
    def filters(self) -> dict:
        return dict(
            status=SCompanies.ACTIVE,
            scompanyusers__user_id=self.user.id,
            scompanyusers__status=SCompanyUsers.ACTIVE,
        )
