from typing import List, Union

from django.db.models.query import QuerySet
from django.db.models.query_utils import Q

from s_cards.models import SCards
from s_user.models import SUsers
from .base import VUBase


class VUCardBase(VUBase):
    model = SCards
    _select_related = ("company_user",)
    _prefetch_related = ("sproperties_set",)

    def get(self, *args, **kwargs) -> SCards:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> SCards:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SCards]]:
        return super().all(*args, **kwargs)


class VUIdentityCard(VUCardBase):
    CARD_TYPE = SCards.IDENTITY

    @property
    def filters(self):
        return dict(
            user_id=self.user.id,
            card_type=self.CARD_TYPE,
        )


class VUScanCard(VUCardBase):
    CARD_TYPE = SCards.SCAN

    @property
    def filter_args(self) -> list:
        return [
            ~Q(status=SCards.DESTROYED),
            ~Q(visibility=SCards.SYSTEM),
        ]

    @property
    def filters(self):
        return dict(
            user_id=self.user.id,
            card_type=self.CARD_TYPE,
        )


class VUPersonalCard(VUScanCard):
    CARD_TYPE = SCards.PERSONAL


class VUBusinessCard(VUScanCard):
    CARD_TYPE = SCards.BUSINESS


class VUPrintedCard(VUCardBase):
    CARD_TYPE = SCards.PRINTED

    @property
    def filter_args(self) -> list:
        return [
            ~Q(status=SCards.DESTROYED),
        ]

    @property
    def filters(self):
        return dict(
            related_contacts__user_id=self.user.id,
            card_type=self.CARD_TYPE,
        )


class VUAdvancedCard(VUCardBase):
    @property
    def filter_args(self) -> list:
        return [
            ~Q(status=SCards.DESTROYED),
        ]

    @property
    def filters(self):
        return dict(
            user_id=self.user.id,
        )


class VUCard(VUCardBase):
    def __init__(self, user: SUsers):
        super().__init__(user)
        self.scan = VUScanCard(user)
        self.personal = VUPersonalCard(user)
        self.identity = VUIdentityCard(user)
        self.business = VUBusinessCard(user)
        self.printed = VUPrintedCard(user)
        self.advanced = VUAdvancedCard(user)

    @property
    def filter_args(self) -> list:
        return [
            ~Q(status=SCards.DESTROYED),
            ~Q(visibility=SCards.SYSTEM),
        ]

    @property
    def filters(self):
        return dict(
            user_id=self.user.id,
        )
