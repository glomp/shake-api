from typing import List, Union

from django.db.models.query import QuerySet

from s_user.models import SUserRequests
from .base import VUBase


class VURequest(VUBase):
    model = SUserRequests

    @property
    def filters(self) -> dict:
        return dict(
            user_id=self.user.id,
        )

    def get(self, *args, **kwargs) -> SUserRequests:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> SUserRequests:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SUserRequests]]:
        return super().all(*args, **kwargs)
