from s_companies.models import SCompanies


class Template:
    def __init__(self, company: SCompanies):
        self.company = company


class DefaultTemplate(Template):
    pass
