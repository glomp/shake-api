from django.conf.urls import url
from django.conf.urls import include


from rest_framework.routers import DefaultRouter
from dynamic_rest.routers import DynamicRouter

from . import views

router = DefaultRouter()
router.register('header', views.SCardTemplateHdrViewSet)
router.register('detail', views.SCardTemplateDtlViewSet)

urlpatterns = [
    url(r'', include(router.urls))
]