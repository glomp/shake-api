from dynamic_rest.fields import DynamicRelationField
from dynamic_rest.serializers import DynamicModelSerializer
from rest_framework import serializers

from .models import STemplates


class SMiniTemplatesSerializer(DynamicModelSerializer):
    class Meta:
        model = STemplates
        fields = (
            "id",
            "company_id",
        )


class STemplatesSerializer(SMiniTemplatesSerializer):
    class Meta:
        model = STemplates
        fields = "__all__"
