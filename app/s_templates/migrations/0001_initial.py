# Generated by Django 2.2.16 on 2020-11-25 04:38

import custom_django.models.fields.unix_timestamp
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('s_companies', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='STemplates',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(choices=[('A', 'ACTIVE'), ('I', 'INACTIVE')], default='A', max_length=1)),
                ('company_name', models.CharField(max_length=63)),
                ('company_image', models.CharField(blank=True, max_length=127, null=True)),
                ('company_background', models.CharField(blank=True, max_length=127, null=True)),
                ('company_phone', models.CharField(blank=True, max_length=31, null=True)),
                ('company_email', models.CharField(blank=True, max_length=255, null=True)),
                ('company_address', models.CharField(blank=True, max_length=255, null=True)),
                ('_created', custom_django.models.fields.unix_timestamp.UnixTimeStampField(auto_now_add=True, null=True)),
                ('_updated', custom_django.models.fields.unix_timestamp.UnixTimeStampField(auto_now=True, null=True)),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='s_companies.SCompanies')),
            ],
            options={
                'db_table': 's_template',
                'managed': True,
            },
        ),
    ]
