from django.db import models
from django_mysql.models import JSONField

from custom_django.models import SModel, fields
from s_companies.models import SCompanies
from s_properties.bases import PropertyTypes


class STemplates(SModel):
    ACTIVE = "A"
    INACTIVE = "I"
    STATUS = (
        (ACTIVE, "ACTIVE"),
        (INACTIVE, "INACTIVE"),
    )

    status = models.CharField(max_length=1, default=ACTIVE, choices=STATUS)
    company = models.ForeignKey(SCompanies, models.DO_NOTHING)

    MANDATORY_FIELDS = {
        "company name": PropertyTypes.NAME,
    }
    NORMAL_FIELDS = {
        "company mobile": PropertyTypes.MOBILE,
        "company email": PropertyTypes.EMAIL,
        "company address": PropertyTypes.ADDRESS,
        "company image": PropertyTypes.OTHER,
        "company background": PropertyTypes.OTHER,
        "company website": PropertyTypes.URL,
    }

    _created = fields.UnixTimeStampField(null=True, auto_now_add=True)
    _updated = fields.UnixTimeStampField(null=True, auto_now=True)

    def get_readonly_fields(self, request, obj=None):
        return ["company"] if obj else []

    class Meta:
        managed = True
        db_table = "s_template"


# class SCardTemplateDtl(SModel):
#     label = models.CharField(max_length=45)
#     value = models.CharField(max_length=45, blank=True, null=True)
#     hdr = models.ForeignKey("SCardTemplateHdr", models.DO_NOTHING)
#     date_created = models.DateTimeField(
#         blank=True, null=True, auto_now_add=True
#     )
#     date_updated = models.DateTimeField(blank=True, null=True, auto_now=True)

#     class Meta:
#         managed = True
#         db_table = "s_card_template_dtl"


# class SCardTemplateHdr(SModel):
#     CARD_TYPES = (("P", "PERSONAL"), ("B", "BUSINESS"))
#     name = models.CharField(max_length=45)
#     companies = models.ForeignKey(SCompanies, models.DO_NOTHING)
#     card_type = models.CharField(max_length=45, choices=CARD_TYPES)
#     date_created = models.DateTimeField(
#         blank=True, null=True, auto_now_add=True
#     )
#     date_updated = models.DateTimeField(blank=True, null=True, auto_now=True)

#     class Meta:
#         managed = True
#         db_table = "s_card_template_hdr"
