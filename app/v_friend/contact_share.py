from typing import List, Union

from django.db.models.query_utils import Q
from django_mysql.models.query import QuerySet

from custom_django.test import status
from s_contacts.models import SShareContactRequests
from s_user.models import SUsers
from .base import VFBase


class VFBaseContactShare(VFBase):
    model = SShareContactRequests

    def get(self, *args, **kwargs) -> SShareContactRequests:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> SShareContactRequests:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SShareContactRequests]]:
        return super().all(*args, **kwargs)


class VFAdvancedContactShare(VFBaseContactShare):
    @property
    def filter_args(self) -> list:
        return [~Q(status=SShareContactRequests.DESTROYED)]

    @property
    def filters(self) -> dict:
        return dict(
            by_card__user_id=self.user.id,
        )


class VFContactShareTargetMe(VFBaseContactShare):
    @property
    def filter_args(self) -> list:
        return [~Q(status=SShareContactRequests.DESTROYED)]

    @property
    def filters(self) -> dict:
        return dict(
            to_card__user_id=self.user.id,
        )


class VFContactShare(VFBaseContactShare):
    def __init__(self, user: SUsers):
        super().__init__(user)
        self.advanced = VFAdvancedContactShare(user)
        self.target_me = VFContactShareTargetMe(user)

    @property
    def filters(self) -> dict:
        return dict(
            status=SShareContactRequests.HOLD,
            by_card__user_id=self.user.id,
        )
