from typing import List, Union

from django_mysql.models.query import QuerySet

from s_cards.models import SCards
from s_user.models import SUsers
from .base import VFBase


class VFCardBase(VFBase):
    model = SCards
    _select_related = ("company_user",)
    _prefetch_related = ("sproperties_set",)

    def get(self, *args, **kwargs) -> SCards:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> SCards:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SCards]]:
        return super().all(*args, **kwargs)


class VFCardKnowMe(VFCardBase):
    @property
    def filters(self) -> dict:
        return dict(
            status=SCards.ACTIVE,
            own_contacts__card__user_id=self.user.id,
        )


class VFCardMeKnow(VFCardBase):
    @property
    def filters(self) -> dict:
        return dict(
            status=SCards.ACTIVE,
            related_contacts__user_id=self.user.id,
        )


class VFCard(VFBase):
    def __init__(self, user: SUsers):
        super().__init__(user)
        self.know_me = VFCardKnowMe(user)
        self.me_know = VFCardMeKnow(user)
