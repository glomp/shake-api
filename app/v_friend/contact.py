from typing import List, Union

from django.db.models.query_utils import Q
from django_mysql.models.query import QuerySet

from s_contacts.models import SContacts
from s_user.models import SUsers
from .base import VFBase


class VFContactBase(VFBase):
    model = SContacts

    def get(self, *args, **kwargs) -> SContacts:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> SContacts:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SContacts]]:
        return super().all(*args, **kwargs)

    @property
    def filter_args(self) -> list:
        return [
            ~Q(status=SContacts.DESTROYED),
        ]


class VFContactTmp(VFContactBase):
    _distinct = True

    @property
    def filters(self) -> dict:
        return dict(
            related_user_id=self.user.id,
        )


class VFContactOfContact(VFContactBase):
    _distinct = True

    def __init__(self, user: SUsers):
        super().__init__(user)
        self.friend_contact = VFContactTmp(user)

    @property
    def filter_args(self) -> list:
        return [
            ~Q(status=SContacts.DESTROYED),
            Q(related_user_id=self.user.id)
            | Q(related_user_id__in=self.friend_contact.all().values("user_id")),
        ]


class VFContactAdvanced(VFContactBase):
    @property
    def filters(self) -> dict:
        return dict(
            related_user_id=self.user.id,
        )


class VFContact(VFContactTmp):
    def __init__(self, user: SUsers):
        super().__init__(user)
        self.of_contact = VFContactOfContact(user)
        self.advanced = VFContactAdvanced(user)
