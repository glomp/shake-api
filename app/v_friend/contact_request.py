from typing import List, Union

from django.db.models.query_utils import Q
from django_mysql.models.query import QuerySet

from s_contacts.models import SContactRequests
from s_user.models import SUsers
from .base import VFBase


class VFBaseContactRequest(VFBase):
    model = SContactRequests

    def get(self, *args, **kwargs) -> SContactRequests:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> SContactRequests:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SContactRequests]]:
        return super().all(*args, **kwargs)


class VFAdvancedContactRequest(VFBaseContactRequest):
    @property
    def filter_args(self) -> list:
        return [
            ~Q(status=SContactRequests.DESTROYED),
        ]

    @property
    def filters(self) -> dict:
        return dict(
            from_card__user_id=self.user.id,
        )


class VFContactRequest(VFBaseContactRequest):
    def __init__(self, user: SUsers):
        super().__init__(user)
        self.advanced = VFAdvancedContactRequest(user)

    @property
    def filters(self) -> dict:
        return dict(
            status=SContactRequests.HOLD,
            from_card__user_id=self.user.id,
        )
