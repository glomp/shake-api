from typing import List, Union

from django_mysql.models.query import QuerySet

from s_user.models import SUsers
from .base import VFBase


class VFUserBase(VFBase):
    model = SUsers

    _select_related = ("user_auth",)

    def get(self, *args, **kwargs) -> SUsers:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> SUsers:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SUsers]]:
        return super().all(*args, **kwargs)


class VFKnowMe(VFUserBase):
    _distinct = True
    _order = ("id",)

    @property
    def filters(self) -> dict:
        return dict(
            scontacts__related_user_id=self.user.id,
        )


class VFMeKnow(VFUserBase):
    _distinct = True
    _order = ("id",)

    @property
    def filters(self) -> dict:
        return dict(
            related_contacts__user_id=self.user.id,
        )


class VFContactOfContact(VFUserBase):
    _distinct = True
    _order = ("id",)

    def __init__(self, user: SUsers):
        super().__init__(user)
        self.friend_know_me = VFKnowMe(user)

    @property
    def filters(self) -> dict:
        return dict(
            scontacts__related_user_id__in=self.friend_know_me.all(),
        )


class VFUser(VFBase):
    def __init__(self, user: SUsers):
        super().__init__(user)
        self.me_know = VFMeKnow(user)
        self.know_me = VFKnowMe(user)
        self.contact_of_contact = VFContactOfContact(user)
