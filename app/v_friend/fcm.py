from typing import List, Union

from django.db.models.aggregates import Count
from django.db.models.query import QuerySet
from django.db.models.query_utils import Q
from fcm_django.models import FCMDevice

from s_notifications.models import SNotifications
from s_user.models import SUsers
from .base import VFBase


class VFFCM(VFBase):
    model = FCMDevice

    _order = ("-date_created",)
    _annotate = dict(
        notification_unread=Count(
            "user__susers__notifications",
            filter=Q(user__susers__notifications__status=SNotifications.UNREAD),
        )
    )

    def __init__(self, user: SUsers = None):
        super().__init__(user)

    def get(self, *args, **kwargs) -> FCMDevice:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> FCMDevice:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[FCMDevice]]:
        return super().all(*args, **kwargs)
