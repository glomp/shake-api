from s_user.models import SUsers
from .base import VFBase
from .card import VFCard
from .contact import VFContact
from .contact_request import VFContactRequest
from .contact_share import VFContactShare
from .fcm import VFFCM
from .user import VFUser


class VFriend(VFBase):
    def __init__(self, user: SUsers):
        super().__init__(user)

        self.user = VFUser(user)

        self.card = VFCard(user)

        self.contact = VFContact(user)
        self.contact_request = VFContactRequest(user)
        self.contact_share = VFContactShare(user)

        self.fcm = VFFCM(user)
