from django.db import transaction

from custom_django.views.viewsets import SDynamicModelViewSet
from .logics.store import SUserStorage
from .models import SMedias
from .serializers import SPublicThoroughMediaSerializer


class SMediasViewSet(SDynamicModelViewSet):
    """Handles creating and updating media."""

    serializer_class = SPublicThoroughMediaSerializer
    model_class = SMedias
    queryset = SMedias.objects.all()

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        user_storage = SUserStorage(self.user)

        if len(request.data.getlist("file")) > 1:
            return SPublicThoroughMediaSerializer(
                [
                    user_storage.upload(file_obj, request.data)
                    for file_obj in request.data.getlist("file")
                ],
                many=True,
            )
        else:
            return SPublicThoroughMediaSerializer(
                user_storage.upload(request.data["file"], request.data)
            )

    @transaction.atomic
    def update(self, request, *args, **kwargs):
        resp = super(SMediasViewSet, self).update(request, *args, **kwargs)
        return resp.data["s_medias"]
