from django.conf import settings

from s_user.models import SUsers
from v_user.view import VUser
from ..models import SMedias
from .store import SStorage


class LMedia:
    SILENT_FAILURE = False

    def __init__(self, user: SUsers, view: VUser, media: SMedias):
        self.user = user
        self.view = view

        self.object = media

    def delete(self, silent_failure=SILENT_FAILURE):
        if settings.ENV == "TEST":
            return

        if self.object.thumbnail is not None:
            LMedia(self.user, self.view, self.object.thumbnail).delete(silent_failure)

        storage: SStorage = self.object.get_storage()
        storage().delete(self.object.filename)
