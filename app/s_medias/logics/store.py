import magic
import os
import subprocess

from django.conf import settings

from helper import slogger
from helper.common import files, hashes, images
from s_cards.models import SCards
from s_properties.models import SProperties
from s_user.models import SUsers
from ..custom_storages import PublicMediaStorage, UserStorage
from ..models import SMedias
from ..serializers import SThoroughMediaSerializer


class SStorage:
    STORAGE = PublicMediaStorage()
    FOLDER = "server"
    MIME = magic.Magic(mime=True)

    VIDEO_MIME = {"video/mp4"}

    def __init__(
        self, user: SUsers = None, card: SCards = None, prop: SProperties = None
    ):
        self.user = user
        self.card = card
        self.prop = prop

    def create_thumbnail_for_video(self, file):
        tmp_video_path = self._save_file_to_local(
            file_content=file.read(), filename=f"{file.name}.jpg"
        )

        thumbnail_image_name = (
            f"{self.user.id if self.user is not None else ''}_{hashes.generate_v1(40)}"
        )
        thumbnail_image_path = f"/tmp/{thumbnail_image_name}.jpg"

        subprocess.call(
            [
                "ffmpeg",
                "-i",
                tmp_video_path,
                "-ss",
                "00:00:00.100",
                "-vframes",
                "1",
                thumbnail_image_path,
                "-y",
                "-v",
                "panic",
            ]
        )
        with open(thumbnail_image_path, "rb+") as thumbnail_image_file:
            thumbnail = SStorage().upload(thumbnail_image_file)

        self._delete_file_from_local(thumbnail_image_path, tmp_video_path)
        return thumbnail

    def upload(
        self, file, data: dict = None, mime: str = None, keep_name=False
    ) -> SMedias:
        if isinstance(file, str):
            file = open(file, "rb+")

        file.seek(0)
        file_hash = files.get_hash(file)
        filename = (
            (
                f"{self.user.id if self.user is not None else ''}_"
                f"{file_hash}.{files.get_extension(files.get_name(file.name))}"
            )
            if keep_name is False
            else files.get_name(file.name)
        )
        self.media = SMedias.objects.filter(filename=filename).first()
        if self.media is not None:
            new_comment = data and data.get("comment", None)
            if self.media.comment == new_comment:
                return self.media
            else:
                media_data = SThoroughMediaSerializer(self.media).data
                media_data.pop("id")
                media_data["comment"] = new_comment
                media_ser = SThoroughMediaSerializer(data=media_data)
                media_ser.is_valid(raise_exception=True)
                return media_ser.save()
        slogger.debug("Upload file: %r", file.__dict__)

        file.seek(0)
        if mime is None:
            mime = self.MIME.from_buffer(file.read())
            file.seek(0)
        file_dimensions = images.get_dimensions(file)
        file.seek(0)

        thumbnail = (
            self.create_thumbnail_for_video(file) if mime in self.VIDEO_MIME else None
        )

        media_data = {
            "filename": filename,
            "size": getattr(file, "size", None),
            "height": file_dimensions[1],
            "width": file_dimensions[0],
            "mime": mime,
            "content_type": getattr(file, "content_type", None),
            "comment": data and data.get("comment", None),
            "hash": file_hash,
            "thumbnail": thumbnail,
            "storage_index": self.STORAGE.index,
            "bucket_location": self.STORAGE.location,
            "bucket_folder": self.FOLDER,
        }

        media_ser = SThoroughMediaSerializer(data=media_data)
        media_ser.is_valid(raise_exception=True)
        self.media = media_ser.save()

        media_path = f"{self.FOLDER}/{media_data['filename']}"
        if settings.ENV != "TEST":
            self.STORAGE.save(media_path, file)

        file.close()
        return self.media

    @staticmethod
    def _save_file_to_local(file_content, filename):
        file_path = f"/tmp/{filename}"

        with open(file_path, "wb+") as tmp_video_file:
            tmp_video_file.write(file_content)
        return file_path

    @staticmethod
    def _delete_file_from_local(*filenames):
        [os.unlink(filename) for filename in filenames]


class SUserStorage(SStorage):
    STORAGE = UserStorage()
    FOLDER = "user"
