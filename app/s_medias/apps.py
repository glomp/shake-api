from django.apps import AppConfig


class SMediasConfig(AppConfig):
    name = 's_medias'
