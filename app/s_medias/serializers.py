from dynamic_rest.fields import DynamicRelationField
from dynamic_rest.serializers import DynamicModelSerializer
from rest_framework import serializers

from .models import SMedias


class SThumbnailSerializer(DynamicModelSerializer):
    url = serializers.CharField(read_only=True)

    class Meta:
        model = SMedias
        exclude = SMedias.PERSONAL_FIELDS


class SPublicThoroughMediaSerializer(DynamicModelSerializer):
    url = serializers.CharField(read_only=True)
    thumbnail = DynamicRelationField(SThumbnailSerializer, embed=True)

    class Meta:
        model = SMedias
        exclude = SMedias.PERSONAL_FIELDS


class SThoroughMediaSerializer(SPublicThoroughMediaSerializer):
    class Meta:
        model = SMedias
        fields = "__all__"
