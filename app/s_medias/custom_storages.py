from django.conf import settings
from storages.backends.s3boto3 import S3Boto3Storage

from helper import slogger


class PublicMediaStorage(S3Boto3Storage):
    index = 1  # index the storage to save in db
    location = settings.AWS_PUBLIC_MEDIA_LOCATION
    default_acl = "public-read"
    file_overwrite = False

    static_url = ""


class PrivateMediaStorage(S3Boto3Storage):
    index = 2
    location = settings.AWS_PRIVATE_MEDIA_LOCATION
    default_acl = "private"
    file_overwrite = False
    custom_domain = False

    static_url = ""


class UserStorage(S3Boto3Storage):
    index = 3
    location = settings.AWS_PUBLIC_MEDIA_LOCATION
    default_acl = "public-read"

    static_url = ""


if settings.ENV != "TEST":
    PublicMediaStorage.static_url = PublicMediaStorage().url("")
    PrivateMediaStorage.static_url = PrivateMediaStorage().url("")
    UserStorage.static_url = UserStorage().url("")
    slogger.info("static_url: %s", PublicMediaStorage().static_url)
