from os import path

from django.conf import settings
from django.db import models

from custom_django.models import SModel, fields
from helper.exceptions import AppException
from s_medias.custom_storages import (
    PrivateMediaStorage,
    PublicMediaStorage,
    UserStorage,
)


class SMedias(SModel):
    filename = models.CharField(max_length=127)
    size = models.IntegerField(null=True)
    height = models.IntegerField(default=None, null=True, blank=True)
    width = models.IntegerField(default=None, null=True, blank=True)
    mime = models.CharField(max_length=63)
    content_type = models.CharField(max_length=63, null=True)

    comment = models.CharField(max_length=1023, blank=True, null=True)

    hash = models.CharField(max_length=255)
    thumbnail = models.ForeignKey("SMedias", models.DO_NOTHING, null=True, blank=True)

    storage_index = models.IntegerField()
    bucket_location = models.CharField(max_length=63)
    bucket_folder = models.CharField(max_length=63)

    _created = fields.UnixTimeStampField(null=True, auto_now_add=True)

    STORAGE_MAP = {
        PublicMediaStorage.index: PublicMediaStorage,
        UserStorage.index: UserStorage,
        PrivateMediaStorage.index: PrivateMediaStorage,
    }

    PERSONAL_FIELDS = (
        "hash",
        "storage_index",
        "bucket_location",
        "bucket_folder",
    )

    @property
    def url(self):
        return path.join(
            self.get_storage().static_url, self.bucket_folder, self.filename
        )

    @property
    def real_url(self):
        if hasattr(self, "_url"):
            return self._url
        if settings.ENV == "TEST":
            return path.join(self.bucket_folder, self.filename)

        static_url = self.url
        storage = self.get_storage()
        file_path = path.join(self.bucket_folder, self.filename)
        url = storage().url(file_path)

        if static_url != url:
            raise AppException(
                errcode=500066,
                message="Something went wrong",
                err_message="Static url and real url are different!",
                data={
                    "storage": str(storage),
                    "url": url,
                    "static_url": static_url,
                    "media": self.id,
                },
            )
        self._url = url
        return self._url

    def get_storage(self) -> PublicMediaStorage:
        return self.STORAGE_MAP[self.storage_index]

    def get_readonly_fields(self, request, obj=None):
        return (
            [
                "filename",
                "size",
                "mime",
                "content_type",
                "hash",
                "thumbnail",
                "storage_index",
                "bucket_location",
                "bucket_folder",
            ]
            if obj
            else []
        )

    class Meta:
        managed = True
        db_table = "s_media"
