from typing import List, Union

from django.db.models import Count
from django.db.models.query import QuerySet
from django.db.models.query_utils import Q

from s_cards.models import SCards
from s_contacts.models import SContactRequests, SContacts
from .base import VMBase


class VMCardBase(VMBase):
    model = SCards

    def get(self, *args, **kwargs) -> SCards:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> SCards:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SCards]]:
        return super().all(*args, **kwargs)


class VMCardView(VMCardBase):
    CARD_TYPE = SCards.BUSINESS

    _select_related = ("company_user",)
    _prefetch_related = (
        "sproperties_set",
        "invitation",
        "invitation__inviter",
        "invitation__inviter__scards",
    )
    _annotate = dict(
        contact_request_number=Count("contact_requests_from"),
        contact_request_success_number=Count(
            "contact_requests_from",
            filter=Q(contact_requests_from__status=SContactRequests.ACCEPTED),
        ),
    )
    _extra = dict(
        select={
            "contact_number": (
                "SELECT count(*) "
                "FROM `s_contact` sct "
                "WHERE `s_card`.id = sct.user_card_id "
                f"AND type = '{SContacts.EMPLOYEE}'"
            ),
            "scan_card_number": (
                "SELECT count(*) "
                "FROM `s_contact` sct "
                "JOIN `s_card` contact_card "
                "ON sct.card_id = contact_card.id "
                "WHERE `s_card`.id = sct.user_card_id "
                f"AND type = '{SContacts.COMPANY}' "
                f"AND contact_card.card_type = '{SCards.PRINTED}'"
            ),
        }
    )

    @property
    def filters(self) -> dict:
        return dict(
            status__in=[SCards.ACTIVE, SCards.REQUIRE_INFO],
            template__company_id=self._company.id,
            card_type=self.CARD_TYPE,
        )


class VMTemplateCard(VMCardBase):
    CARD_TYPE = SCards.TEMPLATE

    @property
    def filters(self) -> dict:
        return dict(
            status__in=[SCards.ACTIVE, SCards.REQUIRE_INFO],
            template__company_id=self._company.id,
            card_type=self.CARD_TYPE,
        )


class VMEmployeeCard(VMTemplateCard):
    CARD_TYPE = SCards.BUSINESS


class VMCard(VMCardBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.view = VMCardView(*args, **kwargs)
        self.template = VMTemplateCard(*args, **kwargs)
        self.employee = VMEmployeeCard(*args, **kwargs)

    @property
    def filters(self) -> dict:
        return dict(
            status__in=[SCards.ACTIVE, SCards.REQUIRE_INFO],
            template__company_id=self._company.id,
        )
