from typing import List, Union

from django.db.models.aggregates import Sum
from django.db.models.expressions import RawSQL
from django.db.models.query_utils import Q
from django_mysql.models.query import QuerySet

from s_balances.models import SCreditLogs
from s_cards.models import SCards
from .card import VMCard


class VMTotalBalanceBase(VMCard):
    _select_related = ("company_user",)
    _prefetch_related = (
        "sproperties_set",
        "invitation",
        "invitation__inviter",
        "invitation__inviter__scards",
    )


class VMTotalBalanceByMonth(VMTotalBalanceBase):
    _annotate = dict(
        month=RawSQL("DATE(FROM_UNIXTIME(_created))", []),
    )
    _values = ["month", *SCards._all_fields()]
    _annotate2 = dict(
        total_company_credit_used=Sum(
            "credit_logs__amount",
            filter=Q(credit_logs__credit_type=SCreditLogs.COMPANY),
        ),
        total_personal_credit_used=Sum(
            "credit_logs__amount",
            filter=Q(credit_logs__credit_type=SCreditLogs.PERSONAL),
        ),
    )
    _extra_filter_args = [
        Q(Q(total_company_credit_used__gt=0) | Q(total_personal_credit_used__gt=0)),
    ]

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SCards]]:
        return list(
            super()
            .all()
            .annotate(**self._annotate2)
            .filter(*args, *self._extra_filter_args, **kwargs)
        )


class VMTotalBalance(VMTotalBalanceBase):
    _annotate = dict(
        total_company_credit_used=Sum(
            "credit_logs__amount",
            filter=Q(credit_logs__credit_type=SCreditLogs.COMPANY),
        ),
        total_personal_credit_used=Sum(
            "credit_logs__amount",
            filter=Q(credit_logs__credit_type=SCreditLogs.PERSONAL),
        ),
    )
    _extra_filter_args = [
        Q(Q(total_company_credit_used__gt=0) | Q(total_personal_credit_used__gt=0)),
    ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.by_month = VMTotalBalanceByMonth(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SCards]]:
        return super().all(*args, *self._extra_filter_args, **kwargs)
