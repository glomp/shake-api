from v_manager.balance import VMTotalBalance
from .balance_log import VMBalanceLog
from .base import VMBase
from .card import VMCard
from .company import VMCompany
from .contact import VMContact
from .contact_card import VMContactCard
from .contact_request import VMContactRequest
from .employee import VMEmployee
from .template import VMTemplate


class VManager(VMBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.company = VMCompany(*args, **kwargs)

        self.template = VMTemplate(*args, **kwargs)

        self.employee = VMEmployee(*args, **kwargs)

        self.contact = VMContact(*args, **kwargs)
        self.contact_card = VMContactCard(*args, **kwargs)
        self.contact_request = VMContactRequest(*args, **kwargs)

        self.card = VMCard(*args, **kwargs)
        self.total_balance = VMTotalBalance(*args, **kwargs)

        self.balance_log = VMBalanceLog(*args, **kwargs)
