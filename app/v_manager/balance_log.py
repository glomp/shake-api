from typing import List, Union

from django.db.models.aggregates import Sum
from django.db.models.expressions import RawSQL
from django.db.models.functions import Extract
from django.db.models.query import QuerySet
from django.db.models.query_utils import Q

from s_balances.models import SCreditLogs
from .base import VMBase


class VMBalanceLogBase(VMBase):
    model = SCreditLogs

    def get(self, *args, **kwargs) -> SCreditLogs:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> SCreditLogs:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SCreditLogs]]:
        return super().all(*args, **kwargs)


class VMPersonalBalanceLogBase(VMBalanceLogBase):
    TYPE = SCreditLogs.PERSONAL

    @property
    def filters(self):
        return dict(
            credit_type=self.TYPE,
            card__company_user__company_id=self._company.id,
        )


class VMCompanyBalanceLogBase(VMPersonalBalanceLogBase):
    TYPE = SCreditLogs.COMPANY


class VMBalanceLogByMonthBase(VMBalanceLogBase):
    TYPE = SCreditLogs.COMPANY

    # _annotate = dict(
    #     month=RawSQL(
    #         f"DATE_FORMAT(FROM_UNIXTIME(`{SCreditLogs._meta.db_table}`._created), '%%Y-%%m')",
    #         [],
    #     ),
    # )
    # _values = ["month"]
    _annotate = dict(
        year=RawSQL(
            f"YEAR(FROM_UNIXTIME(`{SCreditLogs._meta.db_table}`._created))",
            [],
        ),
        month=RawSQL(
            f"MONTH(FROM_UNIXTIME(`{SCreditLogs._meta.db_table}`._created))",
            [],
        ),
    )
    _values = ["year", "month"]
    _annotate2 = dict(
        total_company_credit_used=Sum(
            "amount",
            filter=Q(credit_type=SCreditLogs.COMPANY),
        ),
        total_personal_credit_used=Sum(
            "amount",
            filter=Q(credit_type=SCreditLogs.PERSONAL),
        ),
    )
    _values2 = [
        *_values,
        "total_company_credit_used",
        "total_personal_credit_used",
    ]
    _extra_filter_args = [
        Q(Q(total_company_credit_used__gt=0) | Q(total_personal_credit_used__gt=0)),
    ]

    @property
    def filters(self):
        return dict(
            credit_type=self.TYPE,
            card__company_user__company_id=self._company.id,
        )

    def get(self, *args, **kwargs) -> SCreditLogs:
        raise NotImplementedError()

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SCreditLogs]]:
        return list(
            super()
            .all(*args, **kwargs)
            .values(*self._values)
            .annotate(**self._annotate2)
            .values(*self._values2)
            .filter(*self._extra_filter_args)
        )


class VMBalanceLogByYearBase(VMBalanceLogByMonthBase):
    _annotate = dict(
        year=RawSQL(
            f"YEAR(FROM_UNIXTIME(`{SCreditLogs._meta.db_table}`._created))",
            [],
        ),
    )
    _values = ["year"]
    _values2 = [
        *_values,
        "total_company_credit_used",
        "total_personal_credit_used",
    ]


class VMPersonalBalanceLogByMonth(VMBalanceLogByMonthBase, VMPersonalBalanceLogBase):
    pass


class VMPersonalBalanceLogByYear(VMBalanceLogByYearBase, VMPersonalBalanceLogBase):
    pass


class VMPersonalBalanceLog(VMPersonalBalanceLogBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.by_month = VMPersonalBalanceLogByMonth(*args, **kwargs)
        self.by_year = VMPersonalBalanceLogByYear(*args, **kwargs)


class VMCompanyBalanceLogByMonth(VMBalanceLogByMonthBase, VMCompanyBalanceLogBase):
    pass


class VMCompanyBalanceLogByYear(VMBalanceLogByYearBase, VMCompanyBalanceLogBase):
    pass


class VMCompanyBalanceLog(VMCompanyBalanceLogBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.by_month = VMCompanyBalanceLogByMonth(*args, **kwargs)
        self.by_year = VMCompanyBalanceLogByYear(*args, **kwargs)


class VMBalanceLog(VMBalanceLogBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.personal = VMPersonalBalanceLog(*args, **kwargs)
        self.company = VMCompanyBalanceLog(*args, **kwargs)

    @property
    def filters(self):
        return dict(
            card__company_user__company_id=self._company.id,
        )
