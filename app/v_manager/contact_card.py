from typing import List, Union

from django.db.models import F
from django.db.models.functions import FirstValue
from django.db.models.query import QuerySet
from django.db.models.query_utils import Q

from custom_django.models.functions.group_concat import GroupConcat
from s_cards.models import SCards
from s_contacts.models import SContacts
from .card import VMCardBase


class VMContactCard(VMCardBase):
    _prefetch_related = (
        "sproperties_set",
        "sproperties_set__media",
    )
    _annotate = dict(
        tiers=GroupConcat(
            F("related_contacts__tier_number"),
            "tiers",
            separator=",",
        ),
        belong_to_contacts=GroupConcat(
            F("related_contacts__id"),
            separator=",",
        ),
        belong_to_cards=GroupConcat(
            F("related_contacts__user_card_id"),
            separator=",",
        ),
    )

    @property
    def filters(self) -> dict:
        return dict(
            status=SCards.ACTIVE,
            related_contacts__company_id=self._company.id,
        )

    def get(self, *args, **kwargs) -> SCards:
        contact_card = super().get(*args, **kwargs)
        contact_card._company_contact = self._get_receiver_attribute(contact_card)
        return contact_card

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SCards]]:
        contact_cards = super().all(*args, **kwargs)
        for i in range(len(contact_cards)):
            contact_cards[i]._company_contact = self._get_receiver_attribute(
                contact_cards[i]
            )
        return contact_cards

    # def all(self, *args, **kwargs) -> List[SCards]:
    #     # return SAggregateCompanyContactViews.objects.filter(
    #     #     *args, company_id=self.company.id, **kwargs,
    #     # ).distinct()

    #     contact_cards: List[SCards] = (
    #         SCards.objects.prefetch_related(
    #             # "related_contacts",
    #             # "related_contacts__user_card",
    #             *self._prefetch_related
    #         )
    #         .filter(
    #             *args,
    #             related_contacts__company_id=self._company.id,
    #             **kwargs,
    #         )
    #         .annotate(**self._annotate)
    #         .all()
    #         # .filter(
    #         # related_contacts__type=SContacts.COMPANY,
    #         # )
    #         # .values(
    #         #     "_created",
    #         #     "_updated",
    #         #     "card_type",
    #         #     "company_name",
    #         #     "display",
    #         #     "id",
    #         #     "job_title",
    #         #     "name",
    #         #     "searchable",
    #         #     "shareability",
    #         #     "status",
    #         #     "template_id",
    #         #     "tier_1_view_key",
    #         #     "tier_2_view_key",
    #         #     "tier_3_view_key",
    #         #     "user_id",
    #         #     "user_name",
    #         #     "visibility",
    #         #     "own_contacts",
    #         #     "sproperties",
    #         # )
    #     )
    #     for i in range(len(contact_cards)):
    #         contact_cards[i]._company_contact = self._get_receiver_attribute(
    #             contact_cards[i]
    #         )
    #     return contact_cards

    def _get_receiver_attribute(self, contact_card: SCards) -> SContacts:
        # print("contact_card", contact_card.__dir__())
        receiver_contact = contact_card.related_contacts.select_related(
            "user_card"
        ).get(company_id=self._company.id, type=SContacts.COMPANY)
        return receiver_contact
