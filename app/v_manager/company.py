from typing import List, Union

from django.db.models.query import QuerySet

from s_companies.models import SCompanies, SCompanyUsers
from .base import VMBase


class VMCompany(VMBase):
    model = SCompanies

    @property
    def filters(self) -> dict:
        return dict(
            status=SCompanies.ACTIVE,
            scompanyusers__user_id=self._user.id,
            scompanyusers__status=SCompanyUsers.ACTIVE,
        )

    def get(self, *args, **kwargs) -> SCompanies:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> SCompanies:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SCompanies]]:
        return super().all(*args, **kwargs)
