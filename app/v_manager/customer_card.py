from typing import Dict

from s_contacts.models import SContacts
from .base import VMBase
from .card import VMCardBase


class VMCustomerCardKnowUs(VMCardBase):
    @property
    def filters(self) -> dict:
        return dict(
            own_contacts__card__template__company_id=self._company.id,
        )


class VMCustomerCardWeKnow(VMCardBase):
    @property
    def filters(self) -> dict:
        return dict(
            related_contacts__type=SContacts.COMPANY,
            related_contacts__company_id=self._company.id,
        )


class VMCustomerCard(VMBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.know_us = VMCustomerCardKnowUs(*args, **kwargs)
        self.we_know = VMCustomerCardWeKnow(*args, **kwargs)
