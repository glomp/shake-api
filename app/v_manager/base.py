from custom_django.sort_query import SortQuery
from s_companies.models import SCompanies, SCompanyUsers
from s_user.models import SUsers


class VMBase(SortQuery):
    def __init__(self, user: SUsers, company: SCompanies, company_admin: SCompanyUsers):
        self._user = user
        self._company = company
        self._company_admin = company_admin
