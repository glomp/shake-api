from typing import List, Union

from django.db.models.query import QuerySet

from s_companies.models import SCompanyUsers
from .base import VMBase


class VMEmployee(VMBase):
    model = SCompanyUsers

    @property
    def filters(self) -> dict:
        return dict(
            company_id=self._company.id,
        )

    def get(self, *args, **kwargs) -> SCompanyUsers:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> SCompanyUsers:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SCompanyUsers]]:
        return super().all(*args, **kwargs)
