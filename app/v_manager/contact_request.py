from typing import List, Union

from django.db.models.query import QuerySet

from s_cards.models import SCards
from s_contacts.models import SContactRequests, SContacts
from .base import VMBase


class VMContactRequestBase(VMBase):
    model = SContactRequests

    def get(self, *args, **kwargs) -> SContactRequests:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> SContactRequests:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SContactRequests]]:
        return super().all(*args, **kwargs)


class VMEmployeeContactRequest(VMContactRequestBase):
    TYPE = SContacts.EMPLOYEE

    def __init__(self, employee_card: SCards, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.employee_card = employee_card

    @property
    def filters(self) -> dict:
        return dict(
            from_card__template__company_id=self._company.id,
            from_card__id=self.employee_card.id,
        )


class VMContactRequest(VMContactRequestBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__init_args = args
        self.__init_kwargs = kwargs

    @property
    def filters(self) -> dict:
        return dict(
            from_card__template__company_id=self._company.id,
        )

    def by_employee_card(self, employee_card: SCards) -> VMEmployeeContactRequest:
        return VMEmployeeContactRequest(
            employee_card, *self.__init_args, **self.__init_kwargs
        )
