from typing import List, Union

from django.db.models.query import QuerySet

from s_contacts.models import SContacts
from .base import VMBase


class VMContactBase(VMBase):
    model = SContacts

    def get(self, *args, **kwargs) -> SContacts:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> SContacts:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[QuerySet, List[SContacts]]:
        return super().all(*args, **kwargs)


class VMEmployeeContact(VMContactBase):
    TYPE = SContacts.EMPLOYEE

    @property
    def filters(self) -> dict:
        return dict(
            company_id=self._company.id,
            type=self.TYPE,
        )


class VMCompanyContact(VMEmployeeContact):
    TYPE = SContacts.COMPANY


class VMContact(VMContactBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # self.aggregate = VMAggregateContact(*args, **kwargs)
        self.employee = VMEmployeeContact(*args, **kwargs)
        self.company = VMCompanyContact(*args, **kwargs)

    @property
    def filters(self) -> dict:
        return dict(
            company_id=self._company.id,
        )
