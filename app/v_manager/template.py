from typing import List, Type, Union

from django.db.models.query import QuerySet

from s_templates.models import STemplates
from .base import VMBase


class VMTemplate(VMBase):
    model = STemplates

    @property
    def filters(self) -> dict:
        return dict(
            company_id=self._company.id,
        )

    def get(self, *args, **kwargs) -> Type[model]:
        return super().get(*args, **kwargs)

    def get_if_exist(self, *args, **kwargs) -> Type[model]:
        return super().get_if_exist(*args, **kwargs)

    def all(self, *args, **kwargs) -> Union[List[STemplates], QuerySet]:
        return super().all(*args, **kwargs)
