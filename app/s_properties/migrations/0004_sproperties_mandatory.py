# Generated by Django 2.2.16 on 2020-12-02 09:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('s_properties', '0003_auto_20201127_1018'),
    ]

    operations = [
        migrations.AddField(
            model_name='sproperties',
            name='mandatory',
            field=models.BooleanField(default=False),
        ),
    ]
