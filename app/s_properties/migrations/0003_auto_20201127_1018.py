# Generated by Django 2.2.16 on 2020-11-27 10:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('s_properties', '0002_auto_20201126_0347'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sproperties',
            name='card',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='s_cards.SCards'),
        ),
        migrations.AlterField(
            model_name='sproperties',
            name='media',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='s_medias.SMedias'),
        ),
        migrations.AlterField(
            model_name='sproperties',
            name='property_type',
            field=models.CharField(choices=[('N', 'NAME'), ('E', 'EMAIL'), ('M', 'MOBILE'), ('A', 'ADDRESS'), ('T', 'TAG'), ('C', 'CONTENT'), ('F', 'MEDIA'), ('U', 'URL'), ('O', 'OTHER')], max_length=1),
        ),
        migrations.AlterField(
            model_name='sproperties',
            name='template',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='s_templates.STemplates'),
        ),
    ]
