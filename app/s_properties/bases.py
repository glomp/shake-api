class PropertyTypes:
    NAME = "N"
    EMAIL = "E"
    MOBILE = "M"
    ADDRESS = "A"
    TAG = "T"
    CONTENT = "C"
    MEDIA = "F"
    URL = "U"
    OTHER = "O"
    TYPES = (
        (NAME, "NAME"),
        (EMAIL, "EMAIL"),
        (MOBILE, "MOBILE"),
        (ADDRESS, "ADDRESS"),
        (TAG, "TAG"),
        (CONTENT, "CONTENT"),
        (MEDIA, "MEDIA"),
        (URL, "URL"),
        (OTHER, "OTHER"),
    )
