from django.db import models
from django_mysql.models import ListCharField

from custom_django.models import SModel, fields
from s_cards.models import SCards
from s_medias.models import SMedias
from .bases import PropertyTypes


class SProperties(PropertyTypes, SModel):
    PUBLIC = "P"
    PRIVATE = "I"
    DISPLAY = (
        (PUBLIC, "PUBLIC"),
        (PRIVATE, "PRIVATE"),
    )

    media = models.ForeignKey(
        SMedias, models.DO_NOTHING, default=None, blank=True, null=True
    )
    card = models.ForeignKey(
        SCards, models.DO_NOTHING, default=None, blank=True, null=True
    )

    property_type = models.CharField(max_length=1, choices=PropertyTypes.TYPES)
    mandatory = models.BooleanField(default=False)
    editable = models.BooleanField(default=True)
    searchable = models.BooleanField(default=True)

    label = models.CharField(max_length=31)
    value = models.CharField(max_length=255, blank=True, null=True)
    comment = models.TextField(blank=True, null=True, default=None)

    display_in_printed_card = models.BooleanField(default=False)
    display_in_body = models.BooleanField(default=True)
    x = models.IntegerField(default=0)
    y = models.IntegerField(default=0)
    font_family = models.CharField(max_length=63, default="", blank=True)
    font_color = models.CharField(max_length=7, default="", blank=True)
    font_size = models.IntegerField(default=16)
    font_style = ListCharField(
        base_field=models.CharField(max_length=15),
        size=4,
        max_length=(4 * 16 - 1),
        default=[],
    )

    _created = fields.UnixTimeStampField(null=True, auto_now_add=True)
    _updated = fields.UnixTimeStampField(null=True, auto_now=True)

    NON_INFO_FIELDS = (
        "display_in_printed_card",
        "display_in_body",
        "x",
        "y",
        "font_family",
        "font_color",
        "font_size",
        "font_style",
        "_updated",
    )
    PERSONAL_FIELDS = (
        "editable",
        "mandatory",
        "searchable",
        "_updated",
    )

    def get_readonly_fields(self, request, obj=None):
        return ["card", "template", "media", "property_type"] if obj else []

    class Meta:
        managed = True
        db_table = "s_property"
