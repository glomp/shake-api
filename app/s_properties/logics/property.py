from typing import Dict, List, Tuple

from rest_framework.exceptions import ValidationError

from helper import slogger
from helper.common.string import serialize_name
from helper.exceptions import BadRequestException
from s_cards.models import SCards
from s_medias.models import SMedias
from ..bases import PropertyTypes
from ..models import SProperties
from ..serializers import SThoroughPropertySerializer


class Property:
    serializer_class = SThoroughPropertySerializer
    TYPE_MAP = {key: value for key, value in PropertyTypes.TYPES}

    @classmethod
    def to_dict(cls, obj: SProperties) -> dict:
        if isinstance(obj, dict):
            return obj
        data = cls.serializer_class(obj).data
        if obj.property_type == PropertyTypes.MEDIA:
            data["media"] = obj.media
        return data

    @classmethod
    def validate(cls, data: dict, allow_null=False, raise_exception=True):
        data = cls.to_dict(data)
        try:
            assert bool(
                data.get("label", None)
            ), "Missing key 'label' in a Property object"
            assert bool(
                data.get("property_type", None)
            ), f"Missing 'property_type' in {data['label']} property"
            assert data["property_type"] in cls.TYPE_MAP, (
                f"Property type '{data['property_type']}' is not valid "
                f"in {data['label']} property"
            )

            if data["property_type"] == PropertyTypes.MEDIA:
                if "media" not in data or data["media"] is None:
                    if not allow_null:
                        assert True, f"Missing 'media' in {data['label']} property"
                elif not isinstance(data["media"], SMedias):
                    media = data["media"]
                    media_id = media["id"] if isinstance(media, dict) else media
                    data["media"] = SMedias.objects.get(id=media_id)
            else:
                if not allow_null:
                    if data.get("mandatory", False):
                        assert bool(
                            data.get("value", None) not in {"", None}
                        ), f"Missing 'value' in {data['label']} property"
                    else:
                        assert (
                            data.get("value", None) is not None
                        ), f"Missing 'value' in {data['label']} property"

            if "email" in data["label"]:
                data["value"] = data.get("value", "").lower()
            if "name" == data["label"] and "value" in data:
                data["value"] = data["value"] and serialize_name(data["value"])

            return True
        except Exception as e:
            if raise_exception:
                raise BadRequestException(errcode=400033, message=str(e), data=data)
            else:
                return False

    @classmethod
    def validate_many(
        cls, properties: List[dict], allow_null=False, raise_exception=True
    ):
        return all(
            [
                cls.validate(each_property, allow_null, raise_exception)
                for each_property in properties
            ]
        )


class CardProperty(Property):
    NO_SEARCHABLE = {SProperties.MEDIA, SProperties.OTHER, SProperties.URL}

    def __init__(self, card: SCards, base_attributes: dict = None):
        self.card = card
        self.base_attributes = base_attributes or dict()
        self.is_change: bool = False

        self.all: List[SProperties] = None
        self.map: Dict[str, SProperties] = None

    def get_properties(self, update=False) -> List[SProperties]:
        if not update and self.all:
            return self.all
        self.all = self.card.sproperties_set.all()
        return self.all

    def get_map(self) -> Dict[str, SProperties]:
        if self.map:
            return self.map
        self._update_map()
        return self.map

    def get(self, title) -> str:
        self.get_map()
        _property = self.map.get(title, None)
        return _property.value or "" if _property else ""

    def init(self):
        self.get_properties()
        self.get_map()

    def add(self, new_property: dict) -> SProperties:
        self.is_change = True
        new_property.update(self.base_attributes, card=self.card.id)
        property_ser = SThoroughPropertySerializer(data=new_property)
        try:
            property_ser.is_valid(raise_exception=True)
        except ValidationError as e:
            raise BadRequestException(
                errcode=400089,
                data={"property": new_property, "error": e.__dict__},
                err_message=str(e),
            )

        _property: SProperties = property_ser.save()
        return self._save(_property)

    def add_many(self, new_properties: List[dict]) -> List[SProperties]:
        return [self.add(new_property) for new_property in new_properties]

    def update(
        self,
        new_property: dict,
        old_property: SProperties,
        force_edit: bool = None,
    ) -> SProperties:
        allow_edit = force_edit if force_edit is not None else old_property.editable
        if not allow_edit:
            return old_property

        is_changed = False
        new_property.pop("card", None)
        new_property.pop("card_id", None)
        new_property.update(self.base_attributes)
        for key, new_value in new_property.items():
            if not hasattr(old_property, key) or new_value == getattr(
                old_property, key
            ):
                continue
            self.is_change = True
            is_changed = True
            setattr(old_property, key, new_value)

        return self._save(old_property, is_changed=is_changed)

    def update_many(
        self,
        exist_properties: List[Tuple[dict, SProperties]],
        force_edit: bool = None,
    ) -> List[SProperties]:
        return [
            self.update(*pair_property, force_edit=force_edit)
            for pair_property in exist_properties
        ]

    def delete(self, old_property: SProperties, force_edit: bool = None) -> SProperties:
        if not force_edit and old_property.mandatory is True:
            raise BadRequestException(
                errcode=400036,
                data=old_property.__dict__,
                message=f"Can't delete property '{old_property.label}'",
            )
        self.is_change = True
        return old_property.delete()

    def delete_many(
        self, old_properties: List[SProperties], force_edit: bool = None
    ) -> List[SProperties]:
        return [
            self.delete(old_property, force_edit=force_edit)
            for old_property in old_properties
        ]

    def synchronize(
        self,
        properties: List[dict],
        need_check: bool = True,
        no_delete: bool = False,
        force_edit: bool = None,
    ):
        """
        Create new if a property don't have id.
        And delete a property if it showed in old properties but not in new one.
        And compare old and new one to decide update or not.

        Args:
            properties (List[dict]): new properties
            need_check (bool, optional): False if you checked them all. Defaults to True.
            no_delete (bool, optional): True if you don't delete any missing properties. Defaults to False.
        """
        properties = [self.to_dict(_p) for _p in properties]
        if need_check:
            self.validate_many(properties, True, True)

        self.is_change = False
        old_properties = self.get_properties()
        property_map = {}
        new_properties: List[dict] = []
        exist_properties: List[Tuple[dict, SProperties]] = []
        deleted_properties: List[SProperties] = []

        # slogger.debug("properties: %r", properties)
        for idx in range(len(properties)):
            if properties[idx].get("card", self.card.id) != self.card.id:
                properties[idx].pop("id", None)
                properties[idx]["card"] = self.card.id

        for new_property in properties:
            if new_property.get("id", None) is None:
                new_properties.append(new_property)
            else:
                property_map[new_property["id"]] = new_property

        for old_property in old_properties:
            if old_property.id in property_map:
                new_property = property_map.pop(old_property.id)
                exist_properties.append((new_property, old_property))
            else:
                deleted_properties.append(old_property)
        # slogger.debug("new_properties: %r", new_properties)
        # slogger.debug("old_properties: %r", old_properties)
        # slogger.debug("exist_properties: %r", exist_properties)

        if property_map:  # It must empty
            raise BadRequestException(
                errcode=400035,
                message="Wrong properties data!",
                data={
                    "old_properties": [
                        old_property.id for old_property in old_properties
                    ],
                    "property_map": property_map.keys(),
                },
            )

        not no_delete and self.delete_many(deleted_properties, force_edit=force_edit)
        self.all = self.add_many(new_properties) + self.update_many(
            exist_properties, force_edit=force_edit
        )
        self._update_map()

        return self.all

    def add_require_info_into_existed_properties(
        self, properties: List[dict]
    ) -> List[dict]:
        self.get_map()
        new_properties = properties.copy()
        for idx in range(len(properties)):
            label = properties[idx]["label"]
            if label in self.map:
                current_property = self.map[label]
                new_property = properties[idx]
                new_properties[idx].update(
                    id=current_property.id,
                    property_type=new_property.get(
                        "property_type", current_property.property_type
                    ),
                )
        return new_properties

    def _update_map(self):
        self.get_properties()
        self.map = self.get_map_from_list_obj(self.all)

    def _get_value(self, _property: SProperties) -> SProperties:
        if _property.property_type == PropertyTypes.MEDIA and isinstance(
            _property.media, SMedias
        ):
            return _property.media.real_url
        return _property.value

    def _save(self, _property: SProperties, is_changed: bool = False) -> SProperties:
        _value = self._get_value(_property)
        if _value != _property.value:
            is_changed = True
            _property.value = _value
        if (
            _property.property_type in self.NO_SEARCHABLE
            and _property.searchable is True
        ):
            is_changed = True
            _property.searchable = False

        is_changed and _property.save()
        return _property

    @classmethod
    def get_map_from_list_obj(cls, properties: List[SProperties]) -> dict:
        property_map = {}
        for _property in properties:
            property_map[_property.label] = _property
        return property_map

    @classmethod
    def get_map_from_list_dict(cls, properties: List[dict]) -> dict:
        property_map = {}
        for _property in properties:
            property_map[_property["label"]] = _property
        return property_map
