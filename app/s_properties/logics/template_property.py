from typing import List

from django.db.models.query_utils import Q

from s_cards.models import SCards
from ..models import SProperties
from .property import CardProperty


class TemplateCardProperty(CardProperty):
    def __init__(self, card: SCards, base_attributes: dict):
        super().__init__(card, base_attributes=base_attributes)
        self._all_card_using_this_template: List[SCards] = None

    def add(self, new_property: dict) -> SProperties:
        general_property = new_property.copy()
        template_property = super(TemplateCardProperty, self).add(new_property)

        general_property.update(self.base_attributes)
        general_property.pop("id", None)
        general_property.pop("card", None)
        general_property.pop("card_id", None)
        general_property["searchable"] = (
            general_property["property_type"] not in self.NO_SEARCHABLE
        )
        new_properties = []
        for card in self.all_card_using_this_template:
            new_properties.append(SProperties(card=card, **general_property))
        SProperties(card=self.card, **general_property)
        return template_property

    def update(
        self,
        new_property: dict,
        old_property: SProperties,
        force_edit: bool = None,
    ) -> SProperties:
        general_property = new_property.copy()
        general_property.pop("id", None)
        general_property.pop("card", None)
        general_property.pop("card_id", None)
        general_property.pop("_updated", None)
        general_property.pop("_created", None)

        property_label = general_property.pop("label")
        general_property.update(self.base_attributes)

        updated_attribute = {}
        for key, new_value in general_property.items():
            if not hasattr(old_property, key) or new_value == getattr(
                old_property, key
            ):
                continue
            if key == "value" and new_value is None or new_value == "":
                continue
            updated_attribute[key] = new_value
        if updated_attribute:
            SProperties.objects.filter(
                ~Q(card__status=SCards.DESTROYED),
                card__template_id=self.card.template_id,
                label=property_label,
            ).update(**updated_attribute)

        template_property = super(TemplateCardProperty, self).update(
            new_property=new_property,
            old_property=old_property,
            force_edit=force_edit,
        )
        return template_property

    def delete(self, old_property: SProperties, force_edit: bool = None) -> SProperties:
        template_property = super(TemplateCardProperty, self).delete(
            old_property=old_property, force_edit=force_edit
        )
        SProperties.objects.filter(
            card__template_id=self.card.template_id, label=old_property.label
        ).delete()
        return template_property

    @property
    def all_card_using_this_template(self):
        if self._all_card_using_this_template is None:
            self._all_card_using_this_template = SCards.objects.filter(
                template_id=self.card.id, card_type=SCards.BUSINESS
            )
        return self._all_card_using_this_template
