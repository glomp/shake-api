from django.apps import AppConfig


class SPropertiesConfig(AppConfig):
    name = 's_properties'
