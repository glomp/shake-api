from dynamic_rest.fields import DynamicRelationField
from dynamic_rest.serializers import DynamicModelSerializer
from rest_framework import serializers

from s_medias.serializers import SPublicThoroughMediaSerializer
from .models import SProperties


class SPropertySerializer(DynamicModelSerializer):
    font_style = serializers.ListField(child=serializers.CharField(), default=[])

    class Meta:
        model = SProperties
        fields = "__all__"


class SPublicPropertySerializer(DynamicModelSerializer):
    font_style = serializers.ListField(child=serializers.CharField(), default=[])

    class Meta:
        model = SProperties
        exclude = SProperties.PERSONAL_FIELDS


class SMiniPropertySerializer(DynamicModelSerializer):
    class Meta:
        model = SProperties
        exclude = SProperties.PERSONAL_FIELDS + SProperties.NON_INFO_FIELDS


class SBasicThoroughPropertySerializer(DynamicModelSerializer):
    media = DynamicRelationField(SPublicThoroughMediaSerializer, embed=True)

    class Meta:
        model = SProperties
        exclude = SProperties.NON_INFO_FIELDS


class SPublicThoroughPropertySerializer(DynamicModelSerializer):
    font_style = serializers.ListField(child=serializers.CharField(), default=[])
    media = DynamicRelationField(SPublicThoroughMediaSerializer, embed=True)

    class Meta:
        model = SProperties
        exclude = SProperties.PERSONAL_FIELDS


class SThoroughPropertySerializer(DynamicModelSerializer):
    font_style = serializers.ListField(child=serializers.CharField(), default=[])
    media = DynamicRelationField(SPublicThoroughMediaSerializer, embed=True)

    class Meta:
        model = SProperties
        fields = "__all__"
