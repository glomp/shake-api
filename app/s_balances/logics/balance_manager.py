from os import sys
from typing import List

from helper.exceptions import AppException, BadRequestException
from helper.logger_handler import slogger
from s_cards.models import SCards
from s_contacts.models import SContactRequests
from s_user.models import SUsers
from .balance import (
    Balance,
    BusinessBalance,
    CompanyBalance,
    FreeBalance,
    IdentityBalance,
    PersonalBalance,
)


class BalanceManager:
    def __init__(self, user: SUsers, card: SCards):
        self.card = card
        self.user = user

        self._personal_id = 0
        if card.card_type == SCards.PERSONAL:
            balances = [PersonalBalance(user, card), FreeBalance(user, card)]
        elif card.card_type == SCards.BUSINESS:
            balances = [CompanyBalance(user, card), BusinessBalance(user, card)]
            self._personal_id = 1
        elif card.card_type == SCards.SCAN:
            balances = [PersonalBalance(user, card)]
        elif card.card_type == SCards.IDENTITY:
            balances = [IdentityBalance(user, card)]
        else:
            raise AppException(
                errcode=500076,
                message=f"Don't support balance for {card.card_type}",
                data={"card_id": card.id},
            )

        self.__is_changed: bool = False

        self.__balances: List[Balance] = balances
        self.__balance_credits: List[int] = None
        self._current_balance_i: int = None

        self.init_data()

    def init_data(self):
        self.__balance_credits = [balance.total for balance in self.__balances]
        self._current_balance_i = 0
        for i, balance_credits in enumerate(self.__balance_credits):
            if balance_credits > 0:
                self._current_balance_i = i
                break

    @property
    def personal(self) -> Balance:
        return self.__balances[self._personal_id]

    @property
    def total_credits(self) -> int:
        return sum(self.__balance_credits)

    def withdraw_one_credit(self) -> Balance.CREDIT_TYPE:
        def get_qualified_balance() -> Balance:
            while (
                self.current_balance_amount == 0
                and self._current_balance_i < len(self.__balance_credits) - 1
            ):
                self._current_balance_i += 1
            if self.current_balance_amount == self.current_balance.MINIMUM_CREDIT:
                self.__is_changed = False
                raise BadRequestException(errcode=400078, message="Out of credits.")
            return self.current_balance

        get_qualified_balance()
        self.__balance_credits[self._current_balance_i] -= 1
        self.__is_changed = True
        yield self.current_balance.CREDIT_TYPE

    def save(self):
        for i, balance in enumerate(self.__balances):
            balance.update(self.__balance_credits[i])
        self.__is_changed = False

    def refund(self, credit_type: Balance.CREDIT_TYPE, amount: int = 1):
        Balance.validate_amount(amount=amount)

        for balance in self.__balances:
            if balance.CREDIT_TYPE == credit_type:
                self.__is_changed = True
                balance.update(balance.total + amount)
                break

    def __del__(self):
        if isinstance(self, BalanceManager) and self.__is_changed:
            try:
                slogger.warning("This Balance manager has not been saved directly.")
                self.save()
            except Exception:
                raise AppException(
                    errcode=500077,
                    data={
                        "card_id": self.card.id,
                        "user_id": self.user.id,
                        "balance_names": str(self.__balances),
                        "old_balances": [balance.total for balance in self.__balances],
                        "new_balances": self.__balance_credits,
                    },
                    err_message="This Balance manager is destroy before save.",
                )

    @property
    def current_balance(self) -> Balance:
        return self.__balances[self._current_balance_i]

    @property
    def current_balance_amount(self) -> int:
        return self.__balance_credits[self._current_balance_i]
