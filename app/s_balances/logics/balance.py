from datetime import datetime, timedelta

from helper.exceptions import BadRequestException
from s_cards.models import SCards
from s_companies.models import SCompanies, SCompanyUsers
from s_contacts.models import SContactRequests
from s_notifications.manager_notification.balance import BalanceCompanyNotification
from s_notifications.self_notification.balance import BalanceSelfNotification
from s_user.models import SUsers
from v_user.balance_record import VUFreeBalanceRecord
from ..models import SCreditLogs
from ..serializers import CreditLogSerializer
from .credit_log import CompanyCreditLog, CreditLog, PersonalCardCreditLog


class Balance:
    CREDIT_LOG_CLASS: CreditLog
    SERIALIZER_CLASS = CreditLogSerializer
    CREDIT_TYPE: SContactRequests.CREDIT_TYPE = None

    TOTAL_BALANCE_FIELD: str = None
    MINIMUM_CREDIT: int = 0

    def __init__(self, user: SUsers, card: SCards):
        self.user = user
        self.card = card

        assert (
            self.user is not None and self.card.user_id == self.user.id
        ), "This card is not belongs to you"
        assert (
            self.card.status == SCards.ACTIVE
        ), f"Only activated card can use the credit. [{self.card.id}]"
        self.credit_log: CreditLog = self.CREDIT_LOG_CLASS(card, self.CREDIT_TYPE)

    @classmethod
    def validate_amount(cls, amount: int, raise_exception=True) -> bool:
        assert (
            amount > cls.MINIMUM_CREDIT
        ), f"Amount must be greater than {cls.MINIMUM_CREDIT}. [{amount}]"
        return True

    @classmethod
    def validate_total(cls, total: int, raise_exception=True) -> bool:
        assert (
            total >= cls.MINIMUM_CREDIT
        ), f"Total must be equal or greater than {cls.MINIMUM_CREDIT}. [{total}]"
        return True

    def add(
        self, amount: int, card_name: str = None, log=True, raise_exception=True
    ) -> CreditLog:
        self.validate_amount(amount=amount, raise_exception=raise_exception)
        card_name = card_name or self.card.card_name

        self.update(self.total + amount, raise_exception=raise_exception)
        if log:
            credit_log = self.credit_log.create(
                amount,
                direction=SCreditLogs.INCOMING,
                log=f"You added {amount} credit into your {card_name} card",
            )
            return credit_log

    def subtract(
        self, amount: int, card_name: str = None, log=True, raise_exception=True
    ):
        self.validate_amount(amount=amount, raise_exception=raise_exception)
        card_name = card_name or self.card.card_name
        if self.total < amount:
            raise BadRequestException(
                errcode=400067,
                message="Your {card_name} card don't have enough credits",
                data={
                    "amount": amount,
                    "total": self.total,
                },
            )

        self.update(self.total - amount, raise_exception=raise_exception)

    @property
    def total(self) -> int:
        return getattr(self.card, self.TOTAL_BALANCE_FIELD)

    def _update(self, total: int):
        setattr(self.card, self.TOTAL_BALANCE_FIELD, total)
        self.card.save(update_fields=[self.TOTAL_BALANCE_FIELD])

    def update(self, total: int, raise_exception=True):
        if total == self.total or not self.validate_total(
            total, raise_exception=raise_exception
        ):
            return

        self._update(total)


class FreeBalance(Balance):
    CREDIT_LOG_CLASS = PersonalCardCreditLog
    CREDIT_TYPE = SContactRequests.FREE

    MAX_FREE_CREDIT = 10
    MAX_TRACK_DAY = 10

    def validate(self, amount: int, raise_exception=True) -> bool:
        return super().validate_amount(amount=amount) and (
            self.card.balance_free_lock_until is not None
            and self.card.balance_free_lock_until < datetime.utcnow().timestamp()
        )

    @property
    def total(self) -> int:
        if self.is_lock:
            return 0

        count = self.MAX_FREE_CREDIT - (
            VUFreeBalanceRecord(self.user)
            .all(
                _created__gte=(
                    datetime.utcnow() - timedelta(days=self.MAX_TRACK_DAY)
                ).timestamp(),
            )
            .count()
        )
        return count

    @property
    def is_lock(self) -> bool:
        return self.card.balance_free_lock_until >= datetime.utcnow().timestamp()

    def add(
        self, amount: int, card_name: str = None, log=True, raise_exception=True
    ) -> CreditLog:
        if raise_exception:
            raise NotImplementedError

    def subtract(
        self, amount: int, card_name: str = None, log=True, raise_exception=True
    ):
        pass

    def update(self, total: int, log=True, raise_exception=True):
        if total == 0:
            self.card.balance_free_lock_until = (
                datetime.utcnow() + timedelta(days=self.MAX_TRACK_DAY)
            ).timestamp()
            self.card.save(update_fields=["balance_free_lock_until"])


class IdentityBalance(Balance):
    CREDIT_LOG_CLASS = None
    CREDIT_TYPE = None

    TOTAL_BALANCE_FIELD = "balance_total_personal"

    CARD_TYPES = {SCards.IDENTITY}

    def __init__(self, user: SUsers, card: SCards):
        self.user = user
        self.card = card

        assert (
            self.user is not None and self.card.user_id == self.user.id
        ), "This card is not belongs to you"
        assert (
            self.card.card_type in self.CARD_TYPES
        ), "Identity balance must used by Identity card. {}"

    def add(self, amount: int, card_name: str = None, log=True, raise_exception=True):
        self.validate_amount(amount=amount)
        card_name = card_name or self.card.card_name

        self.update(self.total + amount)

    def subtract(
        self, amount: int, card_name: str = None, log=True, raise_exception=True
    ):
        self.validate_amount(amount=amount)
        card_name = card_name or self.card.card_name
        if self.total < amount:
            raise BadRequestException(
                errcode=400067,
                message="Your {card_name} card don't have enough credits",
                data={
                    "amount": amount,
                    "total": self.total,
                },
            )

        self.update(self.total - amount)


class PersonalBalance(Balance):
    CREDIT_LOG_CLASS = PersonalCardCreditLog
    CREDIT_TYPE = SContactRequests.PERSONAL

    TOTAL_BALANCE_FIELD = "balance_total_personal"

    CARD_TYPES = {SCards.PERSONAL, SCards.SCAN}

    def __init__(self, user: SUsers, card: SCards):
        super().__init__(user, card)

        assert (
            self.card.card_type in self.CARD_TYPES
        ), f"Personal balance must used by Personal or Scan card. {self.card.card_type}"

    def add(
        self, amount: int, card_name: str = None, log=True, raise_exception=True
    ) -> CreditLog:
        log = super().add(amount, card_name)

        BalanceSelfNotification(
            self.user,
            data=self.SERIALIZER_CLASS(log).data,
            target_card=self.card,
            target_id=log.id,
        ).added_credits.by(self.card).notify(
            card_name=card_name or self.card.card_name,
            amount=amount,
            credit="credit" if amount == 1 else "credits",
        )
        return log


class BusinessBalance(PersonalBalance):
    CREDIT_LOG_CLASS = PersonalCardCreditLog
    CREDIT_TYPE = SContactRequests.PERSONAL

    MINIMUM_CREDIT: int = -5

    CARD_TYPES = {SCards.BUSINESS}

    def __init__(self, user: SUsers, card: SCards):
        super(PersonalBalance, self).__init__(user, card)

        assert (
            self.card.card_type == SCards.BUSINESS
        ), "Business balance must used by Business card."

        self.company_user: SCompanyUsers = self.card.company_user
        assert (
            self.company_user is not None
            and self.company_user.status == SCompanyUsers.ACTIVE
        ), "User must be an activated member in their company"

        self.company: SCompanies = self.company_user.company
        assert (
            self.company is not None and self.company.status == SCompanies.ACTIVE
        ), "It must be an activated company."

    def add(
        self, amount: int, card_name: str = None, log=True, raise_exception=True
    ) -> CreditLog:
        log = super().add(amount, card_name or self.card.card_name)

        BalanceCompanyNotification(
            self.company,
            data=self.SERIALIZER_CLASS(log).data,
            target_card=self.card,
            target_id=log.id,
        ).added_personal_credits.by(self.card).notify(
            card_name=card_name or self.card.card_name,
            amount=amount,
            credit="credit" if amount == 1 else "credits",
        )
        return log


class CompanyBalance(BusinessBalance):
    CREDIT_LOG_CLASS = CompanyCreditLog
    CREDIT_TYPE = SContactRequests.COMPANY

    TOTAL_BALANCE_FIELD = "balance_total_company"

    MINIMUM_CREDIT: int = 0

    CARD_TYPES = {SCards.BUSINESS}

    def __init__(self, user: SUsers, card: SCards):
        super().__init__(user, card)

    def add(
        self, amount: int, card_name: str = None, log=True, raise_exception=True
    ) -> CreditLog:
        log = super(PersonalBalance, self).add(amount, card_name or self.card.card_name)

        BalanceSelfNotification(
            self.user,
            data=self.SERIALIZER_CLASS(log).data,
            target_card=self.card,
            target_id=log.id,
        ).added_company_credits.by(self.card).notify(
            card_name=card_name or self.card.card_name,
            amount=amount,
            credit="credit" if amount == 1 else "credits",
        )
        BalanceCompanyNotification(
            self.company,
            data=self.SERIALIZER_CLASS(log).data,
            target_card=self.card,
            target_id=log.id,
        ).added_company_credits.by(self.card).notify(
            card_name=card_name or self.card.card_name,
            amount=amount,
            credit="credit" if amount == 1 else "credits",
        )
        return log
