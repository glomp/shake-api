import googleapiclient.discovery
from google.oauth2 import credentials, service_account
from googleapiclient.http import HttpRequest
from re import compile
from typing import Tuple

from django.conf import settings

from helper.exceptions import BadRequestException
from s_user.models import SUsers
from ..models import MAndroidPurchase
from ..serializers import SAndroidPurchase


class AndriodPurchase:
    credential: credentials.Credentials
    service: googleapiclient.discovery.Resource

    product_id_regex = r"(\d+)\_credit"
    product_id_re = compile(product_id_regex)

    @classmethod
    def renew_connection(cls):
        cls.credential = service_account.Credentials.from_service_account_file(
            "./.secret/google/verify_purchase_service_account.json"
        )
        cls.service = googleapiclient.discovery.build(
            "androidpublisher", "v3", credentials=cls.credential
        )

    @classmethod
    def _verify_token(cls, token: str, product_id: str) -> dict:
        # Build the "service" interface to the API you want
        # service = googleapiclient.discovery.build(
        #     "androidpublisher", "v3", credentials=credentials
        # )

        # Use the token your API got from the app to verify the purchase
        return (
            cls.service.purchases()
            .products()
            .get(
                packageName="app.letsshake.android",
                productId=product_id,
                token=token,
            )
            .execute()
        )
        # result is a python object that looks like this ->
        # {'kind': 'androidpublisher#subscriptionPurchase', 'startTimeMillis': '1534326259450', 'expiryTimeMillis': '1534328356187', 'autoRenewing': False, 'priceCurrencyCode': 'INR', 'priceAmountMicros': '70000000', 'countryCode': 'IN', 'developerPayload': '', 'cancelReason': 1, 'orderId': 'GPA.1234-4567-1234-1234..5', 'purchaseType': 0}

    @classmethod
    def verify_token(cls, token: str, product_id: str) -> dict:
        if settings.ENV == "TEST":
            return {
                "purchaseTimeMillis": 1627633304270.0,
                "purchaseState": 1,
                "consumptionState": 0,
                "developerPayload": "",
                "orderId": "GPA.3357-1361-2574-53416",
                "purchaseType": 0,
                "acknowledgementState": 1,
                "kind": "androidpublisher#productPurchase",
                "regionCode": "SG",
            }

        try:
            result = cls._verify_token(token, product_id)
        except Exception:
            try:
                result = cls._verify_token(token, product_id)
            except Exception as e:
                raise BadRequestException(
                    errcode=400102,
                    err_message=str(e),
                    data=e.__dict__,
                    notify=True,
                    log_to_file=True,
                )
        cls.acknowleage(token=token, product_id=product_id)
        return result

    @classmethod
    def _acknowleage(cls, token: str, product_id: str):
        return (
            cls.service.purchases()
            .products()
            .acknowledge(
                packageName="app.letsshake.android",
                productId=product_id,
                token=token,
            )
            .execute()
        )

    @classmethod
    def acknowleage(cls, token: str, product_id: str):
        try:
            return cls._acknowleage(token=token, product_id=product_id)
        except Exception:
            pass

    @classmethod
    def get_amount(cls, product_id: str) -> int:
        return int(cls.product_id_re.findall(product_id)[0])


AndriodPurchase.renew_connection()


class AndroidClient:
    def __init__(self, user: SUsers):
        self.user = user

    def verify_token(self, token, product_id) -> Tuple[MAndroidPurchase, bool]:
        def _save_purchase(result: dict) -> MAndroidPurchase:
            data = result.copy()
            amount = AndriodPurchase.get_amount(product_id)
            data.update(
                token=token, product_id=product_id, user=self.user.id, amount=amount
            )
            serializer = SAndroidPurchase(data=data, context={"user": self.user})
            serializer.is_valid(raise_exception=True)
            return serializer.save()

        purchase = self._get_token_information(token)
        if purchase is None:
            purchase_info = AndriodPurchase.verify_token(token, product_id)
            purchase = _save_purchase(purchase_info)

        if purchase.acknowledgementState == 0:
            self.acknowleage(token, product_id)
            purchase_info = AndriodPurchase.verify_token(token, product_id)
            if purchase_info["acknowledgementState"] == 1:
                purchase.acknowledgementState = 1
                purchase.save(update_fields=["acknowledgementState"])

        return purchase, purchase.acknowledgementState == 1

    def acknowleage(cls, token: str, product_id: str):
        return AndriodPurchase.acknowleage(token, product_id)

    def validate(self, purchase_obj: MAndroidPurchase):
        if purchase_obj.user_id != self.user.id:
            raise BadRequestException(
                errcode=400103,
                err_message="This Token is not belong to you.",
                data={"user_id": self.user.id},
            )

    @classmethod
    def _get_token_information(cls, token) -> MAndroidPurchase:
        return MAndroidPurchase.objects.filter(token=token).first()
