from typing import Union

from django.db.models.query_utils import Q

from helper.exceptions import BadRequestException
from s_cards.models import SCards
from s_user.models import SUsers
from ..balance_manager import BalanceManager


def move_personal_balance(
    user: SUsers, from_card: SCards, to_card: Union[SCards, int] = None
):
    card_balance = BalanceManager(user, from_card).personal

    remain_balance = card_balance.total
    if remain_balance <= 0:
        return

    if to_card is not None:
        to_card = (
            to_card if isinstance(to_card, SCards) else user.view.card.get(id=to_card)
        )
    else:
        my_card_amount = user.view.card.all(~Q(card_type=SCards.BUSINESS)).count()
        if my_card_amount > 1:
            if to_card is None:
                raise BadRequestException(
                    errcode=400087, err_message="Missing 'receiving_card' field."
                )
        to_card = user.icard

    if from_card.id == to_card.id:
        raise BadRequestException(
            errcode=400088, err_message="Moving balance between same card."
        )

    to_card_balance = BalanceManager(user, to_card).personal

    card_balance.subtract(remain_balance)
    to_card_balance.add(remain_balance)
