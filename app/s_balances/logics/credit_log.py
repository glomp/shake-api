from helper.exceptions import AppException
from s_balances.models import SCreditLogs
from s_cards.models import SCards


class CreditLog:
    CREDIT_TYPE: SCreditLogs.CREDIT_TYPE = None

    CARD_TYPE = {}

    def __init__(
        self,
        card: SCards,
        credit_type: SCreditLogs.CREDIT_TYPE = CREDIT_TYPE,
    ):
        self.card = card
        self.credit_type = credit_type

    def create(self, amount: int, direction: str, **kwargs):
        return SCreditLogs.objects.create(
            card=self.card,
            credit_type=self.credit_type,
            amount=amount,
            direction=direction,
            **kwargs,
        )

    def validate_card(self):
        if self.card.card_type not in self.CARD_TYPE:
            raise AppException(
                errcode=500081,
                err_message="This card_type doesn't match!",
                data={
                    "card_id": self.card.id,
                    "credit_class": self.__class__.__name__,
                    "card_type": self.card.card_type,
                    "class_card_type": list(self.CARD_TYPE),
                },
            )


class PersonalCardCreditLog(CreditLog):
    CREDIT_TYPE = SCreditLogs.PERSONAL

    CARD_TYPE = {SCards.PERSONAL, SCards.SCAN, SCards.BUSINESS}


class CompanyCreditLog(CreditLog):
    CREDIT_TYPE = SCreditLogs.COMPANY

    CARD_TYPE = {SCards.BUSINESS}
