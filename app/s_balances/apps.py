from django.apps import AppConfig


class SBalancesConfig(AppConfig):
    name = 's_balances'
