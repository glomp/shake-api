from dynamic_rest.fields import DynamicRelationField
from dynamic_rest.serializers import DynamicModelSerializer
from rest_framework import serializers

from custom_django.models import fields
from .models import MAndroidPurchase, SCreditLogs


class CreditLogSerializer(DynamicModelSerializer):
    class Meta:
        model = SCreditLogs
        fields = "__all__"


class SAndroidPurchase(DynamicModelSerializer):
    class Meta:
        model = MAndroidPurchase
        fields = "__all__"


class SPublicAndroidPurchase(DynamicModelSerializer):
    class Meta:
        model = MAndroidPurchase
        exclude = MAndroidPurchase.SYSTEM_FIELDS
        read_only_fields = MAndroidPurchase.SYSTEM_FIELDS


# class SCompanyCreditLogsSeializer(DynamicModelSerializer):
#     class Meta:
#         model = SCompanyCreditLogs
#         fields = "__all__"
