# Generated by Django 2.2.19 on 2021-05-13 03:59

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('s_balances', '0005_delete_scompanycreditlogs'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='susercreditlogs',
            table='credit_log',
        ),
    ]
