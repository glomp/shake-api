from django.db import transaction
from django.http.response import Http404
from rest_framework.decorators import action

from custom_django.views.viewsets import SDynamicModelViewSet
from helper.exceptions import BadRequestException
from s_cards.models import SCards
from s_cards.serializers import SCardSerializer
from .logics.android_purchase import AndroidClient
from .logics.balance import (
    BusinessBalance,
    CompanyBalance,
    FreeBalance,
    PersonalBalance,
)
from .logics.balance_manager import BalanceManager
from .models import MAndroidPurchase, SCreditLogs
from .serializers import CreditLogSerializer, SAndroidPurchase, SPublicAndroidPurchase


class SBalanceLogViewSet(SDynamicModelViewSet):
    serializer_class = CreditLogSerializer
    model_class = SCreditLogs
    queryset = SCreditLogs.objects.all()

    def retrieve(self, request, pk):
        return self.view.balance_log.get(id=pk)

    def list(self, request):
        return self.view.balance_log.all()[self.pag_left : self.pag_right]

    def create(self, request):
        raise Http404

    def update(self, request, *args, **kwargs):
        raise Http404

    def destroy(self, request, *args, **kwargs):
        raise Http404


class SBalanceViewSet(SDynamicModelViewSet):
    serializer_class = SCardSerializer
    model_class = SCards
    queryset = SCards.objects.all()

    def retrieve(self, request, pk):
        return self.view.card.get(id=pk)

    def list(self, request):
        return self.view.card.all()[self.pag_left : self.pag_right]

    def create(self, request):
        raise Http404

    def update(self, request, *args, **kwargs):
        raise Http404

    def destroy(self, request, *args, **kwargs):
        raise Http404

    @action(methods=["post"], detail=True)
    @transaction.atomic
    def add(self, request, pk):
        instance = self.view.card.get(id=pk)
        balance_type = request.data["type"]

        # if balance_type == SCreditLogs.PERSONAL:
        #     if instance.card_type == SCards.BUSINESS:
        #         balance_logic = BusinessBalance(self.user, instance)
        #     else:
        #         balance_logic = PersonalBalance(self.user, instance)
        if balance_type == SCreditLogs.COMPANY:
            balance_logic = CompanyBalance(self.user, instance)
        else:
            raise BadRequestException(
                errcode=400082,
                message=f"We don't suport this. [{balance_type}]",
                data={
                    "card_id": instance.id,
                    "card_type": instance.card_type,
                    "balance_type": balance_type,
                },
            )

        log = balance_logic.add(amount=request.data["amount"])
        return {
            "card": self.serializer_class(instance),
            "log": CreditLogSerializer(log).data,
        }

    @action(methods=["get"], detail=False)
    def free(self, request):
        personal_card = self.view.card.personal.get_if_exist(
            select_related=[], prefetch_related=[]
        )
        if personal_card is None:
            raise BadRequestException(
                errcode=404104,
                message="You don't have personal card to get free credit.",
            )
        free_balance = FreeBalance(self.user, personal_card)
        return {
            "total": free_balance.total,
            "is_lock": free_balance.is_lock,
        }

    @action(methods=["get"], detail=True)
    def logs(self, request, pk):
        instance = self.view.card.get(id=pk)
        logs = self.view.balance_log.all(card_id=instance.id)
        return SBalanceLogViewSet.serializer_class(logs, many=True)

    @action(methods=["post"], detail=True, url_path="verify-token")
    def verify_token(self, request, pk):
        card = self.view.card.get(id=pk)
        purchase_obj, is_new = AndroidClient(self.user).verify_token(
            request.data["token"], request.data["product_id"]
        )
        if is_new:
            balance_manager = BalanceManager(self.user, card)
            balance_manager.personal.add(amount=purchase_obj.amount)
        return {
            "card": self.serializer_class(card),
            "purchase": SPublicAndroidPurchase(purchase_obj),
        }


class VAndroidPurchaseViewSet(SDynamicModelViewSet):
    serializer_class = SAndroidPurchase
    model_class = MAndroidPurchase
    queryset = MAndroidPurchase.objects.all()

    def retrieve(self, request, pk):
        return self.view.android_purchase.get(id=pk)

    def list(self, request):
        return self.view.android_purchase.all()

    def create(self, request, *args, **kwargs):
        raise Http404

    def update(self, request, *args, **kwargs):
        raise Http404

    def destroy(self, request, *args, **kwargs):
        raise Http404
