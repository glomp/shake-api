from django.core.validators import MinValueValidator
from django.db import models

from custom_django.models import SModel, fields
from s_cards.models import SCards
from s_companies.models import SCompanies, SCompanyUsers
from s_user.models import SUsers


class SCreditLogs(SModel):
    INCOMING = "I"
    OUTGOING = "O"
    DIRECTION = (
        (INCOMING, "INCOMING"),
        (OUTGOING, "OUTGOING"),
    )

    PERSONAL = "P"
    BUSINESS = "B"
    COMPANY = "C"
    CREDIT_TYPE = (
        (PERSONAL, "PERSONAL"),
        (BUSINESS, "BUSINESS"),
        (COMPANY, "COMPANY"),
    )

    card = models.ForeignKey(
        SCards, on_delete=models.CASCADE, related_name="credit_logs"
    )

    credit_type = models.CharField(max_length=1, choices=CREDIT_TYPE)
    direction = models.CharField(max_length=1, default=OUTGOING, choices=DIRECTION)
    amount = models.IntegerField(
        validators=[MinValueValidator(1, "Amount can't less then 1")]
    )
    log = models.CharField(max_length=255, blank=True, null=True)

    user_credit = models.IntegerField(default=0)
    free_credit = models.IntegerField(default=0)

    is_touch_free_credit_limit = models.BooleanField(default=False)

    _created = fields.UnixTimeStampField(null=True, auto_now_add=True)

    READONLY_FIELDS = ["user", "direction", "amount", "user_credit", "free_credit"]

    class Meta:
        managed = True
        db_table = "credit_log"


class MAndroidPurchase(SModel):
    user = models.ForeignKey(
        SUsers, on_delete=models.CASCADE, related_name="android_purchases"
    )

    token = models.CharField(max_length=255)
    product_id = models.CharField(max_length=127)
    amount = models.IntegerField()

    purchaseTimeMillis = fields.UnixTimeStampField()
    purchaseState = models.SmallIntegerField()
    consumptionState = models.SmallIntegerField()
    developerPayload = models.CharField(max_length=255, null=True, blank=True)
    orderId = models.CharField(max_length=63)
    purchaseType = models.SmallIntegerField()
    acknowledgementState = models.SmallIntegerField()
    kind = models.CharField(max_length=127)
    regionCode = models.CharField(max_length=7)

    _created = fields.UnixTimeStampField(null=True, auto_now_add=True)

    READONLY_FIELDS = ["token", "product_id"]
    SYSTEM_FIELDS = ["token", "product_id"]

    class Meta:
        managed = True
        db_table = "android_purchase"


# class SCompanyCreditLogs(SModel):
#     INCOMING = "I"
#     OUTGOING = "O"
#     DIRECTION = (
#         (INCOMING, "INCOMING"),
#         (OUTGOING, "OUTGOING"),
#     )

#     company_user = models.ForeignKey(SCompanyUsers, models.DO_NOTHING)

#     direction = models.CharField(
#         max_length=1, default=OUTGOING, choices=DIRECTION
#     )
#     amount = models.IntegerField(
#         validators=[MinValueValidator(1, "Amount can't less then 1")]
#     )
#     log = models.CharField(max_length=255, blank=True, null=True)

#     _created = fields.UnixTimeStampField(null=True, auto_now_add=True)

#     def get_readonly_fields(self, request, obj=None):
#         return ["company_user", "direction", "amount"] if obj else []

#     class Meta:
#         managed = True
#         db_table = "company_credit_log"
