from custom_django.test.base import BaseTestCase
from custom_django.test.card_logic import (
    BusinessCardBaseLogic,
    PersonalCardBaseLogic,
    ScanCardBaseLogic,
)
from custom_django.test.request_data import NoneRequest, TestRequestData
from helper.exceptions import BadRequestException
from s_contacts.views import SContactRequestsViewSet
from .models import SCreditLogs
from .views import SBalanceLogViewSet, SBalanceViewSet

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~ CREDIT LOG ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class PersonalBalanceLogTC(PersonalCardBaseLogic, BaseTestCase):
    ViewSet = SBalanceLogViewSet

    credit_amount = 123

    def create_specific_objects(self):
        _, self.card = self._create_card(self.suser, auto_add_credit=False)

    def create_test_objects(self):
        self.test_data, self.test_obj = self.helper.create_personal_credit(
            self.card, self.credit_amount
        )

    def validate_result(self, origin, result) -> bool:
        return (
            result["card"] == origin["card"].id,
            result["amount"] == origin["amount"],
        )

    def test_retrieve(self):
        self.default_test_retrieve()

    def test_list(self):
        self.default_test_list()


class ScanBalanceLogTC(ScanCardBaseLogic, PersonalBalanceLogTC):
    pass


class BusinessBalanceLogTC(BusinessCardBaseLogic, PersonalBalanceLogTC):
    pass


class CompanyBalanceLogTC(BusinessBalanceLogTC):
    def create_test_objects(self):
        self.test_data, self.test_obj = self.helper.create_company_credit(
            self.card, self.credit_amount
        )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~ BALANCE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class PersonalBalanceTC(PersonalCardBaseLogic, BaseTestCase):
    ViewSet = SBalanceViewSet

    credit_amount = 123

    @classmethod
    def create_credit(self):
        self.test_data["credit_amount"] = self.credit_amount
        self.test_data["balance_total_personal"] = self.credit_amount
        _, self.credit_log = self.helper.create_personal_credit(
            self.test_obj, self.credit_amount
        )

    def create_test_objects(self):
        self.test_data, self.test_obj = self._create_card(
            self.suser, auto_add_credit=False
        )
        self.create_credit()

    def validate_result(self, origin, result) -> bool:
        return [result["balance_total_personal"] == origin["credit_amount"]]

    def test_retrieve(self):
        self.default_test_retrieve()

    def test_list(self):
        self.default_test_list()

    # def test_add(self):
    #     new_credit_amount = 23
    #     request = TestRequestData(
    #         {"type": SCreditLogs.PERSONAL, "amount": new_credit_amount},
    #         self.user,
    #         method="POST",
    #     )
    #     assert (
    #         self.run_func("add", request, self.test_obj.id) == self.SUCCESS
    #     ), self.last_run_result()
    #     assert "card" in self.last_result_data(), self.debug_data()
    #     self._compare_result(
    #         {"credit_amount": self.credit_amount + new_credit_amount},
    #         self.last_result_data()["card"],
    #     )

    def test_logs(self):
        assert self.run_func(
            "logs", NoneRequest(user=self.user), self.test_obj.id
        ), self.last_run_result()
        results = self.last_result_data()
        assert (
            isinstance(results, list) and results[0]["amount"] == self.credit_amount
        ), self.debug_data()

    def test_verify_token(self):
        request = TestRequestData(
            {
                "token": "cohcdjkemppnekoaonmomjjp.AO-J1OxQcSCpbxyhylCDkYHQFiSAPeKEtyzzVRhqy38nkd5IQ6QVPNSEDJWCzGMvc3MrQUFV_04U4edA3CZeE3zGpGQ9HWKgaDSf4wTevQvebtOQJPdgXW8",
                "product_id": "sku_20_credits",
            },
            user=self.user,
        )
        assert self.run_func(
            "verify_token", request, pk=self.test_obj.id
        ), self.last_run_result()
        card = self.last_result_data()["card"]
        assert (
            card["balance_total_personal"]
            == self.test_data["balance_total_personal"] + 20
        ), self.debug_data()


class ScanBalanceTC(ScanCardBaseLogic, PersonalBalanceTC):
    pass


class BusinessBalanceLogTC(BusinessCardBaseLogic, PersonalBalanceTC):
    pass


class CompanyBalanceLogTC(BusinessBalanceLogTC):
    def validate_result(self, origin, result) -> bool:
        return [result["balance_total_company"] == origin["credit_amount"]]

    @classmethod
    def create_credit(self):
        self.test_data["credit_amount"] = self.credit_amount
        _, self.credit_log = self.helper.create_company_credit(
            self.test_obj, self.credit_amount
        )

    def test_verify_token(self):
        pass


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~ BALANCE LOGIC ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class PersonalBalanceLogicTC(PersonalCardBaseLogic):
    ViewSet = SContactRequestsViewSet

    CREDIT_AMOUNT = 1
    FREE_CARD_AMOUNT = 10
    CARD_AMOUNT = FREE_CARD_AMOUNT + CREDIT_AMOUNT + 1

    def create_general_objects(self):
        _, self.suser_2 = self.helper.create_user("user2@yopmail.com")
        self.cards = [
            ScanCardBaseLogic._create_card(self.suser_2)[1]
            for i in range(self.CARD_AMOUNT)
        ]
        self.card_ids = [card.id for card in self.cards]

    def create_test_objects(self):
        self.test_data, self.test_obj = self.create_card(auto_add_credit=False)

    def create_related_objects(self):
        self.CREDIT_AMOUNT > 0 and self.helper.create_personal_credit(
            self.test_obj, amount=self.CREDIT_AMOUNT
        )

    def test_limit_1(self):
        expected_successful_requests = self.card_ids[: self.CARD_AMOUNT - 1]
        data = TestRequestData(
            {
                "from_card_id": self.test_obj.id,
                "to_card_id": expected_successful_requests,
                "tier_number": 3,
            },
            self.user,
        )
        assert self.run_func("create", data) == self.SUCCESS, self.last_run_result()
        # This request must fail
        data = TestRequestData(
            {
                "from_card_id": self.test_obj.id,
                "to_card_id": [self.card_ids[-1]],
                "tier_number": 3,
            },
            self.user,
        )
        assert self.run_func("create", data) == self.BAD_REQUEST, self.last_run_result()

    def test_limit_2(self):
        expected_fail_requests = self.card_ids[: self.CARD_AMOUNT]
        data = TestRequestData(
            {
                "from_card_id": self.test_obj.id,
                "to_card_id": expected_fail_requests,
                "tier_number": 3,
            },
            self.user,
        )
        # This request must fail
        assert self.run_func("create", data) == self.BAD_REQUEST, self.last_run_result()


class FreeBalanceLogicTC(PersonalBalanceLogicTC):
    CREDIT_AMOUNT = 0
    FREE_CARD_AMOUNT = 10
    CARD_AMOUNT = FREE_CARD_AMOUNT + CREDIT_AMOUNT + 1

    @BaseTestCase.wrap_exception
    def test_limit_3(self):
        assert (
            self.run_func("free", NoneRequest(user=self.user), ViewSet=SBalanceViewSet)
            == self.SUCCESS
        ), self.last_run_result()
        data = self.last_result_data()
        assert data["total"] == self.FREE_CARD_AMOUNT, self.debug_data(
            f"Total must be {self.FREE_CARD_AMOUNT}"
        )
        assert data["is_lock"] == False, self.debug_data("Balance must be unblocked")

        super().test_limit_1()

        assert (
            self.run_func("free", NoneRequest(user=self.user), ViewSet=SBalanceViewSet)
            == self.SUCCESS
        ), self.last_run_result()
        data = self.last_result_data()
        assert data["total"] == 0, self.debug_data("Total must be 0")
        assert data["is_lock"] == True, self.debug_data("Balance must be blocked")


class BusinessBalanceLogicTC(BusinessCardBaseLogic, PersonalBalanceLogicTC):
    CREDIT_AMOUNT = 1
    FREE_CARD_AMOUNT = 5
    CARD_AMOUNT = FREE_CARD_AMOUNT + CREDIT_AMOUNT + 1


class CompanyBalanceLogicTC(BusinessBalanceLogicTC):
    CREDIT_AMOUNT = 1
    COMPANY_CREDIT_AMOUNT = 2
    FREE_CARD_AMOUNT = 5
    CARD_AMOUNT = COMPANY_CREDIT_AMOUNT + FREE_CARD_AMOUNT + CREDIT_AMOUNT + 1

    def create_related_objects(self):
        self.helper.create_company_credit(self.test_obj, self.COMPANY_CREDIT_AMOUNT)
        return super().create_related_objects(self)
