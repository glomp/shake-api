from django.conf import settings

from helper.exceptions import (
    AppException,
    BadRequestException,
    ConfictException,
    ForbiddenException,
    NotFoundException,
    UnauthorizedException,
)
from helper.logger_handler import slogger


def system_info():
    slogger.info("%-22s%s", "ENV", settings.ENV)
    slogger.info("%-22s%s", "DATABASES[HOST]", settings.DATABASES["default"]["HOST"])


def git_info():
    slogger.info("%-22s%s", "GIT BRANCH", settings.GIT_BRANCH)
    slogger.info("%-22s%s", "GIT HASH SHORT", settings.GIT_HASH_SHORT)
    slogger.info("%-22s%s", "GIT HASH", settings.GIT_HASH)
    slogger.info("%-22s%s", "GIT COMMIT MESSAGE", settings.GIT_COMMIT_MESSAGE)
    if len(settings.GIT_BRANCH) == 0:
        AppException(
            errcode=500105,
            message="Missing git debug info",
            data={"env": settings.ENV},
        )


def exception_info():
    slogger.info(
        "%-22s%-10s%-14s%-14s",
        "Exception Trigger",
        "LOG_EXC",
        "VERBOSE_EXC",
        "NOTIFY_EXC",
    )
    slogger.info(
        "%-22s%-10s%-14s%-14s",
        "AppException",
        AppException.LOG_EXC,
        AppException.VERBOSE_EXC,
        AppException.NOTIFY_EXC,
    )
    slogger.info(
        "%-22s%-10s%-14s%-14s",
        "BadRequestException",
        BadRequestException.LOG_EXC,
        BadRequestException.VERBOSE_EXC,
        BadRequestException.NOTIFY_EXC,
    )
    slogger.info(
        "%-22s%-10s%-14s%-14s",
        "UnauthorizedException",
        UnauthorizedException.LOG_EXC,
        UnauthorizedException.VERBOSE_EXC,
        UnauthorizedException.NOTIFY_EXC,
    )
    slogger.info(
        "%-22s%-10s%-14s%-14s",
        "ForbiddenException",
        ForbiddenException.LOG_EXC,
        ForbiddenException.VERBOSE_EXC,
        ForbiddenException.NOTIFY_EXC,
    )
    slogger.info(
        "%-22s%-10s%-14s%-14s",
        "NotFoundException",
        NotFoundException.LOG_EXC,
        NotFoundException.VERBOSE_EXC,
        NotFoundException.NOTIFY_EXC,
    )
    slogger.info(
        "%-22s%-10s%-14s%-14s",
        "ConfictException",
        ConfictException.LOG_EXC,
        ConfictException.VERBOSE_EXC,
        ConfictException.NOTIFY_EXC,
    )


def info():
    system_info()
    git_info()
    exception_info()

    # from logging_tree import printout

    # printout()
