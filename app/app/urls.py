"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin

from m_company.apps import CompanyManagerRouter
from .api_routers import APIRouter
from .info import info

info()

urlpatterns = [
    url(r"^admin/", admin.site.urls),
    url(r"^api/", include(APIRouter.urlpatterns)),
    url(r"^manager/", include(CompanyManagerRouter.urlpatterns)),
]


if settings.ENV != "PRODUCTION":
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.ENV in {"PRODUCTION"}:
    from .sentry import init_sentry

    init_sentry()
