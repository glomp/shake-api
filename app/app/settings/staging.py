from .defaults import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# AWS
AWS_STORAGE_BUCKET_NAME = "shake-bucket-staging"

# CORS
ALLOWED_HOSTS = ["*"]
CORS_ORIGIN_ALLOW_ALL = True


# Custom settings

# ENV
ENV = "STAGING"

# URI
CUSTOM_DOMAIN = "http://ec2-54-251-236-88.ap-southeast-1.compute.amazonaws.com:8099"
