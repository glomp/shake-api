from .defaults import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

# AWS
AWS_STORAGE_BUCKET_NAME = "shake-bucket-production"

# CORS
ALLOWED_HOSTS = [".letsshake.app", "localhost"]
CORS_ALLOWED_ORIGIN_REGEXES = [
    r"^https://(\w+\.)*letsshake\.app$",
    "http://localhost:8000",
]

# Custom settings

# ENV
ENV = "PRODUCTION"

# URI
CUSTOM_DOMAIN = "https://api.letsshake.app"
