from .defaults import *

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.mysql",
        "NAME": "shake_2020_test",
        "USER": "shake",
        "PASSWORD": "H.@ctiv8",
        "HOST": "127.0.0.1",  # Or an IP Address that your DB is hosted on
        "PORT": "3306",
    }
}

DEBUG = False
ENV = "TEST"

LOGGING["handlers"]["file"]["level"] = "DEBUG"
LOGGING["handlers"]["file"]["formatter"] = "file"
LOGGING["handlers"]["exception_file"]["level"] = "DEBUG"
LOGGING["loggers"]["shake_console"]["handlers"] = ["file"]
# Print SQl
# LOGGING["loggers"]["django.db.backends"] = {
#     "level": "DEBUG",
#     "handlers": ["console"],
# }
