import os
from datetime import date, timedelta

from .. import local_env

# Secret
AWS_ACCESS_KEY_ID = os.environ.get("AWS_ACCESS_KEY_ID", local_env.AWS_ACCESS_KEY_ID)
AWS_SECRET_ACCESS_KEY = os.environ.get(
    "AWS_SECRET_ACCESS_KEY", local_env.AWS_SECRET_ACCESS_KEY
)

# Custom
AWS_STORAGE_BUCKET_NAME = "shake-bucket-staging"
AWS_S3_REGION_NAME = "ap-southeast-1"

# Const
AWS_FILE_EXPIRE = 200
AWS_PRELOAD_METADATA = True
AWS_QUERYSTRING_AUTH = False

AWS_PUBLIC_MEDIA_LOCATION = "media/public"
DEFAULT_FILE_STORAGE = "app.s_medias.custom_storages.PublicMediaStorage"
AWS_PRIVATE_MEDIA_LOCATION = "media/private"
PRIVATE_FILE_STORAGE = "app.s_medias.custom_storages.PrivateMediaStorage"

S3_URL = "//%s.s3.amazonaws.com/" % AWS_STORAGE_BUCKET_NAME
MEDIA_URL = "//%s.s3.amazonaws.com/media/" % AWS_STORAGE_BUCKET_NAME
MEDIA_ROOT = MEDIA_URL
STATIC_URL = S3_URL + "static/"
ADMIN_MEDIA_PREFIX = STATIC_URL + "admin/"

two_months = timedelta(days=61)
date_two_months_later = date.today() + two_months
expires = date_two_months_later.strftime("%A, %d %B %Y 20:00:00 GMT")

AWS_S3_OBJECT_PARAMETERS = {
    "Expires": expires,
    "CacheControl": "max-age=%d" % (int(two_months.total_seconds()),),
}
