from .defaults import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# AWS
AWS_STORAGE_BUCKET_NAME = "shake-bucket-production"

# CORS Middle Ware
ALLOWED_HOSTS = ["*"]
CORS_ORIGIN_ALLOW_ALL = True

# Custom settings

# ENV
ENV = "LOCAL"

# URI
CUSTOM_DOMAIN = "http://localhost:8000"
