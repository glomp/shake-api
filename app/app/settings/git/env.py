GIT_BRANCH = "develop"
GIT_HASH_SHORT = "81993ad"
GIT_HASH = "81993ad0be4612d70d97ff0221a9f8caf67b3de3"
GIT_COMMIT_MESSAGE = "feat: exception: handle request as object, not class"
