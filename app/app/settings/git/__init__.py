import os
import subprocess

try:
    GIT_BRANCH = (
        subprocess.check_output(["git", "branch", "--show-current"]).strip().decode()
    )
    GIT_HASH_SHORT = (
        subprocess.check_output(["git", "rev-parse", "--short", "HEAD"])
        .strip()
        .decode()
    )
    GIT_HASH = subprocess.check_output(["git", "rev-parse", "HEAD"]).strip().decode()
    GIT_COMMIT_MESSAGE = (
        subprocess.check_output(["git", "log", "-1", "--pretty=%B"]).strip().decode()
    )
    if not GIT_BRANCH:
        from .env import *
except Exception:
    from .env import *
