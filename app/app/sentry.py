import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

from django.conf import settings

from helper.logger_handler import slogger


def before_send_to_sentry(event, hint):
    if "exc_info" in hint:
        return event


def init_sentry():
    slogger.info("Init sentry: %s", settings.SENTRY_DSN)
    sentry_sdk.init(
        dsn=settings.SENTRY_DSN,
        integrations=[DjangoIntegration()],
        traces_sample_rate=1.0,
        # Custom arguments
        environment=settings.ENV,
        ignore_errors=[Exception],
        before_send=before_send_to_sentry,
        # If you wish to associate users to errors (assuming you are using
        # django.contrib.auth) you may enable sending PII data.
        send_default_pii=True,
    )
