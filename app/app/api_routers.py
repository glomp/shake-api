from django.conf import settings
from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter
from rest_framework_swagger.views import get_swagger_view

from s_auth.apps import SAuthRouter
from s_balances.views import (
    SBalanceLogViewSet,
    SBalanceViewSet,
    VAndroidPurchaseViewSet,
)
from s_cards.views import SCardsViewSet
from s_companies.views import SCompaniesViewSet, SManagedCompaniesViewSet
from s_contacts.views import (
    SContactCardsViewSet,
    SContactNotesViewSet,
    SContactRequestsViewSet,
    SContactsViewSet,
    SContactTasksViewSet,
    SShareContactRequestsViewSet,
)
from s_medias.views import SMediasViewSet
from s_notifications.views import SNotificationsViewSet
from s_user.views import SUserViewSet


class APIRouter:
    router = DefaultRouter()
    router.register("users", SUserViewSet)

    router.register("cards", SCardsViewSet)
    router.register("medias", SMediasViewSet)

    router.register("contact-requests", SContactRequestsViewSet)
    router.register("share-contact-requests", SShareContactRequestsViewSet)

    router.register("contacts", SContactsViewSet)
    router.register("contact-cards", SContactCardsViewSet)
    router.register("contact-notes", SContactNotesViewSet)
    router.register("contact-tasks", SContactTasksViewSet)

    router.register("balances", SBalanceViewSet)
    router.register("balance-logs", SBalanceLogViewSet)
    router.register("android-purchase", VAndroidPurchaseViewSet)

    router.register("companies", SCompaniesViewSet)
    router.register("managed-companies", SManagedCompaniesViewSet)

    router.register("notifications", SNotificationsViewSet)

    urlpatterns = [
        url(r"^auth/", include(SAuthRouter.urlpatterns)),
        url(r"", include(router.urls)),
    ]


if settings.DEBUG and settings.ENV != "PRODUCTION":
    APIRouter.urlpatterns += [
        url(
            "swagger-api",
            get_swagger_view(
                title="Swagger API",
                patterns=APIRouter.urlpatterns,
            ),
        )
    ]
