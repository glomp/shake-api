# DOCUMENTATION

## [0. About Django](./django.md)

## [1. Installation](./installation.md)

## [2. App Logic](./logic.md)

## [3. Tag](./tag.md)

## [4. Database Diagram](./db_diagram/README.md)

## [5. API](./api.md)

## [6. Report](./report.md)

## [7. Todo](./todo.md)

## [8. Q&A](./Q_and_A.md)
