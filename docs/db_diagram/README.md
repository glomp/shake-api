[<- Back](../README.md)

# DATABASE DIAGRAM

## [Version 2.4.12 - 2021/08/23](./v2.4.12.png): Save user purchase and user device
- Add `android_purchase` table: save Android purchase information
- Add `s_user_request` table: save user device information for debug
- Rename column `name`in table `s_card` become `title`

![v2.4.12.png](./v2.4.12.png)


## [Version 2.4.7 - 2021/07/24](./v2.4.7.png): Save user information
- Add `s_potential_user_information` table: save email and mobile of receiver user outside of system.
- Add `s_company_registration` table and `s_company_other_information` table: save company information when they register in the shake website.
- Add `s_apple_account` table: special table table to map "apple_id" to an account.
- Remove `s_aggregate_contact` view: use code to query complex query.
- Add `inviter` column into `s_company_invitation` table: Get infor of inviter.
- Add some columns into `s_card`: Save policy informations and balance informations. 

![v2.4.7.png](./v2.4.7.png)


## [Version 2.4.1 - 2021/02/03](./v2.4.1.png): Update company logic, seprate company contact and user contact.
- Replace `s_aggregate_contact` table by `s_aggregate_contact` view.
- Copy all template properties to card and remove relation between template and property. 
- Add 4 columns `user_name`, `avatar`, `job_tile` and `company_name` to card and remove `s_card_view` view.
- Move `card_id` from `s_company_user` table to `company_user_id` in `s_card` table.
- Copy all company contact to each personal contact.
- Remove `s_company_credit_log` (because of changing logic).

![v2.4.1.png](./v2.4.1.png)


## [Version 2.3.6 - 2021/01/18](./v2.3.6.png): Add Aggregate contact
- Add `s_share_contact_requerst` table
- Add `card_id` filed into `s_company_user` table
- Add `s_aggregate_contact` table: aggregate all contact, include personal contact and business contact.

![v2.3.6.png](./v2.3.6.png)


## [Version 2.3.5 - 2021/01/18](./v2.3.5.png): Update credit log logic
- Move `user_redit_log` relation because of updating logic.

![v2.3.5.png](./v2.3.5.png)



## [Version 2.3.4 - 2020/12/28](./v2.3.4.png): Card view
- Add `s_card_view` view for `s_card`.

![v2.3.4.png](./v2.3.4.png)


## [Version 2.3.3 - 2020/12/09](./v2.3.3.png): Realtime notification feature.
- Add `fcm-diango` table for Notification function.

![v2.3.3.png](./v2.3.3.png)


## [Version 2.3.2 - 2020/12/09](./v2.3.2.png): Company contact permission.
- `company-invitaion` is link to a `card` because when a company invite a user, the card for that user will be created available at that time.

- Split out `personal-contact` and `company-contact`. It help easy to manage access permission for user in company.
- Add `company-user-contact` to manage company user permission.

![v2.3.2.png](./v2.3.2.png)


## [Version 2.3.1 - 2020/12/04](./v2.3.1.png): Rebuild new Database
- Rebuild Database:
  - `user-info` don't contain any `card` data.

  - A `real-card` will need information from `card` and `template` only:
  - All `property` (fields) on `card` and `template` is dynamic.

![v2.3.1.png](./v2.3.1.png)


## [Version 2.2.2 - 2020/11/24](./v2.2.2.png): Media info
- Add `media` fields into `user-info`

![v2.2.2.png](./v2.2.2.png)


## [Version 2.2.1 - 2020/11/12](./v2.2.1.png): Add some require fields
- The `template` is now representative of the company office:
  - A `company` have many `template`.
  - A `card` will contain information of that office (`template`).
  - A `customer` of that company will related through office (`template`).
  - A `company-member` is belong to only one office (`template`).
  - A `company-invitation` is belong to a office (`template`).

![v2.2.1.png](./v2.2.1.png)


## [Version 2.1.3 - 2020/11/09](./v2.1.3.png): Add some require fields
- Add `company-invitation` table
- Add `contact-request` table
- Update `password-request` table to `user-requet` to handle:
  - password reset request
  - active account request
- Add some field into `company-user` table

![v2.1.3.png](./v2.1.3.png)


## [Version 2.1.2 - 2020/10/23](./v2.1.2.png): Add some require fields
- Add `password-request` table

![v2.1.2.png](./v2.1.2.png)


## [Version 2.0.1 - 2020/10/05](./v2.0.1.png): Rebuild new Database
- All card data in synchronize by save on user table

![v2.0.1.png](./v2.0.1.png)


## [Version 1.1.1 - 2020/08/19](./v1.1.1.png): Add some require fields
![v1.1.1.png](./v1.1.1.png)


## [Version 1.1.0 - 2020/08/14](./v1.1.0.png): Schema on jar
![v1.1.0.png](./v1.1.0.png)


## [Version 1.0.0 - 2020/08/12](./v1.0.0.png): Setup the app
![v1.0.0.png](./v1.0.0.png)
