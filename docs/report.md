[<- Back](./README.md)

# Report

> `...` Don't need yet \
> `[ ]` Not done \
> `[_]` On doing \
> `[x]` Done
>
> `[.]` Can pass this \
> `[!]` Important \
> `[?]` Require some information
>
> `[-]` Need test \
> `[1]` Tested some functions with `SUCCESS` response \
> `[2]` Tested all functions with `SUCCESS` response \
> `[3]` Include `[2]` and tested some functions with `FAIL` response \
> `[4]` Include `[2]` and tested all functions with `FAIL` response

## v2.4.7 - 2021/07/29

### I - Client API:
#### `[x]` `[1]` Authentication:
- `[x]` `[4]` Get Account information - Tested
- `[x]` `[-]` Get Apple account information
- `[x]` `[1]` Verify token
    - `[x]` `[2]` Active account
    - `[x]` `[2]` Delete account
    - `[x]` `[-]` Expired token
- `[x]` `[-]` Reset Password
- `[x]` `[-]` Set Password
- `[x]` `[-]` Reverify account
- `[x]` `[2]` Register
- `[x]` `[-]` Apple Register
- `[x]` `[2]` Company Register
- `[x]` `[-]` Login
- `[x]` `[-]` Logout

#### `[x]` `[1]` User:
- `[x]` `[-]` Read
- `[x]` `[-]` Create
- `[x]` `[-]` Update
- `[x]` `[2]` Delete
- `[x]` `[-]` Get user information
- `[x]` `[-]` Change password
- `[x]` `[2]` Update user location
- `[x]` `[2]` Add push token
- `[x]` `[2]` Hard delete

#### `[x]` `[1]` Card:
- `[x]` `[2]` Read:
    - `[x]` `[2]` Personal Card
    - `[x]` `[2]` Scan Card
    - `[x]` `[2]` Business Card
    - `[x]` `[2]` Printed Card - Copied Scan Card
    - `[x]` `[-]` Identity Card
    - `...` `[-]` Template Card
- `[x]` `[2]` Create:
    - `[x]` `[2]` Personal Card
    - `[x]` `[2]` Scan Card
    - `...` `[-]` Business Card
    - `[x]` `[2]` Printed Card - Copied Scan Card
    - `...` `[-]` Identity Card
    - `...` `[-]` Template Card
- `[x]` `[2]` Update:
    - `[x]` `[2]` Personal Card
    - `[x]` `[2]` Scan Card
    - `[x]` `[2]` Business Card
    - `[x]` `[2]` Printed Card - Copied Scan Card
    - `[x]` `[-]` Identity Card
    - `...` `[-]` Template Card
- `[x]` `[2]` Delete:
    - `[x]` `[2]` Personal Card
    - `[x]` `[2]` Scan Card
    - `[x]` `[2]` Business Card
    - `...` `[-]` Printed Card - Copied Scan Card
    - `...` `[-]` Identity Card
    - `...` `[-]` Template Card
- `[x]` `[2]` Send message to email:
    - `[x]` `[2]` Personal Card
    - `[x]` `[2]` Scan Card
    - `[x]` `[2]` Business Card
    - `...` `[-]` Printed Card - Copied Scan Card
    - `...` `[-]` Identity Card
    - `...` `[-]` Template Card
- `[x]` `[2]` Send message to mobile:
    - `[x]` `[2]` Personal Card
    - `[x]` `[2]` Scan Card
    - `[x]` `[2]` Business Card
    - `...` `[-]` Printed Card - Copied Scan Card
    - `...` `[-]` Identity Card
    - `...` `[-]` Template Card
- `[x]` `[-]` Get all card
- `[x]` `[-]` Get identity card
- `[x]` `[2]` Verify access code
- `[x]` `[1]` Search
    - `[x]` `[2]` Normal search
    - `[x]` `[-]` Search by visibility:
        - `[x]` `[-]` Everyone
        - `[ ]` `[-]` Nearby only
        - `[x]` `[-]` Contact of contact
- `[x]` `[2]` Search nearby
    - `[x]` `[2]` Normal search
    - `[x]` `[-]` Search by visibility:
        - `[x]` `[-]` Everyone
        - `[x]` `[-]` Nearby only
        - `[x]` `[-]` Contact of contact
- `[x]` `[2]` Search advanced
    - `[x]` `[2]` Normal search
    - `[x]` `[-]` Search by visibility:
        - `[x]` `[-]` Everyone
        - `[x]` `[-]` Nearby only
        - `[x]` `[-]` Contact of contact
    - `[x]` `[2]` Search by keywords
- `[x]` `[2]` Relation:
    - `[x]` `[2]` In contact
    - `[x]` `[2]` In contact request

#### `[x]` `[1]` Contact:
- `[x]` `[1]` Contact:
    - `[x]` `[1]` Read from:
        - `[x]` `[-]` All Card
        - `[x]` `[2]` Personal Card
        - `[x]` `[2]` Scan Card
        - `[x]` `[2]` Business Card
        - `...` `[-]` Printed Card - Copied Scan Card
        - `[x]` `[-]` Identity Card
        - `...` `[-]` Template Card
    - `...` `[-]` Create
    - `[.]` `[-]` Update
    - `[.]` `[-]` Delete
    - `[x]` `[2]` Note:
    - `[x]` `[2]` Task:
- `[x]` `[1]` Contact request:
    - `[x]` `[1]` Read from:
        - `[x]` `[-]` All Card
        - `[x]` `[2]` Personal Card
        - `[x]` `[2]` Scan Card
        - `[x]` `[2]` Business Card
        - `...` `[-]` Printed Card - Copied Scan Card
        - `...` `[-]` Identity Card
        - `...` `[-]` Template Card
    - `[x]` `[2]` Create by my:
        - `[x]` `[2]` Personal Card
        - `[x]` `[2]` Scan Card
        - `[x]` `[2]` Business Card
        - `...` `[-]` Printed Card - Copied Scan Card
        - `...` `[-]` Identity Card
        - `...` `[-]` Template Card
    - `...` `[-]` Update - This function is implemented inside `Create` API
    - `[x]` `[2]` Delete by my:
        - `[x]` `[2]` Personal Card
        - `[x]` `[2]` Scan Card
        - `[x]` `[2]` Business Card
        - `...` `[-]` Printed Card - Copied Scan Card
        - `...` `[-]` Identity Card
        - `...` `[-]` Template Card
    - `[x]` `[1]` Confirm
        - `[x]` `[-]` Give information about `send_card_back`
    - `[x]` `[-]` Send one card to me - For staff, who demo the app.
- `[x]` `[1]` Share contact:
    - `[x]` `[1]` Read from:
        - `[x]` `[-]` All Card
        - `[x]` `[2]` Personal Card
        - `[x]` `[2]` Scan Card
        - `[x]` `[2]` Business Card
        - `...` `[-]` Printed Card - Copied Scan Card
        - `...` `[-]` Identity Card
        - `...` `[-]` Template Card
    - `[x]` `[2]` Create by my:
        - `[x]` `[2]` Personal Card
        - `[x]` `[2]` Scan Card
        - `[x]` `[2]` Business Card
        - `...` `[-]` Printed Card - Copied Scan Card
        - `...` `[-]` Identity Card
        - `...` `[-]` Template Card
    - `...` `[-]` Update - This function is implemented inside `Create` API
    - `[x]` `[2]` Delete by my:
        - `[x]` `[2]` Personal Card
        - `[x]` `[2]` Scan Card
        - `[x]` `[2]` Business Card
        - `...` `[-]` Printed Card - Copied Scan Card
        - `...` `[-]` Identity Card
        - `...` `[-]` Template Card
    - `[x]` `[1]` Confirm

#### `[x]` `[2]` Balance:
- `[x]` `[2]` Balance logs:
    - `[x]` `[2]` Read:
        - `[x]` `[2]` Personal balance:
            - `[x]` `[2]` Personal card
            - `[x]` `[2]` Scan card
            - `[x]` `[2]` Business card
        - `[x]` `[2]` Company balance:
            - `[x]` `[2]` Business card
    - `...` `[-]` Create / Update / Delete
- `[x]` `[2]` Balance:
    - `[x]` `[2]` Read:
        - `[x]` `[2]` Personal balance:
            - `[x]` `[2]` Personal card
            - `[x]` `[2]` Scan card
            - `[x]` `[2]` Business card
        - `[x]` `[2]` Company balance:
            - `[x]` `[2]` Business card
    - `...` `[-]` Create / Update / Delete
    - `[x]` `[2]` Add - This is the same with `Create`:
        - `[x]` `[2]` Personal balance:
            - `[x]` `[2]` Personal card
            - `[x]` `[2]` Scan card
            - `[x]` `[2]` Business card
        - `[x]` `[2]` Company balance:
            - `[x]` `[2]` Business card

#### `[x]` `[-]` Media:
- `[x]` `[-]` Read
- `[x]` `[-]` Create
- `[x]` `[-]` Update
- `[x]` `[-]` Delete

#### `[ ]` `[1]` Notification:
- `[ ]` `[1]` Read from:
    - `[x]` `[2]` All Card
    - `[ ]` `[-]` Each Card
- `...` `[-]` Create
- `[x]` `[2]` Update
- `[x]` `[2]` Delete
- `[x]` `[2]` Search advanced

#### `[x]` `[2]` Company:
- `[x]` `[2]` Company User:
    - `[x]` `[2]` Read
    - `[x]` `[2]` Create - For Admin
    - `[x]` `[2]` Update - For Admin
    - `...` `[-]` Delete
- `[x]` `[2]` Managed Company - For shake dashboard:
    - `[x]` `[2]` Read
    - `...` `[-]` Create
    - `...` `[-]` Update
    - `...` `[-]` Delete

### II - Manager API (Company API):
#### `[ ]` `[2]` Template:
- `[x]` `[2]` Create / Read / Update
- `[ ]` `[2]` Delete:
    - `[x]` `[2]` When there are no card use this template
    - `[ ]` `[-]` Other

#### `[x]` `[2]` Employee:
- `[x]` `[2]` Create / Read / Update / Delete

#### `[x]` `[1]` Balance:
- `[x]` `[1]` Balance log:
    - `[x]` `[2]` Read
    - `...` `[-]` Create / Update / Delete
    - `[x]` `[-]` List by month
    - `[x]` `[-]` List by year
- `[x]` `[1]` Total Balance:
    - `[x]` `[2]` Read
    - `...` `[-]` Create / Update / Delete
    - `[x]` `[-]` List by month

#### `[x]` `[2]` Contact:
- `[x]` `[2]` List by contact:
    - `[x]` `[2]` Read
    - `...` `[-]` Create / Update / Delete
- `[x]` `[2]` List by card:
    - `[x]` `[2]` Read
    - `...` `[-]` Create / Update / Delete

#### Employee API:
- `[x]` `[1]` Balance:
    - `[x]` `[1]` Balance log:
        - `[x]` `[2]` Read
        - `...` `[-]` Create / Update / Delete
        - `[x]` `[-]` List by month
        - `[x]` `[-]` List by year

- `[x]` `[2]` Contact:
    - `[x]` `[2]` List by contact:
        - `[x]` `[2]` Read
        - `[x]` `[2]` Create - Allow employee to have a contact in company's contacts
        - `...` `[-]` Update
        - `[x]` `[2]` Delete
    - `...` `[-]` List by card

- `[x]` `[2]` Contact request - Company can see contact request of employee:
    - `[x]` `[2]` Read
    - `...` `[-]` Create
    - `...` `[-]` Update
    - `[x]` `[2]` Delete

### III - User Logic:
#### `[x]` `[-]` Authentication:
- `[x]` `[-]` By Password
- `[x]` `[-]` By Facebook
- `[x]` `[-]` By Google
- `[x]` `[-]` By Apple

#### `[x]` `[2]` User:
- `[x]` `[2]` Delete user:
    - `[x]` `[2]` Soft delete
    - `[x]` `[2]` Hard delete

#### `[x]` `[1]` Card:
- `[x]` `[2]` Personal card:
    - `[x]` `[2]` Create
    - `[x]` `[2]` Update
    - `[x]` `[2]` Delete:
        - `[x]` `[2]` Soft delete
        - `[x]` `[-]` Hard delete
- `[x]` `[2]` Scan card
    - `[x]` `[2]` Create / Update / Delete - Same as Personal card
    - `[.]` `[-]` Merge to business card
- `[x]` `[1]` Business card:
    - `[x]` `[2]` Create:
        - `[x]` `[2]` Create with out user
        - `[ ]` `[-]` Create with user
    - `[x]` `[1]` Update:
        - `[x]` `[-]` Update by company:
            - `[.]` `[-]` With user
            - `[x]` `[-]` With out user
        - `[x]` `[2]` Update by user
    - `[x]` `[1]` Delete - always soft delete:
        - `[x]` `[-]` Delete by company
        - `[x]` `[2]` Delete by user
- `[x]` `[2]` Printed card - Copied scan card:
    - `[x]` `[2]` Create
    - `[x]` `[2]` Update
    - `...` `[-]` Delete
- `[x]` `[1]` Template card:
    - `[x]` `[2]` Create
    - `[x]` `[1]` Update:
        - `[x]` `[2]` Update when have no business card
        - `[x]` `[-]` Update when have some business cards
    - `[x]` `[-]` Delete - always hard delete
- `[x]` `[1]` Identity card:
    - `[x]` `[2]` Create
    - `[x]` `[-]` Update
    - `...` `[-]` Delete

#### `[x]` `[-]` Property - Card's property:
- `[x]` `[-]` Normal property:
    - `[x]` `[-]` Create:
        - `[x]` `[-]` Create one
        - `[x]` `[-]` Create many
    - `[x]` `[-]` Update:
        - `[x]` `[-]` Update one:
            - `[x]` `[-]` Editabel field
            - `[x]` `[-]` Force
        - `[x]` `[-]` Update many
    - `[x]` `[-]` Delete:
        - `[x]` `[-]` Delete one:
            - `[x]` `[-]` Mandatory field
            - `[x]` `[-]` Force
        - `[x]` `[-]` Delete many
    - `[x]` `[-]` Validate:
        - `[x]` `[-]` Lable
        - `[x]` `[-]` Property type
        - `[x]` `[-]` Email
        - `[x]` `[-]` Name
        - `[x]` `[-]` Media
- `[x]` `[-]` Template property:
    - `[x]` `[-]` Create
    - `[x]` `[-]` Update
    - `[x]` `[-]` Delete

#### `[x]` `[1]` Contact:
- `[x]` `[1]` Contact:
    - `[x]` `[1]` Create normal:
        - `[x]` `[2]` Create a new one
        - `[x]` `[-]` Update an exists one
    - `[x]` `[2]` Create from a contact request
    - `[x]` `[-]` Create by company
    - `[x]` `[2]` Destroy
    - `[x]` `[2]` Notify:
        - `[x]` `[2]` To user
        - `[x]` `[2]` To employee
        - `[x]` `[2]` To company

- `[x]` `[1]` Contact request:
    - `[x]` `[1]` Create normal:
        - `[x]` `[2]` Create a new one
        - `[x]` `[-]` Update an exists one
    - `[x]` `[2]` Create from a share contact request
    - `[x]` `[2]` Destroy
    - `[x]` `[2]` Notify

- `[x]` `[1]` Share Contact request:
    - `[x]` `[1]` Create normal:
        - `[x]` `[2]` Create a new one
        - `[x]` `[-]` Update an exists one
    - `[x]` `[2]` Destroy
    - `[x]` `[2]` Notify
    - `[x]` `[-]` `get_my_card_let_another_user_know_me`

#### `[x]` `[-]` Balance:
- `[x]` `[-]` Credit log:
    - `[x]` `[-]` Create
    - `[x]` `[-]` Validate card

- `[x]` `[-]` Balance:
    - `[x]` `[-]` Free balance
        - `[x]` `[-]` Validate
        - `[x]` `[2]` Add
        - `[x]` `[-]` Subtrace
        - `[x]` `[-]` Total
        - `[x]` `[-]` Update
    - `[x]` `[-]` Personal balance
        - `[x]` `[-]` Validate
        - `[x]` `[2]` Add
        - `[x]` `[-]` Subtrace
        - `[x]` `[-]` Total
        - `[x]` `[-]` Update
    - `[x]` `[-]` Business balance
        - `[x]` `[-]` Validate
        - `[x]` `[2]` Add
        - `[x]` `[-]` Subtrace
        - `[x]` `[-]` Total
        - `[x]` `[-]` Update
    - `[x]` `[-]` Company balance
        - `[x]` `[-]` Validate
        - `[x]` `[2]` Add
        - `[x]` `[-]` Subtrace
        - `[x]` `[-]` Total
        - `[x]` `[-]` Update

- `[x]` `[-]` Balance manager:
    - `[x]` `[-]` Get a credit from balance
    - `[x]` `[-]` Refund credit to balance
    - `[x]` `[-]` Save
    - `[x]` `[-]` Raise exception if data is changed but not saved

- `[.]` `[-]` Verify token

#### `[x]` `[1]` Notification:
- `[x]` `[-]` Notifier:
    - `[x]` `[-]` Email - normal smtp
    - `[x]` `[-]` Sendgrid email
    - `[x]` `[-]` Firebase cloud
    - `[x]` `[-]` Message - Twilio
- `[x]` `[-]` Email
- `[x]` `[-]` Email template
- `[x]` `[-]` Message - For sms/mms
- `[x]` `[2]` User notification - from company or system or another user to user
- `[x]` `[2]` Self notification - by my self
- `[x]` `[2]` Manager notification - for company
- `[.]` `[-]` Response - validate firebase response

#### `[x]` `[-]` Media
- `[x]` `[-]` Store - Contect to S3:
    - `[x]` `[-]` Upload:
        - `[x]` `[-]` Skip for uploaded file.
    - `[x]` `[-]` Create thumbnail for video
    - `[x]` `[-]` Delete (hard delete)
- `[x]` `[-]` Media:
    - `[x]` `[-]` Hard delete

### IV - Manager Logic (Company Logic):
#### `[x]` `[1]` Company:
- `[x]` `[1]` Invitation:
    - `[x]` `[1]` Generate code
    - `[x]` `[-]` Validate:
        - `[x]` `[-]` Check email if that email belong to another account
        - `[x]` `[-]` Check email if that email already in the company
        - `[x]` `[-]` Validate template

### V - View Logic:
#### `[x]` `[-]` User view
#### `[x]` `[-]` Friend view
#### `[x]` `[-]` Manager view (Company view)


## v2.3.2 - 2020/12/21

- Client API:
    - `[x]` Authentication:
        - `[x]` By Password
        - `[x]` By Facebook
        - `[x]` By Google
        - `[x]` Reset Password
        - `[x]` Set Password
        - `[x]` Reverify account
        - `[x]` Verify action
    - `[x]` User:
        - `[x]` Create / Read / Update / Delete
        - `[x]` Upload media info
    - `[x]` Card:
        - `[x]` Update / Delete 
        - `[x]` Read card - 1d
        - `[x]` Create Personal card
        - `[x]` Verify access code
        - `[x]` Create scan card - 1d
        - `[x]` Create / Read / Update / Delete - Note & Task
    - `[!]` Request:
        - `[x]` Create / Read / Update / Delete
        - `[!]` Exchange card - 0.5d
    - `[x]` Contact:
        - `[x]` Create
        - `[x]` Read / Delete Primary contact - 0.5d
        - `[x]` Read / Delete Company contact - 0.5d - Require `Share to member` in Manager API
    - `[ ]` Search:
        - `[x]` In personal contact
        - `[!]` In company contact - 0.5d - Require `Share to member` in Manager API
        - `[!]` Near by - 0.5d
        - `[ ]` By favorite - 2d (next version)
    - `[ ]` Balance:
        - `[x]` Get current balance
        - `[ ]` Get history balance - 1d - Require `Balance` logic
        - `[ ]` Get Price api - 1d 
    - `[!]` Notification:
        - `[!]` Create Notification form - 1d
        - `[!]` Read / Update / Delete - 0.5d

- Manager API / Company API:
    - `[x]` Template:
        - `[x]` Create / Read / Update
        - `[x]` Company decide which field company member can edit - 3d (next version)
    - `[x]` Invitation:
        - `[x]` Create / Read / Update / Delete
    - `[x]` Member:
        - `[x]` Read / Update
    - `[ ]` Balance:
        - `[ ]` Read - 1d - Require `Balance` logic
        - `[ ]` Get history - 0.5d - Require `Balance` logic
    - `[x]` Contact:
        - `[x]` Read - 0d
        - `[x]` Share to member - 1d - Require `Company Contact` logic
    - `[ ]` Statistic:
    - `[ ]` Filter:

- Logic:
    - `[ ]` Card:
        - `[ ]` Scan card become a real business card - 1d (next version)
    - `[ ]` Balance:
        - `[ ]` Personal card balance - 1.5d
        - `[ ]` Business card balance - 1d
        - `[ ]` Company every month balance - 0.5d
        - `[ ]` Return balance to company when a member quit the company - 1d
    - `[ ]` Notification:
        - `[ ]` Public Notification - 2d
    - `[x]` Member Invitation:
        - `[x]` Verify access code and check email if that email belong to another account
    - `[ ]` Company Contact:
        - `[x]` Contact permission model - 2d
        - `[ ]` Share note and task to that member - 1d

## v2.3.2 - 2020/12/09

- Client API:
    - `[x]` Authentication:
        - `[x]` By Password
        - `[x]` By Facebook
        - `[x]` By Google
        - `[x]` Reset Password
        - `[x]` Set Password
        - `[x]` Reverify account
        - `[x]` Verify action
    - `[x]` User:
        - `[x]` Create / Read / Update / Delete
        - `[x]` Upload media info
    - `[!]` Card:
        - `[x]` Update / Delete 
        - `[!]` Read card - 1d
        - `[x]` Create Personal card
        - `[x]` Verify access code
        - `[!]` Create scan card - 1d
        - `[x]` Create / Read / Update / Delete - Note & Task
    - `[ ]` Request:
        - `[x]` Create / Read / Update / Delete
        - `[ ]` Exchange card - 0.5d
    - `[!]` Contact:
        - `[x]` Create
        - `[!]` Read / Delete Primary contact - 0.5d
        - `[!]` Read / Delete Company contact - 0.5d - Require `Share to member` in Manager API
    - `[ ]` Search:
        - `[x]` In personal contact
        - `[ ]` In company contact - 0.5d - Require `Share to member` in Manager API
        - `[ ]` Near by - 0.5d
        - `[ ]` By favorite - 2d (next version)
    - `[ ]` Balance:
        - `[x]` Get current balance
        - `[ ]` Get history balance - 1d - Require `Balance` logic
        - `[ ]` Get Price api - 1d 
    - `[ ]` Notification:
        - `[ ]` Create Notification form - 1d
        - `[ ]` Read / Update / Delete - 0.5d

- Manager API / Company API:
    - `[x]` Template:
        - `[x]` Create / Read / Update
        - `[!]` Company decide which field company member can edit - 3d (next version)
    - `[x]` Invitation:
        - `[x]` Create / Read / Update / Delete
    - `[x]` Member:
        - `[x]` Read / Update
    - `[ ]` Balance:
        - `[ ]` Read - 1d - Require `Balance` logic
        - `[ ]` Get history - 0.5d - Require `Balance` logic
    - `[!]` Contact:
        - `[!]` Read - 0d
        - `[!]` Share to member - 1d - Require `Company Contact` logic
    - `[ ]` Statistic:
    - `[ ]` Filter:

- Logic:
    - `[ ]` Card:
        - `[ ]` Scan card become a real business card - 1d (next version)
    - `[ ]` Balance:
        - `[ ]` Personal card balance - 1.5d
        - `[ ]` Business card balance - 1d
        - `[ ]` Company every month balance - 0.5d
        - `[ ]` Return balance to company when a member quit the company - 1d
    - `[ ]` Notification:
        - `[ ]` Public Notification - 2d
    - `[x]` Member Invitation:
        - `[x]` Verify access code and check email if that email belong to another account
    - `[ ]` Company Contact:
        - `[!]` Contact permission model - 2d
        - `[ ]` Share note and task to that member - 1d

## v2.2.1 - 2020/11/13

- Client API:
    - `[x]` Authentication:
        - `[x]` By Password
        - `[x]` By Facebook
        - `[x]` By Google
        - `[x]` Reset Password
        - `[x]` Set Password
        - `[x]` Reverify account
        - `[x]` Verify action
    - `[x]` User:
        - `[x]` Create / Read / Update / Delete
        - `[x]` Upload media info
    - `[ ]` Card:
        - `[x]` Update / Delete 
        - `[ ]` Read card - 1d
        - `[x]` Create Personal card
        - `[x]` Verify access code
        - `[ ]` Create scan card - 1d
        - `[x]` Create / Read / Update / Delete - Note & Task
    - `[ ]` Request:
        - `[x]` Create / Read / Update / Delete
        - `[ ]` Exchange card - 0.5d
    - `[ ]` Contact:
        - `[x]` Create
        - `[ ]` Read / Delete Primary contact - 0.5d
        - `[ ]` Read / Delete Company contact - 0.5d - Require `Share to member` in Manager API
    - `[ ]` Search:
        - `[x]` In personal contact
        - `[ ]` In company contact - 0.5d - Require `Share to member` in Manager API
        - `[ ]` Near by - 0.5d
        - `[ ]` By favorite - 2d (next version)
    - `[ ]` Balance:
        - `[x]` Get current balance
        - `[ ]` Get history balance - 1d - Require `Balance` logic
        - `[ ]` Get Price api - 1d 
    - `[ ]` Notification:
        - `[ ]` Create Notification form - 1d
        - `[ ]` Read / Update / Delete - 0.5d

- Manager API / Company API:
    - `[x]` Template:
        - `[x]` Create / Read / Update
        - `[ ]` Company decide which field company member can edit - 3d (next version)
    - `[x]` Invitation:
        - `[x]` Create / Read / Update / Delete
    - `[x]` Member:
        - `[x]` Read / Update
    - `[ ]` Balance:
        - `[ ]` Read - 1d - Require `Balance` logic
        - `[ ]` Get history - 0.5d - Require `Balance` logic
    - `[ ]` Contact:
        - `[ ]` Read - 0d
        - `[ ]` Share to member - 1d - Require `Company Contact` logic
    - `[ ]` Statistic:
    - `[ ]` Filter:

- Logic:
    - `[ ]` Card:
        - `[ ]` Scan card become a real business card - 1d (next version)
    - `[ ]` Balance:
        - `[ ]` Personal card balance - 1.5d
        - `[ ]` Business card balance - 1d
        - `[ ]` Company every month balance - 0.5d
        - `[ ]` Return balance to company when a member quit the company - 1d
    - `[ ]` Notification:
        - `[ ]` Public Notification - 2d
    - `[x]` Member Invitation:
        - `[x]` Verify access code and check email if that email belong to another account
    - `[ ]` Company Contact:
        - `[ ]` Contact permission model - 2d
        - `[ ]` Share note and task to that member - 1d

- Notice:
    - Every week, I need 1 day to fix the problems.

## v0.1.1 - 2020/08/19

### Feature

- `[!]` User:
    - `[x]` CRUD
    - `[!]` User profile: bad logic

    - `[ ]` Verify account
    - `[!]` Reset password: bad logic

    - `[ ]` Locate user location
    - `[ ]` Search user by name, email, phone number
    - `[!]` Search near by user: bad performance

    - `[!]` Upload profile: nothing

- `[!]` Company:
    - `[x]` CURD
    - `[!]` Company user relation: bad logic

- `[!]` Card:
    - `[!]` CRUD: redundancy
    - `[x]` CRUD Note
    - `[x]` CRUD Task

    - `[!]` Template: nothing

- `[!]` Contact:
    - `[x]` Request
    - `[x]` Confirm
    - `[x]` CRUD History
    - `[!]` Pay: bad logic
    - `[ ]` Share request
    - `[ ]` Share confirm

- `[!]` Credit:
    - `[!]` CRUD: bad logic

- `[!]` Permission:
    - `[ ]` Admin
    - `[ ]` Company
    - `[x]` Normal user

### Problem

- DataBase:
    - `[ ]` The table nameing is too bad, no meaning or wrong the meaning.
    - `[ ]` The `s_users` table is a poor design, I have to redesign it. Poor design means the user can enter any information without structure. That means we cannot control if we want the user to have some fields such as phone number, address.
    - `[ ]` The `location` table is a poor design too. He query all user and search for all user and check if them near you. That is very slow. I can optimize it.
    - `[ ]` The `s_user_notification` is wrong relation.
    - `[ ]` ...
