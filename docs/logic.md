[<- Back](./README.md)

# Shake Logic

## Business logic


## Technical logic

### I - Card

#### 1. Personal card

- One user only have maximum one card.

#### 2. Business card

- One card for one user each company

#### 3. Scan card

- `Scan card` will show out like a `Business card` with a `Default template`.
- When a `Scan card` is created, it won't belong to any company yet. And it work like a `Personal card`.
- When a `Scan card` is assigned to a company, its contacts will belong to that company and also belong to that user.

### II - Property

This is a field of a card or a template.

#### [1. Property type](../app/s_properties/bases.py)

```python
NAME = "N"
EMAIL = "E"
MOBILE = "M"
ADDRESS = "A"
TAG = "T"
CONTENT = "C"
MEDIA = "F"
URL = "U"
OTHER = "O"
```

#### 2. Default property

```python
"name": NAME
"email": EMAIL
"avatar": MEDIA
"background": MEDIA | OTHER
"mobile": MOBILE
"address": ADDRESS
"font_color": OTHER
"tag 1": TAG
"tag 2": TAG
"tag 3": TAG
"media 1": MEDIA
"media 2": MEDIA
"media 3": MEDIA
"social facebook": URL
"social twitter": URL
"social instagram": URL
"social linkedin": URL
"social others": URL
"about me": CONTENT
"did": MOBILE,
"website": URL,

"work mobile": MOBILE,
"work email": EMAIL,
"job title": CONTENT,

"company name": NAME,
"company mobile": MOBILE,
"company email": EMAIL,
"company address": ADDRESS,
"company website": URL,
"company image": MEDIA,
"company template": MEDIA,
"blank card": MEDIA,
```
