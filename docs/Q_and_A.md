[<- Back](./README.md)

# QUESTION

## Account logic:

### - 2021/06/16 - Delete card:

- When a user decides to delete their account. Where are their credits going to be? And can they restore it? (I suggest that: Because I allow users to restore the personal card only - after recreate their account -, so that I will move all the credits of the other cards to their personal card before deleting it. Then, I delete them all.)
  - Credits which their company pay for, gets returned to the company’s account as a credit to their monthly billing and needs to show this in the bill as returned credits.
  - For credits the user paid for via the app stores, I don’t think we need to keep their credits. At least not for now until we see a need.


## Card logic:

### - 2021/08/25 - Merge card:

- I want to know how many times do you want this hint to show? Just once or repeat? Can the user do it manually? 
  - I think you misunderstood. I didn’t say “hint”. They must be asked to merge their scanned card with a matching business card. In fact, I would like to merge it automatically for them and just notify them of the merge provided that their work email matches between the 2 cards. I would like to do this and if we encounter sufficient negative feedback from users about auto merging, then we can change that.

- This action can't "undo".
  - Can’t we create the merged card automatically and inform them but not actually delete the 2 pre-merged cards but instead hide them until they confirm to accept the merge?

- Your company will know all users in the contact of your Scan card. It's ok if you are an employee. But if you are a manager,  there are some customers you don't want the company to know (even the company admin). At the time the merging happens, you don't know how the merging algorithm works, and your employee (company admin) will know some key customers.
  - Managers don’t need to have more privileges than others on Shake. Even the CEO doesn’t need to.

### - 2021/06/16 - Delete card:

- When a user only has one card and they want to delete it. (You need to tell me all the steps and step by step).
  - We let them delete it. Their account will be active and we need to think which contacts they can still see so let’s discuss this on the call.
  - Then they will have no cards so when they reopen the app, we take them to the Getting started page attached but they will have the bottom menu available in case they just want to see their contacts or notifications pages.

### - 2021/05/12 - Soft delete card:

- I personally suggest that we must not delete any card in our system (even when the user deletes their card or account), that is useful information. And it also allows us to restore it and reduce the complex of the balance's logic.
  - Agree


### - 2021/08/04 - Search my card:

- Should we hide my card in searching list?
  - If you can hide a user’s own cards from both location and keyword searches, I’m ok with that.

## Balances Logic

### - 2021/08/17 - Minimun value of balances:

- Is 0 the minimum value of the balances?
  - We will notify users to top up only when their credit for any business card reaches 5 credits. If they don’t top up, we will allow users’ business card’s balance to go to a maximum negative balance of -5. Meaning that even if they have 0 to -4 credits, if they want to send a card, they can. When they do top up, any negative credits balance (eg. -3) will be deducted from their new balance. Once the balance reaches -5, they must top up or they cannot send any more of these cards.
  - Why allow it to go negative? Because I think 5 remaining is a good time to ask them to top up. 10 might seem too soon whereas 5 gives them more urgency. And if haven’t had a chance to top up before they run out (reach zero credits), giving them 5 more seems fair which we will still charge them for.

- I just want to know who can have their balance reached -5? I don't think that everyone needs this for all of their cards.
  - This is for all business cards.

### - 2021/07/15 - When a employee leave:

- When an employee of the company leaves the company. Instead of refunding money to the company bill. Can we refund them the employee credit to the company balance?
  - Yes I didn’t mean that we would pay companies back. We return the credits and that is subtracted from their next month’s bill. I showed this in the invoice. I don’t expect any company to have a positive balance of credits since their employees will continue to send cards and the little credit that is returned for employees who leave wont be much anyway.

### - 2021/07/13 - When a card request is deleted:

- A sent card a1 to card b1 of B. B did not accept card a1 yet. Then B delete card b1. But did not accept card a1 from A yet. Should A receive A's money back?
  - Yes (Zac said yes in his personal call)

### - 2021/06/16 - Reward balance & Reward claimed

- What are they?
  - `Reward balance` is the total total reward balance you received (include `Reward claimed`).
  - `Reward claimed` is the amount of credit you have loaded into the card from the `Reward balance`.
- How to use "Reward balance" and "Reward claimed". And how to have more reward (I forgot about this).
  - Later, once we recruit the rewards partners which will be big retailers including Amazon and Starbucks and a number of charities (Red Cross, Greenpeace, Unisef, etc) and it will take some time, they can redeem 50 points in exchange for a $5 voucher. Don’t worry about this yet as it will take months.
  - So in the meantime, we will reward users by giving them credits to their Personal cards. We’ll discuss on the call.

### - 2021/06/23 - When an account is deleted:

- Should we keep their credits and restore it when they create a new account?
  - For credits the user paid for via the app stores, I don’t think we need to keep their credits. At least not for now until we see a need.

### - 2021/05/12 - Free credits:

- Are free credits shared between cards, or one free credit each?
  - For personal cards, they cannot be used by other business cards of the user.
  - For business cards, they also cannot be used for the user’s other cards. This applies to both free and paid credits.
  - Free credits are given to new business users to get them started and is always assigned to specific cards (sub accounts). 1 free credit is given to each personal card to be used that day or it expires. What does Scanned cards have to do with this?
