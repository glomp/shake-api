[<- Back](./README.md)

# TODO LIST

> `[ ]` Not done
>
> `[_]` On doing
>
> `[x]` Done
>
> --------------------
>
> `[!]` Important
>
> `[?]` Require some information


## 2021-08-10
- `[x]` Research: Sendgrid send emails using another email address.

## 2021-08-06
- `[x]` Check Zac's friend cards. 
- `[x]` Check notifications in Zac account.
- `[x]` Download button for Shake homepage.

## 2021-08-03
- `[x]` Add Certificate to Production server.

## 2021-07-30
- `[ ]` Optimize `delete_other_relation` in delete card logic.
  - `[ ]` Delete multi-note and task by query (join with `contact` table).

## 2021-07-29
- `[!]` Implement some convenience functions into `SDynamicModelViewSet`:
  + `[!]` get_queryset
  + `[!]` get_extra_filters
  + `[!]` paginate_queryset
  + `[ ]` get_serializer
  + `[ ]` get_request_fields


## 2021-07-20
- `[!]` Merge Scan card into Business card feature. When a Scan card become Business card. All other contact with that card will belong to both personal and company.
  + `[x]` Logic
  + `[x]` API
  + `[!]` Notificaion
- `[!]` Allow balance have negative number (up to -5) in Personal balance.

## 2021-07-19
- `[x]` Fix `AppException` class: change `request` from class variable to object variable.

## 2021-07-10
- `[!]` Resize all images that are uploaded to server.
- `[!]` Create an API to export invoices. sample: [../keep/sample/shake_invoice_template.docx](../keep/sample/shake_invoice_template.docx)

## 2021-06-30
- `[!]` Add filter for Company's Dashboard.
- `[!]` Company can share customer card to their employee.

## 2021-06-01
- `[?]` Twilio.

## 2021-05-30
- `[!]` Allows multiple device login with separate token.


## 2021-04-30
- `[ ]` Token or session for S3 Amazon: Only App can query this image or video.
