[<- Back](./README.md)

# INSTALATION

## I. Environment

- Ubuntu (20.04)
- Python3 (3.7.8)
- Mysql (8.0.21)

- Other ubuntu apk:
    - mysql-server
    - libmysqlclient-dev
    - libpython3.7-dev

## II. Python environment

- There are two environment files:
    - [requirements.txt](../requirements.txt) for production (run only). 
    - [requirements.dev.txt](../requirements.dev.txt) for develop.

Run these commands to install the above 2 libraries in turn.

```bash
$ make install-lib
$ make install-dev-lib
```

## III. Update setting

### 1. For local

- Mysql:

    - Update mysql config in app.setting file in [app/app/settings.py](../app/app/settings.py). It's `DATABASES` section.
        - `NAME`: Schema name
        - `USER`: Username
        - `PASSWORD`: Password

    - Update account:
        - Create an mysql account with username: "shake" and password: "shake"
        - Or update your config.

### 2. For production

> Comming soon

## IV. Database

Firstly, you must create the schema by yourself and the schema name must be the same with the name you setted up in the app.setting.

If you had set up the DB configuration correctly, you could run this command:

```bash
$ make migrate
```

## V. Run

Now, you can run the app

```bash
$ make run
```

## VI. Create Admin account

For using admin api, you must have an admin account.

```bash
$ make create-super-user-account
```

Run this command and type your account name, email and password.

