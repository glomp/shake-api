[<- Back](./README.md)

# Django Knowleage

## Define

### [Model](https://docs.djangoproject.com/en/3.1/topics/db/models/)

A `model` is the single, definitive source of information about your data. It contains the essential *fields* and *behaviors* of the data you’re storing. Generally, each model maps to a single database table.

### [Serializers](https://www.django-rest-framework.org/api-guide/serializers/)

We provide a `Serializer` class which gives you a powerful, generic way to control the output of your responses, as well as a `ModelSerializer` class which provides a useful shortcut for creating serializers that deal with model *instances* and *querysets*.

### [ViewSet](https://www.django-rest-framework.org/api-guide/viewsets/)

In other frameworks you may also find conceptually similar implementations named something like **Resources** or **Controllers**.

A `ViewSet` class is simply a type of class-based `View`, that does not provide any method handlers such as `.get()` or `.post()`, and instead provides actions such as `.list()` and `.create()`.

