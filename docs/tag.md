[<- Back](./README.md)

# TAG

## v2.4.14 - 2021.08.30: [5195891](https://bitbucket.org/glomp/shake-api/commits/5195891)
- Add search by long and lat directly.
- Add log to CSV.
- Add send error to admin email.
- Add send badge in firebase notification.
- Fix get 1 return 2 in query in confirm function

## v2.4.13 - 2021.08.25: [9904a34](https://bitbucket.org/glomp/shake-api/commits/9904a34)
- Add notification status API and mark all as read API

## v2.4.12 - 2021.08.23: [1ff2b63](https://bitbucket.org/glomp/shake-api/commits/1ff2b63)
- Add user device API

## v2.4.11 - 2021.08.18: [86f1b60](https://bitbucket.org/glomp/shake-api/commits/86f1b60)
- Add verify purchase
- Update balance logic and add test cases
