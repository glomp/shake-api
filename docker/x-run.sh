echo "$@"
echo PYTHONPATH=./app django-admin runserver 0.0.0.0:"$1" --settings app.settings."$2"

cd /app && PYTHONPATH=./app django-admin runserver 0.0.0.0:"$1" --settings app.settings."$2"
