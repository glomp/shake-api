PYTHONPATH:=./app
PORT?=8000
ENV?=local

BUILD_ENV?=staging
BUILD_BRANCH?=develop
TAG?=$(BUILD_ENV)
IMAGE?=shak3
ACCOUNT?=sihc
IMAGE_TAG:=$(IMAGE):$(TAG)
LIB_REPOSITORY:=$(ACCOUNT)/django-2.2-mysql-8.0:$(BUILD_BRANCH)
REPOSITORY:=$(ACCOUNT)/$(IMAGE):$(TAG)
CACHE?=--no-cache
DOCKER_COMPOSE_NAME:=$(ACCOUNT)_$(IMAGE)_$(BUILD_ENV)

LOGS_LEN?=500

PROJECT_POSITION:=projects/shake-api

APPRUN:=PYTHONPATH=$(PYTHONPATH) manage.py
APPENV:=PYTHONPATH=$(PYTHONPATH) DJANGO_SETTINGS_MODULE=app.settings.$(ENV)

MODULE?=""


run:
	export $$(cat .secret/env/$(ENV).env | xargs) && make run-server


run-server:
	$(APPRUN) runserver 0.0.0.0:$(PORT) --settings app.settings.$(ENV)


migrate:
	export $$(cat .secret/env/$(ENV).env | xargs) && $(APPENV) ./app/manage.py migrate


migration:
	$(APPENV) ./app/manage.py makemigrations


reset-migrate:
	rm app/*/migrations/0* || true
	make migration
	make migrate


deploy:
	make deploy-staging


logs:
	ssh shake-$(BUILD_ENV) "cd $(PROJECT_POSITION) && \
		make dc-logs ENV=$(ENV) BUILD_ENV=$(BUILD_ENV) TAG=$(TAG) LOGS_LEN=$(LOGS_LEN)"


clean:
	rm -rf $$(find . -type d -name __pycache__)
	rm -rf $$(find . -name *.pyc)


# =============== SERVER

server-logs-file:
	ssh shake-$(BUILD_ENV) "cd $(PROJECT_POSITION) && \
		make logs-file ENV=$(ENV) BUILD_ENV=$(BUILD_ENV) LOGS_LEN=$(LOGS_LEN)"


server-logs-file-exception:
	ssh shake-$(BUILD_ENV) "cd $(PROJECT_POSITION) && \
		make logs-file-exception ENV=$(ENV) BUILD_ENV=$(BUILD_ENV) LOGS_LEN=$(LOGS_LEN)"


# =============== DEPLOY

build:
	# ---------------> Start build and push docker 
	make docker-build docker-push
	# ---------------> Build finsh


push-server:
	# ---------------> Go to $(BUILD_ENV) to pull code from branch $(BUILD_BRANCH)
	ssh shake-$(BUILD_ENV) "cd $(PROJECT_POSITION) && \
		git checkout $(BUILD_BRANCH) && \
		git pull origin $(BUILD_BRANCH)"

	# ---------------> Pull new image and restart
	ssh shake-$(BUILD_ENV) "cd $(PROJECT_POSITION) && \
		make dc-restart-clean ENV=$(ENV) BUILD_ENV=$(BUILD_ENV) TAG=$(TAG)"
	# ---------------> Restart docker finish

	# ---------------> Sync DB
	make migrate

	# ---------------> Docker logs
	make logs


fast-deploy:
	# --------> $(BUILD_ENV)
	make build
	make push-server


deploy-staging:
	# --------> $(IMAGE_TAG)
	make fast-deploy ENV=staging BUILD_ENV=staging


deploy-production:
	# make test
	make fast-deploy ENV=production BUILD_ENV=production BUILD_BRANCH=master


# =============== INSTALL PYTHON ENV

install-lib:
	sudo apt-get install libmysqlclient-dev python3-pymysql python3.7-dev
	pip install -r requirements.txt

install-dev-lib:
	pip install -r requirements.dev.txt


# =============== TEST

test:
	# $(APPENV) ./app/manage.py test -v 3 s_auth
	PYTHONPATH=$(PYTHONPATH) DJANGO_SETTINGS_MODULE=app.settings.test ENV=TEST \
		./app/runtests.py $(MODULE)

logs-file:
	tail log/$(shell echo $(ENV) | tr '[:lower:]' '[:upper:]').$(TAG).log.csv -n 500 -f

logs-file-exception:
	tail log/$(shell echo $(ENV) | tr '[:lower:]' '[:upper:]').$(TAG).exception.log.csv -n 500 -f


# =============== DOCKER

docker-build-only:
	# ---------------> Build $(REPOSITORY)
	docker image rm $(REPOSITORY) || true
	docker build $(CACHE) -t $(REPOSITORY) \
		-f docker/$(BUILD_ENV)/Dockerfile ./

docker-build-lib:
	docker image rm $(LIB_REPOSITORY) || true
	docker build --no-cache -t $(LIB_REPOSITORY) \
		-f docker/Dockerfile.lib ./
	docker push $(LIB_REPOSITORY)

docker-prepare-build-folder:
	make clean
	rm -rf .build
	make export_env
	mkdir -p .build/app/app .build/app/templates .build/app/log .build/app/.secret
	rsync -a --copy-links ./app/* ./.build/app/app
	rsync -a --copy-links ./templates/* ./.build/app/templates/

docker-build:
	make docker-prepare-build-folder
	make docker-build-only

docker-build-with-cache:
	make docker-build CACHE=""

docker-push:
	docker push $(REPOSITORY)

docker-pull:
	docker pull $(REPOSITORY)

docker-log:
	docker log -f --tail $(LOGS_LEN) $(DOCKER_COMPOSE_NAME)


# =============== DOCKER COMPOSE

dc-up:
	cd ./docker/$(BUILD_ENV) && \
	IMAGE_TAG=${IMAGE_TAG} docker-compose up -d

dc-down:
	cd ./docker/$(BUILD_ENV) && \
	IMAGE_TAG=${IMAGE_TAG} docker-compose down

dc-pull:
	cd ./docker/$(BUILD_ENV) && \
	IMAGE_TAG=${IMAGE_TAG} docker-compose pull

dc-logs:
	cd ./docker/$(BUILD_ENV) && \
	IMAGE_TAG=${IMAGE_TAG} docker-compose logs --tail=$(LOGS_LEN) -f

dc-restart:
	make dc-down dc-up

dc-restart-clean:
	make dc-pull dc-down dc-up


# =============== OTHER

export_env:
	echo "GIT_BRANCH = \"$$(git branch --show-current)\"\n\
	GIT_HASH_SHORT = \"$$(git rev-parse --short HEAD)\"\n\
	GIT_HASH = \"$$(git rev-parse HEAD)\"\n\
	GIT_COMMIT_MESSAGE = \"$$(git log -1 --pretty=%B)\"" > app/app/settings/git/env.py
