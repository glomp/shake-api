WITH pro AS (
	SELECT 
		ROW_NUMBER() OVER (PARTITION BY card_id) AS idx,
		sp.*
	FROM shake_2020.s_property sp
	WHERE label = 'company name'
)
DELETE sp2 FROM shake_2020.s_property sp2
JOIN pro ON pro.id = sp2.id
WHERE pro.idx = 2
