DELETE FROM shake_2020.s_contact 
WHERE id in (
	SELECT id FROM (
		SELECT 
			id, card_id, company_id,
			ROW_NUMBER() OVER (PARTITION BY card_id, company_id) AS idx
		FROM shake_2020.s_contact sc
		WHERE `type` = 'C'
	) tmp
	WHERE idx > 1
)
