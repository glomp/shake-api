UPDATE shake_2020.s_company_user  scu
JOIN shake_2020.s_user su ON scu.user_id = su.id 
JOIN shake_2020.s_company sc ON sc.id = scu.company_id 
JOIN shake_2020.s_template st ON st.company_id = sc.id
JOIN shake_2020.s_card sca ON sca.template_id = st.id AND sca.user_id = scu.user_id 
SET scu.card_id = sca.id;
