SELECT 
	sc.id, sc.user_name, sc.avatar, sc.job_title , sc.company_name, sct.user_id, 
	GROUP_CONCAT(sct.user_card_id) as belong_to
FROM shake_2020.s_card sc
JOIN shake_2020.s_contact sct ON sc.id = sct.card_id 
GROUP BY sct.card_id, sct.user_id
