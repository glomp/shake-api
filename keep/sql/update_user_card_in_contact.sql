UPDATE shake_2020.s_contact sct
JOIN (
	SELECT DISTINCT cv.contact_id, sc.id AS card_id
	FROM
	(
		SELECT sct.id AS contact_id, scu.user_id, scu.company_id, st.id AS template_id, sct.card_id
		FROM shake_2020.s_contact sct
		JOIN shake_2020.s_company_user_contact scuc ON scuc.contact_id = sct.id
		JOIN shake_2020.s_company_user scu ON scu.id = scuc.company_user_id
		JOIN shake_2020.s_template st ON st.company_id = sct.company_id
	) cv
	JOIN shake_2020.s_card sc ON cv.user_id = sc.user_id AND sc.template_id = cv.template_id
) cv2 ON cv2.contact_id = sct.id 
SET sct.user_card_id = cv2.card_id
