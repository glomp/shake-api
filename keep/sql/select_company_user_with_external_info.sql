explain WITH employee_contact AS (
	SELECT
		user_card_id,
		count(*) AS cnt, 
		COUNT(CASE WHEN sct.status = 'A' THEN sct.id ELSE NULL END) AS cnt2
	FROM
		shake_2020.`s_contact` sct
	GROUP BY
		user_card_id
)
SELECT
	DISTINCT (
	SELECT
		cnt
	FROM
		employee_contact ec
	WHERE
		`s_card`.id = ec.user_card_id
	) AS `contact_number`,
	(SELECT
		cnt2
	FROM
		employee_contact ec
	WHERE
		`s_card`.id = ec.user_card_id
	) AS `active_contact_number`,
	`s_card`.`id`,
	`s_card`.`user_id`,
	COUNT(`s_contact_request`.`id`) AS `contact_request_number`,
	COUNT(CASE WHEN `s_contact_request`.`status` = 'A' THEN `s_contact_request`.`id` ELSE NULL END) AS `contact_request_success_number`,
	`s_company_user`.`id`
FROM
	shake_2020.`s_card`
INNER JOIN shake_2020.`s_template` ON
	(`s_card`.`template_id` = `s_template`.`id`)
LEFT OUTER JOIN shake_2020.`s_contact_request` ON
	(`s_card`.`id` = `s_contact_request`.`from_card_id`)
LEFT OUTER JOIN shake_2020.`s_company_user` ON
	(`s_card`.`company_user_id` = `s_company_user`.`id`)
WHERE
	(`s_card`.`card_type` = 'B'
	AND `s_template`.`company_id` = 6)
GROUP BY
	`s_card`.`id`
ORDER BY
	NULL;