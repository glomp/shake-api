CREATE OR REPLACE
ALGORITHM = UNDEFINED VIEW `shake_2020`.`s_aggregate_contact` AS
select
    `sc`.`id` AS `id`,
    `sc`.`name` AS `name`,
    `sc`.`display` AS `display`,
    `sc`.`card_type` AS `card_type`,
    `sc`.`tier_1_view_key` AS `tier_1_view_key`,
    `sc`.`tier_2_view_key` AS `tier_2_view_key`,
    `sc`.`tier_3_view_key` AS `tier_3_view_key`,
    `sc`.`_created` AS `_created`,
    `sc`.`_updated` AS `_updated`,
    `sc`.`user_id` AS `user_id`,
    `sc`.`status` AS `status`,
    `sc`.`template_id` AS `template_id`,
    `sc`.`company_user_id` AS `company_user_id`,
    `sc`.`avatar` AS `avatar`,
    `sc`.`company_name` AS `company_name`,
    `sc`.`job_title` AS `job_title`,
    `sc`.`user_name` AS `user_name`,
    `sct`.`user_id` AS `contact_user_id`,
    group_concat(`sct`.`tier_number` separator ',') AS `tiers`,
    group_concat(`sct`.`id` separator ',') AS `belong_to_contacts`,
    group_concat(`sct`.`user_card_id` separator ',') AS `belong_to_cards`
from
    (`shake_2020`.`s_card` `sc`
join `shake_2020`.`s_contact` `sct` on
    ((`sc`.`id` = `sct`.`card_id`)))
group by
    `sct`.`card_id`,
    `sct`.`user_id`